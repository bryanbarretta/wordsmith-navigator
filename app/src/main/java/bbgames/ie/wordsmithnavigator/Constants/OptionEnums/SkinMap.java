package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 16/04/2017.
 */

public enum SkinMap {

    GREEN (0, R.string.green, R.color.green_500),
    WHITE (1, R.string.white, R.color.white),
    PURPLE (2, R.string.purple, R.color.purple_500),
    BLUE (3, R.string.blue, R.color.blue_500),
    BLACK (4, R.string.black, R.color.charcoal);

    private int index;
    private int resId;
    private int colorId;

    SkinMap(int index, int resId, int colorId)
    {
        this.index = index;
        this.resId = resId;
        this.colorId = colorId;
    }
    public int getIndex() {
        return index;
    }
    public int getResId() {
        return resId;
    }
    public int getColorId() {
        return colorId;
    }

    public static SkinMap getThemeMap(ApplicationManager applicationManager){
        return SkinMap.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_map, "0"))];
    }

    public static void setThemeMap(ApplicationManager applicationManager, SkinMap themeMap){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_map, themeMap.ordinal()+"");
    }
}
