package bbgames.ie.wordsmithnavigator.Constants;

/**
 * Created by Bryan on 04/08/2018.
 */
public enum Position {
    TOP,
    TOP_RIGHT,
    RIGHT,
    TOP_LEFT,
    LEFT,
    BOTTOM_LEFT,
    BOTTOM,
    BOTTOM_RIGHT;

    public int[] getXy(int[] sourceXy){
        if(sourceXy == null || sourceXy.length != 2){
            return null;
        }
        switch (this){
            case TOP:
                return new int[]{sourceXy[0], sourceXy[1] - 1};
            case BOTTOM:
                return new int[]{sourceXy[0], sourceXy[1] + 1};
            case LEFT:
                return new int[]{sourceXy[0] - 1, sourceXy[1]};
            case RIGHT:
                return new int[]{sourceXy[0] + 1, sourceXy[1]};
            case TOP_LEFT:
                return new int[]{sourceXy[0] - 1, sourceXy[1] - 1};
            case TOP_RIGHT:
                return new int[]{sourceXy[0] + 1, sourceXy[1] - 1};
            case BOTTOM_LEFT:
                return new int[]{sourceXy[0] - 1, sourceXy[1] + 1};
            case BOTTOM_RIGHT:
                return new int[]{sourceXy[0] + 1, sourceXy[1] + 1};
        }
        throw new RuntimeException("Unexpected position");
    }

}
