package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;

/**
 * Created by Bryan on 21/04/2017.
 */

public class SkinLetter {

    private final String LOGTAG = getClass().getSimpleName();

    private int id;
    private String name;
    private List<Integer> textColors;

    private final static String keyId = "id";
    private final static String keyName = "name";
    private final static String keyTextColor0 = "tc0";  //LetterState[0]: Enabled
    private final static String keyTextColor1 = "tc1";  //LetterState[1]: Disabled
    private final static String keyTextColor2 = "tc2";  //LetterState[2]: Playing
    private final static String keyTextColor3 = "tc3";  //LetterState[3]: Red
    private final static String keyTextColor4 = "tc4";  //LetterState[4]: Rogue

    public SkinLetter(String json){
        try {
            JSONObject j = new JSONObject(json);
            int id = j.getInt(keyId);
            String name = j.getString(keyName);
            int textColor0 = j.getInt(keyTextColor0);
            int textColor1 = j.getInt(keyTextColor1);
            int textColor2 = j.getInt(keyTextColor2);
            int textColor3 = j.getInt(keyTextColor3);
            int textColor4 = j.getInt(keyTextColor4);
            init(id, name, textColor0, textColor1, textColor2, textColor3, textColor4);
        } catch (JSONException e) {
            Log.e(LOGTAG, "JSONException parsing: " + json);
            init(-1, null, null, null, null, null, null);
        }
    }

    public SkinLetter(int id,
                      String name,
                      Integer textColor0,
                      Integer textColor1,
                      Integer textColor2,
                      Integer textColor3,
                      Integer textColor4){
        init(id, name, textColor0, textColor1, textColor2, textColor3, textColor4);
    }

    private void init(int id,
                      String name,
                      final Integer textColor0,
                      final Integer textColor1,
                      final Integer textColor2,
                      final Integer textColor3,
                      final Integer textColor4){
        this.id = id;
        this.name = name;
        this.textColors = new ArrayList<Integer>(){
            {add(textColor0);}
            {add(textColor1);}
            {add(textColor2);}
            {add(textColor3);}
            {add(textColor4);}
        };
    }

    public String toJSON(){
        JSONObject j = new JSONObject();
        try {
            j.put(keyId, id);
            j.put(keyName, name);
            j.put(keyTextColor0, textColors.get(0));
            j.put(keyTextColor1, textColors.get(1));
            j.put(keyTextColor2, textColors.get(2));
            j.put(keyTextColor3, textColors.get(3));
            j.put(keyTextColor4, textColors.get(4));
            return j.toString();
        } catch (JSONException e) {
            Log.e(LOGTAG, e.getMessage());
            return null;
        }
    }

    public int getId(){
        return this.id;
    }

    public String getName(){
        return name;
    }

    public int getColor(int ordinal) {
        return textColors.get(ordinal >= 5 ? LetterState.ENABLED.ordinal() : ordinal);
    }

    public static List<SkinLetter> getUnlockedSkins(ApplicationManager applicationManager) {
        return applicationManager.getDatabaseGateway().getSkins();
    }


    public static SkinLetter getUserTheme(ApplicationManager applicationManager){
        int id = Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_letter_user, "0"));
        return applicationManager.getDatabaseGateway().getSkin(id);
    }

    public static void setUserTheme(ApplicationManager applicationManager, SkinLetter themePlayer){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_letter_user, themePlayer.getId()+"");
    }

    public static SkinLetter getCPUTheme(ApplicationManager applicationManager){
        int id = Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_letter_cpu, "1"));
        return applicationManager.getDatabaseGateway().getSkin(id);
    }

    public static void setCPUTheme(ApplicationManager applicationManager, SkinLetter themePlayer){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_letter_cpu, themePlayer.getId()+"");
    }

}
