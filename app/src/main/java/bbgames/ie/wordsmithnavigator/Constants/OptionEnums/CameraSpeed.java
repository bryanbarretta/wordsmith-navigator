package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 03/03/2018.
 */

public enum CameraSpeed {

    SLOW (R.string.slow, 500),
    NORMAL (R.string.normal, 750),
    FAST (R.string.fast, 1000);

    private int resId;
    private int ms;
    CameraSpeed(int resId, int ms){
        this.resId = resId;
        this.ms = ms;
    }
    public int getResId() {
        return resId;
    }
    public int getMs() {
        return ms;
    }

    public static CameraSpeed getCameraSpeed(ApplicationManager applicationManager){
        return CameraSpeed.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.camera_speed, "1"))];
    }

    public static void setCameraSpeed(ApplicationManager applicationManager, CameraSpeed cameraSpeed){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.camera_speed, cameraSpeed.ordinal()+"");
    }
}
