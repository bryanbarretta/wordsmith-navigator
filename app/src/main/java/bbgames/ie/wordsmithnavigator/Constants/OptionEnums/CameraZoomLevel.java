package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 03/03/2018.
 */

public enum CameraZoomLevel {

    VERY_FAR (R.string.very_far, 0.6f),
    FAR (R.string.far, 0.8f),
    NORMAL (R.string.normal, 1.0f),
    CLOSE (R.string.close, 1.2f),
    VERY_CLOSE (R.string.very_close, 1.4f);

    private int resId;
    private float value;
    CameraZoomLevel(int resId, float value){
        this.resId = resId;
        this.value = value;
    }
    public int getResId() {
        return resId;
    }
    public float getValue() {
        return value;
    }

    public static CameraZoomLevel getCameraZoomLevel(ApplicationManager applicationManager){
        return CameraZoomLevel.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.camera_zoom_level, "1"))];
    }

    public static void setCameraZoomLevel(ApplicationManager applicationManager, CameraZoomLevel cameraZoomLevel){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.camera_zoom_level, cameraZoomLevel.ordinal()+"");
    }
}
