package bbgames.ie.wordsmithnavigator.Constants;

import java.util.ArrayList;

/**
 * Created by Bryan on 12/04/2017.
 */

public enum TileCodes {

    MapTile('.'),
    WallTile('-'),
    Blank(' '),
    Diamond('^'),
    Detonator('!'),
    DetonatorCrate('+'),
    Gate('%'),
    StartingZone('0'),
    StartingZoneP2('€'),
    StartingZoneCPU('$'),
    EndZone('1'),
    PowerUp('#'),   //Special case: must parse the PowerUpLink to determine if P/T/M/L (those 4 letters are reserved for letter tiles)
    Bridge('='),
    Invisible('/'); //Special case: any sprite that returns this has no special code, so the underlying code would be displayed

    private final Character code;

    TileCodes(Character code){
        this.code = code;
    }

    public Character getCharacter(){
        return code;
    }

    public static ArrayList<Character> getBlockTileCodes(){
        return new ArrayList<Character>(){{
            add(WallTile.getCharacter());
            add(Blank.getCharacter());
            add(DetonatorCrate.getCharacter());
            add(Gate.getCharacter());
            add(Invisible.getCharacter());
        }};
    }

    public static ArrayList<Character> getPlayableTileCodes(){
        return new ArrayList<Character>(){{
            add(MapTile.getCharacter());
            add(Diamond.getCharacter());
            add(Detonator.getCharacter());
            add(StartingZone.getCharacter());
            add(StartingZoneCPU.getCharacter());
            add(StartingZoneP2.getCharacter());
            add(EndZone.getCharacter());
            add(Bridge.getCharacter());
            add(PowerUp.getCharacter());
        }};
    }

    public static ArrayList<Character>  getTargetTileCodes() {
        return new ArrayList<Character>(){{
            add(Detonator.getCharacter());
            add(EndZone.getCharacter());
        }};
    }

    public static ArrayList<Character> getPlayerStartingZoneTileCodes(){
        return new ArrayList<Character>(){{
            add(StartingZone.getCharacter());
            add(StartingZoneCPU.getCharacter());
            add(StartingZoneP2.getCharacter());
        }};
    }

    public static TileCodes getTileCode(Character c){
        for (TileCodes t : values()){
            if(t.getCharacter().equals(c)){
                return t;
            }
        }
        return null;
    }
}
