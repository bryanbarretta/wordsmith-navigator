package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Constants.SupportedLanguages;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 16/04/2017.
 */

public enum AIDifficulty {

    VERY_EASY (R.string.very_easy),
    EASY (R.string.easy),
    NORMAL (R.string.normal),
    HARD (R.string.hard),
    VERY_HARD (R.string.very_hard),
    IMPOSSIBLE (R.string.impossible);

    private int resId;
    AIDifficulty(int resId){
        this.resId = resId;
    }
    public int getResourceId(){
        return resId;
    }

    public double getMinFrequencyWord(SupportedLanguages lang){
        return getMinFrequencyWord(this, lang);
    }

    public static double getMinFrequencyWord(AIDifficulty difficulty, SupportedLanguages lang){
        switch (lang){
            default:
            case ENGLISH_UK:                                        //Words available
                if(difficulty.equals(VERY_EASY))    return 0.0005;  //2566
                if(difficulty.equals(EASY))         return 0.0002;  //4915
                if(difficulty.equals(NORMAL))       return 0.00005; //11597
                if(difficulty.equals(HARD))         return 0.000005;//35037
                if(difficulty.equals(VERY_HARD))    return 0.000001;//53514
                if(difficulty.equals(IMPOSSIBLE))   return 0;       //74286
                break;
            //case PORTUGUESE_BRAZIL:
            //    break;
        }
        return 0;
    }

    public static AIDifficulty getDifficulty(ApplicationManager applicationManager){
        return AIDifficulty.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.difficulty, "2"))];
    }

    public static void setDifficulty(ApplicationManager applicationManager, AIDifficulty difficulty){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.difficulty, difficulty.ordinal()+"");
    }

    public int getMinWordLength() {
        switch (this){
            case VERY_EASY: return 2;
            case EASY: return 2;
            default:
            case NORMAL: return 2;
            case HARD: return 2;
            case VERY_HARD: return 2;
        }
    }

    public int getMaxWordLength() {
        switch (this){
            case VERY_EASY: return 4;
            case EASY: return 5;
            default:
            case NORMAL: return 6;
            case HARD: return 7;
            case VERY_HARD: return 8;
        }
    }
}
