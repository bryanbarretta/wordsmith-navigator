package bbgames.ie.wordsmithnavigator.Constants;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Bryan on 09/04/2017.
 */

public enum SpecialCharacters {
    á('a'),
    â('a'),
    ã('a'),
    à('a'),
    ç('c'),
    é('e'),
    ê('e'),
    í('i'),
    ó('o'),
    ô('o'),
    õ('o'),
    ú('u');

    private Character baseCharacter;

    SpecialCharacters(Character baseCharacter){
        this.baseCharacter = baseCharacter;
    }

    public Character getBaseCharacter(){
        return baseCharacter;
    }

    public static boolean containsSpecialCharacters(String word){
        List<SpecialCharacters> sc = Arrays.asList(SpecialCharacters.values());
        for(Character c : word.toLowerCase().toCharArray()){
            for(SpecialCharacters s : sc){
                if(s.name().equals(c.toString())){
                    return true;
                }
            }
        }
        return false;
    }

    public static Character getBaseCharacter(Character c){
        List<SpecialCharacters> sc = Arrays.asList(SpecialCharacters.values());
        for(SpecialCharacters s : sc){
            if(s.name().equals(c.toString())){
                return s.baseCharacter;
            }
        }
        return c;
    }
}
