package bbgames.ie.wordsmithnavigator.Constants;

/**
 * Created by Bryan on 04/08/2018.
 */
public enum Direction {
    NORTH,
    EAST,
    WEST,
    SOUTH;

    public boolean isHorizontal() {
        return this.equals(EAST) || this.equals(WEST);
    }

    public boolean isBackwards(){ return this.equals(WEST) || this.equals(NORTH); }

}
