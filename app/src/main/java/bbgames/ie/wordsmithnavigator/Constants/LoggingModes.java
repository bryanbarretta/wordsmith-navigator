package bbgames.ie.wordsmithnavigator.Constants;

public enum LoggingModes {
    NONE,
    LOGCAT,
    DATABASE
}
