package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 03/03/2018.
 */

public enum TileSpeed {

    INSTANT (R.string.instant, 0),
    FAST (R.string.fast, 500),
    NORMAL (R.string.normal, 1000),
    SLOW (R.string.slow, 1500);

    private int resId;
    private int ms;
    TileSpeed(int resId, int ms){
        this.resId = resId;
        this.ms = ms;
    }
    public int getResId() {
        return resId;
    }
    public int getMs() {
        return ms;
    }

    public static TileSpeed getTileSpeed(ApplicationManager applicationManager){
        return TileSpeed.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.tile_speed, "2"))];
    }

    public static void setTileSpeed(ApplicationManager applicationManager, TileSpeed tileSpeed){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.tile_speed, tileSpeed.ordinal()+"");
    }
}
