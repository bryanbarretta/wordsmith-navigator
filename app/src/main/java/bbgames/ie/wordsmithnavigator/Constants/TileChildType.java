package bbgames.ie.wordsmithnavigator.Constants;

/**
 * Different kind of sprites that can sit on top of a sprite, with z-index where lower numbers sit below
 * Created by Bryan on 06/03/2018.
 */
public enum TileChildType {
    Gem(0),
    Detonator(0),
    DetonatorCrate(0),
    Highlight(1),
    Arrow(2),
    Gate(3),
    Bridge(4),
    PowerUps(5),
    Letter(6);

    private final int zIndex;

    private TileChildType(int zIndex) {
        this.zIndex = zIndex;
    }

    public int getZIndex() {
        return zIndex;
    }

}
