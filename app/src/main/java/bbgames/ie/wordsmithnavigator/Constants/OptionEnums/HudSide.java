package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import android.view.Gravity;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 16/04/2017.
 */

public enum HudSide {

    RIGHT (R.string.right, Gravity.RIGHT),
    LEFT (R.string.left, Gravity.LEFT);

    private int resId;
    private int side;

    HudSide(int resId, int side) {
        this.resId = resId;
        this.side = side;
    }
    public int getResId() {
        return resId;
    }

    public int getSide() {
        return side;
    }

    public static HudSide getHudSide(ApplicationManager applicationManager){
        return HudSide.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.hud_side, "0"))];
    }

    public static void setHudSide(ApplicationManager applicationManager, HudSide hudSide){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.hud_side, hudSide.ordinal()+"");
    }
}
