package bbgames.ie.wordsmithnavigator.Constants;

/**
 * Created by Bryan on 09/04/2017.
 */

public enum GameModes {
    SINGLE_PLAYER,
    ONLINE_MULTIPLAYER,
    ONLINE_ONE_SHOT
}
