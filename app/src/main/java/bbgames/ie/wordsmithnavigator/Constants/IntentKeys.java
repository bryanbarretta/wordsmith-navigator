package bbgames.ie.wordsmithnavigator.Constants;

/**
 * Created by Bryan on 06/03/2018.
 */
public enum IntentKeys {
    LEVEL_ID,
    SESSION_STATISTICS,
    LEVEL_DATA,
    THEME_MAP_ID,
    THEME_HUD_ID,
    DIFFICULTY_ID,
    HUD_LETTER_CAPACITY,
    GAME_MODE_ORDINAL
}
