package bbgames.ie.wordsmithnavigator.Constants;

import android.content.Context;

/**
 * Created by Bryan on 09/04/2017.
 */

public enum SupportedLanguages {
    ENGLISH_UK("en_UK"),
    PORTUGUESE_BRAZIL("pt_BR");

    public static SupportedLanguages getLanguageByAbbr(String abbr){
        for(SupportedLanguages l : SupportedLanguages.values()){
            if(l.getAbbreviation().equals(abbr)){
                return l;
            }
        }
        return null;
    }
    private final String abbreviation;
    private SupportedLanguages(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    public String getAbbreviation() {
        return abbreviation;
    }
    public int getIconResourceId(Context context){
        return context.getResources().getIdentifier("ic_" + abbreviation.toLowerCase(), "drawable", context.getPackageName());
    }
    public int getDisplayTextResourceId(Context context){
        return context.getResources().getIdentifier("lang_" + abbreviation.toLowerCase(), "string", context.getPackageName());
    }
    public int getDictionaryResourceId(Context context){
        return context.getResources().getIdentifier("dictionary_" + abbreviation.toLowerCase(), "raw", context.getPackageName());
    }
    public double getMostFrequentWordCount(){
        switch (this){
            default:
            case ENGLISH_UK:
                return 2134713; //you
            case PORTUGUESE_BRAZIL:
                return 3097124; //que
        }
    }
}
