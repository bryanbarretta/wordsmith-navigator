package bbgames.ie.wordsmithnavigator.Constants.OptionEnums;

import android.content.Context;
import android.graphics.drawable.Drawable;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 09/06/2018.
 */

/**
 * Order is important, ordinal value represents assets folder.
 * White_wBlack => assets/gfx/hud/0     | assets/gfx/letters/0
 * Navy_wRed    => assets/gfx/hud/1     | assets/gfx/letters/1
 * etc.
 */
public enum SkinHUD {

    White_wBlack (new int[]{R.drawable.coin_white_1, R.drawable.coin_white_2, R.drawable.coin_white_3, R.drawable.coin_white_4, R.drawable.coin_white_5},
            R.string.white,
            R.string.black,
            R.color.theme_white_1a,
            R.color.theme_white_1b,
            R.color.theme_white_2a,
            R.color.theme_white_2b,
            R.color.theme_white_1b,
            R.color.theme_white_2a),
    Navy_wRed (new int[]{R.drawable.coin_red_1, R.drawable.coin_red_2, R.drawable.coin_red_3, R.drawable.coin_red_4, R.drawable.coin_red_5},
            R.string.navy,
            R.string.red,
            R.color.theme_navy_1a,
            R.color.theme_navy_1b,
            R.color.theme_navy_2a,
            R.color.theme_navy_2b,
            R.color.theme_navy_2b,
            R.color.theme_navy_1b);

    public static SkinHUD getUserTheme(ApplicationManager applicationManager){
        return SkinHUD.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_hud_user, "0"))];
    }

    public static void setUserTheme(ApplicationManager applicationManager, SkinHUD themePlayer){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_hud_user, themePlayer.ordinal()+"");
    }

    public static SkinHUD getCPUTheme(ApplicationManager applicationManager){
        return SkinHUD.values()[Integer.valueOf(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_hud_cpu, "1"))];
    }

    public static void setCPUTheme(ApplicationManager applicationManager, SkinHUD themePlayer){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.theme_hud_cpu, themePlayer.ordinal()+"");
    }

    private int[] coinDrawables;
    private int nameMainColor, nameSubColor;
    private int mainColorA, mainColorB, subColorA, subColorB, hudTextColor, darkestColor;

    SkinHUD(int[] coinDrawables, int nameMainColor, int nameSubColor, int mainColorA, int mainColorB, int subColorA, int subColorB, int hudTextColor, int darkestColor){
        this.coinDrawables = coinDrawables;
        this.nameMainColor = nameMainColor;
        this.nameSubColor = nameSubColor;
        this.mainColorA = mainColorA;
        this.mainColorB = mainColorB;
        this.subColorA = subColorA;
        this.subColorB = subColorB;
        this.hudTextColor = hudTextColor;
        this.darkestColor = darkestColor;
    }

    public int[] getCoinDrawables() {
        return coinDrawables;
    }

    public Drawable[] getCoinDrawables(Context context) {
        Drawable[] output = new Drawable[coinDrawables.length];
        for(int i = 0; i < getCoinDrawables().length; i++){
            output[i] = context.getResources().getDrawable(getCoinDrawables()[i]);
        }
        return output;
    }

    public int getNameMainColor() {
        return nameMainColor;
    }

    public int getNameSubColor() {
        return nameSubColor;
    }

    public int getMainColorA() {
        return mainColorA;
    }

    public int getMainColorB() {
        return mainColorB;
    }

    public int getSubColorA() {
        return subColorA;
    }

    public int getSubColorB() {
        return subColorB;
    }

    public int getHudTextColor() {
        return hudTextColor;
    }

    /**
     * Dark color in this theme, suitable for using on a white or bright background and in keeping with the themes colors
     * @return
     */
    public int getDarkestColor() {
        return darkestColor;
    }
}
