package bbgames.ie.wordsmithnavigator.Activities.BaseActivities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import bbgames.ie.wordsmithnavigator.Activities.Authentication.LoginActivity;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 07/02/2018.
 * Extend this activity to ensure the user is online and logged in before the activity is created
 */

public abstract class BaseActivitySignedIn extends BaseActivity {

    public final static int REQUEST_CODE_USER_LOGGED_IN =    200;
    public final static int REQUEST_CODE_USER_LOGIN_FAILED = 401;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!applicationManager.isNetworkAvailable()){
            onNoNetworkConnection();
        }else {
            if(applicationManager.getGoogleAccountManager().isSignedIn()){
                onConnected();
            }else {
                onNoConnection();
            }
        }
    }

    /**
     * The default behavior when there is no internet connection.
     * Override this method if different behavior desired
     */
    protected void onNoNetworkConnection(){
        Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * The default behavior when the user is not signed in.
     * Override this method if different behavior desired
     */
    protected void onNoConnection(){
        startActivityForResult(new Intent(this, LoginActivity.class), REQUEST_CODE_USER_LOGGED_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            onConnected();
        }
        else if(resultCode == RESULT_FIRST_USER){
            //Pending: Will not be implemented until passwords are used
        }
        else {
            Toast.makeText(this, getString(R.string.login_failed), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * called when we have established both an online connection and a logged in user
     */
    public abstract void onConnected();

}
