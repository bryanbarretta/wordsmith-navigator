package bbgames.ie.wordsmithnavigator.Activities.DebugOnly;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Controls.AdapterListViewCpuLogLevelSelect;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

import static bbgames.ie.wordsmithnavigator.Activities.DebugOnly.ViewCpuLogsLevel.INTENT_EXTRA_LEVEL;

public class ViewCpuLogsLevelSelect extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cpu_logs_level_select);
        ArrayList<String> levels = Util.getApplicationManager(this).getDatabaseGateway().getCpuGameNames();
        ListView lv = (ListView) findViewById(R.id.lvLevels);
        lv.setAdapter(new AdapterListViewCpuLogLevelSelect(this, levels) {
            @Override
            public void onDeleteSelected(String level) {
                Util.getApplicationManager(ViewCpuLogsLevelSelect.this).getDatabaseGateway().clearCpuGameLogs(level);
                recreate();
            }
            @Override
            public void onViewSelected(String level) {
                Intent i = new Intent(ViewCpuLogsLevelSelect.this, ViewCpuLogsLevel.class);
                i.putExtra(INTENT_EXTRA_LEVEL, level);
                startActivity(i);
            }
        });
    }

}
