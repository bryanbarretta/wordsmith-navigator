package bbgames.ie.wordsmithnavigator.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.BaseActivities.BaseSubMenuActivity;
import bbgames.ie.wordsmithnavigator.Controls.LevelCard;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogLevelDetails;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.Level;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class LevelSelectActivity extends BaseSubMenuActivity {

    private final static String KEY_PAGER_LEVELS = "KEY_PAGER_LEVELS";
    private final static int NUM_LEVELS_PER_SECTION = 10;

    private RelativeLayout fragmentLevelSelect;
    private ImageButton btnLeft, btnRight;
    private ViewPager viewPager;
    private SwipeAdapter swipeAdapter;
    private LinearLayout tabDots;       //A Horizontal linear layout of imageViews

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setup();
    }

    private void setup(){
        txtHeader.setText(getString(R.string.level_select));
        fragmentLevelSelect = (RelativeLayout) Util.inflateView(this, R.layout.fragment_level_select);
        btnLeft = (ImageButton)fragmentLevelSelect.findViewById(R.id.btnLeft);
        btnRight = (ImageButton)fragmentLevelSelect.findViewById(R.id.btnRight);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipe(true);
            }
        });
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipe(false);
            }
        });
        viewPager = (ViewPager)fragmentLevelSelect.findViewById(R.id.vpLevelSection);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                refreshViewPagerElements(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        container.addView(fragmentLevelSelect);
        int wButtonSmall = applicationManager.getScreenHeightPixels() / 5;
        GraphicUtils.scalePadViewContent(findViewById(R.id.btnLeft), wButtonSmall);
        GraphicUtils.scalePadViewContent(findViewById(R.id.btnRight), wButtonSmall);
        swipeAdapter = new SwipeAdapter(this);
        viewPager.setAdapter(swipeAdapter);
        createTabDots();

        int latestLevel = applicationManager.getLevelFactory().getLatestUnlockedLevelId();
        int remainder = latestLevel % NUM_LEVELS_PER_SECTION;
        int section = latestLevel == 0 ? 0 :
                (((latestLevel - remainder) / NUM_LEVELS_PER_SECTION) - 1) + (remainder > 0 ? 1 : 0);
        viewPager.setCurrentItem(section);
        refreshViewPagerElements(section);
    }

    @Override
    public boolean showBackButton() {
        return true;
    }

    @Override
    public boolean showReplayButton() {
        return false;
    }

    @Override
    public boolean showTitle() {
        return true;
    }

    @Override
    public boolean showNextButton() {
        return false;
    }

    @Override
    public boolean showStarCount() {
        return true;
    }

    /**
     * The dots above the main fragment representing which page of the viewpager is selected
     */
    private void createTabDots(){
        tabDots = new LinearLayout(this);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT  , RelativeLayout.TRUE);
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT , RelativeLayout.TRUE);
        tabDots.setLayoutParams(lp);
        tabDots.setGravity(Gravity.CENTER);
        tabDots.setOrientation(LinearLayout.HORIZONTAL);
        int pxDot = Util.getDimensValueInPixels(this, R.dimen.icon_tiny);
        for(int i = 0; i < swipeAdapter.getCount(); i++){
            ImageView img = new ImageView(this);
            img.setLayoutParams(new LinearLayout.LayoutParams(pxDot, pxDot));
            img.setImageDrawable(getResources().getDrawable(R.drawable.tab_indicator_default));
            tabDots.addView(img);
        }
        header.addView(tabDots);
    }

    /**
     * Swipe the levels viewPager in this direction
     * @param left : left if true, right if false
     */
    private void swipe(boolean left){
        int target = viewPager.getCurrentItem();
        if(left && viewPager.getCurrentItem() > 0){
            target = viewPager.getCurrentItem() - 1;
        }
        if(!left && viewPager.getCurrentItem() < (swipeAdapter.getCount() - 1)){
            target = viewPager.getCurrentItem() + 1;
        }
        if(target == viewPager.getCurrentItem()){
            //Cannot swipe any further in this direction
            Toast.makeText(this, "Cannot swipe further in this direction", Toast.LENGTH_SHORT).show();
        }else {
            viewPager.setCurrentItem(target);
        }
    }

    /**
     * Hides/Shows the left and right buttons and update the tab dots depending on the page selected in the viewpager
     */
    private void refreshViewPagerElements(int position){
        //Left|Right buttons
        if(position == 0){
            btnLeft.setVisibility(View.INVISIBLE);
            btnRight.setVisibility(View.VISIBLE);
        }else if(position == (swipeAdapter.getCount() - 1)){
            btnLeft.setVisibility(View.VISIBLE);
            btnRight.setVisibility(View.INVISIBLE);
        }
        //Dots
        ((ImageView)tabDots.getChildAt(position)).setImageDrawable(getResources().getDrawable(R.drawable.tab_indicator_selected));
        if(position > 0){
            ((ImageView)tabDots.getChildAt(position - 1)).setImageDrawable(getResources().getDrawable(R.drawable.tab_indicator_default));
        }
        if(position < (swipeAdapter.getCount() - 1)){
            ((ImageView)tabDots.getChildAt(position + 1)).setImageDrawable(getResources().getDrawable(R.drawable.tab_indicator_default));
        }
    }

    //region Level Cards

    /**
     * Given a section, return the 10 levels of that section. e.g. section = 2 returns levels 21-30
     * @param section
     * @return sublist of levelProperties
     */
    private ArrayList<Level> getSectionLevels(int section){
        int from = 1 + section * NUM_LEVELS_PER_SECTION;
        int max =  1 + applicationManager.getLevelFactory().getTotalLevelCount();
        int to = max < (from + NUM_LEVELS_PER_SECTION) ? max : (from + NUM_LEVELS_PER_SECTION);
        return applicationManager.getLevelFactory().getLevels(from, to);
    }

    private class SwipeAdapter extends FragmentPagerAdapter {

        public SwipeAdapter(LevelSelectActivity levelSelectActivity) {
            super(levelSelectActivity.getSupportFragmentManager());
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f = new PagerFragment();
            Bundle b = new Bundle();
            b.putParcelableArrayList(KEY_PAGER_LEVELS, getSectionLevels(position));
            f.setArguments(b);
            return f;
        }

        @Override
        public int getCount() {
            int leftover = applicationManager.getLevelFactory().getTotalLevelCount() % NUM_LEVELS_PER_SECTION;
            int roundedUp = applicationManager.getLevelFactory().getTotalLevelCount() + (NUM_LEVELS_PER_SECTION - leftover);
            return roundedUp / NUM_LEVELS_PER_SECTION;
        }
    }

    /**
     * Inflated view for each level section i.e. each page of the viewpager
     */
    public static class PagerFragment extends Fragment{

        public PagerFragment(){

        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            final Context ctx = inflater.getContext();
            Bundle bundle = getArguments();
            ArrayList<Level> sectionLevels = bundle.getParcelableArrayList(KEY_PAGER_LEVELS);
            LinearLayout ll = (LinearLayout) inflater.inflate (R.layout.fragment_level_section, container, false);
            LinearLayout llTopRow = (LinearLayout) ll.findViewById(R.id.llTopRow);
            LinearLayout llBottomRow = (LinearLayout) ll.findViewById(R.id.llBottomRow);
            llTopRow.removeAllViews();
            llBottomRow.removeAllViews();
            for (int i = 0; i < sectionLevels.size(); i++) {
                final Level l = sectionLevels.get(i);
                LevelCard lc = new LevelCard(inflater.getContext(), l);
                if (l.getLevelPlayerData() != null) {
                    lc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new DialogLevelDetails(Util.getActivity(ctx), l.getLevelStaticData(), new Runnable() {
                                @Override
                                public void run() {
                                    Util.getApplicationManager(ctx).startLevelActivity(Util.getActivity(ctx), l.getLevelStaticData().getId());
                                }
                            }).show();
                        }
                    });
                }
                if (i < (NUM_LEVELS_PER_SECTION / 2)) {
                    llTopRow.addView(lc);
                } else {
                    llBottomRow.addView(lc);
                }
            }
            return ll;
        }
    }

    //endregion
}
