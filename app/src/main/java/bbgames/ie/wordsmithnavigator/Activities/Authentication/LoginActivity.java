package bbgames.ie.wordsmithnavigator.Activities.Authentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import bbgames.ie.wordsmithnavigator.Online.GoogleAccountManager;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class LoginActivity extends Activity {

    private GoogleAccountManager googleAccountManager;
    private com.google.android.gms.common.SignInButton btnSignInGoogle;
    private TextView txtError;

    private static final int REQUEST_CODE_SIGN_IN_GOOGLE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setup();
    }

    private void setup() {
        googleAccountManager = Util.getApplicationManager(this).getGoogleAccountManager();
        txtError = (TextView)findViewById(R.id.txtError);
        btnSignInGoogle = (com.google.android.gms.common.SignInButton) findViewById(R.id.sign_in_google);
        TextView txtBtnSignInGoogle = ((TextView)btnSignInGoogle.getChildAt(0));
        txtBtnSignInGoogle.setText(getString(R.string.signin));
        btnSignInGoogle.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
               Intent signInIntent = googleAccountManager.getGoogleSignInClient().getSignInIntent();
               LoginActivity.this.setResult(RESULT_OK);
               LoginActivity.this.startActivityForResult(signInIntent, REQUEST_CODE_SIGN_IN_GOOGLE);
           }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        googleAccountManager.signInSilently(this, new Runnable() {
            @Override
            public void run() {
                //On Connected
            }
        }, new Runnable() {
            @Override
            public void run() {
                //On Disconnected
            }
        });
    }

    private void showError(String msg){
        txtError.setText(msg);
        txtError.setVisibility(View.VISIBLE);
        Log.e(this.getClass().getSimpleName(), msg);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            GoogleSignInAccount account = task.getResult(ApiException.class);
            googleAccountManager.setAccount(account);
            Toast.makeText(this, getString(R.string.login_ok), Toast.LENGTH_SHORT).show();
            LoginActivity.this.setResult(RESULT_OK);
            LoginActivity.this.finish();
        } catch (Exception e) {
            showError(e.getMessage());
            return;
        }
    }
}
