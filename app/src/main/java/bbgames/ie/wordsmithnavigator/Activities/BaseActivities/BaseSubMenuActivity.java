package bbgames.ie.wordsmithnavigator.Activities.BaseActivities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 08/08/2018.
 */

public abstract class BaseSubMenuActivity extends BaseActivity {

    protected ImageButton btnBack, btnReplay, btnNext;
    protected TextView txtHeader, txtStarCount;
    protected ImageView imgStar;
    protected RelativeLayout header, container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submenu);
        setup();
    }

    private void setup(){
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnReplay = (ImageButton) findViewById(R.id.btnReplay);
        imgStar = (ImageView) findViewById(R.id.imgStar);
        txtStarCount = (TextView) findViewById(R.id.txtStarCount);
        txtHeader = (TextView) findViewById(R.id.txtHeader);
        header = (RelativeLayout) findViewById(R.id.header);
        container = (RelativeLayout) findViewById(R.id.container);
        txtHeader.setTypeface(Fonts.defaultTitle(this), Typeface.BOLD);
        GraphicUtils.scaleSquareView(btnBack, true, true);
        GraphicUtils.scaleSquareView(btnNext, true, true);

        if(showBackButton()){
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }else {
            btnBack.setVisibility(View.GONE);
        }

        if(showReplayButton()){
            //TODO: Implement
        }else {
            btnReplay.setVisibility(View.GONE);
        }

        if(showStarCount()){
            txtStarCount.setText(Util.getApplicationManager(this).getLevelFactory().getTotalUnlockedStarCount() + "");
            txtStarCount.setTypeface(Fonts.defaultFun(this));
        }else {
            imgStar.setVisibility(View.GONE);
            txtStarCount.setVisibility(View.GONE);
        }

        if(showTitle()){

        }else {
            this.txtHeader.setVisibility(View.GONE);
        }

        if(showNextButton()){

        }else {
            this.btnNext.setVisibility(View.GONE);
        }
    }

    public abstract boolean showBackButton();

    public abstract boolean showReplayButton();

    public abstract boolean showTitle();

    public abstract boolean showNextButton();

    public abstract boolean showStarCount();

}
