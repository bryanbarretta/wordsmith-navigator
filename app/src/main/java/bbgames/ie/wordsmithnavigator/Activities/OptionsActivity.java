package bbgames.ie.wordsmithnavigator.Activities;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import bbgames.ie.wordsmithnavigator.Activities.BaseActivities.BaseSubMenuActivity;
import bbgames.ie.wordsmithnavigator.Activities.Options.Options;
import bbgames.ie.wordsmithnavigator.BuildConfig;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.R;

public class OptionsActivity extends BaseSubMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setup();
    }

    private void setup(){
        Options.Builder optionsBuilder = new Options.Builder(this, null, getResources().getColor(R.color.tertiary), null)
                .addHeaderGameplay()
                    .addGameplayDifficulty()
                    .addGameplayHelpDialogsEnabled()
                    .addGameplayTileSpeed()
                    .addGameplayCameraSpeed()
                    .addGameplayCameraZoomLevel()
                    .addGameplayHudSide()
                .addHeaderTheme()
                    .addThemePlayer(PlayerType.USER)
                    .addThemeLetter(PlayerType.USER)
                    .addThemePlayer(PlayerType.CPU)
                    .addThemeLetter(PlayerType.CPU)
                    .addThemeMap();
        if(applicationManager.getGoogleAccountManager().isSignedIn()){
            optionsBuilder.addHeaderAccount()
                    .addAccountLogout();
        }
        if (BuildConfig.DEBUG) {
            optionsBuilder.addHeaderMisc()
                    .addUnlockAllLevels()
                    .addViewErrorDatabaseScreen()
                    .addAndroidDatabaseManagerScreen()
                    .addViewCpuLogicLogs()
                    .addCpuLogicLogsEnabled();
        }
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        scrollView.addView(optionsBuilder.Build());
        container.addView(scrollView);
        txtHeader.setText(getString(R.string.options));
    }

    @Override
    public boolean showBackButton() {
        return true;
    }

    @Override
    public boolean showReplayButton() {
        return false;
    }

    @Override
    public boolean showTitle() {
        return true;
    }

    @Override
    public boolean showNextButton() {
        return false;
    }

    @Override
    public boolean showStarCount() {
        return true;
    }
}
