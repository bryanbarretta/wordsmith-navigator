package bbgames.ie.wordsmithnavigator.Activities;

import android.graphics.Typeface;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.sprite.batch.SpriteBatch;
import org.andengine.entity.sprite.batch.SpriteGroup;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.adt.color.Color;
import org.andengine.util.adt.list.SmartList;

import java.io.IOException;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class RepeatingSpriteGameActivity extends SimpleBaseGameActivity {

    //TODO:
    //SpriteBatch: For map, walls
    //SpriteGroup: For Letters (Watch out for flickering)

    private final float ZOOM_DISTANCE = 1f; //Default = 1

    private final int LEVEL_ROWS = 5;
    private final int LEVEL_COLUMNS = 5;
    private final int LETTER_ROWS = 5;
    private final int LETTER_COLUMNS = 10;

    Scene scene;
    SmoothCamera camera;

    Font mFont;

    AssetBitmapTexture textureMap;
    TextureRegion textureRegionMap;
    BitmapTextureAtlas texLetters;
    TiledTextureRegion texRegionLetters;

    @Override
    protected void onCreateResources() throws IOException {
        this.textureMap = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "map.jpg", TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.textureRegionMap = TextureRegionFactory.extractFromTexture(this.textureMap);
        this.textureMap.load();

        texLetters = new BitmapTextureAtlas(getTextureManager(), 950, 190, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        texRegionLetters = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texLetters, this, "tile_letters.png", 0, 0, 5, 1);
        texLetters.load();
    }

    SpriteBatch sbMap;
    SpriteGroup mSpriteGroup;

    @Override
    protected Scene onCreateScene() {
        scene = new Scene();

        camera.setZoomFactorDirect(ZOOM_DISTANCE);

        scene.setBackground(new Background(0.2f, 0.4f, 0.7f));

        //-----------------------------
        //--    SPRITE BATCH         --
        //-----------------------------
        float offset = 0;
        sbMap = new SpriteBatch(this.textureMap, LEVEL_COLUMNS * LEVEL_ROWS, getVertexBufferObjectManager());
        sbMap.setBlendFunction(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
        for(int c = 0; c < LEVEL_COLUMNS; c++) {
            for (int r = 0; r < LEVEL_ROWS; r++) {
                float x = offset + (c * 190);
                float y = offset + (r * 190);
                sbMap.draw(textureRegionMap, x, y, 190, 190, 1f, 1f, 1f, 1f);
            }
        }
        sbMap.submit();
        scene.attachChild(sbMap);

        //-----------------------------
        //--    SPRITE GROUP         --
        //-----------------------------
        offset = 95f;
        mSpriteGroup = new SpriteGroup(0, 0, texLetters, LETTER_ROWS * LETTER_COLUMNS, getVertexBufferObjectManager()){
            @Override
            protected boolean onUpdateSpriteBatch() {
                return false;
            }
            @Override
            protected void onManagedUpdate(float pSecondsElapsed) {
                super.onManagedUpdate(pSecondsElapsed);
                final SmartList<IEntity> children = this.mChildren;
                if(children != null) {
                    final int childCount = children.size();
                    for(int i = 0; i < childCount; i++) {
                        this.drawWithoutChecks((Sprite)children.get(i));
                    }
                    submit();
                }
            }
        };
        for(int c = 0; c < LETTER_COLUMNS; c++) {
            for (int r = 0; r < LETTER_ROWS; r++) {
                float x = offset + (c * 190);
                float y = offset + (r * 190);
                TiledSprite sprite = createLetter(x,  y, Util.getConsonants()[new Random().nextInt(Util.getConsonants().length - 1)] + "");
                sprite.setCurrentTileIndex(new Random().nextInt(5));
                mSpriteGroup.attachChild(sprite);
            }
        }
        mSpriteGroup.setChildrenVisible(true);
        scene.attachChild(mSpriteGroup);

        return scene;
    }

    private TiledSprite createLetter(float pX, float pY, String letter){
        if(mFont == null){
            BitmapTextureAtlas fontTexture = new BitmapTextureAtlas(getTextureManager(), 500, 500, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            mFont = new Font(getFontManager(), fontTexture, Typeface.createFromAsset(getAssets(), "font/litsans.ttf"), 100, true, Color.BLACK);
            getTextureManager().loadTexture(fontTexture);
            mFont.load();
        }
        TiledSprite sprite = new TiledSprite(pX, pY, texRegionLetters, getVertexBufferObjectManager()){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                setCurrentTileIndex(getCurrentTileIndex() >= 4 ? 0 : getCurrentTileIndex() + 1);
                return true;
            }
        };
        Text t = new Text(95f, 95f, mFont, letter, getVertexBufferObjectManager());
        sprite.attachChild(t);
        scene.registerTouchArea(sprite);
        return sprite;
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        camera = new SmoothCamera(0f, 0f,
                Constant.CAMERA_1920,
                Constant.CAMERA_1080,
                3000f, 3000f, 0f);
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
        engineOptions.getRenderOptions().setDithering(true);
        return engineOptions;
    }

}
