package bbgames.ie.wordsmithnavigator.Activities.BaseActivities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.AsyncTask.AsyncTaskHandler;
import bbgames.ie.wordsmithnavigator.Online.GoogleAccountManager;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;

/**
 * Created by Bryan on 07/02/2018.
 * Extend this activity for easy access to manager classes
 */

public class BaseActivity extends FragmentActivity {

    protected ApplicationManager applicationManager;
    protected AsyncTaskHandler asyncTaskHandler;
    protected GoogleAccountManager googleAccountManager;
    protected DatabaseGateway databaseGateway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.applicationManager = (ApplicationManager)getApplication();
        this.asyncTaskHandler = applicationManager.getAsyncTaskHandler();
        this.googleAccountManager = applicationManager.getGoogleAccountManager();
        this.databaseGateway = applicationManager.getDatabaseGateway();
    }

}
