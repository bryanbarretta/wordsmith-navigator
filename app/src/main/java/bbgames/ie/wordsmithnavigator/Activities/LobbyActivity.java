package bbgames.ie.wordsmithnavigator.Activities;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.Activities.BaseActivities.BaseActivitySignedIn;
import bbgames.ie.wordsmithnavigator.Online.MatchmakingManager;
import bbgames.ie.wordsmithnavigator.R;

public class LobbyActivity extends BaseActivitySignedIn {

    private Button btnPlayOnline;
    private TextView txtInfo;
    private MatchmakingManager matchmakingManager;

    @Override
    public void onConnected() {
        setup();
    }

    private void setup(){
        setContentView(R.layout.activity_one_shot_mode);

        matchmakingManager = new MatchmakingManager(this);

        btnPlayOnline = (Button) findViewById(R.id.btnPlayOnline);
        btnPlayOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    matchmakingManager.createRoom();
                }catch (Exception e){
                    logInfoOnUi("BUTTON CATCH EXCEPTION\n" + e.getMessage());
                }
            }
        });

        txtInfo = (TextView)findViewById(R.id.txtInfo);
    }

    public void logInfoOnUi(final String info){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtInfo.append(info + "\n");
            }
        });
    }

}
