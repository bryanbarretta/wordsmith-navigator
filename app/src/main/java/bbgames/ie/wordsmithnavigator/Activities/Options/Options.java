package bbgames.ie.wordsmithnavigator.Activities.Options;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.DebugOnly.ErrorDatabaseScreen;
import bbgames.ie.wordsmithnavigator.Activities.DebugOnly.ViewCpuLogsLevelSelect;
import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.AsyncTask.iTask;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.CameraSpeed;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.CameraZoomLevel;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.HudSide;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinMap;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.TileSpeed;
import bbgames.ie.wordsmithnavigator.Database.AndroidDatabaseManager;
import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

import static android.widget.LinearLayout.VERTICAL;

/**
 * Created by Bryan on 04/03/2018.
 */

public class Options {

    /**
     * Builder class to return a LinearLayout view with desired Option fields
     */
    public static class Builder {

        private LinearLayout master;
        private Activity activity;

        private int minRowHeight;
        private int padding;
        private Integer colorBackground, colorText, colorBackgroundHeader;

        private ApplicationManager applicationManager;
        private LayoutInflater inflater;
        private Typeface litSans;

        public Builder(Activity activity){
            this(activity, activity.getResources().getColor(R.color.blue_gray_1),
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.blue_gray_2));
        }

        public Builder(Activity activity, Integer colorBackground, Integer colorText, Integer colorBackgroundHeader) {
            master = new LinearLayout(activity);
            this.activity = activity;
            this.applicationManager = Util.getApplicationManager(activity);
            this.minRowHeight = Util.getDimensValueInPixels(activity, R.dimen.button_height);
            this.padding = Util.getDimensValueInPixels(activity, R.dimen.padding_8dp_border);
            this.colorBackgroundHeader = colorBackgroundHeader;
            this.colorBackground = colorBackground;
            this.colorText = colorText;
            this.inflater = LayoutInflater.from(activity);
            this.litSans = Fonts.defaultFun(activity);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            master.setLayoutParams(lp);
            master.setOrientation(VERTICAL);
        }

        public LinearLayout Build(){
            return master;
        }

        //region Headers

        public Builder addHeaderGameplay(){
            master.addView(getHeaderText(activity.getString(R.string.gameplay), activity.getResources().getDrawable(R.drawable.ic_gameplay)));
            return this;
        }

        public Builder addHeaderTheme(){
            master.addView(getHeaderText(activity.getString(R.string.theme), activity.getResources().getDrawable(R.drawable.ic_theme)));
            return this;
        }

        public Builder addHeaderAccount(){
            master.addView(getHeaderText(activity.getString(R.string.account), activity.getResources().getDrawable(R.drawable.ic_user)));
            return this;
        }

        public Builder addHeaderMisc(){
            master.addView(getHeaderText(activity.getString(R.string.miscellaneous), activity.getResources().getDrawable(R.drawable.ic_play)));
            return this;
        }


        //endregion

        //region Children

        public Builder addGameplayDifficulty(){
            ArrayList<String> options = new ArrayList<>();
            for(AIDifficulty d : AIDifficulty.values()){
                options.add(activity.getString(d.getResourceId()));
            }
            master.addView(getOptionSpinner(activity.getString(R.string.difficulty),
                    activity.getString(R.string.difficulty_info),
                    options,
                    AIDifficulty.getDifficulty(applicationManager).ordinal(),
                    new iTask() {
                        @Override
                        public void Task() {
                            AIDifficulty.setDifficulty(applicationManager, AIDifficulty.values()[(int)this.getTaskData()]);
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addGameplayTileSpeed(){
            ArrayList<String> options = new ArrayList<>();
            for(TileSpeed ts : TileSpeed.values()){
                options.add(activity.getString(ts.getResId()));
            }
            master.addView(getOptionSpinner(activity.getString(R.string.tile_speed),
                    activity.getString(R.string.tile_speed_info),
                    options,
                    TileSpeed.getTileSpeed(applicationManager).ordinal(),
                    new iTask() {
                        @Override
                        public void Task() {
                            TileSpeed.setTileSpeed(applicationManager, TileSpeed.values()[(int)this.getTaskData()]);
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addGameplayCameraSpeed(){
            ArrayList<String> options = new ArrayList<>();
            for(CameraSpeed ts : CameraSpeed.values()){
                options.add(activity.getString(ts.getResId()));
            }
            master.addView(getOptionSpinner(activity.getString(R.string.camera_panning_speed),
                    activity.getString(R.string.camera_panning_speed_info),
                    options,
                    CameraSpeed.getCameraSpeed(applicationManager).ordinal(),
                    new iTask() {
                        @Override
                        public void Task() {
                            CameraSpeed c = CameraSpeed.values()[(int)this.getTaskData()];
                            CameraSpeed.setCameraSpeed(applicationManager, c);
                            if(activity instanceof GameActivity){
                                ((GameActivity)activity).getCamera().SetSpeed(c.getMs());
                            }
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addGameplayCameraZoomLevel(){
            ArrayList<String> options = new ArrayList<>();
            for(CameraZoomLevel ts : CameraZoomLevel.values()){
                options.add(activity.getString(ts.getResId()));
            }
            master.addView(getOptionSpinner(activity.getString(R.string.camera_zoom_level),
                    activity.getString(R.string.camera_zoom_level_info),
                    options,
                    CameraZoomLevel.getCameraZoomLevel(applicationManager).ordinal(),
                    new iTask() {
                        @Override
                        public void Task() {
                            CameraZoomLevel c = CameraZoomLevel.values()[(int)this.getTaskData()];
                            CameraZoomLevel.setCameraZoomLevel(applicationManager, c);
                            if(activity instanceof GameActivity){
                                ((GameActivity)activity).getCamera().SetZoom(c.getValue());
                            }
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addGameplayHudSide() {
            master.addView(getOptionSwitch(activity.getString(R.string.hud_side),
                    activity.getString(R.string.hud_side_info),
                    HudSide.getHudSide(applicationManager).getSide() == Gravity.RIGHT,
                    new String[]{activity.getString(R.string.left), activity.getString(R.string.right)},
                    new iTask() {
                        @Override
                        public void Task() {
                            boolean isChecked = ((boolean)this.getTaskData()) == true;
                            HudSide.setHudSide(applicationManager, isChecked ? HudSide.RIGHT : HudSide.LEFT);
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }));
            return this;
        }

        public Builder addThemePlayer(final PlayerType playerType){
            ArrayList<String> options = new ArrayList<>();
            for(SkinHUD t : SkinHUD.values()){
                options.add(activity.getString(t.getNameMainColor()) + " (" + activity.getString(t.getNameSubColor()) + ")");
            }
            master.addView(getOptionSpinner(activity.getString(playerType.equals(PlayerType.USER) ? R.string.theme_hud_user : R.string.theme_hud_cpu),
                    activity.getString(R.string.theme_hud_info),
                    options,
                    playerType.equals(PlayerType.USER) ? SkinHUD.getUserTheme(applicationManager).ordinal() :
                            SkinHUD.getCPUTheme(applicationManager).ordinal(),
                    new iTask() {
                        @Override
                        public void Task() {
                            if(playerType.equals(PlayerType.USER)) {
                                SkinHUD.setUserTheme(applicationManager, SkinHUD.values()[(int) this.getTaskData()]);
                            }else {
                                SkinHUD.setCPUTheme(applicationManager, SkinHUD.values()[(int) this.getTaskData()]);
                            }
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addThemeLetter(final PlayerType playerType){
            ArrayList<String> options = new ArrayList<>();
            for(SkinLetter t : SkinLetter.getUnlockedSkins(applicationManager)){
                options.add(t.getName());
            }
            master.addView(getOptionSpinner(activity.getString(playerType.equals(PlayerType.USER) ? R.string.theme_letter_user : R.string.theme_letter_cpu),
                    activity.getString(R.string.theme_letter_info),
                    options,
                    playerType.equals(PlayerType.USER) ? SkinLetter.getUserTheme(applicationManager).getId() :
                                                         SkinLetter.getCPUTheme(applicationManager).getId(),
                    new iTask() {
                        @Override
                        public void Task() {
                            int id = (int) this.getTaskData();
                            SkinLetter skin = applicationManager.getDatabaseGateway().getSkin(id);
                            if(playerType.equals(PlayerType.USER)) {
                                SkinLetter.setUserTheme(applicationManager, skin);
                            }else {
                                SkinLetter.setCPUTheme(applicationManager, skin);
                            }
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addThemeMap(){
            ArrayList<String> options = new ArrayList<>();
            for(SkinMap t : SkinMap.values()){
                options.add(activity.getString(t.getResId()));
            }
            master.addView(getOptionSpinner(activity.getString(R.string.theme_map),
                    activity.getString(R.string.theme_map_info),
                    options,
                    SkinMap.getThemeMap(applicationManager).ordinal(),
                    new iTask() {
                        @Override
                        public void Task() {
                            SkinMap.setThemeMap(applicationManager, SkinMap.values()[(int)this.getTaskData()]);
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }
            ));
            return this;
        }

        public Builder addAccountLogout(){
            master.addView(getOptionButton(
                    activity.getString(R.string.logout),
                    activity.getString(R.string.logout_info),
                    activity.getString(R.string.logout),
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.white),
                    null, new Runnable() {
                        @Override
                        public void run() {
                            applicationManager.getGoogleAccountManager().signOut(activity, new Runnable() {
                                @Override
                                public void run() {
                                    activity.recreate();
                                }
                            });
                        }
                    }
            ));
            return this;
        }

        @Deprecated
        public Builder addAccountDeleteAccount(){
            master.addView(getOptionButton(
                    activity.getString(R.string.delete_account),
                    activity.getString(R.string.delete_account_info),
                    activity.getString(R.string.delete_account),
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.white),
                    activity.getResources().getDrawable(R.drawable.ic_exclamation),
                    new Runnable() {
                        @Override
                        public void run() {
                            Util.showYesNoDialog(activity,
                                activity.getString(R.string.delete_account),
                                activity.getString(R.string.delete_account_message),
                                activity.getString(R.string.yes_delete_my_account),
                                activity.getString(R.string.cancel),
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        //applicationManager.getAuthenticationManager().requestDeleteUserFromServer(applicationManager.getCurrentUserLogin(), new Runnable() {
                                        //    @Override
                                        //    public void run() {
                                        //        Toast.makeText(activity, activity.getString(R.string.account_deleted_successfully), Toast.LENGTH_SHORT).show();
                                        //        activity.recreate();
                                        //    }
                                        //});
                                    }
                                }, null);
                        }
                    }
            ));
            return this;
        }

        public Builder addViewErrorDatabaseScreen(){
            master.addView(getOptionButton(
                    "Error Database",
                    "This is a debug-only developer option",
                    "Open",
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.white),
                    null, new Runnable() {
                        @Override
                        public void run() {
                            activity.startActivity(new Intent(activity, ErrorDatabaseScreen.class));
                        }
                    }
            ));
            return this;
        }

        public Builder addAndroidDatabaseManagerScreen(){
            master.addView(getOptionButton(
                    "Android Database Manager",
                    "This is a debug-only developer option",
                    "Open",
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.white),
                    null, new Runnable() {
                        @Override
                        public void run() {
                            activity.startActivity(new Intent(activity, AndroidDatabaseManager.class));
                        }
                    }
            ));
            return this;
        }

        public Builder addViewCpuLogicLogs(){
            master.addView(getOptionButton(
                    "View CPU Logic Logs",
                    "This is a debug-only developer option",
                    "View",
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.white),
                    null, new Runnable() {
                        @Override
                        public void run() {
                            activity.startActivity(new Intent(activity, ViewCpuLogsLevelSelect.class));
                        }
                    }
            ));
            return this;
        }

        public Builder addCpuLogicLogsEnabled() {
            //String mainText, String subText, boolean isChecked, final String[] stringsLeftRight, final iTask onCheckChanged
            Boolean enabled = Boolean.parseBoolean(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.cpu_logs_enabled, "false"));
            master.addView(getOptionSwitch(
                    "CPU Logic Logs Enabled",
                    "This is a debug-only developer option",
                    enabled,
                    new String[]{activity.getString(R.string.disabled), activity.getString(R.string.enabled)},
                    new iTask() {
                        @Override
                        public void Task() {
                            boolean isChecked = ((boolean)this.getTaskData()) == true;
                            applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.cpu_logs_enabled, isChecked ? "true" : "false");
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }));
            return this;
        }

        //endregion

        //region private

        private LinearLayout getOptionSpinner(String mainText, String subText, ArrayList<String> options, int defaultIndex, final iTask onItemSelectedTask){
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.options_list_item_spinner, null, false);
            styleText(layout, mainText, subText);
            Spinner spinner = (Spinner) layout.findViewById(R.id.spinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, options);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(defaultIndex);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    onItemSelectedTask.setTaskData(position);
                    Util.getApplicationManager(activity).getAsyncTaskHandler().AddTask(onItemSelectedTask);
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            return layout;
        }

        private LinearLayout getOptionButton(String mainText, String subText,
                                             String btnText, int buttonColorBg, int buttonColorText, Drawable drawableLeft,
                                             final Runnable onButtonClick){
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.options_list_item_button, null, false);
            styleText(layout, mainText, subText);
            Button btn = (Button) layout.findViewById(R.id.button);
            btn.setTypeface(litSans);
            btn.setText(btnText);
            btn.setTextColor(buttonColorText);
            GraphicUtils.buttonColor(btn, buttonColorBg);
            if(drawableLeft != null){
                GraphicUtils.buttonDrawableLeft(btn, drawableLeft, buttonColorText, minRowHeight, padding);
            }
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonClick.run();
                }
            });
            return layout;
        }

        private LinearLayout getOptionSwitch(String mainText, String subText, boolean isChecked, final String[] stringsLeftRight, final iTask onCheckChanged){
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.options_list_item_switch, null, false);
            styleText(layout, mainText, subText);
            final Switch switchView = (Switch) layout.findViewById(R.id.switch_view);
            switchView.setTypeface(litSans);
            switchView.setTextColor(colorText);
            switchView.setChecked(isChecked);
            if(stringsLeftRight != null){
                switchView.setText((isChecked ? stringsLeftRight[1] : stringsLeftRight[0]) + "  ");
            }
            switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(stringsLeftRight != null){
                        switchView.setText((isChecked ? stringsLeftRight[1] : stringsLeftRight[0]) + "  ");
                    }
                    onCheckChanged.setTaskData(isChecked);
                    applicationManager.getAsyncTaskHandler().AddTask(onCheckChanged);
                }
            });
            return layout;
        }

        private void styleText(LinearLayout layout, String mainText, String subText){
            if(colorBackground != null) {
                layout.setBackgroundColor(colorBackground);
            }
            TextView txtMain = (TextView) layout.findViewById(R.id.main_text);
            TextView txtSub = (TextView) layout.findViewById(R.id.sub_text);
            txtMain.setText(mainText);
            txtMain.setTextColor(colorText);
            if(subText == null || subText.isEmpty()){
                txtSub.setVisibility(View.GONE);
            }else {
                txtSub.setVisibility(View.VISIBLE);
                txtSub.setTextColor(colorText);
                txtSub.setText(subText);
            }
        }

        private TextView getHeaderText(String text, Drawable drawable){
            TextView header = (TextView) inflater.inflate(R.layout.options_list_header, null, false);
            header.setText((drawable != null ? "  " : "") + text);
            header.setTypeface(litSans);
            header.setTextColor(colorText);
            if(colorBackgroundHeader != null) {
                header.setBackgroundColor(colorBackgroundHeader);
            }
            if(drawable != null)
                GraphicUtils.textviewDrawableLeft(header, drawable, colorText, minRowHeight, padding);
            return header;
        }

        public Builder addUnlockAllLevels() {
            master.addView(getOptionButton(
                    "Unlock All Levels",
                    "This is a debug-only developer option",
                    "Unlock",
                    activity.getResources().getColor(R.color.charcoal),
                    activity.getResources().getColor(R.color.white),
                    null, new Runnable() {
                        @Override
                        public void run() {
                            ApplicationManager app = Util.getApplicationManager(activity);
                            for(int i = 1; i <= app.getLevelFactory().getTotalLevelCount(); i++) {
                                app.getLevelFactory().unlockLevel(i);
                            }
                        }
                    }
            ));
            return this;
        }

        public Builder addGameplayHelpDialogsEnabled() {
            Boolean enabled = Boolean.parseBoolean(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.help_dialogs_enabled, "true"));
            master.addView(getOptionSwitch(
                    activity.getString(R.string.help_dialogs_enabled),
                    activity.getString(R.string.help_dialogs_enabled_sub_text),
                    enabled,
                    new String[]{activity.getString(R.string.disabled), activity.getString(R.string.enabled)},
                    new iTask() {
                        @Override
                        public void Task() {
                            boolean isChecked = ((boolean)this.getTaskData()) == true;
                            applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.help_dialogs_enabled, isChecked ? "true" : "false");
                        }
                        @Override
                        public void OnTaskCompleted() {

                        }
                    }));
            return this;
        }

        //endregion

    }

}
