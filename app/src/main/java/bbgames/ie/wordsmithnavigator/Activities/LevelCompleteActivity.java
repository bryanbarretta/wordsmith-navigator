package bbgames.ie.wordsmithnavigator.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.BaseActivities.BaseSubMenuActivity;
import bbgames.ie.wordsmithnavigator.Constants.GameModes;
import bbgames.ie.wordsmithnavigator.Controls.TextViewKeyValues;
import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelFactory;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelPlayerData;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelStaticData;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.Constants.IntentKeys;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.UserLevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class LevelCompleteActivity extends BaseSubMenuActivity {

    private static final int MS_ANIMATE_DELAY = 350;
    private static final int PROGRESS_BAR_SCALE_MULTIPLIER = 100;

    private SessionStatistics sessionStatistics;
    private LevelFactory levelFactory;
    private int levelId;
    private GameModes gameMode;

    private Typeface tfFun, tfSerious;
    private int colorText, colorTextDarker;
    private int fontSize;

    private ArrayList<View> viewHolders;
    private boolean animationStarted = false;
    private Runnable runnableAnimateProgressBar;

    private ViewGroup.LayoutParams pFillWidth = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewHolders = new ArrayList<>();
        sessionStatistics = (SessionStatistics) getIntent().getExtras().getSerializable(IntentKeys.SESSION_STATISTICS.name());
        levelId = getIntent().getExtras().getInt(IntentKeys.LEVEL_ID.name());
        gameMode = GameModes.values()[getIntent().getIntExtra(IntentKeys.GAME_MODE_ORDINAL.name(), 0)];
        levelFactory = Util.getApplicationManager(this).getLevelFactory();

        boolean levelPassed = true; //TODO: False if cpu wins
        if(levelPassed) {
            levelFactory.unlockLevel(levelId + 1);
        }
        setup();
        for(View v : viewHolders){
            v.setVisibility(View.INVISIBLE);
        }
        if(levelPassed) {
            updateLevelPlayerData();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!animationStarted){
            animate();
            animationStarted = true;
        }
    }

    private void animate(){
        Handler handler = new Handler();
        int[] msDelays = new int[viewHolders.size() + 1];
        msDelays[0] = MS_ANIMATE_DELAY;
        for(int i = 1; i < msDelays.length; i++){
            msDelays[i] = msDelays[i - 1] + MS_ANIMATE_DELAY;
            if(i == 5 || i == 10){
                msDelays[i] = msDelays[i] + (MS_ANIMATE_DELAY * 2);
            }
        }
        for(int i = 0; i < viewHolders.size(); i++){
            int finalI = i;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolders.get(finalI).setVisibility(View.VISIBLE);
                }
            }, msDelays[i]);
        }
        handler.postDelayed(runnableAnimateProgressBar, msDelays[msDelays.length - 1]);
    }

    private void updateLevelPlayerData(){
        levelFactory.getLevelPlayerData(levelId).setCompleted(true);
        levelFactory.getLevelPlayerData(levelId).updateStars(levelFactory.getLevelStaticData(levelId), sessionStatistics);
        levelFactory.getLevelPlayerData(levelId).updateDiamonds(sessionStatistics);
        levelFactory.getLevelPlayerData(levelId).save(this, levelId);
        levelFactory.refreshLevelPlayerData(Util.getApplicationManager(this).getDatabaseGateway(), levelId);
    }

    private void setup(){
        tfFun = Fonts.defaultFun(this);
        tfSerious = Fonts.defaultSerious(this);
        colorText = getResources().getColor(R.color.tertiary_brighter);
        colorTextDarker = getResources().getColor(R.color.blue_gray_3);
        fontSize = Util.getDimensValueInScaledDensityPixels(this, R.dimen.font_normal);

        txtHeader.setText(getString(R.string.level_complete));
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                switch (gameMode){
                    case SINGLE_PLAYER:
                        if(levelId < Util.getApplicationManager(LevelCompleteActivity.this).getLevelFactory().getTotalLevelCount()){
                            Util.getApplicationManager(LevelCompleteActivity.this)
                                    .startLevelActivity(LevelCompleteActivity.this, levelId + 1);
                        }else {
                            LevelCompleteActivity.this.finish();
                        }
                        break;
                    default:
                        break;
                }
            }
        });

        btnReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.getApplicationManager(LevelCompleteActivity.this)
                        .startLevelActivity(LevelCompleteActivity.this, levelId);
            }
        });

        int p = Util.getDimensValueInPixels(this, R.dimen.padding_2dp_border);
        container.setPadding(p, p, p, p);
        container.setGravity(RelativeLayout.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.fragment_level_complete, null);

        LinearLayout left = (LinearLayout) view.findViewById(R.id.llLeft);
        LinearLayout right = (LinearLayout) view.findViewById(R.id.llRight);
        LinearLayout middle = (LinearLayout) view.findViewById(R.id.llMiddle);
        LinearLayout bottom = (LinearLayout) view.findViewById(R.id.llBottom);

        addLeftPane(left);
        addRightPane(right);
        addMiddlePane(middle);
        addBottomPane(bottom);

        RelativeLayout.LayoutParams pMaster = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        pMaster.addRule(RelativeLayout.CENTER_VERTICAL);
        container.addView(view, pMaster);
    }

    @Override
    public boolean showBackButton() {
        return true;
    }

    @Override
    public boolean showReplayButton() {
        return true;
    }

    @Override
    public boolean showTitle() {
        return true;
    }

    @Override
    public boolean showNextButton() {
        return true;
    }

    @Override
    public boolean showStarCount() {
        return true;
    }

    private void addMiddlePane(LinearLayout middle) {
        //Score:
        middle.addView(generateStat(getString(R.string.score), sessionStatistics.getLevelScore(levelId) + "", false,
                Util.getDimensValueInScaledDensityPixels(this, R.dimen.font_big)), pFillWidth);
        for(int i = 0; i < middle.getChildCount(); i++){
            viewHolders.add(middle.getChildAt(i));
        }
    }

    private void addLeftPane(LinearLayout left){
        //Add content rows
        left.addView(generateStat("", getString(R.string.total), getString(R.string.avrg), colorTextDarker, true, null), pFillWidth);

        //Points
        left.addView(generateStat(getString(R.string.points),
                Util.toNDecimalPlaces(sessionStatistics.getTotalPoints(), 1),
                Util.toNDecimalPlaces(sessionStatistics.getAverageWordPoints(), 1), true), pFillWidth);
        //Letters Played
        left.addView(generateStat(getString(R.string.words),
                Util.toNDecimalPlaces(sessionStatistics.getSubmittedWords().size(), 1) + "",
                Util.toNDecimalPlaces(sessionStatistics.getAverageWordLength(), 1), true), pFillWidth);
        //Moves
        left.addView(generateStat(getString(R.string.moves),
                Util.toNDecimalPlaces(sessionStatistics.getSubmittedWords().size(), 1) + "", "", true), pFillWidth);
        //Seconds
        left.addView(generateStat(getString(R.string.time_s),
                sessionStatistics.getTotalSeconds() + "",
                Util.toNDecimalPlaces(sessionStatistics.getAverageSeconds(), 1), true), pFillWidth);

        for(int i = 0; i < left.getChildCount(); i++){
            viewHolders.add(left.getChildAt(i));
        }
    }

    private void addRightPane(LinearLayout right){
        generateStarsDetails(right);
        right.addView(generateStat(getString(R.string.longest_word), sessionStatistics.getLongestWord().toString(), false), pFillWidth);
        right.addView(generateStat(getString(R.string.best_word), sessionStatistics.getBestWord().toString(), false), pFillWidth);
        for(int i = 0; i < right.getChildCount(); i++){
            viewHolders.add(right.getChildAt(i));
        }
    }

    private void generateStarsDetails(LinearLayout right){
        LevelPlayerData lpd = Util.getApplicationManager(this).getLevelFactory().getLevelPlayerData(levelId);
        LevelStaticData lsd = Util.getApplicationManager(this).getLevelFactory().getLevelStaticData(levelId);
        int px = Util.getDimensValueInPixels(this, R.dimen.icon_24);
        int pd = Util.getDimensValueInPixels(this, R.dimen.padding_2dp_border);
        boolean limitReached = false; //Will prevent further stars from being unlocked if a star isn't unlocked
        for (int i = 0; i < lpd.getMapIdStarUnlocked().size(); i++){
            boolean previouslyUnlocked = lpd.getMapIdStarUnlocked().get(i);
            boolean newlyUnlocked = !limitReached && lsd.getStar(i).isCriteriaMet(sessionStatistics);
            LinearLayout ll = new LinearLayout(this);
            ll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            ll.setGravity(Gravity.CENTER_VERTICAL);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ImageView img;
            if(newlyUnlocked){
                img = GraphicUtils.createIcon(this, getResources().getDrawable(R.drawable.ic_star), px, px);
            }else if(previouslyUnlocked) {
                img = GraphicUtils.createIcon(this, getResources().getDrawable(R.drawable.ic_star_previous), px, px);
            }else {
                img = GraphicUtils.createIcon(this, getResources().getDrawable(R.drawable.ic_star_hollow), px, px);
            }
            ll.addView(img);
            View v = generateStat(lsd.getStar(i).getString(this) + (i == 0 ? "" : " " + getString(R.string.and_above)),
                    null, null, (previouslyUnlocked || newlyUnlocked) ? colorText : colorTextDarker, false, null);
            v.setPadding(pd, 0, 0, 0);
            ll.addView(v);
            ll.setPadding(0, 0, 0, pd);
            right.addView(ll);
            if(!previouslyUnlocked && newlyUnlocked){
                //TODO: Animation explosion
            }
            if(!limitReached){
                //If this star isn't unlocked this session, set limitReached to true so further stars can't be unlocked
                limitReached = !newlyUnlocked;
            }
        }
    }

    private void addBottomPane(LinearLayout bottom) {
        int levelScore = (int)Math.round(Double.parseDouble(sessionStatistics.getLevelScore(levelId)));
        int userTotalPoints = UserLevelUtils.getCurrentUserTotalPoints(applicationManager);
        int userNewTotalPoints = userTotalPoints + levelScore;
        ArrayList<UserLevelUtils.LevelUpData> luds = UserLevelUtils.getLevelUpDataForProgressBar(userTotalPoints, userNewTotalPoints);

        if(luds.isEmpty()){
            return;
        }

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp1.weight = 1;
        lp2.weight = 2;
        lp3.weight = 1;

        TextView txtCurrLvSkillName = generateTextView("-", colorText, tfFun);
        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
        TextView txtNextLevelPoints = generateTextView("0 / 100", getResources().getColor(R.color.white), tfFun);

        txtCurrLvSkillName.setLayoutParams(lp1);
        txtCurrLvSkillName.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        txtCurrLvSkillName.setPadding(0, 0, Util.getDimensValueInPixels(this, R.dimen.padding_4dp_border), 0);
        progressBar.setLayoutParams(lp2);
        txtNextLevelPoints.setLayoutParams(lp3);
        txtNextLevelPoints.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        txtNextLevelPoints.setPadding(Util.getDimensValueInPixels(this, R.dimen.padding_4dp_border), 0, 0, 0);

        bottom.addView(txtCurrLvSkillName);
        bottom.addView(progressBar);
        bottom.addView(txtNextLevelPoints);

        loadProgressBar(luds.get(0), progressBar, txtCurrLvSkillName, txtNextLevelPoints);

        runnableAnimateProgressBar = new Runnable() {
            @Override
            public void run() {
                animateProgressBar(luds, 0, progressBar, txtCurrLvSkillName, txtNextLevelPoints);
            }
        };

        UserLevelUtils.setCurrentUserTotalPoints(applicationManager, userNewTotalPoints);
    }

    private void animateProgressBar(ArrayList<UserLevelUtils.LevelUpData> levelUpDataArray, int index, ProgressBar progressBar, TextView txtCurrLvSkillName, TextView txtNextLevelPoints){
        UserLevelUtils.LevelUpData l = levelUpDataArray.get(index);
        loadProgressBar(l, progressBar, txtCurrLvSkillName, txtNextLevelPoints);
        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(progressBar, "progress",
                l.getCurrAdjusted() * PROGRESS_BAR_SCALE_MULTIPLIER, l.getTargetAdjusted() * PROGRESS_BAR_SCALE_MULTIPLIER);
        objectAnimator.setDuration(l.getDurationMs());
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if((index + 1) < levelUpDataArray.size()){
                    Toast.makeText(LevelCompleteActivity.this, String.format(getString(R.string.level_up), (l.getToLevel() + 1) + ""), Toast.LENGTH_SHORT).show();
                    animateProgressBar(levelUpDataArray, index + 1, progressBar, txtCurrLvSkillName, txtNextLevelPoints);
                }else {
                    //Finished leveling up
                }
            }
        });
        objectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                txtNextLevelPoints.setText(getNextLevelPointsText(
                        (Integer.parseInt(valueAnimator.getAnimatedValue()+"") / PROGRESS_BAR_SCALE_MULTIPLIER),
                        l.getNextLevelPointsAdjusted()));
            }
        });
        objectAnimator.start();
    }

    private void loadProgressBar(UserLevelUtils.LevelUpData levelUpData, ProgressBar progressBar, TextView txtCurrLvSkillName, TextView txtNextLevelPoints){
        progressBar.setMax(levelUpData.getMaxAdjusted() * PROGRESS_BAR_SCALE_MULTIPLIER);
        progressBar.setProgress(levelUpData.getCurrAdjusted() * PROGRESS_BAR_SCALE_MULTIPLIER);
        txtCurrLvSkillName.setText(UserLevelUtils.getUserSkillName(this, levelUpData.getFromLevel()));
        txtNextLevelPoints.setText(getNextLevelPointsText(levelUpData.getCurrAdjusted(), levelUpData.getNextLevelPointsAdjusted()));
    }

    private String getNextLevelPointsText(int pCurr, int pMax){
        return String.format("%s / %s", pCurr+"", pMax+"");
    }

    private LinearLayout generateStat(String key, String value, boolean highlightValue1) {
        return generateStat(key, value, null, highlightValue1);
    }

    private LinearLayout generateStat(String key, String value, boolean highlightValue1, float fontSizes) {
        return generateStat(key, value, null, colorText, highlightValue1, fontSizes);
    }

    private LinearLayout generateStat(String key, String value1, String value2, boolean highlightValue1) {
        return generateStat(key, value1, value2, colorText, highlightValue1, null);
    }

    private LinearLayout generateStat(String key, String value1, String value2, int colorOfText, boolean highlightValue1, Float fontSizes) {
        TextViewKeyValues.Builder b = new TextViewKeyValues.Builder(this);
        b.key().text(key).color(colorOfText).font(tfFun).additionalTypefaceStyle(Typeface.NORMAL);
        b.value1().text(value1).color(colorOfText).bgColor(highlightValue1 ? getResources().getColor(R.color.primary_darker) : null).font(tfFun).additionalTypefaceStyle(Typeface.BOLD);
        b.value2().text(value2).color(colorOfText).font(tfFun).additionalTypefaceStyle(Typeface.NORMAL);
        if(fontSizes != null){
            b.key().fontSize(fontSizes);
            b.value1().fontSize(fontSizes);
            b.value2().fontSize(fontSizes);
        }
        return b.build();
    }

    private TextView generateTextView(String text, int colorOfText, Typeface typeface){
        TextView txtValue2 = new TextView(this);
        txtValue2.setText(text);
        txtValue2.setTypeface(typeface);
        txtValue2.setTextColor(colorOfText);
        txtValue2.setTextSize(fontSize);
        return txtValue2;
    }

}
