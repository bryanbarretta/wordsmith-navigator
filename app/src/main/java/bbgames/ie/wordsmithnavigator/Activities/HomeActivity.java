package bbgames.ie.wordsmithnavigator.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import bbgames.ie.wordsmithnavigator.Activities.BaseActivities.BaseActivity;
import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.AsyncTask.iTask;
import bbgames.ie.wordsmithnavigator.Constants.GameModes;
import bbgames.ie.wordsmithnavigator.Constants.IntentKeys;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinMap;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogQuit;
import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class HomeActivity extends BaseActivity {

    private ApplicationManager applicationManager;
    int wButtonSmall, wButton, colorImages;

    private Button btnPlayCampaign, btnPlayOnline;
    private ImageButton btnBack, btnSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        applicationManager = (ApplicationManager)getApplication();
        initialize();
        setup();
    }

    /**
     * Initialize class variables. Called before setup()
     */
    private void initialize(){
        wButton      = applicationManager.getScreenHeightPixels() / 2;
        wButtonSmall = applicationManager.getScreenHeightPixels() / 4;
        colorImages  = getResources().getColor(R.color.tertiary);

        btnPlayCampaign = (Button) findViewById(R.id.btnPlay);
        btnPlayOnline = (Button) findViewById(R.id.btnOnline);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnSettings = (ImageButton) findViewById(R.id.btnSettings);

        Typeface fontLitSans = Fonts.defaultFun(this);
        btnPlayCampaign.setTypeface(fontLitSans);
        btnPlayOnline.setTypeface(fontLitSans);

        btnPlayCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(HomeActivity.this, CampaignActivity.class));
                startActivity(new Intent(HomeActivity.this, LevelSelectActivity.class));
            }
        });
        btnPlayOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, RepeatingSpriteGameActivity.class);
                startActivity(i);
                //startActivity(new Intent(HomeActivity.this, LobbyActivity.class));
            }
        });
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, OptionsActivity.class));
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogQuit(HomeActivity.this).show();
            }
        });
    }

    /**
     * Set buttons properties: color, size etc.
     * Also loads the dictionary if empty
     */
    private void setup(){
        ViewGroup.LayoutParams params;

        GraphicUtils.scalePadViewContent(btnBack, wButtonSmall);
        GraphicUtils.scalePadViewContent(btnSettings, wButtonSmall);

        params = btnPlayCampaign.getLayoutParams();
        params.height = wButton;
        params.width = wButton;
        btnPlayCampaign.setLayoutParams(params);

        params = btnPlayOnline.getLayoutParams();
        params.height = wButton;
        params.width = wButton;
        btnPlayOnline.setLayoutParams(params);

        GraphicUtils.buttonRoundWithDrawableTop(btnPlayCampaign,
                getResources().getDrawable(R.drawable.ic_campaign),
                getResources().getColor(R.color.tertiary),
                null,
                null);
        GraphicUtils.buttonRoundWithDrawableTop(btnPlayOnline,
                getResources().getDrawable(R.drawable.ic_online),
                getResources().getColor(R.color.tertiary),
                null,
                null);

        applicationManager.loadDictionary(this, false);
        applicationManager.getDatabaseGateway().initialiseSkins();
        applicationManager.getLevelFactory().unlockLevel(1);
    }

    /**
     * TODO: not used...
     */
    private void populateUserProfile(){
        if(applicationManager.isNetworkAvailable()){
            //final UserLogin userLogin = applicationManager.getAuthenticationManager().loadUserLoginFromSharedPreferences();
            if(applicationManager.getGoogleAccountManager().isSignedIn()){
                applicationManager.getAsyncTaskHandler().AddTask(new iTask() {
                    @Override
                    public void Task() {
                        final Drawable d = applicationManager.getGoogleAccountManager().getPhoto();
                        HomeActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //imgHeaderUserPhoto.setImageDrawable(d);
                            }
                        });
                    }
                    @Override
                    public void OnTaskCompleted() { }
                });
                //txtHeaderUserName.setText(applicationManager.getGoogleAccountManager().getAccount().getDisplayName());
                //txtHeaderUserEmail.setText(applicationManager.getGoogleAccountManager().getAccount().getEmail());
            }else {
                //imgHeaderUserPhoto.setImageDrawable(getResources().getDrawable(R.drawable.ic_user));
                //txtHeaderUserName.setText(getString(R.string.tap_to_sign_in));
                //txtHeaderUserEmail.setText(getString(R.string.you_are_not_logged_in));
                //txtHeaderUserEmail.setOnClickListener(new View.OnClickListener() {
                //    @Override
                //    public void onClick(View v) {
                //        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                //    }
                //});
            }
        }else {
            //imgHeaderUserPhoto.setImageDrawable(getResources().getDrawable(R.drawable.ic_signal_wifi_off_white_48dp));
            //txtHeaderUserName.setText(getString(R.string.offline));
            //txtHeaderUserEmail.setText(getString(R.string.no_internet_connection));
        }
    }

}
