package bbgames.ie.wordsmithnavigator.Activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.widget.Toast;

import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.adt.color.Color;

import java.io.IOException;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.GameModes;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogLoading;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogManager;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogPauseGame;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogQuit;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuLogger;
import bbgames.ie.wordsmithnavigator.GameObjects.Camera.GameCamera;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.Misc.Rubbish;
import bbgames.ie.wordsmithnavigator.GameObjects.GameScene;
import bbgames.ie.wordsmithnavigator.Constants.IntentKeys;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Settings.Settings;

/**
 * The in-game activity, built on andEngine.
 * Holds the various manager and UI objects which handle most of the work. Very little logic in this class.
 * Diagram:     https://www.dropbox.com/s/yebqshhttgpfcg5/game_activity_flow.png?dl=0
 * draw.io xml: https://www.dropbox.com/s/rx6v9zc4zrb3js9/game_activity_flow.xml?dl=0
 * Created by Bryan on 07/05/2016.
 */
public class GameActivity extends SimpleBaseGameActivity {

    private int levelId;

    //Manager objects
    private ApplicationManager applicationManager;
    private GraphicsManager graphicsManager;
    private LevelManager levelManager;
    private PlayerManager playerManager;

    //UI objects
    private GameCamera camera;
    private GameScene currentScene;
    private GameHud hud;
    private boolean lockScreen;

    //UI Helper objects
    private Rubbish rubbish;
    private Vibrator vibrator;
    private Settings settings;
    private Toast mToast;
    private DialogPauseGame dialogPauseGame;
    private DialogLoading dialogLoading;

    //Level data
    private int themeHud;
    private int themeMap;
    private AIDifficulty difficulty;
    private GameModes gameMode;

    public enum FLAGS{   //All false by default
        LEVEL_COMPLETE,
        LEVEL_QUIT
    }
    private HashMap<FLAGS, Boolean> flags = new HashMap<>();

    //Logger
    private CpuLogger cpuLogicLogger;

    //region Override

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        dialogLoading = new DialogLoading(this, getString(R.string.setting_up_level));
        dialogLoading.show();
    }

    //#1
    @Override
    public EngineOptions onCreateEngineOptions() {
        applicationManager = (ApplicationManager)this.getApplicationContext();
        settings = new Settings(this);
        levelId = getIntent().getIntExtra(IntentKeys.LEVEL_ID.name(), 1);
        levelManager = new LevelManager(this);
        cpuLogicLogger = new CpuLogger(this);
        playerManager = new PlayerManager(this);
        camera = new GameCamera(this);
        themeHud = getIntent().getIntExtra(IntentKeys.THEME_HUD_ID.name(), 0);
        themeMap = getIntent().getIntExtra(IntentKeys.THEME_MAP_ID.name(), 0);
        gameMode = GameModes.SINGLE_PLAYER.values()[getIntent().getIntExtra(IntentKeys.GAME_MODE_ORDINAL.name(), 0)];
        difficulty = AIDifficulty.values()[getIntent().getIntExtra(IntentKeys.DIFFICULTY_ID.name(), AIDifficulty.NORMAL.ordinal())];
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
        engineOptions.getRenderOptions().setDithering(true);
        return engineOptions;
    }

    //#2
    @Override
    protected void onCreateResources() throws IOException {
        graphicsManager = new GraphicsManager(this, themeHud, themeMap);
        graphicsManager.initiate();
    }

    //#3
    @Override
    protected Scene onCreateScene() {
        mEngine.registerUpdateHandler(new FPSLogger());
        currentScene = new GameScene(this);
        int hudLetterCapacity = getIntent().getIntExtra(IntentKeys.HUD_LETTER_CAPACITY.name(), 10);
        hud = new GameHud(this, hudLetterCapacity);
        rubbish = new Rubbish();
        camera.setHUD(hud);
        graphicsManager.renderLevel(this);
        playerManager.initialisePlayers();
        hud.setState(GameHud.STATE.LOCKED, false, null);
        camera.setCenterDirect(playerManager.getActivePlayer().getListXYLetterTilesOnBoard().get(0));
        DialogManager.showLevelDialogs(this);
        dialogLoading.dismiss();
        return currentScene;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN) {
            if(mEngine.isRunning()){
                showOptions();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        vibrator.cancel();
    }

    @Override
    public void onBackPressed() {
        showOptions();
    }

    //endregion

    //region Getters

    public ApplicationManager getApplicationManager(){
        return applicationManager;
    }

    public GraphicsManager getGraphicsManager(){
        return graphicsManager;
    }

    public GameCamera getCamera(){
        return camera;
    }

    public LevelManager getLevelManager(){
        return levelManager;
    }

    public Rubbish getRubbish(){
        return rubbish;
    }

    public GameHud getHUD(){
        return hud;
    }

    public GameScene getScene(){
        return currentScene;
    }

    public Vibrator getVibrator(){
        return vibrator;
    }

    public Settings getSettings() {
        return settings;
    }

    public PlayerManager getPlayerManager(){
        return playerManager;
    }

    public AIDifficulty getDifficulty(){
        return difficulty;
    }

    public GameModes getGameMode() {
        return gameMode;
    }

    public CpuLogger getCpuLogicLogger(){
        return cpuLogicLogger;
    }

    //endregion

    public void showToast(final String text){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mToast == null){
                    mToast = Toast.makeText(GameActivity.this, "", Toast.LENGTH_SHORT);
                }
                mToast.setText(text);
                mToast.show();
            }
        });
    }

    public void showOptions(){
        this.runOnUiThread(new Runnable(){
            @Override
            public void run() {
                dialogPauseGame = new DialogPauseGame(GameActivity.this);
                dialogPauseGame.show();
            }
        });
    }

    public DialogPauseGame getDialogPauseGame(){
        return dialogPauseGame;
    }

    public void showQuitGameDialog(Runnable onNoSelectedRunnable) {
        new DialogQuit(this, onNoSelectedRunnable).show();
    }

    public void cleanupScreen() {
        this.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                for(Sprite s : rubbish.getSpritesToBeRemoved())
                {
                    currentScene.detachChild(s);
                }
                for(Text t : rubbish.getTextsToBeRemoved())
                {
                    hud.detachChild(t);
                }
                rubbish.getSpritesToBeRemoved().clear();
                rubbish.getTextsToBeRemoved().clear();
            }
        });
    }

    public void setFlag(FLAGS flag, boolean status){
        flags.put(flag, status);
    }

    public boolean getFlag(FLAGS flag){
        if(flags.containsKey(flag)){
            return flags.get(flag);
        }
        return false;
    }

    public int getLevelId() {
        return levelId;
    }

    Rectangle block;

    public void lockScreen() {
        this.lockScreen = true;
        if(block == null) {
            block = new Rectangle(Constant.CAMERA_1920 / 2, Constant.CAMERA_1080 / 2,
                    Constant.CAMERA_1920, Constant.CAMERA_1080, getVertexBufferObjectManager()) {
                @Override
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    return true;
                }
            };
            block.setColor(Color.TRANSPARENT);
        }
        getHUD().registerTouchArea(block);
        getHUD().attachChild(block);
    }

    public void unlockScreen() {
        this.lockScreen = false;
        if(block != null){
            getHUD().unregisterTouchArea(block);
            getHUD().detachChild(block);
        }
    }

    public boolean isScreenLocked(){
        return lockScreen;
    }
}
