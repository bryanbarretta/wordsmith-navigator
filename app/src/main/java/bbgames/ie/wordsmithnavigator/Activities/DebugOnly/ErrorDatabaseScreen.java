package bbgames.ie.wordsmithnavigator.Activities.DebugOnly;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Database.TableErrors;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class ErrorDatabaseScreen extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_database_screen);
        setup();
    }

    private void setup(){
        final ApplicationManager applicationManager = (ApplicationManager)getApplication();

        final ListView listErrors = (ListView) findViewById(R.id.tableErrors);
        Button btnClear = (Button) findViewById(R.id.btnClear);
        Button btnExit = (Button) findViewById(R.id.btnExit);

        btnClear.getBackground().setColorFilter(getResources().getColor(R.color.red_400), PorterDuff.Mode.MULTIPLY);
        btnClear.setTextColor(getResources().getColor(R.color.white));
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applicationManager.getDatabaseGateway().clearErrorsData();
                listErrors.setAdapter(null);
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listErrors.setAdapter(new ErrorListViewAdapter(this, 0, applicationManager.getDatabaseGateway().getExceptions()));
    }

    private class ErrorListViewAdapter extends ArrayAdapter<TableErrors.ExceptionInstance> {

        public ErrorListViewAdapter(Context context, int resource) {
            super(context, resource);
        }

        public ErrorListViewAdapter(Context context, int resource, List<TableErrors.ExceptionInstance> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            LinearLayout row = new LinearLayout(getContext());
            row.setWeightSum(10);
            row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            final TableErrors.ExceptionInstance e = getItem(position);

            TextView txtTimeStamp = new TextView(getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.weight = 3;
            txtTimeStamp.setLayoutParams(lp);
            txtTimeStamp.setText(e.getTimestamp().toString());
            txtTimeStamp.setTextColor(getContext().getResources().getColor(R.color.white));

            TextView txtMessage = new TextView(getContext());
            lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.weight = 6;
            txtMessage.setLayoutParams(lp);
            txtMessage.setText(e.getMessage());
            txtMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Util.showOkDialog(ErrorDatabaseScreen.this, e.getMessageFull());
                }
            });
            txtMessage.setTextColor(getContext().getResources().getColor(R.color.white));

            TextView txtCount = new TextView(getContext());
            lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.weight = 1;
            txtCount.setLayoutParams(lp);
            txtCount.setText(e.getCount()+"");
            txtCount.setTextColor(getContext().getResources().getColor(R.color.red_100));

            row.addView(txtTimeStamp);
            row.addView(txtCount);
            row.addView(txtMessage);
            row.setPadding(10, 10, 10, 10);
            return row;
        }
    }

}
