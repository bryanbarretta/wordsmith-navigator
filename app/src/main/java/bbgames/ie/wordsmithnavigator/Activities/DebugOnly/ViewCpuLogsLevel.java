package bbgames.ie.wordsmithnavigator.Activities.DebugOnly;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import bbgames.ie.wordsmithnavigator.Controls.AdapterListViewCpuLogLevel;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuGameLogs;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class ViewCpuLogsLevel extends Activity {

    public static final String INTENT_EXTRA_LEVEL = "INTENT_EXTRA_LEVEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cpu_logs_level);
        String level = getIntent().getStringExtra(INTENT_EXTRA_LEVEL);
        final CpuGameLogs cpuGameLogs = Util.getApplicationManager(this).getDatabaseGateway().getCpuGameLogs(level);
        final ExpandableListView lv = (ExpandableListView)findViewById(R.id.lvLevel);
        lv.setAdapter(new AdapterListViewCpuLogLevel(this, cpuGameLogs) {
            @Override
            public void onExpandCollapsedPressed(int groupPosition, boolean isExpanded) {
                if(isExpanded){
                    lv.collapseGroup(groupPosition);
                }else {
                    lv.expandGroup(groupPosition);
                }
            }
            @Override
            public boolean isChildSelectable(int groupPosition, int childPosition) {
                return false;
            }
        });
    }
}
