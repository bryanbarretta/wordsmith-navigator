package bbgames.ie.wordsmithnavigator.Database;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Constants.LoggingModes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuGameLogs;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuTurnLog;

/**
 * Created by Bryan on 06/04/2017.
 */

public class TableCPULogicLogs {

    public static final String TABLE_NAME = "CPULogicLogs";
    public static final String COL_GAME_NAME = "GameName";
    public static final String COL_TURN = "Turn";
    public static final String COL_LOG_TYPE = "LogType";
    public static final String COL_VALUE = "Value";

    /**
     * @return HashMap of CPULogicLogs to their respective GameName's
     */
    public static HashMap<String, CpuGameLogs> getCPULogicLogs(Cursor c){
        HashMap<String, CpuGameLogs> gameLogs = new HashMap<>();
        for (int i = 0; i < c.getCount(); i++) {
            if(!c.moveToPosition(i)){
                break;
            }
            String gameName = c.getString(c.getColumnIndex(COL_GAME_NAME));
            if(!gameLogs.containsKey(gameName)){
                ArrayList<LoggingModes> lm = new ArrayList<>();
                lm.add(LoggingModes.DATABASE);
                gameLogs.put(gameName, new CpuGameLogs(gameName, false));
            }
            CpuGameLogs gl = gameLogs.get(gameName);
            int turn = c.getInt(c.getColumnIndex(COL_TURN));
            CpuTurnLog.LogType logType = CpuTurnLog.LogType.get(c.getString(c.getColumnIndex(COL_LOG_TYPE)));
            String value = c.getString(c.getColumnIndex(COL_VALUE));
            gl.loadIntoTurnLogs(turn, logType, value);
            gameLogs.put(gameName, gl);
        }
        c.close();
        return gameLogs;
    }

    public static ArrayList<String> getCPUGameNames(Cursor c){
        ArrayList<String> output = new ArrayList<String>();
        for (int i = 0; i < c.getCount(); i++) {
            if(!c.moveToPosition(i)){
                break;
            }
            output.add(c.getString(c.getColumnIndex(COL_GAME_NAME)));

        }
        c.close();
        return output;
    }

}
