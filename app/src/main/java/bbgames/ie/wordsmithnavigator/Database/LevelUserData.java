package bbgames.ie.wordsmithnavigator.Database;

/**
 * Created by Bryan on 24/07/2016.
 */
public class LevelUserData{

    private int levelId;
    private String userData;

    public LevelUserData(int levelId, String userData){
        this.levelId = levelId;
        this.userData = userData;
    }

    public int getLevelId() {
        return levelId;
    }

    public String getUserData() {
        return userData;
    }
}