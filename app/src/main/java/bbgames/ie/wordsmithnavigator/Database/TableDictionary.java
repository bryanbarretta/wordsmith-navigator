package bbgames.ie.wordsmithnavigator.Database;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;

/**
 * Created by Bryan on 01/04/2017.
 */

public class TableDictionary {

    public static final String TABLE_NAME = "Dictionary";
    public static final String COL_WORD = "Word";
    public static final String COL_FREQUENCY = "Frequency";
    public static final String COL_DISPLAY_WORD = "DisplayWord";

    public static void bulkInsert(SQLiteDatabase database, ArrayList<DictionaryEntry> entries){
        if(entries == null || entries.isEmpty()){
            return;
        }
        String sql = "INSERT OR REPLACE INTO " + TABLE_NAME + "(" +
                COL_WORD + ", " +
                COL_DISPLAY_WORD + ", " +
                COL_FREQUENCY +
                ") VALUES (?, ?, ?)";
        SQLiteStatement insert = database.compileStatement(sql);
        for (DictionaryEntry entry : entries){
            insert.bindString(1, entry.getWord());
            if(entry.getDisplayWord() == null){
                insert.bindNull(2);
            }else {
                insert.bindString(2, entry.getDisplayWord());
            }
            insert.bindDouble(3, entry.getFrequency());
            insert.execute();
        }
    }

    public static boolean isDictionaryEmpty(SQLiteDatabase database){
        return DatabaseUtils.queryNumEntries(database, TABLE_NAME) < 1000;
    }

    public static boolean isWordExists(SQLiteDatabase database, String word){
        return DatabaseUtils.queryNumEntries(database, TABLE_NAME, COL_WORD + "=?", new String[] {word.toLowerCase()}) > 0;
    }

    public static ArrayList<DictionaryEntry> getDictionaryEntries(Cursor c){
        ArrayList<DictionaryEntry> entries = new ArrayList<DictionaryEntry>();
        for (int i = 0; i < c.getCount(); i++) {
            if(!c.moveToPosition(i)){
                break;
            }
            String word = c.getString(c.getColumnIndex(COL_WORD));
            String displayWord = c.isNull(c.getColumnIndex(COL_DISPLAY_WORD)) ? null : c.getString(c.getColumnIndex(COL_DISPLAY_WORD));
            double frequency = c.getDouble(c.getColumnIndex(COL_FREQUENCY));
            entries.add(new DictionaryEntry(word, displayWord, frequency));
        }
        c.close();
        return entries;
    }
}
