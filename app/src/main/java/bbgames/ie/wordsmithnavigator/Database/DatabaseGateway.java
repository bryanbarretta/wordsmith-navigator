package bbgames.ie.wordsmithnavigator.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuGameLogs;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuTurnLog;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Word.HintBoardTile;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelPlayerData;
import bbgames.ie.wordsmithnavigator.Util.Util;
import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;

/**
 * Created by Bryan on 24/07/2016.
 */
public class DatabaseGateway implements BaseColumns {

    protected final SQLiteDatabase database;
    private Context context;
    private HashMap<Double, ArrayList<DictionaryEntry>> preLoadedDictionaryEntries = new HashMap<>();

    public DatabaseGateway(Context context) {
        database = new SQLDatabase(context).getWritableDatabase();
        this.context = context;
    }

    public Context getContext(){
        return context;
    }

    public long addUpdateLevelPlayerData(int id, LevelPlayerData data){
        ContentValues values = new ContentValues();
        values.put(TableLevels.COL_LEVEL_ID, id);
        values.put(TableLevels.COL_USER_DATA, data.toJSONObject().toString());
        long idx = -1;
        database.beginTransaction();
        try {
            idx = database.insertWithOnConflict(TableLevels.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            database.setTransactionSuccessful();
        } catch (Exception e){
            throw e;
        } finally {
            database.endTransaction();
        }
        return idx;
    }

    public long addUpdateLevelPlayerData(int levelId, String levelUserData){
        ContentValues values = new ContentValues();
        values.put(TableLevels.COL_LEVEL_ID, levelId);
        values.put(TableLevels.COL_USER_DATA, levelUserData);
        long id = -1;
        database.beginTransaction();
        try {
            id = database.insertWithOnConflict(TableLevels.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            database.setTransactionSuccessful();
        } catch (Exception e){
            throw e;
        } finally {
            database.endTransaction();
        }
        return id;
    }

    public void addUpdateDictionaryEntries(ArrayList<DictionaryEntry> entries){
        if(entries == null || entries.isEmpty()){
            return;
        }
        database.beginTransaction();
        try {
            TableDictionary.bulkInsert(database, entries);
            database.setTransactionSuccessful();
        } catch (Exception e){
            Util.throwException(this, e);
        } finally {
            database.endTransaction();
        }
    }

    public void addUpdateException(Exception e){
        int existingCount = getExceptionCount(e);
        ContentValues c = new ContentValues();
        c.put(TableErrors.COL_LAST_TIMESTAMP, Util.getUTCNow().toString());
        c.put(TableErrors.COL_MESSAGE, e.getMessage());
        c.put(TableErrors.COL_MESSAGE_FULL, e.toString());
        c.put(TableErrors.COL_COUNT, existingCount + 1);
        database.beginTransaction();
        try {
            if(existingCount == 0){
                database.insert(TableErrors.TABLE_NAME, null, c);
            }else {
                database.update(TableErrors.TABLE_NAME, c, TableErrors.COL_MESSAGE + "=? AND " + TableErrors.COL_MESSAGE_FULL + "=?",
                        new String[] {e.getMessage(), e.toString()});
            }
            database.setTransactionSuccessful();
        } catch (Exception ex){
            Util.throwException(this, ex);
        } finally {
            database.endTransaction();
        }
    }

    public boolean isDictionaryEmtpy(){
        boolean res = true;
        database.beginTransaction();
        try {
            res = TableDictionary.isDictionaryEmpty(database);
            database.setTransactionSuccessful();
        } catch (Exception e){
            Util.throwException(this, e);
        } finally {
            database.endTransaction();
        }
        return res;
    }

    public boolean isWordExists(String word){
        boolean res = true;
        database.beginTransaction();
        try {
            res = TableDictionary.isWordExists(database, word);
            database.setTransactionSuccessful();
        } catch (Exception e){
            Util.throwException(this, e);
        } finally {
            database.endTransaction();
        }
        return res;
    }

    public ArrayList<DictionaryEntry> getDictionaryEntries(double minFrequency, Integer minWordLength, Integer maxWordLength){
        if(!preLoadedDictionaryEntries.containsKey(minFrequency)){
            //Ensure this heavy operation (i.e. loading all of the dictionary entries for a minFrequency) is done only once
            preLoadedDictionaryEntries.put(minFrequency, getDictionaryEntries(minFrequency, minWordLength, maxWordLength, TableDictionary.COL_FREQUENCY + " DESC"));
        }
        return preLoadedDictionaryEntries.get(minFrequency);
    }

    public ArrayList<DictionaryEntry> getDictionaryEntries(String rawSQL){
        return TableDictionary.getDictionaryEntries(database.rawQuery(rawSQL, null));
    }

    public ArrayList<DictionaryEntry> getDictionaryEntries(double minFrequency, Integer minWordLength, Integer maxWordLength, String orderBy){
        return TableDictionary.getDictionaryEntries(database.query(TableDictionary.TABLE_NAME, null,
                TableDictionary.COL_FREQUENCY + ">=" + minFrequency + " AND " +
                        "length(" + TableDictionary.COL_WORD + ") BETWEEN " + minWordLength + " AND " + maxWordLength, null, null, null, orderBy));
    }

    public ArrayList<DictionaryEntry> getDictionaryEntriesStartingWith(String prefix, double minFrequency, Integer minWordLength, Integer maxWordLength){
        return getDictionaryEntriesStartingWith(prefix, minFrequency, minWordLength, maxWordLength, TableDictionary.COL_FREQUENCY + " DESC");
    }

    public ArrayList<DictionaryEntry> getDictionaryEntriesStartingWith(String prefix, double minFrequency, Integer minWordLength, Integer maxWordLength, String orderBy){
        return TableDictionary.getDictionaryEntries(database.query(TableDictionary.TABLE_NAME, null,
                TableDictionary.COL_WORD + " LIKE '" + prefix + "%' AND " +
                        TableDictionary.COL_FREQUENCY + ">=" + minFrequency + " AND " +
                        "length(" + TableDictionary.COL_WORD + ") BETWEEN " + minWordLength + " AND " + maxWordLength,
                null, null, null, orderBy));
    }

    public DictionaryEntry getDictionaryEntry(String exactWord){
        ArrayList<DictionaryEntry> ret = TableDictionary.getDictionaryEntries(database.query(TableDictionary.TABLE_NAME, null,
                TableDictionary.COL_WORD + " = '" + exactWord + "'",
                null, null, null, null));
        if(ret == null || ret.isEmpty()){
            return null;
        }
        return ret.get(0);
    }

    public ArrayList<LevelUserData> getUserLevelData() {
        ArrayList<LevelUserData> data = new ArrayList<>();
        database.beginTransaction();
        try {
            data = TableLevels.GetLevelUserData(database);
            database.setTransactionSuccessful();
        } catch (Exception e){
            Util.throwException(this, e);
        } finally {
            database.endTransaction();
        }
        return data;
    }

    public LevelUserData getUserLevelData(int levelId) {
        LevelUserData data = null;
        database.beginTransaction();
        try {
            data = TableLevels.GetLevelUserData(database, levelId);
            database.setTransactionSuccessful();
        } catch (Exception e){
            Util.throwException(this, e);
        } finally {
            database.endTransaction();
        }
        return data;
    }

    public ArrayList<TableErrors.ExceptionInstance> getExceptions(){
        return TableErrors.getExceptions(database.query(TableErrors.TABLE_NAME, null, null, null, null, null, TableErrors.COL_COUNT));
    }

    public int getExceptionCount(Exception e){
        return (int) TableErrors.getExceptionCount(database, e);
    }

    public void clearErrorsData() {
        database.delete(TableErrors.TABLE_NAME, null, null);
    }

    public void clearDictionaryEntries() {
        database.delete(TableDictionary.TABLE_NAME, null, null);
    }

    public List<SkinLetter> getSkins(){
        List<SkinLetter> output = TableSkins.getSkins(database);
        if(output.isEmpty()){
            return TableSkins.addDefaultSkins(database, getContext());
        }else {
            return output;
        }
    }

    public void initialiseSkins(){
        if(getSkins().isEmpty()){
            TableSkins.addDefaultSkins(database, context);
        }
    }

    public SkinLetter getSkin(int id) {
        return TableSkins.getSkin(database, id);
    }

    public long addSkin(SkinLetter skin){
        return TableSkins.addSkin(database, skin);
    }

    //region CPULogicLogs

    public ArrayList<String> getCpuGameNames(){
        return TableCPULogicLogs.getCPUGameNames(database.query(
                true,
                TableCPULogicLogs.TABLE_NAME,
                new String[]{TableCPULogicLogs.COL_GAME_NAME},
                null,
                null,
                null,
                null,
                TableCPULogicLogs.COL_GAME_NAME,
                null));
    }

    public CpuGameLogs getCpuGameLogs(String gameName){
        HashMap<String, CpuGameLogs> m = TableCPULogicLogs.getCPULogicLogs(database.query(
                TableCPULogicLogs.TABLE_NAME,
                null,
                TableCPULogicLogs.COL_GAME_NAME + " LIKE ?",
                new String[]{gameName},
                null,
                null,
                TableCPULogicLogs.COL_TURN));
        return m.get(gameName);
    }

    public void clearCpuGameLogs(String level) {
        database.delete(TableCPULogicLogs.TABLE_NAME, TableCPULogicLogs.COL_GAME_NAME + " = ?", new String[]{level});
    }

    public void writeCpuGameLogs(CpuGameLogs gameLogs) {
        String sql = "INSERT OR REPLACE INTO " + TableCPULogicLogs.TABLE_NAME + "(" +
                TableCPULogicLogs.COL_GAME_NAME + ", " +
                TableCPULogicLogs.COL_TURN + ", " +
                TableCPULogicLogs.COL_LOG_TYPE + ", " +
                TableCPULogicLogs.COL_VALUE +
                ") VALUES (?, ?, ?, ?)";
        SQLiteStatement insert = database.compileStatement(sql);
        for (Map.Entry<Integer, CpuTurnLog> cpuTurnLog : gameLogs.getTurnLogs().entrySet()){
            int turnId = cpuTurnLog.getKey();
            for(Map.Entry<CpuTurnLog.LogType, String> log : cpuTurnLog.getValue().getMapFieldValues().entrySet()){
                insert.bindString(1, gameLogs.getGameName());
                insert.bindLong(2, turnId);
                insert.bindString(3, log.getKey().toString());
                insert.bindString(4, log.getValue());
                insert.execute();
            }
        }
    }

    public void writeCpuTurnLog(String gameName, int turn, CpuTurnLog.LogType logType, String value) {
        ContentValues c = new ContentValues();
        c.put(TableCPULogicLogs.COL_GAME_NAME, gameName);
        c.put(TableCPULogicLogs.COL_TURN, turn);
        c.put(TableCPULogicLogs.COL_LOG_TYPE, logType.toString());
        c.put(TableCPULogicLogs.COL_VALUE, value);
        database.beginTransaction();
        try {
            database.insertWithOnConflict(TableCPULogicLogs.TABLE_NAME, null, c, SQLiteDatabase.CONFLICT_REPLACE);
            database.setTransactionSuccessful();
        } catch (Exception ex){
            Util.throwException(this, ex);
        } finally {
            database.endTransaction();
        }
    }

    //endregion

    /**
     * Given a map of id-tiles, evaluate each potential letter/word pairing in each tile, and filter out the potential words
     * that are not in the dictionary.
     */
    public HashMap<Integer, HintBoardTile> filterOutInvalidWords(HashMap<Integer, HintBoardTile> input) {

        HashMap<Integer, HintBoardTile> output = new HashMap<>();

        //First, prepare the output
        for(Map.Entry<Integer, HintBoardTile> outer : input.entrySet()){
            output.put(outer.getKey(), outer.getValue());
        }

        //Now build the sql query
        StringBuilder sql = new StringBuilder();
        ArrayList<Integer> mandatoryIds = new ArrayList<>();

        for(Map.Entry<Integer, HintBoardTile> mapIdTile : input.entrySet()){

            if(mapIdTile.getValue().getMapLetterWord().isEmpty()){
                //Either this tile has:
                // - A Locked Letter
                // - A blocker (e.g. wall)
                // - No adjacent locked letter tiles, hence any letter can be played here
                //Therefore, no filtering of potential letters to be done here, so continue to the next tile
                continue;
            }

            mandatoryIds.add(mapIdTile.getKey());

            if(sql.length() > 0){
                sql.append(" UNION ");
            }
            sql.append("SELECT " + mapIdTile.getKey() + ", " + TableDictionary.COL_WORD +
                    " FROM " + TableDictionary.TABLE_NAME +
                    " WHERE " + TableDictionary.COL_WORD + " IN (");
            for(Map.Entry<Character, String> mapPotentialLetterWord : mapIdTile.getValue().getMapLetterWord().entrySet()){
                if(sql.charAt(sql.length() - 1) != '('){
                    sql.append(", ");
                }
                sql.append("'" + mapPotentialLetterWord.getValue() + "'");
            }
            sql.append(")");
        }

        if(sql.toString().isEmpty()){
            //There are no potential letters/words to worry about, so return the same input
            return input;
        }

        //Run the query. This query will look something like this:
        // > SELECT 3, Word FROM Dictionary WHERE Word IN ('tebt', 'test', 'temt', 'tept', 'tent', 'text')
        // > UNION
        // > SELECT 7, Word FROM Dictionary WHERE Word IN ('over', 'ovep', 'ovex', 'oven') ...[etc.]
        //And the output something like this:
        // > 3, test
        // > 3, tent
        // > 3, text
        // > 7, over
        // > 7, oven
        //So all the invalid words will be excluded from the result set.
        Cursor cursor = database.rawQuery(sql.toString(), null);

        //Create a map of valid words
        HashMap<Integer, ArrayList<String>> mapValidIdWords = new HashMap<>();
        for (int i = 0; i < cursor.getCount(); i++) {
            if (!cursor.moveToPosition(i)) {
                break;
            }
            int id = cursor.getInt(0);
            if(mandatoryIds.contains(id)){
                mandatoryIds.remove(Integer.valueOf(id));
            }
            String word = cursor.getString(1);
            if(!mapValidIdWords.containsKey(id)){
                mapValidIdWords.put(id, new ArrayList<String>());
            }
            mapValidIdWords.get(id).add(word);
        }
        cursor.close();

        if(!mandatoryIds.isEmpty() && !mapValidIdWords.isEmpty()){
            for(int id : mandatoryIds){
                if(!mapValidIdWords.containsKey(id)) {
                    //One of the mapIdTile's have a tile where absolutely no letter can be played, so this path is not an option...
                    mapValidIdWords.clear();
                    break;
                }
            }
        }

        //Now iterate each tile's potential word and remove the ones not in the mapValidIdWords
        for(Map.Entry<Integer, ArrayList<String>> mapEntry : mapValidIdWords.entrySet()){
            int id = mapEntry.getKey();
            ArrayList<String> validWords = mapEntry.getValue();
            HashMap<Character, String> mapInvalidLetterWords = new HashMap<>();
            for(Map.Entry<Character, String> kvPotentialLetterAndWord : output.get(id).getMapLetterWord().entrySet()){
                if(!validWords.contains(kvPotentialLetterAndWord.getValue())){
                    //Mark this entry for deletion
                    mapInvalidLetterWords.put(kvPotentialLetterAndWord.getKey(), kvPotentialLetterAndWord.getValue());
                }
            }
            //Delete marked entries
            for(Character invalidLetter : mapInvalidLetterWords.keySet()){
                output.get(id).getMapLetterWord().remove(invalidLetter);
            }
        }
        //...Alternatively, if all words were invalid...
        if(mapValidIdWords.isEmpty()){
            HashMap<Character, String> mapInvalidLetterWords = new HashMap<>();
            for(int id = 0; id < output.size(); id++) {
                for (Map.Entry<Character, String> kvPotentialLetterAndWord : output.get(id).getMapLetterWord().entrySet()) {
                    //Mark this entry for deletion
                    mapInvalidLetterWords.put(kvPotentialLetterAndWord.getKey(), kvPotentialLetterAndWord.getValue());
                }
                //Delete marked entries
                for (Character invalidLetter : mapInvalidLetterWords.keySet()) {
                    output.get(id).getMapLetterWord().remove(invalidLetter);
                }
            }
        }

        return output;
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }
}
