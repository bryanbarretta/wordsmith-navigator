package bbgames.ie.wordsmithnavigator.Database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Bryan on 24/07/2016.
 */
public class TableLevels {

    public static final String TABLE_NAME = "Levels";
    public static final String COL_LEVEL_ID = "LevelID";
    public static final String COL_USER_DATA = "UserData";

    private static enum LevelDataSyntax {
        LevelStatus("L"),
        Stars("S"),
        Diamonds("D"),
        Points("P");

        private final String code;
        private LevelDataSyntax(String code){
            this.code = code;
        }
        public String getCode(){
            return code;
        }
    }

    public static ArrayList<LevelUserData> GetLevelUserData(SQLiteDatabase db){
        ArrayList<LevelUserData> lud = new ArrayList<>();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, COL_LEVEL_ID);
        for (int i = 0; i < c.getCount(); i++) {
            if (!c.moveToPosition(i)) {
                break;
            }
            int levelId = c.getInt(c.getColumnIndex(COL_LEVEL_ID));
            String userData = c.getString(c.getColumnIndex(COL_USER_DATA));
            lud.add(new LevelUserData(levelId, userData));
        }
        c.close();
        return lud;
    }

    public static LevelUserData GetLevelUserData(SQLiteDatabase db, int levelId){
        LevelUserData lud = null;
        Cursor c = db.query(TABLE_NAME, null, COL_LEVEL_ID +"=?", new String[]{String.valueOf(levelId)}, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (!c.moveToPosition(i)) {
                break;
            }
            String userData = c.getString(c.getColumnIndex(COL_USER_DATA));
            lud = new LevelUserData(levelId, userData);
        }
        c.close();
        return lud;
    }

}
