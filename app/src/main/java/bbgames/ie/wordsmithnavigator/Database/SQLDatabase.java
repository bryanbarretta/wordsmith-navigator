package bbgames.ie.wordsmithnavigator.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Bryan on 24/07/2016.
 */
public class SQLDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Databasewordsmithnavigator";
    public static final int DATABASE_VERSION = 19;  //Increment this when schema changes
    private Context mContext;

    public SQLDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableLevels.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableDictionary.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableErrors.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableCPULogicLogs.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableSkins.TABLE_NAME);
        onCreate(db);
    }

    private void createTables(SQLiteDatabase db){
        db.execSQL("create table " + TableLevels.TABLE_NAME + " (" +
                TableLevels.COL_LEVEL_ID + " INTEGER PRIMARY KEY," +
                TableLevels.COL_USER_DATA + " TEXT);");

        db.execSQL("create table " + TableDictionary.TABLE_NAME + " (" +
                TableDictionary.COL_WORD + " TEXT PRIMARY KEY," +
                TableDictionary.COL_DISPLAY_WORD + " TEXT," +
                TableDictionary.COL_FREQUENCY + " DOUBLE);");

        db.execSQL("create table " + TableErrors.TABLE_NAME + " (" +
                TableErrors.COL_MESSAGE + " TEXT," +
                TableErrors.COL_MESSAGE_FULL + " TEXT," +
                TableErrors.COL_LAST_TIMESTAMP + " TEXT," +
                TableErrors.COL_COUNT + " INTEGER," + "" +
                "PRIMARY KEY (" + TableErrors.COL_MESSAGE + ", " + TableErrors.COL_MESSAGE_FULL + "));");

        db.execSQL("create table " + TableCPULogicLogs.TABLE_NAME + " (" +
                TableCPULogicLogs.COL_GAME_NAME + " TEXT," +
                TableCPULogicLogs.COL_TURN + " INTEGER," +
                TableCPULogicLogs.COL_LOG_TYPE + " TEXT," +
                TableCPULogicLogs.COL_VALUE + " TEXT," + "" +
                "PRIMARY KEY ("
                    + TableCPULogicLogs.COL_GAME_NAME + ", "
                    + TableCPULogicLogs.COL_TURN + ", "
                    + TableCPULogicLogs.COL_LOG_TYPE + "));");

        db.execSQL("create table " + TableSkins.TABLE_NAME + " (" +
                TableSkins.COL_ID + " INTEGER PRIMARY KEY," +
                TableSkins.COL_JSON + " TEXT);");
    }

    //region AndroidDatabaseManager

    /**
     * Method used solely for AndroidDatabaseManager
     * @param Query
     * @return
     */
    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }


    }

    //endregion

}
