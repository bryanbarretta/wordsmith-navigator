package bbgames.ie.wordsmithnavigator.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 15/09/2018.
 */

public class TableSkins {

    public static final String TABLE_NAME = "Skins";
    public static final String COL_ID = "ID";
    public static final String COL_JSON = "Json";   //Contains id, name, color details

    public static ArrayList<SkinLetter> getSkins(SQLiteDatabase db){
        ArrayList<SkinLetter> output = new ArrayList<>();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, COL_ID);
        for (int i = 0; i < c.getCount(); i++) {
            if (!c.moveToPosition(i)) {
                break;
            }
            String json = c.getString(c.getColumnIndex(COL_JSON));
            output.add(new SkinLetter(json));
        }
        c.close();
        return output;
    }

    public static SkinLetter getSkin(SQLiteDatabase database, int id) {
        SkinLetter output = null;
        Cursor c = database.query(TABLE_NAME, null, COL_ID + " = ?", new String[]{String.valueOf(id)}, null, null, COL_ID);
        for (int i = 0; i < c.getCount(); i++) {
            if (!c.moveToPosition(i)) {
                break;
            }
            String json = c.getString(c.getColumnIndex(COL_JSON));
            output = new SkinLetter(json);
        }
        c.close();
        if(output == null){
            return getSkins(database).get(0);
        }else {
            return output;
        }
    }

    public static long addSkin(SQLiteDatabase db, SkinLetter skin){
        ContentValues values = new ContentValues();
        values.put(COL_ID, skin.getId());
        values.put(COL_JSON, skin.toJSON());
        long id = -1;
        db.beginTransaction();
        try {
            id = db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            db.setTransactionSuccessful();
        } catch (Exception e){
            throw e;
        } finally {
            db.endTransaction();
        }
        return id;
    }

    public static List<SkinLetter> addDefaultSkins(SQLiteDatabase database, Context context) {
        List<SkinLetter> output = new ArrayList<>();
        int charcoal = context.getResources().getColor(R.color.charcoal);
        int white = context.getResources().getColor(R.color.white);
        int gray = context.getResources().getColor(R.color.gray);
        int maroon = context.getResources().getColor(R.color.maroon);
        int maroon_l = context.getResources().getColor(R.color.maroon_light);
        output.add(new SkinLetter(0, context.getString(R.string.letterskin_default), charcoal, white, charcoal, white, white));
        output.add(new SkinLetter(1, context.getString(R.string.letterskin_cpu), maroon, white, maroon_l, gray, maroon));
        for(SkinLetter s : output){
            addSkin(database, s);
        }
        return output;
    }
}
