package bbgames.ie.wordsmithnavigator.Database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by Bryan on 06/04/2017.
 */

public class TableErrors {

    public static final String TABLE_NAME = "Errors";
    public static final String COL_LAST_TIMESTAMP = "LastTimeStamp";
    public static final String COL_MESSAGE = "Message";
    public static final String COL_MESSAGE_FULL = "MessageFull";
    public static final String COL_COUNT = "Count";

    public static class ExceptionInstance {

        private DateTime timestamp;
        private String message;
        private String messageFull;
        private int count;

        public ExceptionInstance(DateTime timestamp, String message, String messageFull, int count) {
            this.timestamp = timestamp;
            this.message = message;
            this.messageFull = messageFull;
            this.count = count;
        }

        public DateTime getTimestamp() {
            return timestamp;
        }

        public String getMessage() {
            return message;
        }

        public String getMessageFull() {
            return messageFull;
        }

        public int getCount() {
            return count;
        }
    }

    public static ArrayList<ExceptionInstance> getExceptions(Cursor c){
        ArrayList<ExceptionInstance> entries = new ArrayList<ExceptionInstance>();
        for (int i = 0; i < c.getCount(); i++) {
            if(!c.moveToPosition(i)){
                break;
            }
            DateTime lastTimeStamp = DateTime.parse(c.getString(c.getColumnIndex(COL_LAST_TIMESTAMP)));
            String message = c.getString(c.getColumnIndex(COL_MESSAGE));
            String messageFull = c.getString(c.getColumnIndex(COL_MESSAGE_FULL));
            int count = c.getInt(c.getColumnIndex(COL_COUNT));
            entries.add(new TableErrors.ExceptionInstance(lastTimeStamp, message, messageFull, count));
        }
        c.close();
        return entries;
    }

    public static long getExceptionCount(SQLiteDatabase database, Exception e){
        ArrayList<ExceptionInstance> errs = TableErrors.getExceptions(database.query(TableErrors.TABLE_NAME, null,
                COL_MESSAGE + "=? AND " + COL_MESSAGE_FULL + "=?",
                new String[] {e.getMessage(), e.toString()},
                null, null, TableErrors.COL_COUNT));
        if(errs.isEmpty()){
            return 0;
        }
        return errs.get(0).getCount();
    }

}
