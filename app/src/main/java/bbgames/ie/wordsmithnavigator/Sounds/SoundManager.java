package bbgames.ie.wordsmithnavigator.Sounds;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by Bryan on 12/05/2018.
 */

public class SoundManager {

    boolean soundEnabled = true;
    private Context context;

    public SoundManager(Context context){
        this.context = context;
    }

    public void playSound(int id){
        if(!soundEnabled){
            return;
        }
        MediaPlayer.create(context, id).start();
    }


}
