package bbgames.ie.wordsmithnavigator.AsyncTask;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Bryan on 22/03/2016.
 */
public class AsyncTaskHandler {

    public static int THREAD_POOL_CORE_SIZE = 5;
    public static int THREAD_POOL_MAX_SIZE = 128;

    private ArrayList<AsyncTask<Void, Void, Object>> queuedAsyncTasks = new ArrayList<>();
    private ArrayList<AsyncTask<Void, Void, Object>> activeAsyncTasks = new ArrayList<>();

    public void AddTask(final iTask task){
        AddTask(task, Thread.NORM_PRIORITY);
    }

    public void AddTask(final iTask task, final int threadPriority){
        AsyncTask<Void, Void, Object> asyncTask = new AsyncTask<Void, Void, Object>() {
            @Override
            protected Void doInBackground(Void... params) {
                Thread.currentThread().setPriority(threadPriority);
                task.Task();
                return null;
            }
            @Override
            protected void onPostExecute(Object response) {
                refreshTasks(this);
                task.OnTaskCompleted();
                super.onPostExecute(response);
            }
            @Override
            public String toString() {
                return task.getTaskID().toString();
            }
        };
        if(activeAsyncTasks.size() < THREAD_POOL_CORE_SIZE){
            activeAsyncTasks.add(asyncTask);
            executeTask(asyncTask);
        }else {
            queuedAsyncTasks.add(asyncTask);
        }
    }

    private synchronized void refreshTasks(AsyncTask<Void, Void, Object> asyncTask) {
        activeAsyncTasks.remove(asyncTask);
        if (!queuedAsyncTasks.isEmpty()) {
            AsyncTask<Void, Void, Object> nextTask = queuedAsyncTasks.get(0);
            queuedAsyncTasks.remove(0);
            activeAsyncTasks.add(nextTask);
            executeTask(nextTask);
        }
    }

    public synchronized void deleteQueuedTasks(UUID taskID){
        for(int i = queuedAsyncTasks.size() - 1; i >= 0; i--){
            if(getAsyncTaskID(queuedAsyncTasks.get(i)).equals(taskID)){
                queuedAsyncTasks.remove(i);
            }
        }
    }

    private UUID getAsyncTaskID(AsyncTask<Void, Void, Object> task){
        return UUID.fromString(task.toString());
    }

    public int getActiveAsyncTaskCount(){
        return activeAsyncTasks.size();
    }

    public int getQueuedAsyncTaskCount(){
        return queuedAsyncTasks.size();
    }

    /**
     * Protected so that it can be overwritten by junit tests. They need to execute tasks on the ui thread, see: https://stackoverflow.com/a/9790710
     * @param task
     */
    protected void executeTask(AsyncTask<Void, Void, Object> task){
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
