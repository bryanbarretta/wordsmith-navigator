package bbgames.ie.wordsmithnavigator.AsyncTask;

import java.util.UUID;

/**
 * Created by Bryan on 22/03/2016.
 */
public abstract class iTask {

    UUID taskID;

    public iTask(){
        this(UUID.randomUUID());
    }

    public iTask(UUID taskID){
        this.taskID = taskID;
    }

    private Object taskData;

    public Object getTaskData() {
        return taskData;
    }

    public void setTaskData(Object taskData) {
        this.taskData = taskData;
    }

    public UUID getTaskID(){
        return taskID;
    }

    public abstract void Task();

    public abstract void OnTaskCompleted();
}
