package bbgames.ie.wordsmithnavigator.Online;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.google.android.gms.games.Games;
import com.google.android.gms.games.multiplayer.realtime.OnRealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateCallback;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateCallback;

import java.util.List;

import bbgames.ie.wordsmithnavigator.Activities.BaseActivities.BaseActivitySignedIn;
import bbgames.ie.wordsmithnavigator.Activities.LobbyActivity;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 24/03/2018.
 */

public class MatchmakingManager {

    private final String LOGTAG = getClass().getSimpleName();
    private BaseActivitySignedIn activity;
    private GoogleAccountManager googleAccountManager;

    private RoomConfig mJoinedRoomConfig;
    private RoomUpdateCallback mRoomUpdateCallback;
    private OnRealTimeMessageReceivedListener mMessageReceivedHandler;
    private RoomStatusUpdateCallback mRoomStatusCallbackHandler;

    public MatchmakingManager(BaseActivitySignedIn activity){
        this.activity = activity;
        this.googleAccountManager = Util.getApplicationManager(activity).getGoogleAccountManager();
        setup();
    }

    private void setup(){
        mRoomUpdateCallback = new RoomUpdateCallback() {
            @Override
            public void onRoomCreated(int i, @Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onRoomCreated");
            }

            @Override
            public void onJoinedRoom(int i, @Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onJoinedRoom");
            }

            @Override
            public void onLeftRoom(int i, @NonNull String s) {
                ((LobbyActivity)activity).logInfoOnUi("onLeftRoom");
            }

            @Override
            public void onRoomConnected(int i, @Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onRoomConnected");
            }
        };

        mMessageReceivedHandler = new OnRealTimeMessageReceivedListener() {
            @Override
            public void onRealTimeMessageReceived(@NonNull RealTimeMessage realTimeMessage) {
                ((LobbyActivity)activity).logInfoOnUi("onRealTimeMessageReceived");
            }
        };

        mRoomStatusCallbackHandler = new RoomStatusUpdateCallback() {
            @Override
            public void onRoomConnecting(@Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onRoomConnecting");
            }

            @Override
            public void onRoomAutoMatching(@Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onRoomAutoMatching");
            }

            @Override
            public void onPeerInvitedToRoom(@Nullable Room room, @NonNull List<String> list) {
                ((LobbyActivity)activity).logInfoOnUi("onPeerInvitedToRoom");
            }

            @Override
            public void onPeerDeclined(@Nullable Room room, @NonNull List<String> list) {
                ((LobbyActivity)activity).logInfoOnUi("onPeerDeclined");
            }

            @Override
            public void onPeerJoined(@Nullable Room room, @NonNull List<String> list) {
                ((LobbyActivity)activity).logInfoOnUi("onPeerJoined");
            }

            @Override
            public void onPeerLeft(@Nullable Room room, @NonNull List<String> list) {
                ((LobbyActivity)activity).logInfoOnUi("onPeerLeft");
            }

            @Override
            public void onConnectedToRoom(@Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onConnectedToRoom");
            }

            @Override
            public void onDisconnectedFromRoom(@Nullable Room room) {
                ((LobbyActivity)activity).logInfoOnUi("onDisconnectedFromRoom");
            }

            @Override
            public void onPeersConnected(@Nullable Room room, @NonNull List<String> list) {
                ((LobbyActivity)activity).logInfoOnUi("onPeersConnected");
            }

            @Override
            public void onPeersDisconnected(@Nullable Room room, @NonNull List<String> list) {
                ((LobbyActivity)activity).logInfoOnUi("onPeersDisconnected");
            }

            @Override
            public void onP2PConnected(@NonNull String s) {
                ((LobbyActivity)activity).logInfoOnUi("onP2PConnected");
            }

            @Override
            public void onP2PDisconnected(@NonNull String s) {
                ((LobbyActivity)activity).logInfoOnUi("onP2PDisconnected");
            }
        };
    }

    public void createRoom() {

        long role = 1; //Player one ?

        // auto-match criteria to invite one random automatch opponent.
        // You can also specify more opponents (up to 3).
        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(1, 1, role);

        // build the room config:
        RoomConfig roomConfig = RoomConfig.builder(mRoomUpdateCallback)
                        .setOnMessageReceivedListener(mMessageReceivedHandler)
                        .setRoomStatusUpdateCallback(mRoomStatusCallbackHandler)
                        .setAutoMatchCriteria(autoMatchCriteria)
                        .build();

        // prevent screen from sleeping during handshake
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Save the roomConfig so we can use it if we call leave().
        mJoinedRoomConfig = roomConfig;

        // create room:
        Games.getRealTimeMultiplayerClient(activity, googleAccountManager.getAccount()).create(roomConfig);
    }

}
