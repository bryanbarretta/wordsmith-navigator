package bbgames.ie.wordsmithnavigator.Online;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.InputStream;
import java.net.URL;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 24/03/2018.
 */

public class GoogleAccountManager {

    private static final String ANDROID_WEB_CLIENT_ID = "682241103576-at45o27g5r5ctj8e40ur2djl3l8rornn.apps.googleusercontent.com";

    private final String LOGTAG = getClass().getSimpleName();

    private ApplicationManager applicationManager;

    private GoogleSignInOptions googleSignInOptions;
    private GoogleSignInClient googleSignInClient;
    private GoogleSignInAccount googleAccount;

    public GoogleAccountManager(ApplicationManager applicationManager){
        this.applicationManager = applicationManager;
        signIn();
    }

    /**
     * Run in onResume to ensure the user is signed in
     * @param activity
     * @param onConnectedRunnable Run when we confirm the user is signed in
     * @param onDisconnectedRunnable Run when we fail to sign in
     */
    public void signInSilently(Activity activity, final Runnable onConnectedRunnable, final Runnable onDisconnectedRunnable){
        if(googleAccount != null && GoogleSignIn.hasPermissions(googleAccount, Games.SCOPE_GAMES_LITE)){
            onConnectedRunnable.run();
            return;
        }
        googleSignInClient.silentSignIn().addOnCompleteListener(activity,
            new OnCompleteListener<GoogleSignInAccount>() {
                @Override
                public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                    if (task.isSuccessful()) {
                        Log.d(LOGTAG, "signInSilently(): success");
                        googleAccount = task.getResult();
                        onConnectedRunnable.run();
                    } else {
                        Log.d(LOGTAG, "signInSilently(): failure", task.getException());
                        onDisconnectedRunnable.run();
                    }
                }
            });
    }

    public void signIn(){
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestEmail()
                .requestIdToken(ANDROID_WEB_CLIENT_ID)
                .requestScopes(Games.SCOPE_GAMES_LITE)
                .build();
        googleSignInClient = GoogleSignIn.getClient(applicationManager, googleSignInOptions);
        googleAccount = GoogleSignIn.getLastSignedInAccount(applicationManager);
    }

    public void signOut(Activity activity, final Runnable onDisconnectedRunnable) {
        Log.d(LOGTAG, "signOut()");
        if (!isSignedIn()) {
            Log.w(LOGTAG, "signOut() called, but was not signed in!");
            return;
        }
        googleSignInClient.signOut().addOnCompleteListener(activity,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        boolean successful = task.isSuccessful();
                        if(successful){
                            googleAccount = null;
                        }
                        Log.d(LOGTAG, "signOut(): " + (successful ? "success" : "failed"));
                        onDisconnectedRunnable.run();
                    }
                });
    }

    //region Getters

    public GoogleSignInOptions getGoogleSignInOptions() {
        return googleSignInOptions;
    }

    public GoogleSignInClient getGoogleSignInClient() {
        return googleSignInClient;
    }

    public boolean isSignedIn(){
        return googleAccount != null;
    }

    public GoogleSignInAccount getAccount(){
        return googleAccount;
    }

    public Drawable getPhoto(){
        if(googleAccount == null || googleAccount.getPhotoUrl() == null || Util.isNullOrEmpty(googleAccount.getPhotoUrl().getPath())){
            return applicationManager.getResources().getDrawable(R.drawable.ic_user);
        }
        String photoUrl = googleAccount.getPhotoUrl().getPath();
        String currentPhotoUrl = applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.user_photo_url);
        String currentPhotoInputStream = applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.user_photo_input_stream);
        if(currentPhotoUrl != null && currentPhotoInputStream != null && !currentPhotoInputStream.isEmpty() && photoUrl.equals(currentPhotoUrl)){
            byte[] b = Base64.decode(currentPhotoInputStream, Base64.DEFAULT);
            return new BitmapDrawable(applicationManager.getResources(), BitmapFactory.decodeByteArray(b, 0, b.length));
        }
        try {
            Log.d(getClass().getSimpleName(), "Downloading new user photo from " + photoUrl);
            InputStream is = (InputStream) new URL(photoUrl).getContent();
            Drawable d = Drawable.createFromStream(is, null);
            applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.user_photo_url, photoUrl);
            applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.user_photo_input_stream, Util.getBase64(d));
            Log.d(getClass().getSimpleName(), "Successfully set new user photo " + photoUrl);
            return d;
        }catch (Exception e){
            Log.d(getClass().getSimpleName(), "Failed to download photoUrl: " + photoUrl + "\n" + e.getMessage());
            return applicationManager.getResources().getDrawable(R.drawable.ic_user);
        }
    }

    //endregion

    //region Setters

    public void setAccount(GoogleSignInAccount account) {
        this.googleAccount = account;
    }

    //endregion

}
