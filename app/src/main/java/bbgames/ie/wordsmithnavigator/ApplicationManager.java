package bbgames.ie.wordsmithnavigator;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.AsyncTask.AsyncTaskHandler;
import bbgames.ie.wordsmithnavigator.AsyncTask.iTask;
import bbgames.ie.wordsmithnavigator.Constants.GameModes;
import bbgames.ie.wordsmithnavigator.Constants.IntentKeys;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinMap;
import bbgames.ie.wordsmithnavigator.Dialogs.DialogLoading;
import bbgames.ie.wordsmithnavigator.Files.FileManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelFactory;
import bbgames.ie.wordsmithnavigator.Online.GoogleAccountManager;
import bbgames.ie.wordsmithnavigator.Constants.SupportedLanguages;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.Settings.Settings;
import bbgames.ie.wordsmithnavigator.Sounds.SoundManager;
import bbgames.ie.wordsmithnavigator.Util.Util;
import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;

/**
 * Responsible for core android management tasks for the entire application
 *  - Loading levels (done on app start up)
 *  - Database management
 *  - Asynchronous task management
 *  - Google Account management
 *  - Sound management
 *  - Shared Preferences management
 * Created by Bryan on 07/05/2016.
 */
public class ApplicationManager extends Application{

    private LevelFactory levelFactory;
    private DatabaseGateway databaseGateway;
    private AsyncTaskHandler asyncTaskHandler;
    private GoogleAccountManager googleAccountManager;
    private SoundManager soundManager;
    private FileManager fileManager;

    private static final String SHARED_PREFERENCES_NAME = "WUNDER_WORDS_SETTINGS_1";
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    Integer screenWidthPixels;
    Integer screenHeightPixels;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        databaseGateway = new DatabaseGateway(this);
        soundManager = new SoundManager(this);
        googleAccountManager = new GoogleAccountManager(this);
        asyncTaskHandler = new AsyncTaskHandler();
        levelFactory = new LevelFactory(getAssets(), this);
        fileManager = new FileManager(this);
    }

    /**
     * More language dictionaries at: http://www.winedt.org/dict.html
     * https://onedrive.live.com/?id=3732E80B128D016F%213584&cid=3732E80B128D016F
     * @param force: if false, will only load the dictionary if the table is empty
     */
    public void loadDictionary(Context context, final boolean force){
        if(!force && !databaseGateway.isDictionaryEmtpy())
            return;
        final DialogLoading loadingDialog = new DialogLoading(context, getString(R.string.setting_up_dictionary));
        loadingDialog.show();
        asyncTaskHandler.AddTask(new iTask() {
            @Override
            public void Task() {
                SupportedLanguages language = new Settings(ApplicationManager.this).getLanguage();
                int dictionaryResourceId = language.getDictionaryResourceId(ApplicationManager.this);
                ArrayList<DictionaryEntry> entries = new ArrayList<>();
                InputStream dict = getResources().openRawResource(dictionaryResourceId);
                InputStreamReader isr = new InputStreamReader(dict , Charset.forName("UTF-8"));
                BufferedReader br = new BufferedReader(isr );
                String line="";
                try {
                    double mostFreq = language.getMostFrequentWordCount();
                    double factor = Util.getFactor(mostFreq);
                    while ((line = br.readLine()) != null) {
                        String[] parts = line.split(",");
                        String displayWord = parts[0].toLowerCase();
                        String baseWord = Util.getBaseWord(displayWord);
                        double freq = parts.length > 1 ? Double.valueOf(parts[1]) : 0;
                        entries.add(new DictionaryEntry(baseWord,
                                displayWord.equals(baseWord) ? null : displayWord,
                                freq <= 0 ? 0 : Math.round((freq / mostFreq) * factor) / factor));
                    }
                    br.close(); dict.close(); isr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(force){
                    databaseGateway.clearDictionaryEntries();
                }
                databaseGateway.addUpdateDictionaryEntries(entries);
            }
            @Override
            public void OnTaskCompleted() {
                loadingDialog.dismiss();
            }
        });
    }

    public LevelFactory getLevelFactory(){
        return levelFactory;
    }

    public DatabaseGateway getDatabaseGateway(){
        return databaseGateway;
    }

    public AsyncTaskHandler getAsyncTaskHandler(){
        return asyncTaskHandler;
    }

    public GoogleAccountManager getGoogleAccountManager() {
        return googleAccountManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public int getScreenHeightPixels(){
        if(screenHeightPixels == null){
            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            screenWidthPixels = metrics.widthPixels;
            screenHeightPixels = metrics.heightPixels;
        }
        return screenHeightPixels;
    }

    public int getScreenWidthPixels(){
        if(screenWidthPixels == null){
            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            screenWidthPixels = metrics.widthPixels;
            screenHeightPixels = metrics.heightPixels;
        }
        return screenWidthPixels;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public SupportedLanguages getLanguage() {
        return SupportedLanguages.getLanguageByAbbr(
                getSharedPreferenceValue(SHARED_PREF_KEY.language, SupportedLanguages.ENGLISH_UK.getAbbreviation()));
    }

    public void startLevelActivity(Activity fromActivity, int id) {
        Intent i = new Intent(fromActivity, GameActivity.class);
        i.putExtra(IntentKeys.GAME_MODE_ORDINAL.name(), GameModes.SINGLE_PLAYER.ordinal());
        i.putExtra(IntentKeys.LEVEL_ID.name(), id);
        i.putExtra(IntentKeys.THEME_HUD_ID.name(), SkinHUD.getUserTheme(this).ordinal());
        i.putExtra(IntentKeys.THEME_MAP_ID.name(), SkinMap.getThemeMap(this).getIndex());
        i.putExtra(IntentKeys.DIFFICULTY_ID.name(), AIDifficulty.getDifficulty(this).ordinal());
        startActivity(i);
        fromActivity.finish();
    }

    public enum SHARED_PREF_KEY{
        //Options
        language,
        camera_speed,
        camera_zoom_level,
        difficulty,
        hud_side,
        theme_map,
        theme_hud_user,
        theme_hud_cpu,
        theme_letter_user,
        theme_letter_cpu,
        tile_speed,
        help_dialogs_enabled,
        cpu_logs_enabled,
        //Account
        user_photo_url,
        user_photo_input_stream,
        //Progress
        user_total_points
    }

    public void setSharedPreferenceValue(SHARED_PREF_KEY key, String value){
        editor.putString(key.name(), value);
        editor.commit();
    }

    public String getSharedPreferenceValue(SHARED_PREF_KEY key){
        return prefs.getString(key.name(), null);
    }

    public String getSharedPreferenceValue(SHARED_PREF_KEY key, String defaultValue){
        return prefs.getString(key.name(), defaultValue);
    }

    public void deleteSharedPreferenceValue(SHARED_PREF_KEY key){
        editor.remove(key.name());
        editor.commit();
    }

    public FileManager getFileManager() {
        return fileManager;
    }

}

