package bbgames.ie.wordsmithnavigator.WordDefinition.LanguageImpl;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Curl.GetRequest;
import bbgames.ie.wordsmithnavigator.WordDefinition.WordDefinition;

/**
 * Created by Bryan on 10/04/2017.
 */

public abstract class WordRequestEnUk extends WordRequest{

    private String API_KEY = "a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"; //TODO: This is a demo key...
    private String API_URL = "http://api.wordnik.com:80/v4";   //http://developer.wordnik.com/docs.html
    private String API_URL_GET_1_WORD_DEFINITION = API_URL + "/word.json/%s/definitions?" +
            "includeRelated=true&sourceDictionaries=all&useCanonical=true&includeTags=true&api_key=" + //&limit=5
            API_KEY;

    public WordRequestEnUk(Activity activity){
        super(activity);
    }

    @Override
    public void getDefinition(final String word) {
        String url = String.format(API_URL_GET_1_WORD_DEFINITION, word);
        asyncTaskHandler.AddTask(new GetRequest(url) {
            @Override
            public void OnTaskCompleted() {
                try {
                    String JSON = getTaskData().toString();
                    JSONArray jsonDefinitions = new JSONArray(JSON);
                    if(jsonDefinitions.length() < 1){
                        onDefinitionFound(word, null);
                    }
                    ArrayList<WordDefinition> definitions = new ArrayList<>();
                    for(int i = 0; i < jsonDefinitions.length(); i++){
                        definitions.add(new WordDefinition((JSONObject) jsonDefinitions.get(i)));
                    }
                    onDefinitionFound(word, definitions);
                } catch (Exception e) {
                    onErrorResponse(e);
                }
            }
        });
    }
}
