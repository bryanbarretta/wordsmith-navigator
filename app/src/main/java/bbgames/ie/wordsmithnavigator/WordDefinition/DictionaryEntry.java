package bbgames.ie.wordsmithnavigator.WordDefinition;

/**
 * Created by Bryan on 01/04/2017.
 */

public class DictionaryEntry {

    private String word;
    private String displayWord;
    private double frequency;

    public DictionaryEntry(String word, String displayWord, double frequency){
        this.word = word;
        this.displayWord = displayWord;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public double getFrequency() {
        return frequency;
    }

    public String getDisplayWord() {
        return displayWord;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof DictionaryEntry){
            DictionaryEntry other = (DictionaryEntry) o;
            return word.equals(other.word);
        }else {
            return false;
        }
    }
}
