package bbgames.ie.wordsmithnavigator.WordDefinition;

import android.app.Activity;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.SupportedLanguages;
import bbgames.ie.wordsmithnavigator.WordDefinition.LanguageImpl.WordRequest;
import bbgames.ie.wordsmithnavigator.WordDefinition.LanguageImpl.WordRequestEnUk;
import bbgames.ie.wordsmithnavigator.WordDefinition.LanguageImpl.WordRequestPtBr;

/**
 * Created by Bryan on 10/04/2017.
 */

public abstract class WordRequestFactory {

    private Activity activity;

    public WordRequestFactory(Activity activity){
        this.activity = activity;
    }

    public WordRequest getWordRequester(SupportedLanguages language){
        switch (language){
            default:
            case ENGLISH_UK:
                return new WordRequestEnUk(activity) {
                    @Override
                    public void onDefinitionFound(String word, ArrayList<WordDefinition> definitions) {
                        WordRequestFactory.this.onDefinitionFound(word, definitions);
                    }
                    @Override
                    public void onErrorResponse(Exception e) {
                        WordRequestFactory.this.onErrorResponse(e);
                    }
                };
            case PORTUGUESE_BRAZIL:
                return new WordRequestPtBr(activity) {
                    @Override
                    public void onDefinitionFound(String word, ArrayList<WordDefinition> definitions) {
                        WordRequestFactory.this.onDefinitionFound(word, definitions);
                    }
                    @Override
                    public void onErrorResponse(Exception e) {
                        WordRequestFactory.this.onErrorResponse(e);
                    }
                };
        }
    }

    public abstract void onDefinitionFound(String word, ArrayList<WordDefinition> definitions);

    public abstract void onErrorResponse(Exception e);

}
