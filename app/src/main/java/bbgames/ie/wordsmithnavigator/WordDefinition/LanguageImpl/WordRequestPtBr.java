package bbgames.ie.wordsmithnavigator.WordDefinition.LanguageImpl;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Curl.GetRequest;
import bbgames.ie.wordsmithnavigator.WordDefinition.WordDefinition;

/**
 * Created by Bryan on 10/04/2017.
 */

public abstract class WordRequestPtBr extends WordRequest{

    private String API_URL_WORD = "http://dicionario-aberto.net/search-json/%s";
    private String API_URL_SIMILAR_WORDS = "http://dicionario-aberto.net/search-json?like=%s";

    private String SOURCE = "dicionario-aberto";
    private String KEY_ENTRY = "entry";
    private String KEY_DEFINITIONS = "sense";
    private String KEY_PART_OF_SPEECH = "gramGrp";
    private String KEY_DEFINITION = "def";
    private String KEY_LIST = "list";

    public WordRequestPtBr(Activity activity){
        super(activity);
    }

    @Override
    public void getDefinition(final String word) {
        getDefinition(word, true);
    }

    private void getDefinition(final String word, final boolean findSimilarWordsIfNoResult) {
        final String urlInitialWord = String.format(API_URL_WORD, word);
        asyncTaskHandler.AddTask(new GetRequest(urlInitialWord) {
            @Override
            public void OnTaskCompleted() {
                try {
                    String JSON = getTaskData().toString();
                    if(JSON == null || JSON.isEmpty())
                    {
                        onDefinitionFound(word, null);
                        return;
                    }
                    if(JSON.contains("Error 404")){
                        if(findSimilarWordsIfNoResult){
                            getSimilarWords(word);
                        }else {
                            onDefinitionFound(word, null);
                        }
                        return;
                    }
                    JSONObject jsonObject = new JSONObject(JSON);
                    if(jsonObject == null || !jsonObject.has(KEY_ENTRY))
                    {
                        onDefinitionFound(word, null);
                        return;
                    }
                    JSONObject entry = jsonObject.getJSONObject(KEY_ENTRY);
                    if(entry == null || !entry.has(KEY_DEFINITIONS)){
                        onDefinitionFound(word, null);
                        return;
                    }
                    String w = entry.get("@id").toString();
                    JSONArray definitions = entry.getJSONArray(KEY_DEFINITIONS);
                    if(definitions == null || definitions.length() < 1){
                        onDefinitionFound(word, null);
                        return;
                    }
                    ArrayList<WordDefinition> output = new ArrayList<>();
                    for(int i = 0; i < definitions.length(); i++){
                        JSONObject def = (JSONObject) definitions.get(i);
                        String pos = def.get(KEY_PART_OF_SPEECH).toString();
                        String d = def.get(KEY_DEFINITION).toString();
                        WordDefinition wd = new WordDefinition(SOURCE, w, pos, d, urlInitialWord);
                        output.add(wd);
                    }
                    onDefinitionFound(word, output);
                } catch (Exception e) {
                    onErrorResponse(e);
                }
            }
        });
    }

    private void getSimilarWords(final String word) {
        final String urlSimilarWords = String.format(API_URL_SIMILAR_WORDS, word);
        asyncTaskHandler.AddTask(new GetRequest(urlSimilarWords) {
            @Override
            public void OnTaskCompleted() {
                try {
                    String JSON = getTaskData().toString();
                    if(JSON == null || JSON.isEmpty() || JSON.contains("Error 404"))
                    {
                        onDefinitionFound(word, null);
                        return;
                    }
                    JSONObject jsonObject = new JSONObject(JSON);
                    if(jsonObject == null || !jsonObject.has(KEY_LIST))
                    {
                        onDefinitionFound(word, null);
                        return;
                    }
                    JSONArray similarWords = jsonObject.getJSONArray(KEY_LIST);
                    if(similarWords == null || similarWords.length() < 1){
                        onDefinitionFound(word, null);
                        return;
                    }
                    for (int i = 0; i < similarWords.length(); i++){
                        String sw = similarWords.getString(i);
                        if(word.contains(sw)){
                            getDefinition(sw, false);
                            return;
                        }
                    }
                    getDefinition(similarWords.getString(0), false);
                } catch (Exception e) {
                    onErrorResponse(e);
                }
            }
        });
    }
}
