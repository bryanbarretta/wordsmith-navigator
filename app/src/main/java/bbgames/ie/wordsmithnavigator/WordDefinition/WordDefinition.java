package bbgames.ie.wordsmithnavigator.WordDefinition;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bryan on 01/04/2017.
 */

public class WordDefinition {

    private final static String KEY_SOURCE_DICTIONARY = "sourceDictionary";
    private final static String KEY_WORD = "word";
    private final static String KEY_PART_OF_SPEECH = "partOfSpeech";
    private final static String KEY_TEXT = "text";
    private final static String KEY_ATTRIBUTION_TEXT = "attributionText";

    private String source;
    private String word;
    private String partOfSpeech;
    private String definition;
    private String attributionText;

    public WordDefinition(JSONObject jsonDefinitionWordnik) throws JSONException {
        this(jsonDefinitionWordnik.getString(KEY_SOURCE_DICTIONARY),
                jsonDefinitionWordnik.getString(KEY_WORD),
                jsonDefinitionWordnik.getString(KEY_PART_OF_SPEECH),
                jsonDefinitionWordnik.getString(KEY_TEXT),
                jsonDefinitionWordnik.getString(KEY_ATTRIBUTION_TEXT));
    }

    public WordDefinition(String source, String word, String partOfSpeech, String definition, String attributionText) {
        this.source = source;
        this.word = word;
        this.partOfSpeech = partOfSpeech;
        this.definition = definition;
        this.attributionText = attributionText;
    }

    public String getSource() {
        return source;
    }

    public String getWord() {
        return word;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public String getDefinition() {
        return definition;
    }

    public String getAttributionText() {
        return attributionText;
    }
}
