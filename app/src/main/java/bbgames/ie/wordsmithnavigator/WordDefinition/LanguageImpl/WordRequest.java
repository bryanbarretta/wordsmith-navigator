package bbgames.ie.wordsmithnavigator.WordDefinition.LanguageImpl;

import android.app.Activity;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.AsyncTask.AsyncTaskHandler;
import bbgames.ie.wordsmithnavigator.WordDefinition.WordDefinition;

/**
 * Created by Bryan on 31/03/2017.
 */

//http://developer.wordnik.com/#!/faq

public abstract class WordRequest {

    AsyncTaskHandler asyncTaskHandler;

    public WordRequest(){
        asyncTaskHandler = new AsyncTaskHandler();
    }

    public WordRequest(Activity activity){
        asyncTaskHandler = ((ApplicationManager)((activity).getApplication())).getAsyncTaskHandler();
    }

    public abstract void getDefinition(final String word);

    public abstract void onDefinitionFound(String word, ArrayList<WordDefinition> definitions);

    public abstract void onErrorResponse(Exception e);

}
