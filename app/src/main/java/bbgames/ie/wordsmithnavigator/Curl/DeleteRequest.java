package bbgames.ie.wordsmithnavigator.Curl;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;

import java.io.IOException;
import java.net.URI;

/**
 * Created by Bryan on 27/02/2016.
 */
public abstract class DeleteRequest extends CurlRequest {

    private String authorization;

    public DeleteRequest(String url) {
        super(url, null);
    }

    @Override
    public HttpResponse makeRequest(URI uri, HttpClient httpClient) throws IOException {
        HttpDelete httpDelete = new HttpDelete();
        httpDelete.setURI(uri);
        httpDelete.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpDelete.getParams().setParameter(PARAM_TIMEOUT_CONNECTION, TIMEOUT_SECONDS * 1000);
        httpDelete.getParams().setParameter(PARAM_TIMEOUT_SOCKET, TIMEOUT_SECONDS * 1000);
        if(authorization != null && !authorization.isEmpty()){
            httpDelete.setHeader("Authorization", authorization);
        }
        HttpResponse response = httpClient.execute(httpDelete);
        return response;
    }
}
