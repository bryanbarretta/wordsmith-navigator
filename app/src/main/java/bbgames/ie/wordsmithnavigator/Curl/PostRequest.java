package bbgames.ie.wordsmithnavigator.Curl;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;

/**
 * Created by Bryan on 27/02/2016.
 */
public abstract class PostRequest extends CurlRequest {

    private String authorization;

    public PostRequest(String url, JSONObject body) {
        super(url, body);
    }

    @Override
    public HttpResponse makeRequest(URI uri, HttpClient httpClient) throws IOException {
        HttpPost httpPost = new HttpPost();
        httpPost.setURI(uri);
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.getParams().setParameter(PARAM_TIMEOUT_CONNECTION, TIMEOUT_SECONDS * 1000);
        httpPost.getParams().setParameter(PARAM_TIMEOUT_SOCKET, TIMEOUT_SECONDS * 1000);
        if(getBody() != null){
            httpPost.setEntity(new StringEntity(getBody().toString()));
        }
        if(authorization != null && !authorization.isEmpty()){
            httpPost.setHeader("Authorization", authorization);
        }
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }
}
