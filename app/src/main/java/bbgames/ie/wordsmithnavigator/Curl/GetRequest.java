package bbgames.ie.wordsmithnavigator.Curl;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.net.URI;

/**
 * Created by Bryan on 27/02/2016.
 */
public abstract class GetRequest extends CurlRequest {

    public GetRequest(final String url){
        super(url);
    }

    public GetRequest(String url, String authorization){
        super(url);
        this.authorization = authorization;
    }

    private String authorization;

    @Override
    public HttpResponse makeRequest(URI uri, HttpClient httpClient) throws IOException {
        HttpGet httpGet = new HttpGet();
        httpGet.setURI(uri);
        //httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpGet.getParams().setParameter(PARAM_TIMEOUT_CONNECTION, TIMEOUT_SECONDS * 1000);
        httpGet.getParams().setParameter(PARAM_TIMEOUT_SOCKET, TIMEOUT_SECONDS * 1000);
        if(authorization != null && !authorization.isEmpty()){
            httpGet.setHeader("Authorization", authorization);
        }
        HttpResponse response = httpClient.execute(httpGet);
        return response;
    }
}
