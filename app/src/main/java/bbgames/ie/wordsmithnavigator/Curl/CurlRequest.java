package bbgames.ie.wordsmithnavigator.Curl;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import bbgames.ie.wordsmithnavigator.AsyncTask.iTask;

/**
 * Created by Bryan on 27/02/2016.
 */
public abstract class CurlRequest extends iTask {

    protected final String PARAM_TIMEOUT_SOCKET = "http.socket.timeout";
    protected final String PARAM_TIMEOUT_CONNECTION = "http.connection.timeout";
    protected final int TIMEOUT_SECONDS = 30;

    private JSONObject body;
    private String url;

    public CurlRequest(final String url, final JSONObject body){
        this.body = body;
        this.url = url;
    }

    public CurlRequest(final String url){
        this(url, null);
    }

    @Override
    public void Task() {
        try {
            makeRequest(url);
        } catch (Exception e) {
            Log.e(getClass().getCanonicalName(), e != null && e.getMessage() != null ? e.getMessage() : "???");
        }
    }

    private void makeRequest(String url) throws URISyntaxException, IOException, JSONException {
        StringBuffer stringBuffer = new StringBuffer("");
        BufferedReader bufferedReader = null;
        HttpClient httpClient = new DefaultHttpClient();
        URI uri = new URI(url);
        HttpResponse httpResponse = makeRequest(uri, httpClient);
        InputStream inputStream = httpResponse.getEntity().getContent();
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String readLine = bufferedReader.readLine();
        while (readLine != null) {
            stringBuffer.append(readLine);
            stringBuffer.append("\n");
            readLine = bufferedReader.readLine();
        }
        setTaskData(stringBuffer.toString());
    }

    public JSONObject getBody() {
        return body;
    }

    public abstract HttpResponse makeRequest(URI uri, HttpClient httpClient) throws IOException;
}
