package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.entity.sprite.AnimatedSprite;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 27/05/2016.
 */
public class Diamond extends AnimatedSprite implements iTileChild {

    private GameActivity gameActivity;
    private int diamondID;
    private Integer[] colRow;

    @Override
    public boolean isBlocker() {
        return false;
    }

    @Override
    public Character getTileCode() {
        return TileCodes.Diamond.getCharacter();
    }

    public enum Color{
        GREY,
        GOLD
    }

    public Diamond(GameActivity gameActivity, int diamondID){
        super(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                gameActivity.getGraphicsManager().GetTiledTextureRegion(GraphicsManager.TiledTextureKeys.Anim_Diamond),
                gameActivity.getVertexBufferObjectManager());
        this.gameActivity = gameActivity;
        this.diamondID = diamondID;
    }

    public int GetDiamondId(){
        return diamondID;
    }

    @Override
    public Integer[] getColumnRow() {
        if(colRow == null){
            int[] a = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{getX(), getY()});
            colRow = new Integer[]{a[0], a[1]};
        }
        return colRow;
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.Gem;
    }
}
