package bbgames.ie.wordsmithnavigator.GameObjects.Graphics.SpriteFactory;

import android.util.Log;
import android.widget.Toast;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.batch.SpriteBatch;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;

import java.io.IOException;
import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * NOTE: draw() method anchors sprites on the bottom left, unlike normal sprites which anchors on the center of the sprite.
 * e.g. a tile (190 x 190) drawn at x:400 y:400 ... The bottom left of the tile is at 400, 400
 *      a tile sprite attached at x:400 y:400 ... The bottom left of the tile is at 305, 305
 * Created by Bryan on 02/09/2016.
 */
public class SpriteBatchManager {

    private static final String LOGTAG = SpriteBatchManager.class.getSimpleName();
    private static final int MAX_CAPACITY = 500;
    private static final String PATH_MAP_ASSET =     "gfx/theme/%d/tile_map.jpg";
    private static final String PATH_ENDZONE_ASSET = "gfx/theme/%d/tile_endzone.jpg";
    private static final String PATH_WALL_ASSET =    "gfx/theme/%d/tile_wall.jpg";
    private static final String PATH_BORDER_ASSET =  "gfx/common/border.jpg";
    private static final String PATH_CORNER_ASSET =  "gfx/common/border_corner.jpg";
    private static final float ARGB = 1f;

    GameActivity gameActivity;

    SpriteBatch sbMapTiles;
    SpriteBatch sbEndzoneTiles;
    SpriteBatch sbWallTiles;
    SpriteBatch sbBorder;
    SpriteBatch sbCorner;
    AssetBitmapTexture textureMap;
    AssetBitmapTexture textureEnd;
    AssetBitmapTexture textureWal;
    AssetBitmapTexture textureBor;
    AssetBitmapTexture textureCor;
    TextureRegion textureRegionMap;
    TextureRegion textureRegionEnd;
    TextureRegion textureRegionWal;
    TextureRegion textureRegionBor;
    TextureRegion textureRegionCor;

    public SpriteBatchManager(GameActivity gameActivity, int themeMap){
        this.gameActivity = gameActivity;
        try {
            textureMap = new AssetBitmapTexture(gameActivity.getTextureManager(), gameActivity.getAssets(), String.format(PATH_MAP_ASSET,     themeMap), TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            textureEnd = new AssetBitmapTexture(gameActivity.getTextureManager(), gameActivity.getAssets(), String.format(PATH_ENDZONE_ASSET, themeMap), TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            textureWal = new AssetBitmapTexture(gameActivity.getTextureManager(), gameActivity.getAssets(), String.format(PATH_WALL_ASSET,    themeMap), TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            textureBor = new AssetBitmapTexture(gameActivity.getTextureManager(), gameActivity.getAssets(), PATH_BORDER_ASSET,                           TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            textureCor = new AssetBitmapTexture(gameActivity.getTextureManager(), gameActivity.getAssets(), PATH_CORNER_ASSET,                           TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            textureRegionMap = TextureRegionFactory.extractFromTexture(this.textureMap);
            textureRegionEnd = TextureRegionFactory.extractFromTexture(this.textureEnd);
            textureRegionWal = TextureRegionFactory.extractFromTexture(this.textureWal);
            textureRegionBor = TextureRegionFactory.extractFromTexture(this.textureBor);
            textureRegionCor = TextureRegionFactory.extractFromTexture(this.textureCor);
            textureMap.load();
            textureEnd.load();
            textureWal.load();
            textureBor.load();
            textureCor.load();
            sbMapTiles = new SpriteBatch(textureMap, MAX_CAPACITY, gameActivity.getVertexBufferObjectManager());
            sbEndzoneTiles = new SpriteBatch(textureEnd, MAX_CAPACITY, gameActivity.getVertexBufferObjectManager());
            sbWallTiles = new SpriteBatch(textureWal, MAX_CAPACITY, gameActivity.getVertexBufferObjectManager());
            sbBorder = new SpriteBatch(textureBor, MAX_CAPACITY, gameActivity.getVertexBufferObjectManager());
            sbCorner = new SpriteBatch(textureCor, MAX_CAPACITY, gameActivity.getVertexBufferObjectManager());
        } catch (IOException e) {
            Log.e(LOGTAG, e.getMessage());
            Toast.makeText(gameActivity, "Error loading Sprite Batch Manager!", Toast.LENGTH_SHORT).show();
            gameActivity.finish();
        }
    }

    /**
     * There are 3 types of base tiles:
     * 1. Map Tile (Green)
     * 2. EndZone Tile (Yellow)
     * 3. Wall Tile (Grey)
     * Every tile on the board is one of these 3. Any other elements/sprites are children of these base tiles.
     */
    public void renderBaseTiles() {
        for(int row = 0; row < gameActivity.getLevelManager().getLevelStaticData().getRows(); row++){
            String r = gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal().get(row);
            for(int col = 0; col < r.length(); col++){
                Character tileCode = r.charAt(col);
                renderBaseTile(tileCode, row, col);
            }
        }
        sbMapTiles.submit();
        sbEndzoneTiles.submit();
        sbWallTiles.submit();
        sbBorder.submit();
        sbCorner.submit();
        gameActivity.getScene().attachChild(sbMapTiles);
        gameActivity.getScene().attachChild(sbEndzoneTiles);
        gameActivity.getScene().attachChild(sbWallTiles);
        gameActivity.getScene().attachChild(sbBorder);
        gameActivity.getScene().attachChild(sbCorner);
        gameActivity.getScene().registerTouchArea(sbMapTiles);
        gameActivity.getScene().registerTouchArea(sbEndzoneTiles);
        gameActivity.getScene().registerTouchArea(sbWallTiles);
        gameActivity.getScene().registerTouchArea(sbBorder);
        gameActivity.getScene().registerTouchArea(sbCorner);
    }

    private void renderBaseTile(Character tileCode, int row, int col) {
        float[] xy = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), row, col);
        float offset = Constant.TILE_SIZE / 2;
        xy = new float[]{xy[0] - offset, xy[1] - offset}; //Adjust for offset when using draw() method for sprites
        if(tileCode.equals(TileCodes.WallTile.getCharacter())){
            sbWallTiles.draw(textureRegionWal, xy[0], xy[1], Constant.TILE_SIZE, Constant.TILE_SIZE, ARGB, ARGB, ARGB, ARGB);
        }else if(tileCode.equals(TileCodes.EndZone.getCharacter())){
            sbEndzoneTiles.draw(textureRegionEnd, xy[0], xy[1], Constant.TILE_SIZE, Constant.TILE_SIZE, ARGB, ARGB, ARGB, ARGB);
        }else {
            sbMapTiles.draw(textureRegionMap, xy[0], xy[1], Constant.TILE_SIZE, Constant.TILE_SIZE, ARGB, ARGB, ARGB, ARGB);
        }
        renderAnyBorder(gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal(), row, col, xy);
    }

    private void renderAnyBorder(ArrayList<String> layout, int row, int col, float[] xy){
        Character n =  LevelUtils.getCharacterAt(layout, new int[]{col, row - 1});
        Character nw = LevelUtils.getCharacterAt(layout, new int[]{col - 1, row - 1});
        Character ne = LevelUtils.getCharacterAt(layout, new int[]{col + 1, row - 1});
        Character w =  LevelUtils.getCharacterAt(layout, new int[]{col - 1, row});
        Character e =  LevelUtils.getCharacterAt(layout, new int[]{col + 1, row});
        Character s =  LevelUtils.getCharacterAt(layout, new int[]{col, row + 1});
        Character sw = LevelUtils.getCharacterAt(layout, new int[]{col - 1, row + 1});
        Character se = LevelUtils.getCharacterAt(layout, new int[]{col + 1, row + 1});
        if(Util.isNullOrBlank(n)){
            sbBorder.draw(textureRegionBor,
                    xy[0],
                    xy[1] + (Constant.TILE_SIZE / 2) + (Constant.BORDER),
                    Constant.TILE_SIZE, Constant.BORDER, 0f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(w )){
            sbBorder.draw(textureRegionBor,
                    xy[0] - (Constant.TILE_SIZE / 2) - (Constant.BORDER / 2),
                    xy[1] + (Constant.BORDER / 2),
                    Constant.TILE_SIZE, Constant.BORDER, 90f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(e )){
            sbBorder.draw(textureRegionBor,
                    xy[0] + (Constant.TILE_SIZE / 2) + (Constant.BORDER / 2),
                    xy[1] + (Constant.BORDER / 2),
                    Constant.TILE_SIZE, Constant.BORDER, 270f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(s )){
            sbBorder.draw(textureRegionBor,
                    xy[0],
                    xy[1] - (Constant.TILE_SIZE / 2),
                    Constant.TILE_SIZE, Constant.BORDER, 180f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(nw) && Util.isNullOrBlank(n) && Util.isNullOrBlank(w)){
            sbCorner.draw(textureRegionCor,
                    xy[0] - Constant.BORDER,
                    xy[1] + Constant.TILE_SIZE,
                    Constant.BORDER, Constant.BORDER, 0f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(ne) && Util.isNullOrBlank(n) && Util.isNullOrBlank(e)){
            sbCorner.draw(textureRegionCor,
                    xy[0] + Constant.TILE_SIZE,
                    xy[1] + Constant.TILE_SIZE,
                    Constant.BORDER, Constant.BORDER, 270f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(sw) && Util.isNullOrBlank(s) && Util.isNullOrBlank(w)){
            sbCorner.draw(textureRegionCor,
                    xy[0] - Constant.BORDER,
                    xy[1] - Constant.BORDER,
                    Constant.BORDER, Constant.BORDER, 90f, ARGB, ARGB, ARGB, ARGB);
        }
        if(Util.isNullOrBlank(se) && Util.isNullOrBlank(s) && Util.isNullOrBlank(e)){
            sbCorner.draw(textureRegionCor,
                    xy[0] + Constant.TILE_SIZE,
                    xy[1] - Constant.BORDER,
                    Constant.BORDER, Constant.BORDER, 180f, ARGB, ARGB, ARGB, ARGB);
        }
    }

}
