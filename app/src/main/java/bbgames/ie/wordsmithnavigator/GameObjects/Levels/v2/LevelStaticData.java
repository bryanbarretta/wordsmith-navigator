package bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.BridgeLockTurns;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.DetonatorLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.Star;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.GateLockPoints;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * This data is loaded from the levels_en_uk.txt file
 * Level data that is absolutely static. Loaded once on app startup and then remains unchanged. All variables final.
 *  - Number
 *  - Original Layout
 *  - Dimensions, Bounds
 *  - Stars | Detonators | Gates | Timer
 * Created by Bryan on 19/08/2018.
 */

public class LevelStaticData implements Serializable{

    private final ArrayList<String> layoutOriginal;
    private final int id;
    private final int columns;
    private final int rows;
    private final Integer heightPixels;
    private final Integer widthPixels;
    private final Bounds bounds;
    private final String hardcodedLetters;
    private final String excludedLetters;
    private final int playerCount;

    private final HashMap<Integer, Star> stars;
    private final Integer clockSecondsPerTurn;
    private final ArrayList<DetonatorLink> detonatorLinks;
    private final ArrayList<GateLockPoints> pointsLocks;
    private final ArrayList<BridgeLockTurns> turnsLocks;
    private final ArrayList<PowerUpLink> powerUpLinks;
    private final int diamondCount;

    //region Constructor

    /**
     * Holds static level data: That is, data in the appropriate levels text file e.g. levels_en_uk.txt
     * @param layoutOriginal The String array definition of the level
     * @param id The number of the level. The first level in this game has id of 1; this is not zero-based index
     * @param rows total number of rows
     * @param columns total numbers of columns (i.e. length of longest row)
     * @param conditionalLetters special letters for certain letters e.g. Hardcoded order of letters drawn from sack, Excluded letters from sack
     * @param playerCount Number of players in this level
     * @param clockSecondsPerTurn -1 for infinite time. Otherwise the time limit per turn
     * @param detonatorLinks
     * @param stars
     * @param pointsLocks
     * @param turnsLocks
     * @param powerUpLinks
     * @param diamondCount
     */
    public LevelStaticData(ArrayList<String> layoutOriginal,
                           int id,
                           int rows,
                           int columns,
                           String conditionalLetters,
                           int playerCount,
                           Integer clockSecondsPerTurn,
                           ArrayList<DetonatorLink> detonatorLinks,
                           HashMap<Integer, Star> stars,
                           ArrayList<GateLockPoints> pointsLocks,
                           ArrayList<BridgeLockTurns> turnsLocks,
                           ArrayList<PowerUpLink> powerUpLinks,
                           int diamondCount) {
        this.layoutOriginal = layoutOriginal;
        this.id = id;
        this.rows = rows;
        this.columns = columns;
        if(conditionalLetters == null || conditionalLetters.isEmpty()){
            hardcodedLetters = "";
            excludedLetters = "";
        }else {
            if(conditionalLetters.trim().startsWith("!")){
                excludedLetters = conditionalLetters.substring(conditionalLetters.indexOf("!")+1).trim();
                hardcodedLetters = "";
            }else {
                excludedLetters = "";
                hardcodedLetters = conditionalLetters.trim();
            }
        }
        this.playerCount = playerCount;
        this.clockSecondsPerTurn = clockSecondsPerTurn;
        this.detonatorLinks = detonatorLinks;
        this.stars = stars;
        this.pointsLocks = pointsLocks;
        this.turnsLocks = turnsLocks;
        this.powerUpLinks = powerUpLinks;
        this.diamondCount = diamondCount;
        this.heightPixels = rows * Constant.TILE_SIZE;
        this.widthPixels = columns * Constant.TILE_SIZE;
        float[] bottomLeft = new float[]{0, 0};
        float[] topLeft = new float[]{0, heightPixels};
        float[] bottomRight = new float[]{widthPixels, 0};
        float[] topRight = new float[]{widthPixels, heightPixels};
        bounds = new Bounds(topLeft, topRight, bottomLeft, bottomRight);
    }

    public static LevelStaticData newInstance(ArrayList<String> arrLevel){
        ArrayList<String> mLayoutOriginal = new ArrayList<>();
        int mId = 0;
        String mHardcodedLetters = null;
        int mPlayerCount = 0;
        Integer mClockSecondsPerTurn = null;
        HashMap<Integer, Star> mStars = new HashMap<>();
        ArrayList<DetonatorLink> mDetonatorLinks = new ArrayList<>();
        ArrayList<GateLockPoints> mPointsLocks = new ArrayList<>();
        ArrayList<BridgeLockTurns> mTurnsLocks = new ArrayList<>();
        ArrayList<PowerUpLink> mPowerUpLinks = new ArrayList<>();
        int mDiamondCount = 0;
        int mRows = 0;
        int mColumns = 0;
        for(String line : arrLevel){
            if(line.startsWith("[")) {
                String key = line.trim().substring(1, line.trim().indexOf("]"));
                if (android.text.TextUtils.isDigitsOnly(key)) {
                    mId = Integer.parseInt(key);
                    mHardcodedLetters = line.substring(line.indexOf("]") + 1);
                }else {
                    switch (key.trim()){
                        case "*":
                            Star s = new Star(line.substring(3));
                            mStars.put(s.getId(), s);
                            break;
                        case "!":
                            mDetonatorLinks.add(new DetonatorLink(mLayoutOriginal, line.substring(3)));
                            break;
                        case "%":
                            mPointsLocks.add(new GateLockPoints(line.substring(3)));
                            break;
                        case "=":
                            mTurnsLocks.add(new BridgeLockTurns(line.substring(3)));
                            break;
                        case "S":
                            mClockSecondsPerTurn = Integer.valueOf(line.substring(line.indexOf("=") + 1).trim());
                            break;
                        case "P":
                        case "T":
                        case "M":
                        case "L":
                            mPowerUpLinks.addAll(PowerUpLink.build(line));
                            break;
                    }
                }
            }else {
                mLayoutOriginal.add(line);
                if(line.contains(TileCodes.StartingZone.getCharacter().toString())){
                    mPlayerCount++;
                }
                if(line.contains(TileCodes.StartingZoneCPU.getCharacter().toString())){
                    mPlayerCount++;
                }
                if(line.contains(TileCodes.StartingZoneP2.getCharacter().toString())){
                    mPlayerCount++;
                }
                if(line.length() > mColumns){
                    mColumns = line.length();
                }
                if(line.contains(TileCodes.Diamond.getCharacter().toString())){
                    mDiamondCount += Util.countMatches(line, TileCodes.Diamond.getCharacter());
                }
                mRows++;
            }
        }
        return new LevelStaticData(mLayoutOriginal, mId, mRows, mColumns, mHardcodedLetters, mPlayerCount, mClockSecondsPerTurn, mDetonatorLinks, mStars, mPointsLocks, mTurnsLocks, mPowerUpLinks, mDiamondCount);
    }

    //endregion

    //region Getters

    public ArrayList<String> getLayoutOriginal() {
        return layoutOriginal;
    }

    public int getId() {
        return id;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public Integer getHeightPixels() {
        return heightPixels;
    }

    public Integer getWidthPixels() {
        return widthPixels;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public String getHardcodedLetters() {
        return hardcodedLetters;
    }

    public String getExcludedLetters() {
        return excludedLetters;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public HashMap<Integer, Star> getStars() {
        return stars;
    }

    public Star getStar(int id) {
        return stars.get(id);
    }

    public Integer getClockSecondsPerTurn() {
        return clockSecondsPerTurn;
    }

    public ArrayList<DetonatorLink> getDetonatorLinks() {
        return detonatorLinks;
    }

    public ArrayList<GateLockPoints> getPointsLocks() {
        return pointsLocks;
    }

    public ArrayList<PowerUpLink> getPowerUpLinks(){
        return powerUpLinks;
    }

    public ArrayList<BridgeLockTurns> getTurnsLocks() {
        return turnsLocks;
    }

    public int getDiamondCount() {
        return diamondCount;
    }

    //endregion

    //region Helper classes

    /**
     * Holds the x, y pixels of the level bounds
     */
    public static class Bounds implements Serializable {

        private float[] topLeft;
        private float[] topRight;
        private float[] bottomLeft;
        private float[] bottomRight;

        public Bounds(float[] topLeft, float[] topRight, float[] bottomLeft, float[] bottomRight){
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomLeft = bottomLeft;
            this.bottomRight = bottomRight;
        }

        public float[] getTopLeft() {
            return topLeft;
        }

        public float[] getTopRight() {
            return topRight;
        }

        public float[] getBottomLeft() {
            return bottomLeft;
        }

        public float[] getBottomRight() {
            return bottomRight;
        }

        public float getCenterX(){
            return topLeft[0] + ((topRight[0] - topLeft[0]) / 2);
        }

        public float getCenterY(){
            return bottomRight[1] + ((topRight[1] - bottomRight[1]) / 2);
        }
    }

    //endregion

}
