package bbgames.ie.wordsmithnavigator.GameObjects.Players;

import android.util.Pair;

import org.andengine.opengl.texture.region.TextureRegion;

import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.Animation.AnimationManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.LetterTileOnBoard;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.GameObjects.Misc.LetterSack;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 08/08/2016.
 */
public abstract class Player {

    protected int id;
    protected PlayerType playerType;
    protected String name;
    protected GraphicsManager.TextureKeys iconKey;
    protected TextureRegion icon, icHudBackground;
    protected GameActivity gameActivity;
    protected LetterSack letterSack;
    protected SkinLetter letterSkin;
    protected SkinHUD themePlayer;

    protected ArrayList<int[]> listXyLetterTilesOnBoard = new ArrayList<>();
    protected int totalPoints = 0;
    protected int totalSeconds = 0;
    protected int totalMoves = 0;
    private int bonusSecondsNextTurn = 0;

    protected int[] xyStart;
    protected int[] xyTarget;
    protected ArrayList<HintWord> playedWords = new ArrayList<>();

    //Stores each tile letter on the rack, mapped to their position
    HashMap<Integer, TileLetter> mapHudLetterTiles = new HashMap<>();
    //Stores tile letters that have been removed from the rack and placed on the board for this turn
    ArrayList<LetterTileOnBoard> hudLetterTilesOnBoard = new ArrayList<>();

    private Integer[] nextTileToPanTo;

    public Player(int id, PlayerType playerType, String name, GameActivity gameActivity, int[] xyStart, SkinLetter letterSkin, SkinHUD themePlayer, GraphicsManager.TextureKeys iconKey){
        this.id = id;
        this.playerType = playerType;
        this.name = name;
        this.gameActivity = gameActivity;
        this.letterSkin = letterSkin;
        this.letterSack = new LetterSack(gameActivity.getLevelManager().getLevelStaticData());
        this.xyStart = xyStart;
        this.themePlayer = themePlayer;
        this.iconKey = iconKey;
    }

    //region Getters

    public int getId() {
        return id;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public int getTotalSeconds() {
        return totalSeconds;
    }

    public int getTotalMoves() {
        return totalMoves;
    }

    public void addBonusSecondsToNextTurn(int bonusSecondsNextTurn) {
        this.bonusSecondsNextTurn = bonusSecondsNextTurn;
    }

    public int getBonusSecondsDue(){
        final int output = bonusSecondsNextTurn;
        bonusSecondsNextTurn = 0;
        return output;
    }

    public String getHudLetters(){
        String s = "";
        for(int i = 0; i < mapHudLetterTiles.size(); i++){
            s += mapHudLetterTiles.get(i).getLetter();
        }
        return s;
    }

    public HashMap<Integer, TileLetter> getMapHudLetterTiles() {
        return mapHudLetterTiles;
    }

    public ArrayList<LetterTileOnBoard> getHudLetterTilesOnBoard() {
        if(hudLetterTilesOnBoard == null){
            hudLetterTilesOnBoard = new ArrayList<>();
        }
        return hudLetterTilesOnBoard;
    }

    public boolean isHumanUser(){
        return playerType.equals(PlayerType.USER);
    }

    public PlayerType getPlayerType(){
        return playerType;
    }

    public ArrayList<int[]> getListXYLetterTilesOnBoard(){
        return listXyLetterTilesOnBoard;
    }

    public int[] getXYLastLetterTilePlayed(){
        return listXyLetterTilesOnBoard.get(listXyLetterTilesOnBoard.size() - 1);
    }

    public SkinLetter getLetterTileSkin(){
        return letterSkin;
    }

    public String getName(){
        if(Util.isNullOrEmpty(name)){
            return String.format(gameActivity.getString(R.string.playerN), "" + (id + 1));
        }
        return name;
    }

    public SkinHUD getThemePlayer() {
        return themePlayer;
    }

    /**
     * @return All of the players words on the boards. This includes the automatic starting word
     */
    public ArrayList<HintWord> getPlayedWords() {
        if(playedWords.isEmpty() && listXyLetterTilesOnBoard.size() > 0){
            playedWords.add(LevelUtils.getInitialBoardWord(Util.getApplicationManager(gameActivity).getDatabaseGateway(),
                    xyStart,
                    gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal()));
        }
        return playedWords;
    }

    //endregion

    //region Setters

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public void setTotalSeconds(int totalSeconds) {
        this.totalSeconds = totalSeconds;
    }

    public void setTotalMoves(int totalMoves) {
        this.totalMoves = totalMoves;
    }

    public void updateListXYLetterTilesOnBoard(int[] newXY){
        if(!Util.contains(listXyLetterTilesOnBoard, newXY)){
            listXyLetterTilesOnBoard.add(newXY);
        }
    }

    /**
     * If there are no hud tiles for this player (i.e. on level start), adds 10 hud tiles
     * else update hud tile state for the player (e.g. Lock if animation in progress etc.)
     */
    public void addUpdateHudTiles(){
        if(getMapHudLetterTiles().isEmpty()){
            for(int i = 0; i < gameActivity.getHUD().GetHudLetterCapacity(); i++){
                float[] xy = gameActivity.getHUD().getMapTileRackLetterCoordinates().get(i);
                TileLetter t = gameActivity.getGraphicsManager().getLetterFactory().createLetterHUD(letterSkin,
                        letterSack.getLetterBalancingVowelsAndConsonants(getMapHudLetterTiles()),
                        xy[0], xy[1], LetterState.HIDDEN, this);
                mapHudLetterTiles.put(i, t);
            }
        }else {
            for(int i = 0; i < getMapHudLetterTiles().size(); i++){
                TileLetter t = getMapHudLetterTiles().get(i);
                t.setLetter(t.getLetter());
                t.updateState(this);
            }
            getHudLetterTilesOnBoard().clear();
        }
    }

    public void addPlayedWord(HintWord hintWord) {
        playedWords.add(hintWord);
    }

    public void setTargetXY(int[] xyTarget){
        this.xyTarget = xyTarget;
    }

    public int[] getTargetXY() {
        return LevelUtils.getOptimalTargetXY(gameActivity.getLevelManager().getLayoutModified(),
                    getListXYLetterTilesOnBoard().get(getListXYLetterTilesOnBoard().size() - 1));
    }

    public TextureRegion getIcon() {
        if(icon == null){
            icon = gameActivity.getGraphicsManager().GetTextureRegion(iconKey);
        }
        return icon;
    }

    public TextureRegion getIcHudBackground(){
        if(icHudBackground == null){
            if (themePlayer.ordinal() == gameActivity.getGraphicsManager().GetDefaultThemeHudId()) {
                icHudBackground = gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudBackground);
            } else {
                icHudBackground = gameActivity.getGraphicsManager().LoadDynamicTextureRegion("gfx/hud/" + themePlayer.ordinal() + "/",
                        "hud_bg.jpg", Constant.HUD_WIDTH, Constant.CAMERA_1080);
            }
        }
        return icHudBackground;
    }

    public HintLetter getLastLetterPlayed() {
        HintWord lastWord = getPlayedWords().get(getPlayedWords().size() - 1);
        return lastWord.getLetters().get(lastWord.getLetters().size() - 1);
    }

    public HintLetter getMiddleLetterOfLastWordPlayed(){
        HintWord lastWord = getPlayedWords().get(getPlayedWords().size() - 1);
        return lastWord.getLetters().get(lastWord.getLetters().size() / 2);
    }

    public LetterSack getLetterSack(){
        return letterSack;
    }

    public void setNextTileToPanTo(Integer[] nextTileToPanTo) {
        this.nextTileToPanTo = nextTileToPanTo;
    }

    public Integer[] getNextTileToPanTo() {
        return nextTileToPanTo;
    }

    //endregion

}
