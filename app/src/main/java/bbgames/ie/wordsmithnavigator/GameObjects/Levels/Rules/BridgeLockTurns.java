package bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules;

/**
 * Logic class for underlying BridgeLockTurns mechanism
 */
public class BridgeLockTurns {

    private final int[] xyBridge;
    private final int mOriginalTurns;

    /**
     * @param key "[bridgeX, bridgeY]->[turns]"      e.g. "[3,4]->[40]"
     */
    public BridgeLockTurns(String key){
        String[] parts = key.trim().split("->");
        String bridgeRule = parts[0].trim();
        xyBridge = new int[]{
                Integer.valueOf(bridgeRule.substring(1, bridgeRule.indexOf(",")).trim()),
                Integer.valueOf(bridgeRule.substring(bridgeRule.indexOf(",") + 1, bridgeRule.length()-1))
        };
        mOriginalTurns = Integer.parseInt(parts[1].trim().substring(1, parts[1].trim().indexOf("]")));
    }

    public int[] getXYBridge() {
        return xyBridge;
    }

    public int getOriginalTurns() {
        return mOriginalTurns;
    }
}
