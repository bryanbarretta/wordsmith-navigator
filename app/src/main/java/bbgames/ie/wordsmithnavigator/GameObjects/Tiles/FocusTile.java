package bbgames.ie.wordsmithnavigator.GameObjects.Tiles;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 09/08/2018.
 */

public class FocusTile {

    private int[] xy;
    private Direction direction;

    public FocusTile(int[] xy, Direction direction) {
        this.xy = xy;
        this.direction = direction;
    }

    public int[] getXy() {
        return xy;
    }

    public Direction getDirection() {
        return direction;
    }

    /**
     * @return a list of xy's of the sub focus tiles; i.e. The xy of every playable tile after this.xy in this.direction (stop at the first blocker tile)
     */
    public ArrayList<int[]> getSubFocusTileXYs(ArrayList<String> layoutModified) {
        ArrayList<int[]> output = new ArrayList<>();
        int[] nextXy = LevelUtils.incrementXY(xy, direction);
        Character next = LevelUtils.getCharacterAt(layoutModified, nextXy);
        while (next != null && !TileCodes.getBlockTileCodes().contains(next)){
            //Keep going until a blocker tile is met
            output.add(nextXy);
            nextXy = LevelUtils.incrementXY(nextXy, direction);
            next = LevelUtils.getCharacterAt(layoutModified, nextXy);
        }
        return output;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof FocusTile) {
            FocusTile f = (FocusTile) o;
            return equals(f.xy[1], f.xy[0], f.direction);
        }
        return false;
    }

    public boolean equals(int col, int row, Direction direction) {
        return this.xy[1] == col && this.xy[0] == row && this.direction.equals(direction);
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
