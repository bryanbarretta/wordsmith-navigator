package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 17/07/2016.
 */
public class DetonatorCrate extends Sprite implements iTileChild {

    private GameActivity gameActivity;
    private Integer[] colRow;

    public DetonatorCrate(GameActivity gameActivity) {
        super(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.WallDynamite),
                gameActivity.getVertexBufferObjectManager());
        this.gameActivity = gameActivity;
    }

    public void detonate(){
        final AnimatedSprite fire = new AnimatedSprite(getX(), getY(),
                gameActivity.getGraphicsManager().GetTiledTextureRegion(GraphicsManager.TiledTextureKeys.Anim_Explosion),
                this.getVertexBufferObjectManager());
        gameActivity.getScene().attachChild(fire);
        fire.animate(50, false, new AnimatedSprite.IAnimationListener() {
            @Override
            public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
                                           int pInitialLoopCount) {
            }
            @Override
            public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
                                                int pRemainingLoopCount, int pInitialLoopCount) {
            }
            @Override
            public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
                                                int pOldFrameIndex, int pNewFrameIndex) {
            }
            @Override
            public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
                gameActivity.runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.getRubbish().RecycleSprite(fire);
                    }
                });
            }
        });
        gameActivity.getScene().detachTileChild(DetonatorCrate.this);
    }

    @Override
    public boolean isBlocker() {
        return true;
    }

    @Override
    public Character getTileCode() {
        return TileCodes.DetonatorCrate.getCharacter();
    }

    @Override
    public Integer[] getColumnRow() {
        if(colRow == null){
            int[] a = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{getX(), getY()});
            colRow = new Integer[]{a[0], a[1]};
        }
        return colRow;
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.DetonatorCrate;
    }
}