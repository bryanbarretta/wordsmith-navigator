package bbgames.ie.wordsmithnavigator.GameObjects.Tiles;

import bbgames.ie.wordsmithnavigator.Constants.TileChildType;

public interface iTileChild {

    /**
     * This child object acts like a wall i.e. blocks words/highlights etc.
     * @return
     */
    boolean isBlocker();

    /**
     * Refer to the TileCodes enum to see corresponding value. letter tiles will be [a-zA-Z]
     * @return
     */
    Character getTileCode();

    /**
     * @return null if this child is not on the board, otherwise return the column & row of the board it is on
     */
    Integer[] getColumnRow();

    /**
     * @return
     */
    TileChildType getTileChildType();

}
