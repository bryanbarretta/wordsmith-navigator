package bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.Util.Util;
import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;

/**
 * Created by Bryan on 10/03/2017.
 * A detailed representation of a potential word or word locked on board, in the form of HintLetters
 */
public class HintWord {

    private DictionaryEntry dictionaryEntry;
    private ArrayList<HintLetter> letters;
    private Float totalDesirability;
    private Float averageDesirability;
    private Integer points;

    public HintWord(final HintLetter letter){
        this(new ArrayList<HintLetter>(){{add(letter);}});
    }

    public HintWord(){
        this.letters = new ArrayList<>();
    }

    public HintWord(ArrayList<HintLetter> letters){
        this.letters = letters;
    }

    public HintWord(ArrayList<HintLetter> letters, DatabaseGateway databaseGateway){
        this.letters = letters;
        this.dictionaryEntry = databaseGateway.getDictionaryEntry(new HintWord(letters).toString());
    }

    public HintWord(HintWord hintWord, DatabaseGateway databaseGateway){
        this(hintWord, databaseGateway.getDictionaryEntry(hintWord.toString()));
    }

    public HintWord(HintWord hintWord, DictionaryEntry answer){
        ArrayList<HintLetter> hintLetters = new ArrayList<>();
        for(HintLetter h : hintWord.getLetters()){
            hintLetters.add(new HintLetter(h));
        }
        this.dictionaryEntry = answer;
        this.letters = hintLetters;
    }

    @Override
    public String toString() {
        String s = "";
        for(HintLetter l : letters){
            s += l.getLetter();
        }
        return s;
    }

    public void addLetter(HintLetter letter){
        addLetter(letter, false);
    }

    public void addLetter(HintLetter letter, boolean addToStart){
        if(addToStart){
            letters.add(0, letter);
        }else {
            letters.add(letter);
        }
    }

    public ArrayList<HintLetter> getLetters(){
        return letters;
    }

    public HintLetter getFirstLetter(){
        if(letters.isEmpty()){
            return null;
        }
        return letters.get(0);
    }

    public HintLetter getLastLetter(){
        if(letters.isEmpty()){
            return null;
        }
        return letters.get(letters.size() - 1);
    }

    public int getPoints(){
        if(letters.isEmpty()){
            return 0;
        }
        if(points == null){
            points = Util.getPointsForWord(toString());
        }
        return points;
    }

    public DictionaryEntry getDictionaryEntry(){
        return dictionaryEntry;
    }

    public void removeLetter(HintLetter potentialLetter) {
        if(letters.contains(potentialLetter)){
            letters.remove(potentialLetter);
        }
    }

    public static HintWord clone(HintWord boardLetters) {
        ArrayList<HintLetter> letters = new ArrayList<>();
        for(HintLetter l : boardLetters.getLetters()){
            letters.add(new HintLetter(l));
        }
        return new HintWord(letters);
    }

    public Float getTotalDesirability() {
        if(totalDesirability == null){
            totalDesirability = 0.0f;
            for(HintLetter l : letters){
                totalDesirability += l.getDesirabilityScore();
            }
        }
        return totalDesirability;
    }

    /**
     * @return true if any of the letters in this word will land on the map tile xy argument
     */
    public boolean hasLetterTouchingXY(int[] nextPathTurnXY) {
        for (HintLetter l : getLetters()){
            if(l.getXY()[0] == nextPathTurnXY[0] && l.getXY()[1] == nextPathTurnXY[1]){
                return true;
            }
        }
        return false;
    }

    public void setDictionaryEntry(DictionaryEntry dictionaryEntry) {
        this.dictionaryEntry = dictionaryEntry;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof HintWord){
            HintWord other = (HintWord) o;
            if(letters.size() != other.letters.size()){
                return false;
            }
            for(int i = 0; i < letters.size(); i++){
                if(!letters.get(i).equals(other.letters.get(i))){
                    return false;
                }
            }
            return dictionaryEntry.equals(other.dictionaryEntry);
        }else {
            return false;
        }
    }
}
