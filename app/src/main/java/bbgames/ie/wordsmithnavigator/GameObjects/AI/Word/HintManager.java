package bbgames.ie.wordsmithnavigator.GameObjects.AI.Word;

import android.os.AsyncTask;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuLoggerInterface;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;

/**
 * Given current level data, find valid playable words either synchronously or asynchronously
 * Created by Bryan on 20/09/2018.
 */
public abstract class HintManager {

    CpuLoggerInterface cpuLogger;
    HintSQLHandler hintSQLHandler;

    public HintManager(CpuLoggerInterface cpuLogger) {
        this.cpuLogger = cpuLogger;
    }

    /**
     * Finds words in a background async task then calls onWordsFound() some time later when it is ready
     * @param layout The board layout as it currently is
     * @param xyFocusTile Focus Tile XY
     * @param direction Focus Tile Direction
     * @param databaseGateway db access
     * @param hudLetters Players letters on the HUD
     * @param minFrequencyOfWord
     */
    public void findWordsAsynchronously(ArrayList<String> layout,
                                        final int[] xyFocusTile,
                                        Direction direction,
                                        DatabaseGateway databaseGateway,
                                        String hudLetters,
                                        double minFrequencyOfWord){
        new FindWordsAsyncTask(layout, xyFocusTile, direction, databaseGateway, hudLetters, minFrequencyOfWord).execute();
    }

    public void findWordsAsynchronously(GameActivity gameActivity){
        findWordsAsynchronously(gameActivity.getLevelManager().getLayoutModified(),
                gameActivity.getLevelManager().getFocusTile().getXy(),
                gameActivity.getLevelManager().getFocusTile().getDirection(),
                gameActivity.getApplicationManager().getDatabaseGateway(),
                gameActivity.getPlayerManager().getActivePlayer().getHudLetters(),
                gameActivity.getPlayerManager().getActivePlayer().isHumanUser() ?
                        0 :
                        gameActivity.getDifficulty().getMinFrequencyWord(gameActivity.getApplicationManager().getLanguage()));
    }

    /**
     * Finds words in the current thread synchronously, so onWordFounds() will be the next immediate code executed after calling this
     * @param layout The board layout as it currently is
     * @param xyFocusTile Focus Tile XY
     * @param direction Focus Tile Direction
     * @param databaseGateway db access
     * @param hudLetters Players letters on the HUD
     * @param minFrequencyOfWord
     */
    public void findWordsSynchronously(ArrayList<String> layout,
                                       final int[] xyFocusTile,
                                       Direction direction,
                                       DatabaseGateway databaseGateway,
                                       String hudLetters,
                                       double minFrequencyOfWord){
        this.hintSQLHandler = new HintSQLHandler(layout, direction, xyFocusTile, hudLetters, databaseGateway, minFrequencyOfWord, cpuLogger);
        onWordsFound(hintSQLHandler.getHints());
    }

    public HintSQLHandler getHintSQLHandler(){
        return hintSQLHandler;
    }

    public abstract void onWordsFound(ArrayList<HintWord> hints);

    /**
     * AsyncTask to find words
     */
    private class FindWordsAsyncTask extends AsyncTask<Void, Void, ArrayList<HintWord>> {

        private final ArrayList<String> layout;
        private final int[] xyFocusTile;
        private final Direction direction;
        private final DatabaseGateway databaseGateway;
        private final String hudLetters;
        private final double minFrequencyOfWord;

        public FindWordsAsyncTask(ArrayList<String> layout,
                                  int[] xyFocusTile,
                                  Direction direction,
                                  DatabaseGateway databaseGateway,
                                  String hudLetters,
                                  double minFrequencyOfWord){
            this.layout = layout;
            this.xyFocusTile = xyFocusTile;
            this.direction = direction;
            this.databaseGateway = databaseGateway;
            this.hudLetters = hudLetters;
            this.minFrequencyOfWord = minFrequencyOfWord;
        }

        @Override
        protected ArrayList<HintWord> doInBackground(Void... voids) {
            HintManager.this.hintSQLHandler = new HintSQLHandler(layout, direction, xyFocusTile, hudLetters, databaseGateway, minFrequencyOfWord , cpuLogger);
            return hintSQLHandler.getHints();
        }

        @Override
        protected void onPostExecute(ArrayList<HintWord> result) {
            super.onPostExecute(result);
            onWordsFound(result);
        }
    }

}
