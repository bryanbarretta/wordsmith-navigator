package bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Object to map a detonator to its crates
 * Created by Bryan on 11/03/2018.
 */

public class DetonatorLink {

    private int[] xyDetonator;
    private ArrayList<int[]> xyCrates;

    /**
     * @param key "[detonatorX, detonatorY]->[crateX, crateY]"      e.g. "[3,4]->[8,8]"
     */
    public DetonatorLink(ArrayList<String> layout, String key){
        String[] parts = key.trim().split("->");
        String detonator = parts[0].trim();
        xyDetonator = new int[]{
                Integer.valueOf(detonator.substring(1, detonator.indexOf(",")).trim()),
                Integer.valueOf(detonator.substring(detonator.indexOf(",") + 1, detonator.length()-1))
        };
        String crate = parts[1].trim();
        int[] xyCrate = new int[]{
                Integer.valueOf(crate.substring(1, crate.indexOf(",")).trim()),
                Integer.valueOf(crate.substring(crate.indexOf(",") + 1, crate.length()-1))
        };
        if(!LevelUtils.getCharacterAt(layout, xyDetonator).equals(TileCodes.Detonator.getCharacter())){
            throw new RuntimeException(xyCrate.toString() + " does not match detonator tile on layout");
        }
        if(!LevelUtils.getCharacterAt(layout, xyCrate).equals(TileCodes.DetonatorCrate.getCharacter())){
            throw new RuntimeException(xyCrate.toString() + " does not match detonator crate tile on layout");
        }
        xyCrates = LevelUtils.getXYsOfTouchingTilesOfTypeRecursively(layout, xyCrate, TileCodes.DetonatorCrate);

    }

    public int[] getXyDetonator() {
        return xyDetonator;
    }

    public ArrayList<int[]> getXyCrates() {
        return xyCrates;
    }

}
