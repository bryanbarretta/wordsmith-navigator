package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.entity.sprite.Sprite;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 17/07/2016.
 */
public class Detonator extends Sprite implements iTileChild {

    private GameActivity gameActivity;
    private Integer[] colRow;

    public Detonator(GameActivity gameActivity) {
        super(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Detonator),
                gameActivity.getVertexBufferObjectManager());
        this.setCullingEnabled(true);
        this.gameActivity = gameActivity;
    }

    @Override
    public boolean isBlocker() {
        return false;
    }

    @Override
    public Character getTileCode() {
        return TileCodes.Detonator.getCharacter();
    }

    @Override
    public Integer[] getColumnRow() {
        if(colRow == null){
            int[] a = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{getX(), getY()});
            colRow = new Integer[]{a[0], a[1]};
        }
        return colRow;
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.Detonator;
    }
}
