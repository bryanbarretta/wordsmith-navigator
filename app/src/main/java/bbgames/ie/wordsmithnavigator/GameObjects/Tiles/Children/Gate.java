package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.util.GLState;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.GateLockPoints;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 18/08/2018.
 */

public class Gate extends AnimatedSprite implements iTileChild {

    private final int row, col;
    private GameActivity gameActivity;
    private Boolean isHorizontalAligned;
    private GateLockPoints mPointsLock;
    private int mCurrentPoints;
    private Text text;

    public Gate(GameActivity gameActivity, int row, int col) {
        super(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                gameActivity.getGraphicsManager().GetTiledTextureRegion(GraphicsManager.TiledTextureKeys.Gate),
                gameActivity.getVertexBufferObjectManager());
        this.gameActivity = gameActivity;
        this.row = row;
        this.col = col;
        this.isHorizontalAligned = isHorizontalAligned();
        this.mPointsLock = gameActivity.getLevelManager().getPointsLock(new int[]{col, row});
        this.mCurrentPoints = mPointsLock.getOriginalPoints();
        setText(mCurrentPoints);
        if(!isHorizontalAligned){
            setRotation(90f);
            text.setRotation(-90f);
        }
        this.setCullingEnabled(true);
    }

    @Override
    protected void preDraw(final GLState pGLState, final Camera pCamera)
    {
        super.preDraw(pGLState, pCamera);
        pGLState.enableDither();
    }

    @Override
    public boolean isBlocker() {
        return mCurrentPoints > 0;
    }

    @Override
    public Character getTileCode() {
        return TileCodes.Gate.getCharacter();
    }

    public void setText(int points){
        if(text == null){
            text = new Text(getWidth() / 2, getHeight() / 2,
                    gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.litsans, Constant.COLOR_BLACK),
                    points == 0 ? "" : Util.toNDecimalPlaces(points, 2),
                    2,
                    gameActivity.getVertexBufferObjectManager());
            attachChild(text);
            return;
        }
        text.setText(points == 0 ? "" : Util.toNDecimalPlaces(points, 2));
    }

    private boolean isHorizontalAligned(){
        if(this.isHorizontalAligned != null){
            return this.isHorizontalAligned;
        }
        Character cLeft = BoardUtils.getAdjacentCharacter(gameActivity.getLevelManager().getLayoutModified(),
                new int[]{col, row}, Direction.WEST);
        if(TileCodes.getPlayableTileCodes().contains(cLeft)){
            //The tile to the left is playable, so the top tile is our anchor: vertical
            isHorizontalAligned = false;
        }else {
            isHorizontalAligned = true;
        }
        return isHorizontalAligned;
    }

    public void openAnimation() {
        this.animate(30, false, new IAnimationListener() {
            @Override
            public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {

            }
            @Override
            public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {

            }
            @Override
            public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {

            }
            @Override
            public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
                gameActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setVisible(false);
                        gameActivity.getScene().detachTileChild(row, col, Gate.this);
                        gameActivity.getLevelManager().getGates().remove(Gate.this);
                    }
                });
            }
        });
    }

    public int getCurrentPoints() {
        return mCurrentPoints;
    }

    public void setCurrentPoints(int currentPoints) {
        this.mCurrentPoints = currentPoints;
    }

    public GateLockPoints getPointsLock() {
        return mPointsLock;
    }

    @Override
    public Integer[] getColumnRow() {
        return new Integer[]{col, row};
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.Gate;
    }
}
