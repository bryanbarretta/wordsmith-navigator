package bbgames.ie.wordsmithnavigator.GameObjects.AI.AStar;

/**
 * Node Class
 *
 * @author Marcelo Surriabre
 * @version 2.0, 2018-02-23
 */
public class Node {

    private int p;      //penalty cost + parent.p;              Penalty (cumulative) cost
    private int f;      //p + h;                                Final cost
    private int h;      //nRows + nColumns from final node;     Heuristic cost

    private int row;
    private int col;

    private boolean isBlock;
    private boolean isLetter;
    private Node parent;

    public Node(int row, int col) {
        super();
        this.row = row;
        this.col = col;
    }

    public void calculateHeuristic(Node finalNode) {
        this.h = Math.abs(finalNode.getRow() - getRow()) + Math.abs(finalNode.getCol() - getCol());
    }

    /**
     * Set node passed in as parent, and apply cost to p, f
     */
    public void setNodeData(Node currentNode, int cost) {
        int pCost = currentNode.getP() + cost;      //0 + 10 (HV)
        setParent(currentNode);
        setP(pCost);
        calculateFinalCost();
    }

    /**
     * This Node already has a parent node assigned. Now we want to check if assigning currentNode as parent will yield a lower cost.
     * @param currentNode
     * @param cost The cost that would be applied
     * @return
     */
    public boolean checkBetterPath(Node currentNode, int cost) {
        int pCost = currentNode.getP() + cost;
        if (pCost < getP()) {
            setNodeData(currentNode, cost);
            return true;
        }
        if (pCost == getP()){
            //Its a tie: follow the chain back to see which has been the better path up til now
            Node next = currentNode;
        }
        return false;
    }

    private void calculateFinalCost() {
        int finalCost = getP() + getH();
        setF(finalCost);
    }

    @Override
    public boolean equals(Object arg0) {
        Node other = (Node) arg0;
        return this.getRow() == other.getRow() && this.getCol() == other.getCol();
    }

    @Override
    public String toString() {
        return String.format("[%s,%s]", col, row);
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean isBlock) {
        this.isBlock = isBlock;
    }

    public boolean isLetter() {
        return isLetter;
    }

    public void setLetter(boolean letter) {
        isLetter = letter;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }
}