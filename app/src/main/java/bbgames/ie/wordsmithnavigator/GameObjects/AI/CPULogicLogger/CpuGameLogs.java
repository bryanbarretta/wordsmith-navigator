package bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Constants.LoggingModes;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;

/**
 * Created by Bryan on 16/06/2018.
 */

public class CpuGameLogs {

    private String gameName;
    private HashMap<Integer, CpuTurnLog> turnLogs;
    private DatabaseGateway databaseGateway;
    private boolean loggingEnabled = false;

    public CpuGameLogs(String gameName, boolean loggingEnabled){
        this.gameName = gameName;
        this.loggingEnabled = loggingEnabled;
        this.turnLogs = new HashMap<>();
    }

    public void log(int turn, CpuTurnLog.LogType logType, String value) {
        if(!loggingEnabled){
            return;
        }
        if(!turnLogs.containsKey(turn)){
            turnLogs.put(turn, new CpuTurnLog());
        }
        turnLogs.get(turn).getMapFieldValues().put(logType, value);
        if(loggingEnabled){
            Log.d(getClass().getSimpleName(), gameName + " Turn [" + turn + "] - " + logType + "\n\t" + value);
            databaseGateway.writeCpuTurnLog(gameName, turn, logType, value);
        }
    }

    public void loadIntoTurnLogs(int turn, CpuTurnLog.LogType logType, String value){
        if(!turnLogs.containsKey(turn)){
            turnLogs.put(turn, new CpuTurnLog());
        }
        turnLogs.get(turn).getMapFieldValues().put(logType, value);
    }

    public String getGameName(){
        return gameName;
    }

    public HashMap<Integer, CpuTurnLog> getTurnLogs(){
        return turnLogs;
    }

    public boolean enableWriteToDatabase(DatabaseGateway databaseGateway) {
        if(!loggingEnabled){
            return false;
        }
        this.databaseGateway = databaseGateway;
        databaseGateway.writeCpuGameLogs(this);
        return true;
    }
}
