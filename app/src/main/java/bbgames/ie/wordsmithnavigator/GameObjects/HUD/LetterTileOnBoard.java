package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;

/**
 * Created by Bryan on 23/05/2016.
 * When a letter tile is played from the hud to the board, this stores the hud sprite and the corresponding mapTile its been played to.
 */
public class LetterTileOnBoard {

    private TileLetter HudLetterTile;
    private int[] boardColRow;

    public LetterTileOnBoard(TileLetter hudLetterTile, int[] boardColRow){
        this.HudLetterTile = hudLetterTile;
        this.boardColRow = boardColRow;
    }

    public TileLetter getHudLetterTile() {
        return HudLetterTile;
    }

    public int[] getBoardColRow() {
        return boardColRow;
    }
}
