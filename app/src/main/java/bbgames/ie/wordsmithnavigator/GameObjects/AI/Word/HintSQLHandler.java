package bbgames.ie.wordsmithnavigator.GameObjects.AI.Word;

import android.database.Cursor;
import android.util.Log;
import android.util.Pair;

import org.andengine.util.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.Database.TableDictionary;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuLoggerInterface;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;

/**
 * Represents a line of tiles in the board (horizontal if the focus tile arrow is EAST or WEST, otherwise vertical)
 * This exact line is based off the position of the focus tile.
 * This class is used to evaluate each tile in this line, and determine what letters can be played on each tile:
 *  - Blocked tiles such as wall, and also already placed letters, obviously no tiles can be played here.
 *  - Playable Map Tiles not adjacent to any letter, any letter can be played here.
 *  - Playable Map Tiles adjacent to a letter: We must evaluate each distinct letter in our hud, and filter out the ones that
 *                                             do not make a valid word.
 * The end result is we have a clear picture of what letters can be played where in this line of the board.
 * Created by Bryan on 21/09/2018.
 */
public class HintSQLHandler {

    private final String LOGTAG = getClass().getSimpleName();
    private CpuLoggerInterface cpuLogger;
    private StringBuilder logDecisionLogic;

    private HashMap<Integer, HintBoardTile> mapIdTile;

    private Character focusCharacter;
    private Integer idFocusTile;
    private ArrayList<String> layout;
    private Direction direction;
    private int[] xyFocusTile;
    private String hudLetters, distinctHudLetters;
    private DatabaseGateway databaseGateway;
    private double minFrequencyOfWord;

    private ArrayList<Integer> iLockedLetters = new ArrayList<>();

    private static String SQL_SELECT_FROM_BASE_STATEMENT = "SELECT %s AS x, %s AS y, " + TableDictionary.COL_WORD + ", " +
            TableDictionary.COL_DISPLAY_WORD + ", " + TableDictionary.COL_FREQUENCY + " FROM " + TableDictionary.TABLE_NAME;

    /**
     * @param layout The board layout as it currently is
     * @param direction Focus Tile Direction
     * @param xyFocusTile Focus Tile XY
     * @param hudLetters Players letters on the HUD
     * @param databaseGateway db access
     * @param minFrequencyOfWord
     */
    public HintSQLHandler(ArrayList<String> layout,
                          Direction direction,
                          int[] xyFocusTile,
                          String hudLetters,
                          DatabaseGateway databaseGateway,
                          double minFrequencyOfWord,
                          CpuLoggerInterface cpuLogger){
        this.layout = layout;
        this.xyFocusTile = xyFocusTile;
        this.focusCharacter = LevelUtils.getCharacterAt(layout, xyFocusTile);
        this.direction = direction;
        this.minFrequencyOfWord = minFrequencyOfWord;
        this.hudLetters = hudLetters;
        this.databaseGateway = databaseGateway;
        this.cpuLogger = cpuLogger;
    }

    public ArrayList<HintWord> getHints(){

        logDecisionLogic = new StringBuilder();

        //fullLine is a string that will represent the line the focus tile is in (vertical or horizontal depending on focus direction)
        //e.g. ...-...0..r...-1
        String fullLine = LevelUtils.getRowOrColumnAsString(direction.isHorizontal(), layout, xyFocusTile);
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("hsh1: fullLine = %s", fullLine));

        //This is the full line of the relevant tile/column layout with walls applied to redundant tiles ()
        //e.g. ----...0..r...--
        String boardHint = trimHintToMaxPlayableTiles(fullLine);
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("\nhsh2: boardHint = %s", boardHint));

        //evaluate each tile in mapIdTile, marking properties such as canPlayAnyLetterHere,
        //isTouchingAdjacentLetters, and what letters can be played on the tile
        //e.g. ----...0..r...--
        evaluateBoardHintsPerpendicularPotentialWords(boardHint);
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("\nhsh3: postFilter::mapIdTile =%s", logMapIdTileInfo()));

        if(idFocusTile == null){
            Log.e(LOGTAG, "idFocusTile is null when executing toSQLWhereClause()!");
            return new ArrayList<>();
        }

        //This is the id of the closest map tile to the focus tile that has an adjacent letter tile touching it.
        //This will be < idFocusTile if the direction is WEST/NORTH. Otherwise it will be > idFocusTile.
        //Any words we find must at least span the number of characters between idFocusTile and idClosestTouchedMapTile
        //e.g. ----...0..r . . . - -
        //     0123456789101112131415
        // idFocusTile = 7, idClosestTouchedMapTile = 10
        Integer idClosestTouchedMapTile = calculateClosestTouchedMapTile();
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("\nhsh4: idClosestTouchedMapTile = %s", idClosestTouchedMapTile));

        if(idClosestTouchedMapTile == null){
            Log.e(LOGTAG, "idClosestTouchedMapTile is null when executing toSQLWhereClause()!");
            return new ArrayList<>();
        }

        //e.g. [-][-][edparosit][edparosit][edparosit][s][-]
        String maxSearchTerm = getMaxSearchTerm();
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("\nhsh5: maxSearchTerm = %s", maxSearchTerm));

        ArrayList<String> whereClauses = generateWhereClauses(boardHint, maxSearchTerm, idClosestTouchedMapTile);
        if(cpuLogger.isLoggingEnabled()){
            logDecisionLogic.append(String.format("\nhsh6: whereClauses ="));
            for(String w : whereClauses){
                logDecisionLogic.append("\n" + w);
            }
        }

        if(whereClauses.isEmpty()){
            Log.d(LOGTAG, "No words possible in this scenario");
            return new ArrayList<>();
        }

        String sql = generateFullSqlString(whereClauses);
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("\nhsh7: sql =\n%s", sql));

        ArrayList<HintWord> output = queryDictionaryDb(sql);
        if(cpuLogger.isLoggingEnabled()) logDecisionLogic.append(String.format("\nhsh8: result: %s words", output.size()));

        return output;
    }

    private int[] getMinStartAndEndIndexes(int idClosestTouchedMapTile) {
        //Any valid words must traverse the minStartI AND minEndI tiles.
        int minStartI = Math.min(idClosestTouchedMapTile, idFocusTile); //Of the 2, the tile closest to index 0
        int minEndI =   Math.max(idClosestTouchedMapTile, idFocusTile); //Of the 2, the tile closest to max index n

        //minStartI and minEndI must be extended if board letters are either side, so they're considered for word searches
        if(direction.isBackwards()){
            while (minStartI < 0 && mapIdTile.get(minStartI+1).getLockedLetter() != null && Character.isLetter(mapIdTile.get(minStartI+1).getLockedLetter())){
                minStartI++;
            }
            while (minEndI > (mapIdTile.size() + 1) && mapIdTile.get(minEndI - 1).getLockedLetter() != null && Character.isLetter(mapIdTile.get(minEndI - 1).getLockedLetter())){
                minEndI--;
            }
        }else {
            while (minStartI > 0 && mapIdTile.get(minStartI - 1).getLockedLetter() != null && Character.isLetter(mapIdTile.get(minStartI - 1).getLockedLetter())) {
                minStartI--;
            }
            while (minEndI < (mapIdTile.size() - 1) && mapIdTile.get(minEndI + 1).getLockedLetter() != null && Character.isLetter(mapIdTile.get(minEndI + 1).getLockedLetter())) {
                minEndI++;
            }
        }
        return new int[]{minStartI, minEndI};
    }

    public String getLogDecisionLogic(){
        return logDecisionLogic.toString();
    }

    //region private methods

    /**
     * Carry out the evaluation of each tile in the line.
     * For each tile, examine the adjacent tiles either side:
     *  - If there are no letters adjacent, all hud letters can potentially be played here
     *  - If there is letters adjacent, we need to respect the words that could be inadvertently created, so we store all
     *    combinations of hud letters with respect to adjacent letters. Then test these against the dictionary; only the
     *    hud letters that produce valid words will be considered for our answer.
     */
    private void evaluateBoardHintsPerpendicularPotentialWords(String boardHint){

        distinctHudLetters = removeDuplicateCharacters(hudLetters);

        //Maps the tile id to its HintBoardLineTile object
        //Build this map up with potential letters (and words) so we can validate them all at once in a single SQL query
        mapIdTile = new HashMap<>();

        //Traverse each character in the boardhint, evaluating potential letters in map tiles touching adjacent letters
        //and storing the potential letter and word combo's in mapTileIdTrialLetterAndWord
        for(int id = 0; id < boardHint.length(); id++){

            HintBoardTile tile = new HintBoardTile(id, boardHint.charAt(id), new int[]{
                    direction.isHorizontal() ? id : xyFocusTile[0],
                    direction.isHorizontal() ? xyFocusTile[1] : id});

            if((direction.isHorizontal() && xyFocusTile[0] == id) || (!direction.isHorizontal() && xyFocusTile[1] == id)){
                idFocusTile = tile.getId();
            }

            if(!TileCodes.getPlayableTileCodes().contains(tile.getTileCode())){
                tile.setLockedLetter(tile.getTileCode());
                mapIdTile.put(id, tile);
                continue;
            }

            boolean isTouchingLetters = !LevelUtils.getXYsOfAdjacentLetters(layout, tile.getXy()).isEmpty();

            if(!isTouchingLetters) {
                tile.setCanPlayAnyLetterHere(true);
                mapIdTile.put(id, tile);
                continue;
            }

            tile.setIsTouchingAdjacentLetters(true);

            for (Character potentialLetter : distinctHudLetters.toCharArray()) {
                //Lets evaluate if potentialLetter can fit on this map tile and still be legal
                StringBuilder sb = new StringBuilder();
                if(direction.isHorizontal()){
                    int y;
                    //Prefix
                    y = tile.getXy()[1] - 1;
                    while (true){
                        Character character = LevelUtils.getCharacterAt(layout, new int[]{id, y});
                        if(character == null || !Character.isLetter(character)){
                            break;
                        }else {
                            sb.insert(0, character);
                        }
                        y--;
                    }
                    //This letter
                    sb.append(potentialLetter);
                    //Suffix
                    y = tile.getXy()[1] + 1;
                    while (true){
                        Character character = LevelUtils.getCharacterAt(layout, new int[]{id, y});
                        if(character == null || !Character.isLetter(character)){
                            break;
                        }else {
                            sb.append(character);
                        }
                        y++;
                    }
                    //Finally, queue to evaluate
                    tile.addPotentialLetterAndWord(potentialLetter, sb.toString().toLowerCase());
                }else {
                    int x;
                    //Prefix
                    x = tile.getXy()[0] - 1;
                    while (true){
                        Character character = LevelUtils.getCharacterAt(layout, new int[]{x, id});
                        if(character == null || !Character.isLetter(character)){
                            break;
                        }else {
                            sb.insert(0, character);
                        }
                        x--;
                    }
                    //This letter
                    sb.append(potentialLetter);
                    //Suffix
                    x = tile.getXy()[0] + 1;
                    while (true){
                        Character character = LevelUtils.getCharacterAt(layout, new int[]{x, id});
                        if(character == null || !Character.isLetter(character)){
                            break;
                        }else {
                            sb.append(character);
                        }
                        x++;
                    }
                    //Finally, queue to evaluate
                    tile.addPotentialLetterAndWord(potentialLetter, sb.toString());
                }
            }
            mapIdTile.put(id, tile);
        }

        //Now execute
        mapIdTile = databaseGateway.filterOutInvalidWords(mapIdTile);
    }

    /**
     * Given a fullLine of a board (either verticle or horizontal), trim it to treat unplayable tiles (e.g. on the
     * other side of a wall) as walls so they will be ignored.
     * e.g: "..-.R..0-..." => "---.R..0----"
     * e.g: "..-.R..0-..." => "---.R..0----"
     * @return
     */
    private String trimHintToMaxPlayableTiles(String fullLine){

        //To speed up calculations, treat unreachable map tiles in this line (e.g. on the other side of a wall) as wall tiles,
        //so they will be ignored.
        StringBuilder sb = new StringBuilder();
        boolean hasPassedFocusTile = false;
        boolean markRemainingLettersAsWall = false;
        int id = 0;
        for(Character c : fullLine.toCharArray()){
            if(markRemainingLettersAsWall){
                //We have been instructed to mark the remaining tiles as walls
                sb.append(TileCodes.WallTile.getCharacter());
            }else if(TileCodes.getBlockTileCodes().contains(c)){
                //This tile is a blocker!
                if(hasPassedFocusTile){
                    //We have already passed the focus tile and just met a wall. Every map tile from here on out can be treated as a wall.
                    markRemainingLettersAsWall = true;
                }else {
                    //This is a blocker tile, and we haven't met the focus tile yet. So turn everything up to now to a wall.
                    for(int i = 0; i < sb.length(); i++){
                        sb.setCharAt(i, TileCodes.WallTile.getCharacter());
                    }
                }
                sb.append(TileCodes.WallTile.getCharacter());
            }else {
                if(!hasPassedFocusTile){
                    if((direction.isHorizontal() && xyFocusTile[0] == id) ||
                      (!direction.isHorizontal() && xyFocusTile[1] == id)){
                        hasPassedFocusTile = true;
                    }
                }
                if(Character.isLetter(c)){
                    iLockedLetters.add(id); //This is a letter tile locked on the board
                }
                sb.append(c);
            }
            id++;
        }
        return sb.toString();
    }

    /**
     * @return a string representing what letters can be played at each position in the boardHint
     *  e.g. [-][-][edparosit][edparosit][edparosit][s][-]
     *       index 0, 1 & 6 are wall tiles, nothing can be played. index 5 MUST be s. 2, 3 & 4 can be any letter from the hud ie. [edparosit]
     */
    private String getMaxSearchTerm() {
        StringBuilder maxSearchTerm = new StringBuilder();
        for(int id = 0; id < mapIdTile.size(); id++){
            HintBoardTile tile = mapIdTile.get(id);
            if(tile.isCanPlayAnyLetterHere()) {
                maxSearchTerm.append("[" + distinctHudLetters + "]");
            }else if(tile.getLockedLetter() != null){
                if(Character.isLetter(tile.getLockedLetter())) {
                    maxSearchTerm.append("[" + Character.toLowerCase(tile.getLockedLetter()) + "]");
                }else {
                    maxSearchTerm.append("[-]");
                }
            }else if(!tile.getMapLetterWord().isEmpty()){
                maxSearchTerm.append("[");
                for(Character c : tile.getMapLetterWord().keySet()){
                    maxSearchTerm.append(Character.toLowerCase(c));
                }
                maxSearchTerm.append("]");
            }else if(!tile.isCanPlayAnyLetterHere() && tile.isTouchingAdjacentLetters()){
                //Map Tile but due to adjacent letters, can't play any of our current hud letters.
                maxSearchTerm.append("[ ]");
            }else {
                throw new RuntimeException("Unexpected tile scenario when creating maxSearchTerm");
            }
        }
        return maxSearchTerm.toString();
    }

    /**
     * @return e.g. changes "aaabbsdfeeg" -> "absdfeg"
     */
    private String removeDuplicateCharacters(String input){
        char[] chars = input.toCharArray();
        Set<Character> charSet = new LinkedHashSet<Character>();
        for (char c : chars) {
            charSet.add(c);
        }
        StringBuilder sb = new StringBuilder();
        for (Character character : charSet) {
            sb.append(character);
        }
        return sb.toString();
    }

    /**
     * @return the id of the closest touched map tile (i.e. has adjacent locked letter) to the focus tile
     */
    private Integer calculateClosestTouchedMapTile(){

        Integer idClosestTouchedMapTile = null;

        for(int i = 0; i < mapIdTile.size(); i++){
            HintBoardTile tile = mapIdTile.get(i);
            boolean skip = BoardUtils.isReverseDirection(direction) ? tile.getId() > idFocusTile : tile.getId() < idFocusTile;
            if(skip){
                continue;
            }
            if(!tile.isPlayable()){
                continue;
            }
            if(tile.isTouchingAdjacentLetters() || (tile.getLockedLetter() != null && Character.isLetter(tile.getLockedLetter().charValue()))){
                if(idClosestTouchedMapTile == null){
                    idClosestTouchedMapTile = tile.getId();
                    continue;
                }
                if(direction.isBackwards()){
                    if(tile.getId() < idFocusTile && tile.getId() > idClosestTouchedMapTile){
                        //This tile is closer to the focus tile, so this is the new min length of the hint
                        idClosestTouchedMapTile = tile.getId();
                    }
                }else {
                    if(tile.getId() > idFocusTile && tile.getId() < idClosestTouchedMapTile){
                        //This tile is closer to the focus tile, so this is the new min length of the hint
                        idClosestTouchedMapTile = tile.getId();
                    }
                }
            }
        }
        return idClosestTouchedMapTile;
    }

    /**
     * @param sbReplaceClause The current replace clause so far (will have all the replace clauses from the hud letters)
     * @param parts e.g. "[qwerty][qwerty][qwerty][a][qwerty]"   --> a is a locked letter here (if its index is in iLockedLetters)
     * @return the input replaceClause updated with replace clauses for the locked letters in the like clause
     */
    private String generateReplaceClause(StringBuilder sbReplaceClause, String[] parts){
        if(sbReplaceClause == null || sbReplaceClause.length() == 0){
            sbReplaceClause.append(TableDictionary.COL_WORD);
        }
        for(int i = 0; i < parts.length; i++){
            if(iLockedLetters.contains(i)){
                //This part is 1 letter locked on the board
                String str = ", '" + parts[i].substring(1, 2) + "', '')";
                if(!sbReplaceClause.toString().contains(str)) {
                    sbReplaceClause.insert(0, "REPLACE(");
                    sbReplaceClause.append(str);
                }
            }
        }
        return sbReplaceClause.toString();
    }

    /**
     * Using the boardhint, generate WHERE clauses that will query the db and return x, y and words that can be played there
     * @param boardHint
     * @param maxSearchTerm
     * @param idClosestTouchedMapTile
     * @return ArrayList of WHERE clauses e.g:
     * 0 = "SELECT 4 AS x, (1 + 1) - LENGTH(Word) AS y, Word, DisplayWord, Frequency FROM Dictionary WHERE (Word GLOB '[r][parsdteio]' AND LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Word, 'e', ''), 'd', ''), 'p', ''), 'a', ''), 'r', ''), 'o', ''), 's', ''), 'i', ''), 't', '')) = 0)"
     * 1 = "SELECT 4 AS x, (2 + 1) - LENGTH(Word) AS y, Word, DisplayWord, Frequency FROM Dictionary WHERE (Word GLOB '[r][parsdteio][edparosit]' AND LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Word, 'e', ''), 'd', ''), 'p', ''), 'a', ''), 'r', ''), 'o', ''), 's', ''), 'i', ''), 't', '')) = 0)"
     * 2 = "SELECT 4 AS x, (3 + 1) - LENGTH(Word) AS y, Word, DisplayWord, Frequency FROM Dictionary WHERE (Word GLOB '[r][parsdteio][edparosit][edparosit]' AND LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Word, 'e', ''), 'd', ''), 'p', ''), 'a', ''), 'r', ''), 'o', ''), 's', ''), 'i', ''), 't', '')) = 0)"
     * 3 = "SELECT 4 AS x, (4 + 1) - LENGTH(Word) AS y, Word, DisplayWord, Frequency FROM Dictionary WHERE (Word GLOB '[r][parsdteio][edparosit][edparosit][edparosit]' AND LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Word, 'e', ''), 'd', ''), 'p', ''), 'a', ''), 'r', ''), 'o', ''), 's', ''), 'i', ''), 't', '')) = 0)"
     * 4 = "SELECT 4 AS x, (6 + 1) - LENGTH(Word) AS y, Word, DisplayWord, Frequency FROM Dictionary WHERE (Word GLOB '[r][parsdteio][edparosit][edparosit][edparosit][parsdteio][t]' AND LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Word, 'e', ''), 'd', ''), 'p', ''), 'a', ''), 'r', ''), 'o', ''), 's', ''), 'i', ''), 't', '')) = 0)"
     */
    private ArrayList<String> generateWhereClauses(String boardHint, String maxSearchTerm, int idClosestTouchedMapTile){

        ArrayList<String> clauses = new ArrayList<>();

        //https://stackoverflow.com/a/28743311
        StringBuilder sbReplaceClause = new StringBuilder();
        sbReplaceClause.append(TableDictionary.COL_WORD);
        for(Character c : distinctHudLetters.toCharArray()){
            sbReplaceClause.insert(0, "REPLACE(");
            sbReplaceClause.append(", '" + c + "', '')");
        }

        /**
         * maxSearchTerm example: "[folemnrpt][folemnrpt][folemnrpt][r][folemnrpt][folemnrpt]"
         */
        String[] parts = maxSearchTerm.split("(?<=])"); //Split string by ']' but retain the delimiter

        int[] minStartEndI = getMinStartAndEndIndexes(idClosestTouchedMapTile);
        int minStartI = minStartEndI[0];
        int minEndI   = minStartEndI[1];

        /**WHERE Clauses will have 2 parts:
         - 1. like
         - 2. replace

         1. LIKE
         we have a maxSearchTerm and a range. For every playable maptile outside the range, add a new OR LIKE clause...
         e.g. maxSearchTerm = ...0...
         T   T   [Where T is a locked letter tile, so hints must at least reach their adjacent map tile]
         if forward direction, we want: 0.. | 0...
         if backward direction, we want: ...0 | ...0. | ...0.. | ...0... | ..0 | ..0. | ..0.. | ..0...

         2. REPLACE
         After matching a word with a dictionary entry using the like clause, we must check the user can play the word
         with their existing hud letters (and letters locked on the board).
         We use multipe REPLACE clauses (1 for each hud/board letter), and if the length of the final result = 0, then we
         can play this word
         */
        if(direction.isBackwards()){
            //Backwards
            for(int i = 0; i < parts.length; i++){
                //..t..0..
                //minStartI = 2 (that is 't')
                //minEndI   = 5 (that is '0')
                //Any answers must cover these 2 tiles or they will be invalid
                if(i > minStartI){
                    //Since we are going backwards, if i is > than minStartI, it means any words found here won't be the
                    //required minimum length, so skip.
                    //e.g. don't want a word that ends at i=4 in the example [..t..0..] such as [..t.be..]. Once we pass 't' we are finished.
                    continue;
                }
                if(parts[i].equals("[-]")){
                    //Skip as we cannot have words touching wall tiles
                    continue;
                }
                boolean previousTileIsLetter = i > 0 && Character.isLetter(boardHint.charAt(i - 1));
                if(previousTileIsLetter){
                    //The previous tile was a locked letter. Skip this as it has already been handled in the previous iteration.
                    continue;
                }
                for(int suffix = minEndI; suffix < parts.length; suffix++){
                    if(parts[suffix].equals("[-]")){
                        continue;
                    }
                    if(suffix < (boardHint.length() - 1) && Character.isLetter(boardHint.charAt(suffix + 1))){
                        //The next tile is a locked letter, so skip this clause as we need to consider the adjacent locked
                        //letter (which will happen on the next iteration)
                        continue;
                    }
                    String likeClause = TextUtils.join("", Arrays.asList(parts).subList(i, suffix + 1)).replace("[-]", "");
                    likeClause = likeClause.substring(1, likeClause.length() - 1).replaceAll(", ", "");
                    String where = "(" + TableDictionary.COL_WORD + " GLOB '" + likeClause + "' AND " +
                            "LENGTH(" + generateReplaceClause(sbReplaceClause, parts) + ") = 0)";
                    int[] xy = new int[]{direction.isHorizontal() ? i : xyFocusTile[0],
                            direction.isHorizontal() ? xyFocusTile[1] : i};
                    clauses.add(String.format(SQL_SELECT_FROM_BASE_STATEMENT, xy[0], xy[1]) + " WHERE " + where);
                }
            }
        }else {
            //Forward
            for(int i = minStartI; i < parts.length; i++){
                //..0..t..
                //minStartI = 2 (that is '0')
                //minEndI   = 5 (that is 't')
                //Any answers must cover these 2 tiles or they will be invalid
                if(i < minEndI){
                    //Since we are going forwards, if i is < than minEndI, it means any words found here won't be the
                    //required minimum length, so skip.
                    //e.g. don't want a word that ends at i=3 in the example [..0..t..] such as [..be.t..]. Keep continuing until we reach 't'
                    continue;
                }
                if(parts[i].equals("[-]")){
                    //Skip as we cannot have words touching wall tiles
                    continue;
                }
                boolean nextTileIsLetter =     i < (boardHint.length() - 1) && Character.isLetter(boardHint.charAt(i + 1));
                if(nextTileIsLetter){
                    //The next tile is a locked letter. Skip this as it will be handled in the next iteration.
                    continue;
                }
                String likeClause = TextUtils.join("", Arrays.asList(parts).subList(minStartI, i + 1)).replaceAll("[-]", "");
                likeClause = likeClause.substring(1, likeClause.length() - 1).replaceAll(", ", "");
                String where = "(" + TableDictionary.COL_WORD + " GLOB '" + likeClause + "' AND " +
                        "LENGTH(" + generateReplaceClause(sbReplaceClause, parts) + ") = 0)";
                int[] xy = new int[]{direction.isHorizontal() ? i : xyFocusTile[0],
                        direction.isHorizontal() ? xyFocusTile[1] : i};
                //i is at the end of the word, so the starting x/y of the word is (i + 1) - word length
                String startingX = direction.isHorizontal() ?
                        "(" + xy[0] + " + 1) - LENGTH(" + TableDictionary.COL_WORD + ")" :
                        String.valueOf(xy[0]);
                String startingY = direction.isHorizontal() ?
                        String.valueOf(xy[1]) :
                        "(" + xy[1] + " + 1) - LENGTH(" + TableDictionary.COL_WORD + ")";
                clauses.add(String.format(SQL_SELECT_FROM_BASE_STATEMENT, startingX, startingY) + " WHERE " + where);
            }
        }

        return clauses;
    }

    /**
     * The full sql string that will be used to query the dictionary db for playable words
     */
    private String generateFullSqlString(ArrayList<String> whereClauses){

        //Each clause string in the list will look something like this:
        //"Word GLOB '[natirs][t][natirs][natirs][natirs]' AND LENGTH(REPLACE(REPLACE(Word, 'a', ''), 'i', '')) = 0"
        StringBuilder sqlWhere = new StringBuilder();
        for(String clause : whereClauses){
            if(sqlWhere.length() > 0){
                sqlWhere.append(" UNION ALL ");
            }
            sqlWhere.append(clause);
        }

        //Write the full sql string
        String sql = "SELECT x, y, " + TableDictionary.COL_WORD + ", " +
                TableDictionary.COL_DISPLAY_WORD + ", " +
                TableDictionary.COL_FREQUENCY +
                " FROM (" + sqlWhere.toString() + ") " +
                " WHERE " + TableDictionary.COL_FREQUENCY + " >= " + minFrequencyOfWord +
                " ORDER BY LENGTH(" + TableDictionary.COL_WORD + ") DESC, " + TableDictionary.COL_WORD + " ASC";

        return sql;
    }

    private ArrayList<HintWord> queryDictionaryDb(String sql){
        ArrayList<HintWord> output = new ArrayList<>();
        Cursor c = databaseGateway.getDatabase().rawQuery(sql, null);
        HashMap<Pair<Integer, Integer>, Float> mapXyDesirabilityScore = new HashMap<>();
        for (int i = 0; i < c.getCount(); i++) {
            if(!c.moveToPosition(i)){
                break;
            }
            String word = c.getString(c.getColumnIndex(TableDictionary.COL_WORD));
            String displayWord = c.isNull(c.getColumnIndex(TableDictionary.COL_DISPLAY_WORD)) ? null : c.getString(c.getColumnIndex(TableDictionary.COL_DISPLAY_WORD));
            double frequency = c.getDouble(c.getColumnIndex(TableDictionary.COL_FREQUENCY));
            int startingX = c.getInt(c.getColumnIndex("x"));
            int startingY = c.getInt(c.getColumnIndex("y"));

            HintWord hintWord = new HintWord();
            hintWord.setDictionaryEntry(new DictionaryEntry(word, displayWord, frequency));
            for(int iLetter = 0; iLetter < word.length(); iLetter++){
                int x = direction.isHorizontal() ? (startingX + iLetter) : startingX;
                int y = direction.isHorizontal() ? startingY : (startingY + iLetter);
                Pair<Integer, Integer> xy = new Pair<>(x, y);
                boolean lockedOnBoard = iLockedLetters.contains(direction.isHorizontal() ? x : y);
                boolean isFocusTile = idFocusTile == (direction.isHorizontal() ? x : y);
                HintLetter hintLetter = new HintLetter(x, y, word.charAt(iLetter), lockedOnBoard, isFocusTile);
                if(mapXyDesirabilityScore.containsKey(xy)){
                    hintLetter.setDesirabilityScore(mapXyDesirabilityScore.get(xy));
                }else {
                    hintLetter.setDesirabilityScore(layout);
                    mapXyDesirabilityScore.put(xy, hintLetter.getDesirabilityScore());
                }
                hintWord.addLetter(hintLetter);
            }
            //Note: SQLite is extremely fast but we are limited by the REPLACE clause (unfortunately there is no REPLACE FIRST clause)
            //The idea of the replace clause is to replace letters in the potential word with our hud/locked letters, and if
            //the length of the result is 0 then we can play this word. REPLACE FIRST would handle this correctly, but REPLACE means
            //we may get 0 if there are multiple occurrences of a letter in a word and we only have 1 of that letter.
            //For that reason, we must re-do the validation of the resulting words to ensure we have enough letters to play them.
            //At least we only have to do this manual validation on a much smaller subset of words thanks to sqlite.
            if(!output.contains(hintWord) && canPlayThisWord(hintWord, hudLetters)){
                output.add(hintWord);
            }
        }
        c.close();
        return output;
    }

    /**
     * @return true if hudletters contains all the letters we need to play hintWord (acknowledging locked letters on board)
     */
    private boolean canPlayThisWord(HintWord hintWord, String hudLetters) {
        StringBuilder sb = new StringBuilder(hudLetters);
        for(HintLetter l : hintWord.getLetters()){
            if(l.isLockedOnBoard()){
                continue;
            }
            int index = sb.indexOf(String.valueOf(l.getLetter()));
            if(index == -1){
                return false;
            }else {
                sb.replace(index, index + 1, "");
            }
        }
        return true;
    }

    private String logMapIdTileInfo(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < mapIdTile.size(); i++){
            HintBoardTile t = mapIdTile.get(i);
            sb.append(String.format("\n[%s, %s]\t%s\t%s\t%s\t%s",
                    t.getXy()[0], t.getXy()[1],
                    t.getTileCode().toString(),
                    "canPlayAnyLetterHere: " + (t.isCanPlayAnyLetterHere() ? "1" : "0"),
                    "isTouchingAdjacentLetters: " + (t.isTouchingAdjacentLetters() ? "1" : "0"),
                    TextUtils.join(", ", t.getMapLetterWord())));
        }
        return sb.toString();
    }

    //endregion

}
