package bbgames.ie.wordsmithnavigator.GameObjects.Players;

import java.util.ArrayList;
import java.util.Random;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.FocusTile;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerImpl.PlayerCPU;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerImpl.PlayerMulti;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerImpl.PlayerUser;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Responsible for managing the players participating in the GameActivity
 * Created by Bryan on 08/08/2016.
 */
public class PlayerManager {

    private GameActivity gameActivity;
    ArrayList<Player> players;
    int activePlayerId;

    public PlayerManager(GameActivity gameActivity){
        this.gameActivity = gameActivity;
        players = new ArrayList<>();
        activePlayerId = 0;
        addPlayers();
    }

    public void setActivePlayer(int playerId){
        activePlayerId = playerId;
    }

    public void startGame(){
        startNewTurn();
        gameActivity.getHUD().SetUser(getActivePlayer());
        gameActivity.getHUD().RefreshHud();
    }

    /**
     * Call when a players turn has ended. This method will update the activePlayerId to the next player in the queue.
     */
    public void updateCurrentPlayer(){
        if(players.size() <= 1) {
            return;
        }
        Player previousPlayer = getActivePlayer();
        if(activePlayerId < (players.size() - 1)){
            activePlayerId++;
        }else {
            activePlayerId = 0;
        }
        if(previousPlayer.getPlayerType().equals(PlayerType.CPU)){
            //CPU has just finished its turn. Start planning it's next move
            ((PlayerCPU)previousPlayer).LoadOptimalPaths();
        }
    }

    /**
     * Call to kick off a new turn.
     * This method enables the controls if it is your turn, and kicks off the timer.
     */
    public void startNewTurn() {
        gameActivity.getScene().SetEnabled(getActivePlayer().isHumanUser());
        gameActivity.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                gameActivity.getHUD().detachChild(gameActivity.getGraphicsManager().getSceneShadow());
            }
        });
        gameActivity.getHUD().setState(getActivePlayer().isHumanUser() ? GameHud.STATE.ACTIVE_CONTROLLER : GameHud.STATE.LOCKED, true, null);
        if(getActivePlayer().isHumanUser() && getActivePlayer().getPlayedWords().size() == 1){
            //If its the users first go, set the focus tile
            int[] xyFocusTile = LevelUtils.getFirstXYof(gameActivity.getLevelManager().getLayoutModified(), TileCodes.StartingZone.getCharacter());
            Direction d = LevelUtils.generateFocusTileDirection(gameActivity.getLevelManager().getLayoutModified());
            gameActivity.getLevelManager().setFocusTile(new FocusTile(xyFocusTile, d), true);
        }
        if(getActivePlayer().getPlayerType().equals(PlayerType.CPU)){
            ((PlayerCPU) getActivePlayer()).AutomateTurn();
        }
        gameActivity.getHUD().GetClock().startTimer();
    }

    //region getters

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public Player getPlayer(PlayerType playerType) {
        for(Player player : players){
            if(playerType.equals(player.getPlayerType())){
                return player;
            }
        }
        return null;
    }

    public Player getActivePlayer(){
        return players.get(activePlayerId);
    }

    //endregion

    //region create players

    private void addPlayers(){
        ArrayList<String> layout = gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal();
        for(int row = 0; row < layout.size(); row++){
            for(int col = 0; col < layout.get(row).length(); col++){
                Character c = layout.get(row).charAt(col);
                if(TileCodes.getPlayerStartingZoneTileCodes().contains(c)){
                    addPlayer(new int[]{col, row}, PlayerType.getPlayerType(c));
                }
            }
        }
    }

    private void addPlayer(int[] xyPlayer, PlayerType playerType){
        if(playerType == null || playerType.equals(PlayerType.ROGUE)){
            return;
        }
        Player player = createPlayer(playerType, xyPlayer);
        players.add(player);
    }

    private Player createPlayer(PlayerType playerType, int[] xyPlayer){
        switch (playerType){
            case USER:
                return new PlayerUser(players.size(), playerType, gameActivity, xyPlayer, SkinLetter.getUserTheme(gameActivity.getApplicationManager()), SkinHUD.getUserTheme(gameActivity.getApplicationManager()));
            case MULTI_PLAYER_USER:
                return new PlayerMulti(players.size(), playerType, gameActivity, xyPlayer, SkinLetter.getCPUTheme(gameActivity.getApplicationManager()), getRandomAvailableColorTheme());
            case CPU:
                return new PlayerCPU(players.size(), playerType, gameActivity, xyPlayer, SkinLetter.getCPUTheme(gameActivity.getApplicationManager()), SkinHUD.getCPUTheme(gameActivity.getApplicationManager()));
        }
        throw new RuntimeException("NON SUPPORTED PLAYERTYPE: " + playerType.name());
    }

    private SkinHUD getRandomAvailableColorTheme(){
        ArrayList<SkinHUD> colors = new ArrayList<>();
        colors.add(SkinHUD.White_wBlack);
        colors.add(SkinHUD.Navy_wRed);
        SkinHUD tpUser = SkinHUD.getUserTheme(gameActivity.getApplicationManager());
        SkinHUD tpCPU = SkinHUD.getCPUTheme(gameActivity.getApplicationManager());
        if(colors.contains(tpUser)){
            colors.remove(tpUser);
        }
        if(colors.contains(tpCPU)){
            colors.remove(tpCPU);
        }
        for(Player player : players){
            if(colors.contains(player.getThemePlayer())){
                colors.remove(colors.indexOf(player.getThemePlayer()));
            }
        }
        return colors.get(new Random().nextInt(colors.size()));
    }

    public void initialisePlayers() {
        for(Player player : players) {
            //mark letter tiles as players
            ArrayList<int[]> touchingLetters =
                    LevelUtils.getXYsOfAdjacentLetters(gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal(), player.xyStart);
            if (touchingLetters.size() != 1) {
                Util.throwException(gameActivity, "Exactly 1 letter should be touching " + player.getPlayerType().getTileCode().getCharacter().toString() + " on level " + gameActivity.getLevelManager().getLevelStaticData().getId());
            }
            ArrayList<int[]> playersLetterCluster =
                    LevelUtils.getLetterTilesTouchingLetter(gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal(), touchingLetters.get(0));
            for (int[] xy : playersLetterCluster) {
                TileLetter t = gameActivity.getLevelManager().getLetterTile(xy[1], xy[0]);
                t.setOwner(player);
                player.updateListXYLetterTilesOnBoard(xy);
            }
            player.addUpdateHudTiles();
            if (player.getPlayerType().equals(PlayerType.CPU)) {
                ((PlayerCPU) player).LoadOptimalPaths();
            }
        }
    }

    //endregion

}
