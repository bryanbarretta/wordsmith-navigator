package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.PowerUp;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;

import static bbgames.ie.wordsmithnavigator.Constant.SPEECH_BUBBLE_CENTER_NATURAL_X;
import static bbgames.ie.wordsmithnavigator.Constant.SPEECH_BUBBLE_LEFT_PADDING;

/**
 * Created by Bryan on 09/09/2018.
 */

public class SpeechBubble extends Sprite {

    private GameActivity gameActivity;
    private Text text;

    private HashMap<GraphicsManager.TextureKeys, Sprite> mapIcons = new HashMap<>();
    private List<GraphicsManager.TextureKeys> visibleSprites = new ArrayList<>();

    private final float baseX = Constant.HUD_PADDING + (Constant.ICON_SMALL_64 / 2);
    private final float baseY = getHeight() / 2;

    public SpeechBubble(GameActivity gameActivity, float pX, float pY) {
        super(pX, pY,
                gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.SpeechBubble),
                gameActivity.getVertexBufferObjectManager());
        this.gameActivity = gameActivity;
        this.text = new Text(SPEECH_BUBBLE_CENTER_NATURAL_X, Constant.SPEECH_BUBBLE_HEIGHT / 2,
                gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.belepotan),
                "",
                50,
                gameActivity.getVertexBufferObjectManager());
        attachChild(text);
    }

    public void setText(String text) {
        this.text.setColor(gameActivity.getResources().getColor(
            gameActivity.getPlayerManager().getActivePlayer().getThemePlayer().getDarkestColor())
        );
        this.text.setText(text);
    }

    private GraphicsManager.TextureKeys getTextureKey(TileChildType child, Object obj){
        switch (child){
            default:
                return null;
            case Gem:
                return GraphicsManager.TextureKeys.Diamond;
            case PowerUps:
                PowerUp powerUp = (PowerUp) obj;
                return PowerUp.getMiniSprite(powerUp.getPowerUpLink().getType());
        }
    }

    public void showIcon(GraphicsManager.TextureKeys k){
        if(!mapIcons.containsKey(k)){
            Sprite s = new Sprite(baseX, baseY, gameActivity.getGraphicsManager().GetTextureRegion(k), gameActivity.getVertexBufferObjectManager());
            s.setVisible(false);
            attachChild(s);
            mapIcons.put(k, s);
        }
        if(!visibleSprites.contains(k)) {
            visibleSprites.add(k);
            Sprite s = mapIcons.get(k);
            s.setVisible(true);
            refreshPositions();
        }
    }

    public void showIcons(int[] xy){
        iTileChild tDiamond = gameActivity.getScene().getTileChild(xy[1], xy[0], TileChildType.Gem);
        if(tDiamond != null){
            showIcon(getTextureKey(TileChildType.Gem, tDiamond));
        }
        iTileChild tPowerUp = gameActivity.getScene().getTileChild(xy[1], xy[0], TileChildType.PowerUps);
        if(tPowerUp != null){
            showIcon(getTextureKey(TileChildType.PowerUps, tPowerUp));
        }
    }

    public void hideIcons(){
        for(int i = visibleSprites.size() - 1; i >= 0; i--){
            hideIcon(visibleSprites.get(i));
        }
    }

    public void hideIcon(GraphicsManager.TextureKeys k){
        if(visibleSprites.contains(k) && mapIcons.containsKey(k)){
            visibleSprites.remove(k);
            mapIcons.get(k).setVisible(false);
            refreshPositions();
        }
    }

    private void refreshPositions(){
        for(int i = 0; i < visibleSprites.size(); i++){
            GraphicsManager.TextureKeys k = visibleSprites.get(i);
            if(mapIcons.containsKey(k)){
                Sprite s = mapIcons.get(k);
                float offsetX = SPEECH_BUBBLE_LEFT_PADDING + baseX + (i * (Constant.HUD_PADDING + Constant.ICON_SMALL_64));
                s.setX(offsetX);
            }
        }
        repositionTextX();
    }

    private void repositionTextX(){
        text.setX(SPEECH_BUBBLE_CENTER_NATURAL_X + (visibleSprites.size() * (Constant.ICON_SMALL_64 / 2)));
    }
}
