package bbgames.ie.wordsmithnavigator.GameObjects.Animation;

import android.os.Handler;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.LetterTileOnBoard;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;

/**
 * Created by Bryan on 14/09/2018.
 */

public class ReplenishHUDTiles implements Animation{

    private final static int MS_DRAW_SACK_INTERVAL = 500;

    private GameActivity gameActivity;
    private HashMap<TileLetter, PowerUpLink> flagPowerUps;
    private Runnable nextAnimationRunnable;

    private ArrayList<TileLetter> lettersToReplenish;
    private Handler handler;
    private Runnable runnableReplenishment;
    private Runnable runnableSackHudLetters;
    private int remainingLettersOnHud;

    public ReplenishHUDTiles(GameActivity gameActivity,
                             HashMap<TileLetter, PowerUpLink> flagPowerUps,
                             Runnable nextAnimationRunnable) {
        this.gameActivity = gameActivity;
        this.flagPowerUps = flagPowerUps;
        this.nextAnimationRunnable = nextAnimationRunnable;
        this.lettersToReplenish = new ArrayList<>();
        this.handler = new Handler();
        this.runnableReplenishment = new Runnable() {
            @Override
            public void run() {
                runReplenishment();
            }
        };
        this.runnableSackHudLetters = new Runnable() {
            @Override
            public void run() {
                runSackHudLetters();
            }
        };
        remainingLettersOnHud = gameActivity.getPlayerManager().getActivePlayer().getMapHudLetterTiles().size() -
                            gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().size();
    }

    @Override
    public void run() {
        populateLettersToReplenishWithHudLetterTilesOnBoard();
        if(hasJumblePowerUp()){
            runnableSackHudLetters.run();
        }else {
            runnableReplenishment.run();
        }
    }

    private void populateLettersToReplenishWithHudLetterTilesOnBoard(){
        for(LetterTileOnBoard letterTileOnBoard : gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard()){
            lettersToReplenish.add(letterTileOnBoard.getHudLetterTile());
        }
    }

    /**
     * @return true if the player collected a jumble power up this turn
     */
    private boolean hasJumblePowerUp(){
        for(PowerUpLink p : flagPowerUps.values()){
            if(p.getType().equals(PowerUpLink.Type.LETTER_SACK_JUMBLE)){
                return true;
            }
        }
        return false;
    }

    /**
     * Triggers the main replenishment animation
     */
    private void runReplenishment(){
        int msDelay = 0;
        //Sort arr so that furthest letter tile gaps on rack are replenished first (for a quicker animation)
        final float xLetterSackSource = gameActivity.getHUD().getHudSceneWindow().getLeftX() + gameActivity.getHUD().getHudFloatingElements().getLetterSackXY()[0];
        Collections.sort(lettersToReplenish, new Comparator<TileLetter>() {
            @Override
            public int compare(TileLetter lhs, TileLetter rhs) {
                return Math.abs(xLetterSackSource - lhs.getX()) >
                        Math.abs(xLetterSackSource - rhs.getX()) ? -1 : 1;
            }
        });
        //Update each played letter tile to its new letter
        for(final TileLetter tlh : lettersToReplenish){
            //1. Choose a random letter to assign to this tile
            Character c = gameActivity.getPlayerManager().getActivePlayer().getLetterSack().getLetterBalancingVowelsAndConsonants(
                    gameActivity.getPlayerManager().getActivePlayer().getMapHudLetterTiles());
            if(c == null){
                //There are no more letters left to draw (Happens with hardcoded letters sack)
                handler.post(nextAnimationRunnable);
                break;
            }
            //2. Set the letter
            tlh.setLetter(c);
            //3. Hide the letter
            tlh.setState(LetterState.HIDDEN);
            //4. Set the letters position to be on top of the hud sack and move to slot
            RunnableDrawTileFromSack r = new RunnableDrawTileFromSack(tlh, gameActivity.getHUD(), new Runnable() {
                @Override
                public void run() {
                    if(lettersToReplenish.indexOf(tlh) == (lettersToReplenish.size() - 1)) {
                        nextAnimationRunnable.run();
                    }
                }
            });
            handler.postDelayed(r, msDelay);
            msDelay += MS_DRAW_SACK_INTERVAL;
        }
        gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().clear();
    }

    /**
     * Triggers the initial animation to move existing letters into sack i.e. jumble animation
     */
    private void runSackHudLetters(){
        for(final TileLetter hudLetterTile : gameActivity.getPlayerManager().getActivePlayer().getMapHudLetterTiles().values()){
            if(hudLetterTile.getState().equals(LetterState.HIDDEN)){
                continue;
            }
            if(!lettersToReplenish.contains(hudLetterTile)) {
                lettersToReplenish.add(hudLetterTile);
            }
            final float initialY = hudLetterTile.getY();
            MoveYModifier down = new MoveYModifier(0.5f, initialY, initialY - Constant.TILE_SIZE);
            AlphaModifier fade = new AlphaModifier(0.5f, 1.0f, 0.0f);
            hudLetterTile.registerEntityModifier(new ParallelEntityModifier(down, fade){
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    super.onModifierFinished(pModifier, pItem);
                    hudLetterTile.setY(initialY);
                    hudLetterTile.setAlpha(1.0f);
                    hudLetterTile.setState(LetterState.HIDDEN);
                    decrementRemainingLettersOnHud();
                    if(getRemainingLettersOnHud() == 0){
                        runnableReplenishment.run();
                    }
                }
            });
        }
    }

    private final Object lock = new Object();

    private void decrementRemainingLettersOnHud(){
        synchronized (lock){
            remainingLettersOnHud--;
        }
    }

    private int getRemainingLettersOnHud(){
        synchronized (lock){
            return remainingLettersOnHud;
        }
    }

    //region helper sub-classes

    /**
     * Given a tile, fade it in and raise it from the letter sack, then move x to position on tile rack, and drop into place
     */
    private class RunnableDrawTileFromSack implements Runnable {

        private float[] xyTargetOnTileRack;
        private TileLetter tile;
        private GameHud hud;
        private Runnable onAnimationFinishedRunnable;

        public RunnableDrawTileFromSack(TileLetter tile, GameHud hud, Runnable onAnimationFinishedRunnable) {
            this.xyTargetOnTileRack = new float[]{tile.getX(), tile.getY()};
            this.tile = tile;
            this.hud = hud;
            this.onAnimationFinishedRunnable = onAnimationFinishedRunnable;
        }

        @Override
        public void run() {
            float[] xyStarting = new float[]{hud.getHudFloatingElements().getLetterSackXY()[0], hud.getHudFloatingElements().getYRaised()};
            tile.setPosition(xyStarting[0], xyStarting[1]);
            tile.setState(LetterState.DISABLED);
            float yTop = xyStarting[1] + (Constant.TILE_SIZE * 1.2f);
            final MoveYModifier lower = new MoveYModifier((float)MS_DRAW_SACK_INTERVAL / 1000, yTop, xyTargetOnTileRack[1]){
                @Override
                protected void onModifierFinished(IEntity pItem) {
                    super.onModifierFinished(pItem);
                    if(onAnimationFinishedRunnable != null){
                        onAnimationFinishedRunnable.run();
                    }
                }
            };
            final MoveXModifier xMove = new MoveXModifier(Math.abs(xyStarting[0] - xyTargetOnTileRack[0]) / 1000,
                    tile.getX(), xyTargetOnTileRack[0]);
            final MoveYModifier raise = new MoveYModifier((float)MS_DRAW_SACK_INTERVAL / 1000, xyStarting[1], yTop);
            tile.clearEntityModifiers();
            tile.registerEntityModifier(new SequenceEntityModifier(raise, xMove, lower));
        }

    }

    //endregion

}

