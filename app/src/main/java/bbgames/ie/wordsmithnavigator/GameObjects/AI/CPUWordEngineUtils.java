package bbgames.ie.wordsmithnavigator.GameObjects.AI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.PathFinderOption;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 04/06/2018.
 */

public class CPUWordEngineUtils {

    /**
     * Filter all possible answers based on these enums to choose the most suitable answer
     */
    enum FILTERS{
        PATH_NEXT_TURN,         //null : if present, filter out words that don't touch the next turn (or cancel if that means filtering everything out)
        DESIRABLE_WORD_ENDING,  //null : if present, choose words with desirable word endings. See getDesirableWordEndings() javadoc
        DESIRABILITY,           //0.0 - 1.0f: The lower the value, the more likely the AI will choose a highly desirable word e.g..
        ENDZONE_PRIORITY,       //0.0 - 1.0f: The higher the value, the more likely the AI will choose the optimal path to get to the endzone i.e. Path[0]
        POPULARITY,             //0.1 - 1.0f: The lower the value, the more popular the word chosen will be e.g. 0.2f -> Chosen answer will be top 20% most popular answer available
        MIN_LENGTH,             //2f  - 10f : The minimum answer word length size.
        MAX_LENGTH,             //2f  - 10f : The maximum answer word length size
        LENGTH;                 //0.1 - 1.0f: The lower the value, the longer the word will be e.g. 0.2f -> Chosen answer will be top 20% of words available

        /**
         * We are willing to remove this pathOption if none of the answers satisfy this FILTER
         */
        public boolean isMandatory(){
            return this.equals(DESIRABLE_WORD_ENDING);
        }
    }

    public static LinkedHashMap<FILTERS, Float> getFiltersOrdered(AIDifficulty difficulty){
        LinkedHashMap<FILTERS, Float> output = new LinkedHashMap<>();
        switch (difficulty){
            case VERY_EASY:
                output.put(FILTERS.DESIRABILITY, 0.3f);     //Remove the bottom 30% desirable answers
                output.put(FILTERS.DESIRABLE_WORD_ENDING, null); //Remove any non-desirable word ending words
                output.put(FILTERS.MIN_LENGTH, 3f);         //Remove all words less than 2 letters long
                output.put(FILTERS.MAX_LENGTH, 4f);         //Remove all words more than 4 letters long
                output.put(FILTERS.POPULARITY, 0.2f);       //Remove words outside the top 20% ordered by most popular DESC
                output.put(FILTERS.LENGTH, 1f);             //Remove words outside the top 100% ordered by longest DESC
                output.put(FILTERS.ENDZONE_PRIORITY, 0.2f); //Consider the top 85% most optimal paths to take
                //output.put(FILTERS.PATH_NEXT_TURN, null);   //VERY_EASY does not filter these words out
                break;
            case EASY:
                output.put(FILTERS.DESIRABILITY, 0.4f);     //Remove the bottom 40% desirable answers
                output.put(FILTERS.MIN_LENGTH, 3f);         //Remove all words less than 2 letters long
                output.put(FILTERS.MAX_LENGTH, 5f);         //Remove all words more than 5 letters long
                output.put(FILTERS.POPULARITY, 0.35f);      //Remove words outside the top 35% ordered by most popular DESC
                output.put(FILTERS.DESIRABLE_WORD_ENDING, null); //Remove any non-desirable word ending words
                output.put(FILTERS.LENGTH, 0.8f);           //Remove words outside the top 80% ordered by longest DESC
                output.put(FILTERS.ENDZONE_PRIORITY, 0.5f); //Consider the top 40% most optimal paths to take
                output.put(FILTERS.PATH_NEXT_TURN, null);   //filter down to words that reach the next turn in the a star path (if possible)
                break;
            case NORMAL:
                output.put(FILTERS.DESIRABILITY, 0.5f);     //Remove the bottom 50% desirable answers
                output.put(FILTERS.MIN_LENGTH, 3f);         //Remove all words less than 3 letters long
                output.put(FILTERS.MAX_LENGTH, 6f);         //Remove all words more than 6 letters long
                output.put(FILTERS.DESIRABLE_WORD_ENDING, null); //Remove any non-desirable word ending words
                output.put(FILTERS.POPULARITY, 0.45f);      //Remove words outside the top 45% ordered by most popular DESC
                output.put(FILTERS.LENGTH, 0.7f);           //Remove words outside the top 70% ordered by longest DESC
                output.put(FILTERS.ENDZONE_PRIORITY, 0.75f); //Consider the top 25% most optimal paths to take
                output.put(FILTERS.PATH_NEXT_TURN, null);   //filter down to words that reach the next turn in the a star path (if possible)
                break;
            case HARD:
                output.put(FILTERS.DESIRABILITY, 0.6f);     //Remove the bottom 60% desirable answers
                output.put(FILTERS.DESIRABLE_WORD_ENDING, null); //Remove any non-desirable word ending words
                output.put(FILTERS.MIN_LENGTH, 4f);         //Remove all words less than 4 letters long
                output.put(FILTERS.MAX_LENGTH, 7f);         //Remove all words more than 7 letters long
                output.put(FILTERS.POPULARITY, 0.65f);      //Remove words outside the top 65% ordered by most popular DESC
                output.put(FILTERS.LENGTH, 0.5f);           //Remove words outside the top 50% ordered by longest DESC
                output.put(FILTERS.ENDZONE_PRIORITY, 0.9f); //Consider the top 10% most optimal paths to take
                output.put(FILTERS.PATH_NEXT_TURN, null);   //filter down to words that reach the next turn in the a star path (if possible)
                break;
            case VERY_HARD:
                output.put(FILTERS.DESIRABLE_WORD_ENDING, null); //Remove any non-desirable word ending words
                output.put(FILTERS.DESIRABILITY, 0.7f);     //Remove the bottom 70% desirable answers
                output.put(FILTERS.MIN_LENGTH, 5f);         //Remove all words less than 5 letters long
                output.put(FILTERS.MAX_LENGTH, 10f);        //Remove all words more than 10 letters long
                output.put(FILTERS.PATH_NEXT_TURN, null);   //filter down to words that reach the next turn in the a star path (if possible)
                output.put(FILTERS.POPULARITY, 1f);         //Remove words outside the top 100% ordered by most popular DESC
                output.put(FILTERS.LENGTH, 0.2f);           //Remove words outside the top 20% ordered by longest DESC
                output.put(FILTERS.ENDZONE_PRIORITY, 1f);   //Consider the most optimal path to take
                break;
            case IMPOSSIBLE:
                output.put(FILTERS.DESIRABLE_WORD_ENDING, null); //Remove any non-desirable word ending words
                output.put(FILTERS.DESIRABILITY, 0.9f);
                output.put(FILTERS.MIN_LENGTH, 6f);
                output.put(FILTERS.MAX_LENGTH, 10f);
                output.put(FILTERS.PATH_NEXT_TURN, null);
                output.put(FILTERS.POPULARITY, 1f);
                output.put(FILTERS.LENGTH, 0.1f);
                output.put(FILTERS.ENDZONE_PRIORITY, 1f);
                break;
        }
        return output;
    }

    public static PathFinderOption applyFilter(PathFinderOption option, ArrayList<String> layout, FILTERS filter, Float filterValue){
        ArrayList<HintWord> originalWords = option.getWords();
        ArrayList<HintWord> filteredWords = new ArrayList<>();
        int topN = filterValue == null ? 1 : (int) (originalWords.size() * filterValue);
        if(!filter.isMandatory() && originalWords.size() == 1 || topN < 1){
            return option;
        }
        switch (filter){
            case DESIRABLE_WORD_ENDING:
                if(option.getNextPathTurnXY() == null || option.getNextPathTurnDirection() == null){
                    return option;
                }
                filteredWords.addAll(getDesirableWordEndings(option, layout));
                break;
            case DESIRABILITY:
                Collections.sort(originalWords, new Comparator<HintWord>() {
                    @Override
                    public int compare(HintWord lhs, HintWord rhs) {
                        return Float.compare(rhs.getTotalDesirability(), lhs.getTotalDesirability());
                    }
                });
                for(int i = 0; i < originalWords.size() - topN; i++){
                    filteredWords.add(originalWords.get(i));
                }
                break;
            case PATH_NEXT_TURN:
                if(option.getNextPathTurnXY() == null){
                    return option;
                }
                for(HintWord w : originalWords){
                    if(w.hasLetterTouchingXY(option.getNextPathTurnXY())){
                        filteredWords.add(w);
                    }
                }
                if(filteredWords.isEmpty()){
                    return option; //Give up, none of the words suit, fall back to original
                }
                break;
            case POPULARITY:
                Collections.sort(originalWords, new Comparator<HintWord>() {
                    @Override
                    public int compare(HintWord lhs, HintWord rhs) {
                        return Double.compare(rhs.getDictionaryEntry().getFrequency(), lhs.getDictionaryEntry().getFrequency());
                    }
                });
                for(int i = 0; i < topN; i++){
                    filteredWords.add(originalWords.get(i));
                }
                break;
            case MIN_LENGTH:
                ArrayList<HintWord> longestWords = new ArrayList<>(); //Fallback in case all originalWords are shorter than MIN_LENGTH
                for(HintWord w : originalWords){
                    if(w.getLetters().size() >= filterValue){
                        filteredWords.add(w);
                    }
                    if(filteredWords.isEmpty()){
                        if(longestWords.isEmpty() || w.getLetters().size() > longestWords.get(0).getLetters().size()){
                            longestWords.clear();
                            longestWords.add(w);
                        }else if(!longestWords.isEmpty() && longestWords.get(0).getLetters().size() == w.getLetters().size()){
                            longestWords.add(w);
                        }
                    }
                }
                if(filteredWords.isEmpty()){
                    filteredWords.addAll(longestWords);
                }
                break;
            case MAX_LENGTH:
                ArrayList<HintWord> shortestWords = new ArrayList<>(); //Fallback in case all originalWords are longer than MAX_LENGTH
                for(HintWord w : originalWords){
                    if(w.getLetters().size() <= filterValue){
                        filteredWords.add(w);
                    }
                    if(filteredWords.isEmpty()){
                        if(shortestWords.isEmpty() || w.getLetters().size() < shortestWords.get(0).getLetters().size()){
                            shortestWords.clear();
                            shortestWords.add(w);
                        }else if(!shortestWords.isEmpty() && shortestWords.get(0).getLetters().size() == w.getLetters().size()){
                            shortestWords.add(w);
                        }
                    }
                }
                if(filteredWords.isEmpty()){
                    filteredWords.addAll(shortestWords);
                }
                break;
            case LENGTH:
                Collections.sort(originalWords, new Comparator<HintWord>() {
                    @Override
                    public int compare(HintWord lhs, HintWord rhs) {
                        return rhs.getDictionaryEntry().getWord().length() - lhs.getDictionaryEntry().getWord().length();
                    }
                });
                for(int i = 0; i < topN; i++){
                    filteredWords.add(originalWords.get(i));
                }
                int wordLength = filteredWords.get(filteredWords.size()-1).getDictionaryEntry().getWord().length();
                for(int i = filteredWords.size(); i < originalWords.size(); i++){
                    //After topN is reached, add the remaining words with the same LENGTH count as the last added word
                    if(originalWords.get(i).getDictionaryEntry().getWord().length() == wordLength){
                        filteredWords.add(originalWords.get(i));
                    }else {
                        break;
                    }
                }
                break;
            default:
                filteredWords.addAll(originalWords);
                break;
        }
        return new PathFinderOption(option.getXy(), option.getDirection(), filteredWords, option.getPathScore(), option.getNextPathTurnXY(), option.getNextPathTurnDirection());
    }

    /**
     * Returns all the words in an option *except* any 'One Tile Short' words.
     * A 'One Tile Short' word is an answer that, if played, would leave the CPU deadlocked and unlikely to further its progress.
     * Consider:
     *      ---TEEN
     *      ...$---
     *      .......
     *      .......
     *      ....---
     *      ....111
     * If the CPU plays 'THEIR' they are in a very bad position.
     * In order to get to the endzone they need to use the middle map tile on the last row. but there are not many 6-Letter words that
     * begin with 'THEIR'. This method will remove all 'One Tile Short' words, which in this example is any 5-letter words.
     * If all the 6-letter words have been filtered out, we will fall back to 4-letter words, then 3-letter words etc.
     * @param option
     * @param layout
     * @return
     */
    private static ArrayList<HintWord> getDesirableWordEndings(PathFinderOption option, ArrayList<String> layout) {

        boolean isWordBackwards = option.getDirection() == Direction.WEST || option.getDirection() == Direction.NORTH;

        //We need to eliminate words that end on xyOneTileShort[] OR end on an undesirable tile
        ArrayList<HintWord> output = new ArrayList<>();
        for(HintWord answer : option.getWords()){
            boolean isTouchingTargetXY = option.getNextPathTurnXY() != null && option.getNextPathTurnDirection() == null;
            if(isTouchingTargetXY){
                output.add(answer); //Playing this word will touch the target (endzone/detonator etc.), therefore its desirable word ending
                continue;
            }
            HintLetter lastLetterToBePlayed;
            if(isWordBackwards){
                lastLetterToBePlayed = answer.getLetters().get(0);
            }else {
                lastLetterToBePlayed = answer.getLetters().get(answer.getLetters().size()-1);
            }
            int[] adjacentTileXY = BoardUtils.getAdjacentTileXY(layout, lastLetterToBePlayed.getXY(), option.getNextPathTurnDirection());
            Character adjacentTile = LevelUtils.getCharacterAt(layout, adjacentTileXY);
            if(TileCodes.getTargetTileCodes().contains(adjacentTile)){
                output.add(answer);
            }else {
                if(Character.isLetter(adjacentTile)){
                    //The adjacent tile is occupied by a board letter. Is it a parallel word?
                    Direction d1 = BoardUtils.getInvertedDirection_NorthOrWest(option.getNextPathTurnDirection());
                    Direction d2 = BoardUtils.getOppositeDirection(d1);
                    Character ac1 = BoardUtils.getAdjacentCharacter(layout, adjacentTileXY, d1);
                    Character ac2 = BoardUtils.getAdjacentCharacter(layout, adjacentTileXY, d2);
                    boolean isParallelToBoardWord = (ac1 != null && Character.isLetter(ac1) && option.getDirection().equals(d2)) ||
                                                    (ac2 != null && Character.isLetter(ac2) && option.getDirection().equals(d1));
                    if(!isParallelToBoardWord){
                        output.add(answer);
                        continue;
                    }
                }
                boolean isAdjacentTileDesirable = Util.contains(
                        LevelUtils.getXYsOfTouchingPlayableTiles(layout, adjacentTileXY),
                        BoardUtils.getAdjacentTileXY(layout, adjacentTileXY, option.getNextPathTurnDirection()));
                if (TileCodes.getBlockTileCodes().contains(adjacentTile) || !isAdjacentTileDesirable) {
                    //This word ends on an undesirable map tile
                } else {
                    output.add(answer);
                }
            }
        }

        return output;
    }

}
