package bbgames.ie.wordsmithnavigator.GameObjects.Animation;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.MoveYModifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.CameraSpeed;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.DetonatorLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Bridge;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.DetonatorCrate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Gate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Responsible for complex UI Animations in the Game Activity (i.e. multiple frames)
 * Created by Bryan on 21/04/2017.
 */
public abstract class AnimationManager {

    private final static int MS_DRAW_SACK_INTERVAL = 500;
    private final static int MS_DETONATE_CRATE_INTERVAL = 200;
    private final static int MS_GATE_POINTS_DECREMENT_INTERVAL = 150;
    private final static int MS_GATE_FINISH_ANIMATION_DELAY = 100;
    private final static int MS_GATE_FINISH_ANIMATION_DURATION = 500;
    private final static int MS_BRIDGE_TURNS_DECREMENT_INTERVAL = 300;
    private final static int MS_BRIDGE_FINISH_ANIMATION_DURATION = 1000;
    private final static int MS_ROGUE_TILE_TRANSFORMATION_DURATION = 500;
    private final static int MS_FULL_ANIMATION_LENGTH = 2000;

    private enum Anim{
        detonateCrates,
        rogueTileTransformation,
        gatePointsUpdate,
        bridgeTurnsUpdate,
        successfulWord,
        replenishHudTiles,
        panCamera,
        swapPlayersHud
    }

    /**
     * Use for chaining multiple animations together
     */
    public static class Builder{

        private GameActivity gameActivity;
        private ArrayList<int[]> flagDetonatorMapTiles;
        private ArrayList<int[]> flagRogueTiles;
        private HashMap<TileLetter, PowerUpLink> flagPowerUps;
        private String successfulWord;
        private int points;
        private Player previousPlayer;
        private Player nextPlayer;
        private ArrayList<Anim> queuedAnimations;

        public Builder(GameActivity gameActivity){
            this.gameActivity = gameActivity;
            this.queuedAnimations = new ArrayList<>();
        }

        public void queueExplodeDetonateCrates(ArrayList<int[]> flagDetonatorMapTiles){
            this.flagDetonatorMapTiles = flagDetonatorMapTiles;
            this.queuedAnimations.add(Anim.detonateCrates);
        }

        public void queueTransformRogueTilesToLetterTiles(ArrayList<int[]> flagRogueTiles){
            this.flagRogueTiles = flagRogueTiles;
            this.queuedAnimations.add(Anim.rogueTileTransformation);
        }

        public void queueSuccessfulWordAnimation(String word, int points, HashMap<TileLetter, PowerUpLink> flagPowerUps) {
            this.successfulWord = word;
            this.points = points;
            this.queuedAnimations.add(Anim.successfulWord);
            this.flagPowerUps = flagPowerUps;
        }

        public void queueCameraToNextWord(Player nextPlayer){
            this.queuedAnimations.add(Anim.panCamera);
            this.nextPlayer = nextPlayer;
        }

        public void queueSwapPlayersHud(Player previousPlayer, Player nextPlayer){
            this.queuedAnimations.add(Anim.swapPlayersHud);
            this.previousPlayer = previousPlayer;
            this.nextPlayer = nextPlayer;
        }

        public void queueReplenishUsedHudTilesAnimation(HashMap<TileLetter, PowerUpLink> flagPowerUps) {
            this.flagPowerUps = flagPowerUps;
            this.queuedAnimations.add(Anim.replenishHudTiles);
        }

        public void queueGatePointsUpdate(int points) {
            this.points = points;
            this.queuedAnimations.add(Anim.gatePointsUpdate);
        }

        public void queueBridgeTurnsUpdate(){
            this.queuedAnimations.add(Anim.bridgeTurnsUpdate);
        }

        public void execute(boolean lockHudDuringAnimations, final Runnable onAnimationsFinishedRunnable){
            if(lockHudDuringAnimations){
                gameActivity.getHUD().setState(GameHud.STATE.LOCKED);
            }
            Collections.sort(queuedAnimations);
            executeNextAnimation(queuedAnimations, onAnimationsFinishedRunnable);
        }

        private void executeNextAnimation(final ArrayList<Anim> queuedAnimations, final Runnable onAnimationsFinishedRunnable) {
            if(queuedAnimations.isEmpty()){
                if(onAnimationsFinishedRunnable != null) {
                    onAnimationsFinishedRunnable.run();
                }
            }else {
                Anim nextAnim = queuedAnimations.get(0);
                queuedAnimations.remove(0);
                Runnable nextAnimationRunnable = new Runnable() {
                    @Override
                    public void run() {
                        executeNextAnimation(queuedAnimations, onAnimationsFinishedRunnable);
                    }
                };
                switch (nextAnim) {
                    case detonateCrates:
                        detonateCrates(gameActivity, flagDetonatorMapTiles, nextAnimationRunnable);
                        break;
                    case rogueTileTransformation:
                        transformRogueTilesToLetterTiles(gameActivity, flagRogueTiles, nextAnimationRunnable);
                        break;
                    case successfulWord:
                        successfulWordAnimation(gameActivity, successfulWord, points, flagPowerUps, nextAnimationRunnable);
                        break;
                    case panCamera:
                        panCameraToNextTile(gameActivity, nextPlayer, nextAnimationRunnable);
                        break;
                    case swapPlayersHud:
                        swapPlayersHud(gameActivity, previousPlayer, nextPlayer, nextAnimationRunnable);
                        break;
                    case replenishHudTiles:
                        new ReplenishHUDTiles(gameActivity, flagPowerUps, nextAnimationRunnable).run();
                        break;
                    case gatePointsUpdate:
                        updateAllGatePoints(gameActivity, points, nextAnimationRunnable);
                        break;
                    case bridgeTurnsUpdate:
                        updateAllBridgePoints(gameActivity, nextAnimationRunnable);
                        break;
                }
            }
        }
    }

    //region private individual animations

    private static void panCameraToNextTile(final GameActivity gameActivity, Player player, final Runnable onAnimationFinishedRunnable){
        final int currentSpeed = CameraSpeed.getCameraSpeed(gameActivity.getApplicationManager()).getMs();
        final int maxSpeed = CameraSpeed.FAST.getMs();
        gameActivity.getCamera().setMaxVelocity(maxSpeed, maxSpeed);
        int[] xyPanTo;
        if(player.getPlayedWords().size() == 1){
            //If the player is only starting, pan to the middle letter in the default word
            xyPanTo = player.getMiddleLetterOfLastWordPlayed().getXY();
        }else {
            xyPanTo = getXyOfNextTileToPanTo(player);
        }
        gameActivity.getCamera().panToXY(xyPanTo, true, new Runnable() {
            @Override
            public void run() {
                gameActivity.getCamera().setMaxVelocity(currentSpeed, currentSpeed);
                onAnimationFinishedRunnable.run();
            }
        });
    }

    private static int[] getXyOfNextTileToPanTo(Player player){
        if(player.getNextTileToPanTo() != null){
            Integer[] target = player.getNextTileToPanTo();
            player.setNextTileToPanTo(null);
            return Util.toPrimitive(target);
        }else {
            return player.getMiddleLetterOfLastWordPlayed().getXY();
        }
    }

    private static void swapPlayersHud(final GameActivity gameActivity, final Player previousPlayer, final Player nextPlayer, final Runnable onAnimationFinishedRunnable){

        final float yOnScreen =  Constant.TILE_SIZE / 2;
        final float yOffScreen = yOnScreen - Constant.TILE_SIZE;

        //Pre-Animation setup:
        for(TileLetter t : previousPlayer.getMapHudLetterTiles().values()){
            //All current players tiles must be on screen and visible
            t.setState(LetterState.DISABLED);
            t.setPosition(t.getX(), yOnScreen);
        }
        for(TileLetter t : nextPlayer.getMapHudLetterTiles().values()){
            //All new players tiles must be off screen and invisible
            t.setState(LetterState.HIDDEN);
            t.setPosition(t.getX(), yOffScreen);
        }

        new RunnableLowerSwapAndRaiseHudLetter(gameActivity, previousPlayer, nextPlayer, onAnimationFinishedRunnable, yOnScreen, yOffScreen).run();
    }

    private static void detonateCrates(final GameActivity gameActivity, ArrayList<int[]> flagDetonatorMapTiles, final Runnable onAnimationFinishedRunnable){
        ArrayList<DetonatorLink> detonatorLinks = new ArrayList<>();
        for(int[] xy : flagDetonatorMapTiles){
            detonatorLinks.addAll(gameActivity.getLevelManager().getLevelStaticData().getDetonatorLinks());
            gameActivity.getScene().detachTileChild(xy[1], xy[0], TileChildType.Detonator);
        }
        final ArrayList<DetonatorCrate> crates = new ArrayList<>();
        for(DetonatorLink detonatorLink : detonatorLinks){
            for (int[] xyCrate : detonatorLink.getXyCrates()){
                DetonatorCrate detonatorCrate = (DetonatorCrate) gameActivity.getScene().getTileChild(xyCrate[1], xyCrate[0], TileChildType.DetonatorCrate);
                crates.add(detonatorCrate);
            }
        }
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    int iCrate = 0;
                    boolean nextTileToPanToFound = false;
                    @Override
                    public void run() {
                        if(iCrate < crates.size()){
                            DetonatorCrate crate = crates.get(iCrate);
                            if(!nextTileToPanToFound && LevelUtils.isAdjacentToBoardLetter(gameActivity.getLevelManager().getLayoutModified(), Util.toPrimitive(crate.getColumnRow()))){
                                nextTileToPanToFound = true;
                                updatePlayersNextPanToTile(gameActivity, crate.getColumnRow());
                            }
                            gameActivity.getCamera().setCenterDirect(crate.getX(), crate.getY());
                            crate.detonate();
                            gameActivity.getVibrator().vibrate(MS_DETONATE_CRATE_INTERVAL);
                            iCrate++;
                            handler.postDelayed(this, MS_DETONATE_CRATE_INTERVAL);
                        }else {
                            onAnimationFinishedRunnable.run();
                        }
                    }
                };
                handler.postDelayed(runnable, MS_DETONATE_CRATE_INTERVAL);
            }
        });
    }

    private static void updateAllGatePoints(final GameActivity gameActivity, final int points, final Runnable onAnimationFinishedRunnable) {
        ArrayList<Gate> gates = gameActivity.getLevelManager().getGates();
        if(gates.isEmpty()){
            onAnimationFinishedRunnable.run();
        }else {
            updateGatePoints(0, gates, points, gameActivity, onAnimationFinishedRunnable);
        }
    }

    private static void updateGatePoints(final int index, final ArrayList<Gate> gates, final int points, final GameActivity gameActivity, final Runnable onAnimationFinishedRunnable){
        final Gate g = gates.get(index);
        gameActivity.getCamera().setCenterDirect(g.getPointsLock().getXYGate());
        final int oldScore = g.getCurrentPoints();
        final int newScore = oldScore - points > 0 ? (oldScore - points) : 0;
        TimerHandler timer = new TimerHandler ((float)MS_GATE_POINTS_DECREMENT_INTERVAL / 1000, new ITimerCallback()
        {
            int tempScore = oldScore;
            @Override
            public void onTimePassed(final TimerHandler pTimer)
            {
                if(tempScore > newScore)
                {
                    tempScore--;
                    g.setText(tempScore);
                    pTimer.reset();
                }
                else
                {
                    Handler h = new Handler(Looper.getMainLooper());
                    if(tempScore == 0){
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                g.openAnimation();
                            }
                        },MS_GATE_FINISH_ANIMATION_DELAY);
                        updatePlayersNextPanToTile(gameActivity, g.getColumnRow());
                    }
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            g.setCurrentPoints(tempScore);
                            gameActivity.getScene().unregisterUpdateHandler(pTimer);
                            if(index < (gates.size() - 1)){
                                updateGatePoints(index + 1, gates, points, gameActivity, onAnimationFinishedRunnable);
                            }else {
                                onAnimationFinishedRunnable.run();
                            }
                        }
                    }, MS_GATE_FINISH_ANIMATION_DURATION);
                }
            }
        });
        gameActivity.getScene().registerUpdateHandler(timer);
    }

    private static void updateAllBridgePoints(final GameActivity gameActivity, final Runnable onAnimationFinishedRunnable) {
        ArrayList<Bridge> bridges = gameActivity.getLevelManager().getBridges();
        if(bridges.isEmpty()){
            onAnimationFinishedRunnable.run();
        }else {
            updateBridgePoints(0, bridges, gameActivity, onAnimationFinishedRunnable);
        }
    }

    private static void updateBridgePoints(final int index, final ArrayList<Bridge> bridges, final GameActivity gameActivity, final Runnable onAnimationFinishedRunnable){
        final Bridge b = bridges.get(index);
        gameActivity.getCamera().setCenterDirect(b.getTurnsLock().getXYBridge());
        final int oldScore = b.getCurrentTurnsRemaining();
        final int newScore = oldScore > 0 ? oldScore - 1 : 0;
        TimerHandler timer = new TimerHandler ((float)MS_BRIDGE_TURNS_DECREMENT_INTERVAL / 1000, new ITimerCallback()
        {
            int tempScore = oldScore;
            @Override
            public void onTimePassed(final TimerHandler pTimer)
            {
                if(tempScore > newScore)
                {
                    tempScore--;
                    b.setTurns(tempScore);
                    pTimer.reset();
                }
                else
                {
                    Handler h = new Handler(Looper.getMainLooper());
                    if(tempScore == 0){
                        b.explodeAnimation();
                    }
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            b.setTurns(tempScore);
                            gameActivity.getScene().unregisterUpdateHandler(pTimer);
                            if(index < (bridges.size() - 1)){
                                updateBridgePoints(index + 1, bridges, gameActivity, onAnimationFinishedRunnable);
                            }else {
                                onAnimationFinishedRunnable.run();
                            }
                        }
                    }, MS_BRIDGE_FINISH_ANIMATION_DURATION);
                }
            }
        });
        gameActivity.getScene().registerUpdateHandler(timer);
    }

    private static void transformRogueTilesToLetterTiles(final GameActivity gameActivity,
                                                        final ArrayList<int[]> xyRogueTiles,
                                                        final Runnable onAnimationFinishedRunnable) {
        //Don't think we need to add these letters to players.listXYLetterTilesOnBoard, its just used for camera panning purposes
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gameActivity.getLevelManager().setFlagValue(LevelManager.StateFlag.USER_IS_CREATING_WORD, true);
                final Handler handler = new Handler();
                for(final int[] xyRogueTile : xyRogueTiles){
                    final float fadeDurationSeconds = (float)MS_ROGUE_TILE_TRANSFORMATION_DURATION / 1000;
                    final TileLetter rogueLetterSprite = gameActivity.getGraphicsManager().getLetterFactory().getLetter(xyRogueTile[1], xyRogueTile[0]);
                    rogueLetterSprite.registerEntityModifier(new AlphaModifier(fadeDurationSeconds, 1f, 0f));
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Character letter = Character.toLowerCase(rogueLetterSprite.getLetter());
                            Float[] pXpY = new Float[]{rogueLetterSprite.getX(), rogueLetterSprite.getY()};
                            gameActivity.getGraphicsManager().getLetterFactory().detachLetter(rogueLetterSprite);
                            TileLetter newTileLetter = gameActivity.getGraphicsManager().getLetterFactory().createLetterBoard(
                                    gameActivity.getPlayerManager().getActivePlayer().getLetterTileSkin(),
                                    letter, pXpY[0], pXpY[1], LetterState.LOCKED, gameActivity.getPlayerManager().getActivePlayer());
                            newTileLetter.setAlpha(0f);
                            newTileLetter.registerEntityModifier(new AlphaModifier(fadeDurationSeconds, 0f, 1f));
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if(xyRogueTiles.indexOf(xyRogueTile) == xyRogueTiles.size()-1) { //If ensures the code is only executed once, not each letter
                                        gameActivity.getLevelManager().setFlagValue(LevelManager.StateFlag.USER_IS_CREATING_WORD, false);
                                        onAnimationFinishedRunnable.run();
                                    }
                                }
                            }, (long)(fadeDurationSeconds * 1000));
                            //Update layout modified string
                            String layoutModifiedRow = gameActivity.getLevelManager().getLayoutModified().get(xyRogueTile[1]);
                            String newLayoutModified = layoutModifiedRow.substring(0, xyRogueTile[0]) + newTileLetter.getLetter() +
                                    (layoutModifiedRow.length() > (xyRogueTile[0] + 1) ? layoutModifiedRow.substring(xyRogueTile[0] + 1) : "");
                            gameActivity.getLevelManager().getLayoutModified().set(xyRogueTile[1], newLayoutModified);
                        }
                    }, (long)(fadeDurationSeconds * 1000));
                    updatePlayersNextPanToTile(gameActivity, Util.fromPrimitive(xyRogueTiles.get(xyRogueTiles.size() / 2)));
                }
            }
        });
    }

    private static void successfulWordAnimation(final GameActivity gameActivity,
                                                final String successfulWord,
                                                final int points,
                                                final HashMap<TileLetter, PowerUpLink> flagPowerUps,
                                                final Runnable nextAnimationRunnable){
        int bonusTurns = 0;
        int bonusPoints = 0;
        int bonusTime = 0;
        for(Map.Entry<TileLetter, PowerUpLink> p : flagPowerUps.entrySet()){
            switch (p.getValue().getType()){
                case TIME: bonusTime += p.getValue().getValue(); break;
                case POINTS: bonusPoints += (Util.getPointsForLetter(p.getKey().getLetter()) * p.getValue().getValue()); break;
                case MOVE: bonusTurns += p.getValue().getValue(); break;
            }
        }
        final int bTurns = bonusTurns;
        final int bPoints = bonusPoints;
        final int bTime = bonusTime;
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gameActivity.getHUD().attachChild(gameActivity.getGraphicsManager().getSceneShadow());
                gameActivity.getHUD().sortChildren();
                gameActivity.getHUD().SetPleaseWaitText();
                gameActivity.getHUD().getHudFloatingElements().stickyItems();
                gameActivity.getHUD().getHudFloatingElements().getTextSuccessfulWord().setText(successfulWord.toUpperCase());
                gameActivity.getHUD().getHudFloatingElements().getTextSuccessfulWord().setVisible(true);
                gameActivity.getHUD().getHudFloatingElements().incrementPoints(points, bPoints);
                gameActivity.getHUD().getHudFloatingElements().incrementTurns(bTurns);
                gameActivity.getHUD().getHudFloatingElements().incrementSeconds(bTime);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.getHUD().getHudFloatingElements().getTextSuccessfulWord().setVisible(false);
                        nextAnimationRunnable.run();
                    }
                }, MS_FULL_ANIMATION_LENGTH);
            }
        });
    }

    //endregion

    private static class RunnableLowerSwapAndRaiseHudLetter implements Runnable{

        private GameActivity gameActivity;
        private Player previousPlayer;
        private Player nextPlayer;
        private Runnable onAnimationFinishedRunnable;
        private float yOnScreen;
        private float yOffScreen;

        public RunnableLowerSwapAndRaiseHudLetter(GameActivity gameActivity, Player previousPlayer, Player nextPlayer, Runnable onAnimationFinishedRunnable, float yOnScreen, float yOffScreen){
            this.gameActivity = gameActivity;
            this.previousPlayer = previousPlayer;
            this.nextPlayer = nextPlayer;
            this.onAnimationFinishedRunnable = onAnimationFinishedRunnable;
            this.yOnScreen = yOnScreen;
            this.yOffScreen = yOffScreen;
        }

        @Override
        public void run() {

            final Runnable raise = new Runnable() {
                @Override
                public void run() {
                    previousPlayer.addUpdateHudTiles();
                    nextPlayer.addUpdateHudTiles();
                    gameActivity.getHUD().setState(nextPlayer.isHumanUser() ?
                            GameHud.STATE.ACTIVE_CONTROLLER :
                            GameHud.STATE.LOCKED);
                    ArrayList<TileLetter> letters = new ArrayList<>(nextPlayer.getMapHudLetterTiles().values());
                    for (TileLetter l : letters){
                        if(letters.indexOf(l) == letters.size() - 1){
                            l.registerEntityModifier(new MoveYModifier((float)MS_DRAW_SACK_INTERVAL / 1000, yOffScreen, yOnScreen){
                                @Override
                                protected void onModifierFinished(IEntity pItem) {
                                    super.onModifierFinished(pItem);
                                    onAnimationFinishedRunnable.run();
                                }
                            });
                        }else {
                            l.registerEntityModifier(new MoveYModifier((float)MS_DRAW_SACK_INTERVAL / 1000, yOffScreen, yOnScreen));
                        }
                    }
                }
            };

            final Runnable lower = new Runnable() {
                @Override
                public void run() {
                    ArrayList<TileLetter> letters = new ArrayList<>(previousPlayer.getMapHudLetterTiles().values());
                    for(TileLetter t : letters){
                        if(letters.indexOf(t) == letters.size() - 1){
                            t.registerEntityModifier(new MoveYModifier((float)MS_DRAW_SACK_INTERVAL / 1000, yOnScreen, yOffScreen){
                                @Override
                                protected void onModifierFinished(IEntity pItem) {
                                    gameActivity.getHUD().SetUser(nextPlayer);
                                    raise.run();
                                    super.onModifierFinished(pItem);
                                }
                            });
                        }else {
                            t.registerEntityModifier(new MoveYModifier((float)MS_DRAW_SACK_INTERVAL / 1000, yOnScreen, yOffScreen));
                        }
                    }
                }
            };

            lower.run();
        }
    }

    private static void updatePlayersNextPanToTile(GameActivity gameActivity, Integer[] xyNextTileToAimFor){
        Player player = gameActivity.getPlayerManager().getActivePlayer();
        Integer[] nextXy = LevelUtils.getClosestPlayedLetterTo(player, gameActivity.getLevelManager().getLayoutModified(), xyNextTileToAimFor);
        gameActivity.getPlayerManager().getActivePlayer().setNextTileToPanTo(nextXy);
    }


}
