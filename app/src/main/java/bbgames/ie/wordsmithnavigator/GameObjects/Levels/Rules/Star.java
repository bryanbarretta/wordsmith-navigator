package bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.LinkedHashMap;

import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelPlayerData;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelStaticData;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Static Star Information
 * Created by Bryan on 23/07/2016.
 */
public class Star implements Serializable{

    public enum Operation{
        GREATER_THAN(">"),
        GREATER_THAN_OR_EQUAL_TO(">="),
        LESS_THAN("<"),
        LESS_THAN_OR_EQUAL_TO("<=");

        private final String asString;
        private Operation(String asString){
            this.asString = asString;
        }
        public String getStringValue(){
            return asString;
        }
    }

    public enum Condition {
        MOVES(R.string.star_condition_moves),      //Finish the level with OPERATION x moves
        SECONDS(R.string.star_condition_seconds),  //Finish the level in OPERATION x seconds
        POINTS(R.string.star_condition_points);    //Finish the level with OPERATION x points

        private final int asStringResourceId;
        private Condition(int asStringResourceId){
            this.asStringResourceId = asStringResourceId;
        }
        public String getStringValue(Context context){
            return context.getResources().getString(asStringResourceId);
        }
    }

    private int id;
    private Operation operation;
    private int number;
    private Condition condition;

    public Star(String line){
        String[] parts = line.trim().split(" ");
        String part = parts[0].trim();
        if(part.equals("<")){
            operation = Star.Operation.LESS_THAN;
        }else if(part.equals("<=")){
            operation = Star.Operation.LESS_THAN_OR_EQUAL_TO;
        }else if(part.equals(">")){
            operation = Star.Operation.GREATER_THAN;
        }else if(part.equals(">=")){
            operation = Star.Operation.GREATER_THAN_OR_EQUAL_TO;
        }else {
            throw new RuntimeException("Invalid IconPoints operation: " + part);
        }
        number = Integer.valueOf(parts[1].trim());
        part = parts[2].trim().toLowerCase();
        if(part.startsWith("m")){
            condition = Star.Condition.MOVES;
            id = 0;
        }else if(part.startsWith("s")){
            condition = Star.Condition.SECONDS;
            id = 1;
        }else if(part.startsWith("p")){
            condition = Star.Condition.POINTS;
            id = 2;
        }else {
            throw new RuntimeException("Invalid IconPoints condition: " + part);
        }
    }

    public Star(Operation operation, int number, Condition condition){
        this.operation = operation;
        this.number = number;
        this.condition = condition;
        switch (condition){
            case MOVES:     id = 0; break;
            case SECONDS:   id = 1; break;
            case POINTS:    id = 2; break;
        }
    }

    public Operation getOperation() {
        return operation;
    }

    public int getNumber() {
        return number;
    }

    public Condition getCondition() {
        return condition;
    }

    public String getString(Context context){
        return getOperation().getStringValue() + " " + getNumber() + " " + getCondition().getStringValue(context);
    }

    public int getId() {
        return id;
    }

    public static void generateStarDetailsView(LinearLayout container, Activity activity, int levelId, Typeface tf){
        LevelPlayerData lpd = Util.getApplicationManager(activity).getLevelFactory().getLevelPlayerData(levelId);
        LevelStaticData lsd = Util.getApplicationManager(activity).getLevelFactory().getLevelStaticData(levelId);
        for (int i = 0; i < lpd.getMapIdStarUnlocked().size(); i++){
            boolean unlocked = lpd.getMapIdStarUnlocked().get(i);
            View rowStar = Util.inflateView(activity, R.layout.row_star);
            if(unlocked){
                ((ImageView)rowStar.findViewById(R.id.imgStar)).setImageResource(R.drawable.ic_star);
            }
            TextView txtStar = (TextView) rowStar.findViewById(R.id.txtStar);
            txtStar.setTypeface(tf);
            if(lpd.isComplete()){
                txtStar.setText(lsd.getStar(i).getString(activity) + (i == 0 ? "" : " " + activity.getString(R.string.and_above)));
                if(unlocked){
                    txtStar.setPaintFlags(txtStar.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
            }else {
                txtStar.setText("?");
            }
            container.addView(rowStar);
        }
    }

    /**
     * @param sessionStatistics
     * @return true if the criteria for this star is met in this session
     */
    public boolean isCriteriaMet(SessionStatistics sessionStatistics) {
        if(getCondition() == Star.Condition.MOVES){
            if(getOperation().equals(Star.Operation.GREATER_THAN)){
                if(sessionStatistics.getSubmittedWords().size() > getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.GREATER_THAN_OR_EQUAL_TO)){
                if(sessionStatistics.getSubmittedWords().size() >= getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.LESS_THAN)){
                if(sessionStatistics.getSubmittedWords().size() < getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.LESS_THAN_OR_EQUAL_TO)){
                if(sessionStatistics.getSubmittedWords().size() <= getNumber()){
                    return true;
                }
            }
        }
        else if(getCondition() == Star.Condition.POINTS){
            if(getOperation().equals(Star.Operation.GREATER_THAN)){
                if(sessionStatistics.getTotalPoints() > getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.GREATER_THAN_OR_EQUAL_TO)){
                if(sessionStatistics.getTotalPoints() >= getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.LESS_THAN)){
                if(sessionStatistics.getTotalPoints() < getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.LESS_THAN_OR_EQUAL_TO)){
                if(sessionStatistics.getTotalPoints() <= getNumber()){
                    return true;
                }
            }
        }
        else if(getCondition() == Star.Condition.SECONDS){
            if(getOperation().equals(Star.Operation.GREATER_THAN)){
                if(sessionStatistics.getTotalSeconds() > getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.GREATER_THAN_OR_EQUAL_TO)){
                if(sessionStatistics.getTotalSeconds() >= getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.LESS_THAN)){
                if(sessionStatistics.getTotalSeconds() < getNumber()){
                    return true;
                }
            }
            else if(getOperation().equals(Star.Operation.LESS_THAN_OR_EQUAL_TO)){
                if(sessionStatistics.getTotalSeconds() <= getNumber()){
                    return true;
                }
            }
        }
        return false;
    }

}
