package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Manager class for the clock of each player and how they appear on the HUD
 * Created by Bryan on 15/04/2018.
 */

public class HudClock {

    private GameActivity gameActivity;

    private TimerHandler timerHandler;
    private final boolean clockModeLimitedSecondsPerTurn;
    private final Integer clockSecondsPerTurn;
    private HashMap<Player, Text> mapPlayerClock;

    private Player currentPlayer;
    private Text clockText;
    private Sprite clock;
    private Sprite clockHand;

    private int xClock, yClock, xAnchorClockText;

    public HudClock(GameActivity gameActivity, int xClock, int yClock){
        this.gameActivity = gameActivity;
        this.clockSecondsPerTurn = gameActivity.getLevelManager().getLevelStaticData().getClockSecondsPerTurn();
        this.clockModeLimitedSecondsPerTurn = clockSecondsPerTurn != null;
        this.mapPlayerClock = new HashMap<>();
        this.xClock = xClock;
        this.yClock = yClock;
    }

    private void setup(){

        Integer xClockIcon = null;

        for(Player player : gameActivity.getPlayerManager().getPlayers()){
            Text clockText = new Text(xClock, yClock - 5,
                    gameActivity.getGraphicsManager().getFont(
                            GraphicsManager.FontKeys.alcubierre,
                            gameActivity.getResources().getColor(player.getThemePlayer().getHudTextColor())),
                    "00:00",
                    6, gameActivity.getVertexBufferObjectManager());
            mapPlayerClock.put(player, clockText);
            if(xClockIcon == null) {
                xClockIcon = (int) (xClock - (clockText.getWidth() / 2) - 48);
            }
        }

        clock = new Sprite(xClockIcon, yClock,
                gameActivity.getHUD().gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Clock),
                gameActivity.getHUD().gameActivity.getVertexBufferObjectManager());
        clockHand = new Sprite(xClockIcon, yClock,
                gameActivity.getHUD().gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.ClockHand),
                gameActivity.getHUD().gameActivity.getVertexBufferObjectManager());
        xAnchorClockText = (int) (clock.getX() + (Constant.ICON_SMALL_64 / 2) + Constant.HUD_PADDING_SMALL);
        gameActivity.getHUD().hudbg.attachChild(clock);
        gameActivity.getHUD().hudbg.attachChild(clockHand);
    }

    /**
     * A second has passed
     */
    private void onClockSecondPassed(){
        int ts = Util.convertMMSStoSeconds(clockText.getText().toString());
        ts = clockModeLimitedSecondsPerTurn ? (ts - 1) : (ts + 1);
        if(ts < 0){
            gameActivity.toastOnUiThread("Time is up! TODO: implement");
        }else {
            currentPlayer.setTotalSeconds(currentPlayer.getTotalSeconds() + 1);
            updateClocks(ts);
        }
    }

    private void updateClocks(int totalSeconds){
        String txt = Util.convertTotalSecondsToStringMMSS(totalSeconds);
        gameActivity.getHUD().getHudFloatingElements().setTimeSeconds(txt);
        clockText.setText(txt);
        clockText.setPosition(xAnchorClockText + clockText.getWidth() / 2, clockText.getY());
        if(timerHandler != null) {
            timerHandler.reset();
        }
        clockHand.setRotation((totalSeconds % 60) * 6f);
    }

    public void showPlayersClock(Player player){
        if(mapPlayerClock.isEmpty()){
            setup();
        }
        if(clockText != null){
            gameActivity.getHUD().hudbg.detachChild(clockText);
        }
        clockText = mapPlayerClock.get(player);
        int ts = clockModeLimitedSecondsPerTurn ? clockSecondsPerTurn : player.getTotalSeconds();
        clockText.setText(Util.convertTotalSecondsToStringMMSS(ts));
        clockHand.setRotation((ts % 60) * 6f);
        clockText.setPosition(xAnchorClockText + clockText.getWidth() / 2, clockText.getY());
        currentPlayer = player;
        clock.setColor(clockText.getColor());
        gameActivity.getHUD().hudbg.attachChild(clockText);
    }

    public void startTimer(){
        if(currentPlayer == null){
            currentPlayer = gameActivity.getPlayerManager().getActivePlayer();
        }
        if(clockText == null){
            if(mapPlayerClock.isEmpty()){
                setup();
            }
            clockText = mapPlayerClock.get(currentPlayer);
            clockText.setText(Util.convertTotalSecondsToStringMMSS(clockModeLimitedSecondsPerTurn ?
                    clockSecondsPerTurn :
                    currentPlayer.getTotalSeconds()
            ));
            clockText.setPosition(xAnchorClockText + clockText.getWidth() / 2, clockText.getY());
            gameActivity.getHUD().hudbg.attachChild(clockText);
        }
        if(timerHandler == null){
            timerHandler = new TimerHandler(1f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    onClockSecondPassed();
                }
            });
        }
        gameActivity.getScene().registerUpdateHandler(timerHandler);
    }

    public void stopTimer(){
        if(timerHandler != null){
            gameActivity.getScene().unregisterUpdateHandler(timerHandler);
            timerHandler = null;
        }
    }

    public void addBonusSeconds(int bonusSeconds) {
        if(clockModeLimitedSecondsPerTurn){
            currentPlayer.addBonusSecondsToNextTurn(bonusSeconds);
        }else {
            currentPlayer.setTotalSeconds(currentPlayer.getTotalSeconds() - bonusSeconds);
            clockText = mapPlayerClock.get(currentPlayer);
            updateClocks(currentPlayer.getTotalSeconds());
        }
    }
}
