package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.camera.hud.controls.DigitalOnScreenControl;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.HudSide;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelStaticData;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Highlight;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.FocusTile;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.Sprites.Hourglass;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 08/05/2016.
 */
public class GameHud extends HUD {

    public enum STATE{
        ACTIVE_CONTROLLER,  //HUD enabled, analog camera controller stick visible
        ACTIVE_BUTTONS,     //HUD enabled, tick/undo/cancel buttons visible
        LOCKED              //HUD disabled, text saying "Please wait/CPU's turn" etc. visible
    }

    private String LOGTAG = getClass().getSimpleName();

    //region local variables

    public static final int MIN_NUM_VOWELS = 2;
    public static final int MIN_NUM_CONSONANTS = 5;
    private static final float OPACITY_VISIBLE = 1.0f;
    private static final float OPACITY_LOCKED = 0.1f;

    protected int side;
    protected GameActivity gameActivity;
    protected int hudLetterCapacity;
    protected LevelStaticData.Bounds hudBounds;
    protected HudClock hudClock;
    protected HudFloatingElements hudFloatingElements;

    //Stores the x,y coordinates of each tile letter slot on the rack
    protected HashMap<Integer, float[]> mapTileRackLetterCoordinates = new HashMap<>();

    public Sprite spriteTileRack;
    private DigitalOnScreenControl controller;
    protected Sprite hudbg, controllerBackground;
    protected SpeechBubble speechBubble;
    protected ButtonSprite btnTick, btnUndo, btnCancel, btnMenu, btnInfo;
    protected Hourglass hourglass;
    protected Text textLock;
    protected Text textName;
    protected Sprite imgPlayer;

    protected boolean controllerEnabled = true;
    protected boolean cpuIsPlayingWord = false;

    private STATE state;
    private HudSceneWindow hudSceneWindow;

    //endregion

    public GameHud(GameActivity gameActivity, int hudLetterCapacity){
        this(HudSide.getHudSide(gameActivity.getApplicationManager()).getSide(), gameActivity, hudLetterCapacity);
    }

    public GameHud(int side, GameActivity gameActivity, int hudLetterCapacity){
        this.side = side == Gravity.LEFT ? Gravity.LEFT : Gravity.RIGHT;
        this.gameActivity = gameActivity;
        this.hudLetterCapacity = hudLetterCapacity;
        createHud();
    }

    private String getPotentialWord(TileLetter hudLetter){        String potentialWord = gameActivity.getLevelManager().getCurrentWord().toString();
        if(BoardUtils.isReverseDirection(gameActivity.getLevelManager().getFocusTile().getDirection())){
            potentialWord = hudLetter.getLetter() + potentialWord;
        }else {
            potentialWord += hudLetter.getLetter();
        }
        return potentialWord;
    }

    public void moveLetterFromHudToBoard(TileLetter hudLetter){
        if(!hudLetter.hasParent()){
            Log.e(LOGTAG, "'" + hudLetter.getLetter() + "' is a rogue sprite! Parent should be the hud");
            return;
        }
        if(gameActivity.getLevelManager().getFocusTile() == null){
            Log.e(LOGTAG, "Trying to moveLetterFromHudToBoard but focusTile is null!");
            return;
        }

        int[] nextXY = LevelUtils.incrementXY(gameActivity.getLevelManager().getFocusTile().getXy(),
                gameActivity.getLevelManager().getFocusTile().getDirection());
        Character nextChar = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), nextXY);

        String potentialWord = getPotentialWord(hudLetter);
        boolean isPotentialWordValid = gameActivity.getApplicationManager().getDatabaseGateway().isWordExists(potentialWord);
        boolean autoSubmitWord = nextChar == null || TileCodes.getBlockTileCodes().contains(nextChar); //as the next tile is a blocker

        if(autoSubmitWord && !isPotentialWordValid){
            gameActivity.showToast(String.format(gameActivity.getString(R.string.word_does_not_exist), potentialWord));
            return;
        }

        //Update the current word and UI (place the hud letter on the board)
        gameActivity.getLevelManager().setFlagValue(LevelManager.StateFlag.USER_IS_CREATING_WORD, true);
        HintLetter potentialLetter = new HintLetter(gameActivity.getLevelManager().getFocusTile().getXy()[1],
                                                    gameActivity.getLevelManager().getFocusTile().getXy()[0],
                                                    hudLetter.getLetter(), false, true);
        HintWord currentWord = gameActivity.getLevelManager().getCurrentWord();
        if(BoardUtils.isReverseDirection(gameActivity.getLevelManager().getFocusTile().getDirection())){
            currentWord.addLetter(potentialLetter, true);
        }else {
            currentWord.addLetter(potentialLetter);
        }
        gameActivity.getLevelManager().setCurrentWord(currentWord);
        detachHudLetterAndPlaceOnMapTile(hudLetter, gameActivity.getLevelManager().getFocusTile().getXy());

        if(autoSubmitWord){
            //We have hit a wall/the border, so submit the word
            gameActivity.getLevelManager().submitCurrentWord(true);
        }else {
            if (Character.isLetter(nextChar)) {
                //Hop the letter/s
                gameActivity.getLevelManager().hopLetterTilesFromFocusedTile();
            } else {
                //Move to the next map tile
                if (gameActivity.getPlayerManager().getActivePlayer().isHumanUser()) {
                    setState(STATE.ACTIVE_BUTTONS, true, hudLetter);
                    setButtonXEnabled(true);
                    setButtonUndoEnabled(true);
                    setButtonTickEnabled(isPotentialWordValid);
                } else {
                    setState(STATE.LOCKED);
                    setButtonXEnabled(false);
                    setButtonUndoEnabled(false);
                    setButtonTickEnabled(false);
                }
                gameActivity.getLevelManager().setFocusTile(
                        new FocusTile(nextXY, gameActivity.getLevelManager().getFocusTile().getDirection()),
                        false);
            }
        }
    }

    public void moveWordFromHudToBoard(final HintWord hintWord, int milliDelayPerLetterPlayed){
        cpuIsPlayingWord = true;
        try {
            Handler handler = new Handler();
            setState(STATE.LOCKED);
            PlayWordToBoardRunnable playWordToBoardRunnable = new PlayWordToBoardRunnable(hintWord, milliDelayPerLetterPlayed, handler) {
                @Override
                public void onWordPlayed() {
                    gameActivity.unlockScreen();
                    cpuIsPlayingWord = false;
                    setState(STATE.LOCKED, true, null);
                }
            };
            handler.postDelayed(playWordToBoardRunnable, milliDelayPerLetterPlayed);
        } catch (Exception e) {
            Util.throwException(gameActivity, "moveWordFromHudToBoard Error trying to play " + hintWord.toString() + ": " + e.getMessage());
        }
    }

    //region public onClick handlers

    public void btnXPressed(){
        if(btnCancel.isEnabled()){
            gameActivity.getLevelManager().clearCurrentWord();
            setState(STATE.ACTIVE_CONTROLLER);
        }
    }

    public void btnTickPressed(){
        if(btnTick.isEnabled()){
            gameActivity.getLevelManager().submitCurrentWord();
        }
    }

    public void btnMenuPressed(){
        gameActivity.showOptions();
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnMenu.setAlpha(OPACITY_LOCKED);
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnMenu.setAlpha(OPACITY_VISIBLE);
                            }
                        });
                    }
                };
                new Handler().postDelayed(r, 200);
            }
        });
    }

    public void btnInfoPressed(){
        //gameActivity.showOptions();
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnInfo.setAlpha(OPACITY_LOCKED);
                hudFloatingElements.animate(true);
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnInfo.setAlpha(OPACITY_VISIBLE);
                            }
                        });
                    }
                };
                new Handler().postDelayed(r, 200);
            }
        });
    }

    public void btnUndoPressed() {
        ArrayList<LetterTileOnBoard> hudLetters = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard();
        if(btnUndo.isEnabled()){
            LetterTileOnBoard lastHudLetter = null;
            if(!hudLetters.isEmpty()) {
                lastHudLetter = hudLetters.get(hudLetters.size() - 1);
                gameActivity.getLevelManager().moveLetterFromBoardToHud(lastHudLetter.getBoardColRow()[1], lastHudLetter.getBoardColRow()[0]);
            }
            if (hudLetters.isEmpty()) {
                gameActivity.getLevelManager().clearCurrentWord();
            }
        }
    }

    //endregion

    //region public HUD ui state

    public void setState(STATE state){
        setState(state, false, null);
    }

    /**
     * @param state
     * @param updateHudLetterTiles If true, updates the state of the hud letter tiles also
     * @param excludedHudLetters These hud letters state will NOT be updated
     */
    public void setState(STATE state, boolean updateHudLetterTiles, TileLetter... excludedHudLetters){
        if(this.state != null && this.state.equals(state)){
            return;
        }
        this.state = state;
        switch (state){
            case ACTIVE_CONTROLLER:
                setHudAnalogueVisibility(true);
                setHudButtonsTickUndoCancelVisibility(false);
                setHudButtonsInfoMenuVisibility(true);
                setHudLockTextVisibility(false);
                if(updateHudLetterTiles) {
                    setStateHudTiles(LetterState.ENABLED, excludedHudLetters);
                }
                break;
            case ACTIVE_BUTTONS:
                setHudAnalogueVisibility(false);
                setHudButtonsTickUndoCancelVisibility(true);
                setHudButtonsInfoMenuVisibility(true);
                setHudLockTextVisibility(false);
                if(updateHudLetterTiles) {
                    setStateHudTiles(LetterState.ENABLED, excludedHudLetters);
                }
                break;
            case LOCKED:
                setHudAnalogueVisibility(false);
                setHudButtonsTickUndoCancelVisibility(false);
                setHudButtonsInfoMenuVisibility(false);
                setHudLockTextVisibility(true);
                if(updateHudLetterTiles) {
                    setStateHudTiles(LetterState.DISABLED, excludedHudLetters);
                }
                break;
        }
    }

    public STATE getState() {
        return this.state;
    }

    public void setButtonTickEnabled(boolean enabled){
        btnTick.setEnabled(enabled);
        btnTick.setAlpha(enabled ? OPACITY_VISIBLE : OPACITY_LOCKED);
    }

    public void setButtonXEnabled(boolean enabled){
        btnCancel.setEnabled(enabled);
        btnCancel.setAlpha(enabled ? OPACITY_VISIBLE : OPACITY_LOCKED);
    }

    public void setButtonUndoEnabled(boolean enabled) {
        btnUndo.setEnabled(enabled);
        btnUndo.setAlpha(enabled ? OPACITY_VISIBLE : OPACITY_LOCKED);
    }

    public void initialiseWunderClockManager(int xClock, int yClock){
        this.hudClock = new HudClock(gameActivity, xClock, yClock);
    }

    //endregion

    //region public functionality

    public void ShowSpeechBubble(String text){
        speechBubble.setVisible(true);
        speechBubble.setText(text);
    }

    public void HideSpeechBubble(){
        speechBubble.setVisible(false);
        speechBubble.hideIcons();
    }

    void AddPointsToScore(int points) {
        final int oldScore = gameActivity.getPlayerManager().getActivePlayer().getTotalPoints();
        final int newScore = gameActivity.getPlayerManager().getActivePlayer().getTotalPoints() + points;
        gameActivity.getPlayerManager().getActivePlayer().setTotalPoints(newScore);
        TimerHandler timer = new TimerHandler (0.05f, new ITimerCallback()
        {
            int tempScore = oldScore;
            @Override
            public void onTimePassed(final TimerHandler pTimer)
            {
                if(tempScore < newScore)
                {
                    tempScore++;
                    hudFloatingElements.setTextPoints(tempScore);
                    pTimer.reset();
                }
                else
                {
                    gameActivity.getScene().unregisterUpdateHandler(pTimer);
                }
            }
        });
        gameActivity.getScene().registerUpdateHandler(timer);
    }

    /**
     * Normally we increment move count +1 every turn. If there are bonus move power ups, we subtract these.
     * e.g. 1 bonus moves = +1 (every turn) - 1 (bonus) = 0 moves incremented this turn
     * e.g. 2 bonus moves = +1 (every turn) - 2 (bonus) = -1 moves incremented this turn
     * @param bonusMoves
     */
    void IncrementMoves(int bonusMoves){
        int actualTurns = 1 - bonusMoves;
        gameActivity.getPlayerManager().getActivePlayer().setTotalMoves(
                gameActivity.getPlayerManager().getActivePlayer().getTotalMoves() + actualTurns
        );
        hudFloatingElements.setTextTurns(gameActivity.getPlayerManager().getActivePlayer().getTotalMoves());
    }

    public void addBonusSeconds(int bonusSeconds) {
        this.hudClock.addBonusSeconds(bonusSeconds);
    }

    public void RefreshHud(){
        Player player = gameActivity.getPlayerManager().getActivePlayer();
        player.addUpdateHudTiles();
        setState(player.isHumanUser() ? STATE.ACTIVE_CONTROLLER : STATE.LOCKED, true, null);
        HideSpeechBubble();
        for (Player p : gameActivity.getPlayerManager().getPlayers()){
            boolean isActivePlayer = p.equals(player);
            for(Map.Entry<Integer, TileLetter> entry : p.getMapHudLetterTiles().entrySet()){
                if(isActivePlayer){
                    entry.getValue().setVisible(true);
                    registerTouchArea(entry.getValue());
                }else {
                    entry.getValue().setVisible(false);
                    unregisterTouchArea(entry.getValue());
                }
            }
        }
    }

    public void SetUser(Player nextPlayer) {
        String name = nextPlayer.getName();
        if(name.length() > textName.getCharactersMaximum()){
            name = name.substring(0, textName.getCharactersMaximum() - 1);
        }
        if(name.contains(" ") && name.indexOf(" ") > 3){
            name = name.substring(0, name.indexOf(" "));
        }
        int textColor = gameActivity.getResources().getColor(nextPlayer.getThemePlayer().getHudTextColor());
        this.hudClock.showPlayersClock(nextPlayer);
        this.textName.setText(name);
        this.textName.setPosition((this.imgPlayer.getX() * 2) + (this.textName.getWidth() / 2), this.textName.getY());
        this.textName.setColor(textColor);
        this.textLock.setColor(textColor);
        this.textLock.setText(gameActivity.getString(R.string.please_wait));
        this.imgPlayer.setTextureRegion(nextPlayer.getIcon());
        this.hudbg.setTextureRegion(nextPlayer.getIcHudBackground());
        this.hudFloatingElements.setPlayer(nextPlayer);
    }

    public void SetPleaseWaitText() {
        this.textLock.setText(gameActivity.getString(R.string.please_wait));
    }

    //endregion

    //region public getters

    public ArrayList<LetterTileOnBoard> getHudLetterTilesOnBoard(){
        return gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard();
    }

    public int GetTotalSeconds(){
        return gameActivity.getPlayerManager().getActivePlayer().getTotalSeconds();
    }

    public HashMap<Integer, float[]> getMapTileRackLetterCoordinates() {
        return mapTileRackLetterCoordinates;
    }

    public int GetHudLetterCapacity(){
        return hudLetterCapacity;
    }

    public HudClock GetClock() {
        return hudClock;
    }

    public HudFloatingElements getHudFloatingElements(){
        return hudFloatingElements;
    }

    public HudSceneWindow getHudSceneWindow() {
        if(hudSceneWindow == null){
            hudSceneWindow = new HudSceneWindow(gameActivity);
        }
        return hudSceneWindow;
    }

    public SpeechBubble getSpeechBubble() {
        return speechBubble;
    }

    //endregion

    //region private

    private void createHud() {
        hudBounds = new LevelStaticData.Bounds(
                new float[]{side == Gravity.LEFT ? 0 : Constant.CAMERA_1920 - Constant.HUD_WIDTH, Constant.CAMERA_1080}, //topLeft
                new float[]{side == Gravity.LEFT ? Constant.HUD_WIDTH : Constant.CAMERA_1920, Constant.CAMERA_1080},     //topRight
                new float[]{side == Gravity.LEFT ? 0 : Constant.CAMERA_1920 - Constant.HUD_WIDTH, 0},                    //bottomLeft
                new float[]{side == Gravity.LEFT ? Constant.HUD_WIDTH : Constant.CAMERA_1920, 0}                         //bottomRight
        );
        HudBuilder.createBackground(this);
        HudBuilder.createTileRack(this);
        HudBuilder.createSpeechBubble(this);
        HudBuilder.assignTileRackLetterCoordinates(this);
        HudBuilder.createSideHudControls(this);
        hudFloatingElements = new HudFloatingElements(this);
        controller = HudBuilder.createController(this);
        setChildScene(controller);
    }

    private void detachHudLetterAndPlaceOnMapTile(TileLetter hudLetter, int[] boardXY){
        LetterTileOnBoard letterTileOnBoard = new LetterTileOnBoard(hudLetter, boardXY);
        gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().add(letterTileOnBoard);
        //1. Remove the letter from the Hud, but store it in hudLetterTilesOnBoard in case it comes back
        hudLetter.setState(LetterState.HIDDEN);
        //2.Analyse the map tile and take action if it has children
        speechBubble.showIcons(boardXY);
        //3. Add a clone of the letterTile to the board
        Player owner = gameActivity.getPlayerManager().getActivePlayer();
        float[] pXY = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), boardXY[1], boardXY[0]);
        gameActivity.getGraphicsManager().getLetterFactory().createLetterBoard(
                owner.getLetterTileSkin(),
                hudLetter.getLetter(),
                pXY[0],
                pXY[1],
                LetterState.PLAYING,
                owner);
        int[] xyFocus = gameActivity.getLevelManager().getFocusTile().getXy();
        Highlight h = (Highlight) gameActivity.getScene().getTileChild(xyFocus[1], xyFocus[0], TileChildType.Highlight);
        h.setArrowVisible(false);
    }

    private void setStateHudTiles(LetterState state, TileLetter... excludedHudLetter){
        for(TileLetter hudTile : gameActivity.getPlayerManager().getActivePlayer().getMapHudLetterTiles().values()){
            if(excludedHudLetter != null && excludedHudLetter.length > 0 && Arrays.asList(excludedHudLetter).contains(hudTile)){
                continue;
            }
            hudTile.setState(state);
        }
    }

    private void setHudAnalogueVisibility(boolean visible){
        this.controller.setVisible(visible);
        this.controllerBackground.setVisible(visible);
        this.controllerEnabled = visible;
        if(hasChildScene() && !visible){
            clearChildScene();
        }else if(!hasChildScene() && visible){
            setChildScene(controller);
        }
    }

    private void setHudButtonsTickUndoCancelVisibility(boolean visible){
        btnTick.setVisible(visible);
        btnCancel.setVisible(visible);
        btnUndo.setVisible(visible);
        setButtonTickEnabled(visible);
        setButtonUndoEnabled(visible);
        setButtonXEnabled(visible);
    }

    private void setHudButtonsInfoMenuVisibility(boolean visible){
        btnInfo.setVisible(visible);
        btnInfo.setEnabled(visible);
        btnInfo.setAlpha(visible ? OPACITY_VISIBLE : OPACITY_LOCKED);
        btnMenu.setVisible(visible);
        btnMenu.setEnabled(visible);
        btnMenu.setAlpha(visible ? OPACITY_VISIBLE : OPACITY_LOCKED);
    }

    private void setHudLockTextVisibility(boolean visible){
        textLock.setVisible(visible);
        hourglass.setVisible(visible);
    }

    private abstract class PlayWordToBoardRunnable implements Runnable {

        private HintWord word;
        private int delay;
        private Handler handler;

        private int index;
        private boolean wordSubmitted;

        public PlayWordToBoardRunnable(HintWord word, int delay, Handler handler){
            this.word = word;
            this.delay = delay;
            this.handler = handler;
        }

        @Override
        public void run() {
            moveFocusToWordStartPosition();
            String lettersToPlay = "";
            for(int i = 0; i < word.getLetters().size(); i++){
                HintLetter h = word.getLetters().get(i);
                if(h != null && !h.isLockedOnBoard()){
                    lettersToPlay += h.getLetter();
                }
            }
            index = 0;
            final int[] indexHudLettersToPlay = getIndexOfHudLettersToPlay(lettersToPlay.toString(), gameActivity.getPlayerManager().getActivePlayer().getHudLetters());
            wordSubmitted = false;
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if(index < indexHudLettersToPlay.length){
                        TileLetter h = gameActivity.getPlayerManager().getActivePlayer().getMapHudLetterTiles().get(indexHudLettersToPlay[index]);
                        moveLetterFromHudToBoard(h);
                        index++;
                        handler.postDelayed(this, delay);
                    }else {
                        if(!wordSubmitted){
                            gameActivity.getLevelManager().submitCurrentWord();
                            wordSubmitted = true;
                            handler.postDelayed(this, delay);
                        }else {
                            onWordPlayed();
                        }
                    }
                }
            };
            handler.postDelayed(runnable, delay);
        }

        private int[] getIndexOfHudLettersToPlay(String word, String hudLetters) {
            int[] arr = new int[word.length()];
            for(int i = 0; i < word.length(); i++){
                arr[i] = hudLetters.indexOf(word.charAt(i));
                hudLetters = hudLetters.replaceFirst(word.charAt(i)+"", "-");
            }
            return arr;
        }

        private void moveFocusToWordStartPosition(){
            int[] newFocusTileXY = null;
            Direction newDirection;
            newDirection = BoardUtils.getDirectionRelativeToRowColumns(
                    word.getLetters().get(0).getRow(), word.getLetters().get(0).getColumn(),
                    word.getLetters().get(1).getRow(), word.getLetters().get(1).getColumn());
            for(HintLetter h : word.getLetters()){
                if(!h.isLockedOnBoard()){
                    newFocusTileXY = h.getXY();
                    break;
                }
            }
            if(newFocusTileXY == null){
                Util.throwException(gameActivity, "moveWordFromHudToBoard - no newFocusTile available!");
            }
            gameActivity.getScene().touchMapTile(newFocusTileXY[1], newFocusTileXY[0], newDirection);
        }

        public abstract void onWordPlayed();
    }

    //endregion

}
