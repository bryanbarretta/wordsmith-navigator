package bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules;

import java.util.ArrayList;
import java.util.List;

import bbgames.ie.wordsmithnavigator.Constant;

/**
 * Created by Bryan on 08/09/2018.
 */

public class PowerUpLink {

    public enum Type{
        POINTS("P"),
        TIME("T"),
        MOVE("M"),
        LETTER_SACK_JUMBLE("L");
        private String key;
        Type(String key){
            this.key = key;
        }
        public static Type getType(String key){
            for(Type t : values()){
                if(t.key.equals(key.toUpperCase().trim())){
                    return t;
                }
            }
            return null;
        }
        public int getColor() {
            switch (this){
                default:
                case POINTS:
                    return Constant.COLOR_GREEN;
                case MOVE:
                    return Constant.COLOR_BLUE;
                case TIME:
                    return Constant.COLOR_YELLOW_DARK;
                case LETTER_SACK_JUMBLE:
                    return Constant.COLOR_BROWN;
            }
        }
    }

    private Type type;
    private int[] xy;
    private int value;

    /**
     * Use the public static method called 'build' to create new instances
     * @param type
     * @param map
     */
    private PowerUpLink(Type type, String map){
        this.type = type;
        String[] parts = map.split("\\|");
        this.value = type.equals(Type.LETTER_SACK_JUMBLE) ? 1 : Integer.valueOf(parts[0]);
        String[] xyStrings = parts[type.equals(Type.LETTER_SACK_JUMBLE) ? 0 : 1].split(",");
        this.xy = new int[]{Integer.valueOf(xyStrings[0].trim()), Integer.valueOf(xyStrings[1].trim())};
    }

    public Type getType() {
        return type;
    }

    public int[] getXy() {
        return xy;
    }

    public int getValue() {
        return value;
    }

    /**
     * Only way to instantiate a power up/s
     * @param rule: 4 examples below
     * [P] [2|3,0][3|5,2]   ...would mean 2x Points bonus at 3,0 & 3x Points bonus at 5,2
     * [T] [5|3,4]          ...would mean 5 second bonus at 3,4 (i.e. -5 seconds)
     * [M] [1|2,7]          ...would mean 1 move bonus at 2,7 (i.e. next move does not increment move counter)
     * [L] [3,7]            ...would mean letter sack jumble at 3,7
     * @return
     */
    public static List<PowerUpLink> build(String rule){
        List<PowerUpLink> powerUps = new ArrayList<>();

        String key = rule.trim();
        int iClosedBracket = key.indexOf("]");
        Type type = Type.getType(key.trim().substring(1, iClosedBracket));

        String mapping = key.substring(iClosedBracket + 1).trim();
        List<String> maps = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for(Character c : mapping.toCharArray()){
            if(c == '[' || c == ' '){
                continue;
            }
            if(c == ']'){
                maps.add(sb.toString());
                sb = new StringBuilder();
                continue;
            }
            sb.append(c);
        }

        for (String map : maps){
            powerUps.add(new PowerUpLink(type, map));
        }

        return powerUps;
    }

}
