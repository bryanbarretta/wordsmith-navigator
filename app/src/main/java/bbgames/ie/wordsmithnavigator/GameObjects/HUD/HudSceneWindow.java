package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import android.view.Gravity;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.HudSide;

/**
 * The scene window is the inverse of the hud. It is the scene portion of the screen, not take up by hud.
 * i.e. Beside the Hud side bar, and above the tile rack at the bottom of the screen.
 * This class holds the scene window co-ordinates and dimensions (may be different depending which side the sidebar is on)
 * Created by Bryan on 15/06/2018.
 */

public class HudSceneWindow {

    private final float leftX, centerX, rightX, bottomY, centerY, topY, width, height;

    public HudSceneWindow(GameActivity gameActivity) {
        HudSide hudSide = HudSide.getHudSide(gameActivity.getApplicationManager());
        this.width = Constant.CAMERA_1920 - Constant.HUD_WIDTH;
        this.height = Constant.CAMERA_1080 - Constant.TILE_RACK_HEIGHT;
        this.leftX = hudSide.getSide() == Gravity.RIGHT ? 0.0f : Constant.HUD_WIDTH;
        this.rightX = leftX + width;
        this.centerX = leftX + (width / 2);
        this.bottomY = Constant.TILE_RACK_HEIGHT;
        this.topY = Constant.CAMERA_1920;
        this.centerY = bottomY + (height / 2);
    }

    public float getLeftX() {
        return leftX;
    }

    public float getCenterX() {
        return centerX;
    }

    public float getRightX() {
        return rightX;
    }

    public float getBottomY() {
        return bottomY;
    }

    public float getCenterY() {
        return centerY;
    }

    public float getTopY() {
        return topY;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
