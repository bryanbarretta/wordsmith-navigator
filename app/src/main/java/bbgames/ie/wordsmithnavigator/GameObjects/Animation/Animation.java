package bbgames.ie.wordsmithnavigator.GameObjects.Animation;

/**
 * Created by Bryan on 14/09/2018.
 */

public interface Animation {

    void run();

}
