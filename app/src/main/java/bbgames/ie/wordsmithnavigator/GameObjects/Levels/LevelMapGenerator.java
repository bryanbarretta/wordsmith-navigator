package bbgames.ie.wordsmithnavigator.GameObjects.Levels;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelStaticData;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Generate a level map given a LevelProperties object
 * Example: https://www.dropbox.com/s/70s31eix2h05mka/screenshot_level_map_generator.PNG?dl=0
 * Created by Bryan on 06/08/2016.
 */
public class LevelMapGenerator {

    private int colorMap = Constant.COLOR_GREEN;
    private int colorStartTile = Constant.COLOR_BLUE;
    private int colorEndTile = Constant.COLOR_YELLOW;
    private int colorDynamite = Constant.COLOR_RED;
    private int colorCrate = Constant.COLOR_BROWN;
    private int colorWall = Constant.COLOR_GRAY;
    private int colorLetter = Constant.COLOR_WHITE;
    private int colorRogueLetter = Constant.COLOR_BLACK;
    private int colorHintLetter = Constant.COLOR_LT_GREEN;

    private Direction arrowDirection;

    private final ArrayList<String> layout;
    private final int rows;
    private final int columns;

    public LevelMapGenerator(LevelStaticData l){
        this.layout = l.getLayoutOriginal();
        this.rows = l.getRows();
        this.columns = l.getColumns();
    }

    public LevelMapGenerator(LevelManager l){
        this.layout = l.getLayoutModified();
        this.rows = l.getLevelStaticData().getRows();
        this.columns = l.getLevelStaticData().getColumns();
    }


    public void GenerateMap(final ImageView mapContainer, final HintWord selectedWord, final boolean showLetters){
        ViewTreeObserver textViewTreeObserver=mapContainer.getViewTreeObserver();
        textViewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            public void onGlobalLayout() {
                applyMapToView(mapContainer, selectedWord, showLetters);
                mapContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    //region private map methods

    private void applyMapToView(ImageView mapContainer, HintWord selectedWord, boolean showLetters){

        int maxHeight = mapContainer.getMeasuredHeight();
        int maxWidth = mapContainer.getMeasuredWidth();
        if(maxHeight == 0 || maxWidth == 0){
            return;
        }

        int tileSquareSize;

        if(rows > columns)
        {
            tileSquareSize = maxHeight / rows;
        }
        else
        {
            tileSquareSize = maxWidth / columns;
        }

        Bitmap bg = Bitmap.createBitmap(tileSquareSize * columns,
                tileSquareSize * rows,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bg);
        Paint paint = new Paint();
        paint.setColor(mapContainer.getContext().getResources().getColor(R.color.primary));
        canvas.drawRect(-3, -3,
                (tileSquareSize * layout.get(0).length()) + 3,
                (tileSquareSize * layout.size()) + 3,
                paint);

        int x = 0;
        int y = 0;
        for(String row : layout){
            for(Character character : row.toCharArray()){
                paint.setColor(getColorForTile(character, isHintTile(x, y, selectedWord)));
                canvas.drawRect((x*tileSquareSize)+1,
                        (y*tileSquareSize)+1,
                        (x*tileSquareSize)+(tileSquareSize-1),
                        (y*tileSquareSize)+(tileSquareSize-1),
                        paint);
                if(character.equals(TileCodes.StartingZone.getCharacter())){
                    drawArrowTile(canvas, paint, x, y, tileSquareSize,
                            getArrowDirection() != null ? getArrowDirection() : LevelUtils.generateFocusTileDirection(layout));
                }
                if(character.equals(TileCodes.Gate.getCharacter())){
                    drawGate(canvas, paint, x, y, tileSquareSize);
                }
                if(character.equals(TileCodes.Bridge.getCharacter())){
                    drawBridge(canvas, paint, x, y, tileSquareSize);
                }
                if(Character.isLetter(character) && showLetters){
                    drawLetter(character, canvas, paint, x, y, tileSquareSize);
                }
                x++;
            }
            y++;
            x = 0;
        }

        mapContainer.setImageBitmap(bg);
    }

    private void drawGate(Canvas canvas, Paint paint, int x, int y, int tileSquareSize) {
        int strokeWidth = tileSquareSize / 6;
        int xLeftBar =  (tileSquareSize / 3);
        int xRightBar = xLeftBar * 2;
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(Color.DKGRAY);

        Path path = new Path();
        path.moveTo((x * tileSquareSize) + xLeftBar, (y * tileSquareSize));
        path.lineTo((x * tileSquareSize) + xLeftBar, (y * tileSquareSize) + tileSquareSize);
        path.close();
        canvas.drawPath(path, paint);

        path = new Path();
        path.moveTo((x * tileSquareSize) + xRightBar, (y * tileSquareSize));
        path.lineTo((x * tileSquareSize) + xRightBar, (y * tileSquareSize) + tileSquareSize);
        path.close();
        canvas.drawPath(path, paint);

        paint.setStyle(Paint.Style.FILL);
    }

    private void drawBridge(Canvas canvas, Paint paint, int x, int y, int tileSquareSize) {
        int strokeWidth = tileSquareSize / 3;

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(colorCrate);

        Path path = new Path();
        path.moveTo((x * tileSquareSize) + 0, (y * tileSquareSize) + 0);
        path.lineTo((x * tileSquareSize) + tileSquareSize, (y * tileSquareSize) + tileSquareSize);
        path.close();
        canvas.drawPath(path, paint);

        paint.setStyle(Paint.Style.FILL);
    }

    private boolean isHintTile(int x, int y, HintWord selectedWord) {
        if(selectedWord == null){
            return false;
        }
        for (HintLetter l : selectedWord.getLetters()){
            if(l.getXY()[0] == x && l.getXY()[1] == y){
                return true;
            }
        }
        return false;
    }

    private void drawArrowTile(Canvas canvas, Paint paint, int x, int y, int tileSquareSize, Direction direction){
        int centreX = (x*tileSquareSize) + (tileSquareSize/2);
        int centreY = (y*tileSquareSize) + (tileSquareSize/2);
        int radius = tileSquareSize/3;
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(2);
        paint.setColor(Color.YELLOW);
        Path path = new Path();
        switch (direction){
            default:
            case SOUTH:
                path.moveTo(centreX, centreY + radius);
                path.lineTo(centreX + radius, centreY - radius);
                path.lineTo(centreX - radius, centreY - radius);
                break;
            case NORTH:
                path.moveTo(centreX, centreY - radius);
                path.lineTo(centreX + radius, centreY + radius);
                path.lineTo(centreX - radius, centreY + radius);
                break;
            case EAST:
                path.moveTo(centreX + radius, centreY);
                path.lineTo(centreX - radius, centreY + radius);
                path.lineTo(centreX - radius, centreY - radius);
                break;
            case WEST:
                path.moveTo(centreX - radius, centreY);
                path.lineTo(centreX + radius, centreY + radius);
                path.lineTo(centreX + radius, centreY - radius);
                break;
        }
        path.close();
        canvas.drawPath(path, paint);
        paint.setStyle(Paint.Style.FILL);
    }

    private void drawLetter(Character character, Canvas canvas, Paint paint, int x, int y, int tileSquareSize){
        int fontSize = (int)(tileSquareSize * 0.75);
        paint.setColor(Util.isRogueTile(character) ? Color.WHITE : Color.BLACK);
        paint.setTextSize(fontSize);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(Typeface.MONOSPACE);
        int centreX = (x*tileSquareSize) + (tileSquareSize/2);
        int centreY = (y*tileSquareSize) + (tileSquareSize/2) - (int)((paint.descent() + paint.ascent()) / 2);
        canvas.drawText(Character.toUpperCase(character)+"", centreX, centreY, paint);
    }

    private int getColorForTile(Character tile, boolean isHintTile) {
        if(Character.isLetter(tile)) {
            return Util.isRogueTile(tile) ? colorRogueLetter : colorLetter;
        }else if(tile.equals(TileCodes.MapTile.getCharacter())){
            return isHintTile ? colorHintLetter : colorMap;
        }else if(tile.equals(TileCodes.Blank.getCharacter())){
            return Color.TRANSPARENT;
        }else if(tile.equals(TileCodes.StartingZone.getCharacter())){
            return colorStartTile;
        }else if(tile.equals(TileCodes.EndZone.getCharacter())){
            return isHintTile ? colorHintLetter : colorEndTile;
        }else if(tile.equals(TileCodes.Detonator.getCharacter())){
            return isHintTile ? colorHintLetter : colorDynamite;
        }else if(tile.equals(TileCodes.DetonatorCrate.getCharacter())){
            return isHintTile ? colorHintLetter : colorCrate;
        }else if(tile.equals(TileCodes.WallTile.getCharacter())){
            return colorWall;
        }else if(tile.equals(TileCodes.Bridge.getCharacter())){
            return Color.BLACK;
        }else if(tile.equals(TileCodes.PowerUp.getCharacter())){
            return isHintTile ? colorHintLetter : colorMap;
        }
        else {
            return colorMap;
        }
    }

    //endregion

    public Direction getArrowDirection() {
        return arrowDirection;
    }

    public void setArrowDirection(Direction arrowDirection) {
        this.arrowDirection = arrowDirection;
    }

}
