package bbgames.ie.wordsmithnavigator.GameObjects.Graphics;

import android.graphics.Color;
import android.graphics.Typeface;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.SpriteFactory.SpriteBatchManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.SpriteFactory.LetterFactory;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Bridge;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Gate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.PowerUp;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Detonator;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.DetonatorCrate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Diamond;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Responsible for loading the required textures (.png, .jpg, fonts etc) from assets folder on GameActivity creation.
 * Textures are loaded once into this manager, and held here to avoid re-loading the same textures again during the game.
 * Created by Bryan on 07/05/2016.
 */
public class GraphicsManager {

    private static final String BASE_PATH = "gfx/";

    private GameActivity gameActivity;
    private LetterFactory letterFactory;
    private SpriteBatchManager spriteBatchManager;
    private int themeHud;
    private int themeMap;
    private List<SkinLetter> skins;
    private int diamondId = 0;

    private HashMap<TextureKeys, TextureRegion> mapTextures = new HashMap<>();
    private HashMap<TiledTextureKeys, TiledTextureRegion> mapKeyTiledTextures = new HashMap<>();

    public int GetDefaultThemeHudId() {
        return themeHud;
    }

    public enum TextureKeys{
        TileMap,
        TileEndzone,
        TileWall,
        TileHighlight,
        TileArrowUp,
        TileRack,
        HudBackground,
        HudControllerBase,
        HudControllerKnob,
        HudControllerBackground,
        HudControllerTickEnabled,
        HudControllerTickDisabled,
        HudControllerCancelEnabled,
        HudControllerCancelDisabled,
        HudTextShadow,
        Btn_Tick,
        Btn_Cancel,
        Btn_Undo,
        Btn_Menu,
        Btn_Info,
        Detonator,
        WallDynamite,
        Diamond,
        DiamondHallow,
        Trophy,
        TrophyHallow,
        Border,
        BorderCorner,
        User_CPU,       //TODO: Add more User or allow user to customize
        User,
        LetterSack,
        IconTurns,
        IconPoints,
        IconTime,
        Clock,
        ClockHand,
        SpeechBubble,
        Hourglass,
        HourglassSand,
        Bridge_hole,
        Bridge,
        BonusClock,
        BonusMoves,
        BonusPoints,
        BonusSack,
        BonusClockMini,
        BonusMovesMini,
        BonusPointsMini,
        BonusSackMini,
        Shuffle
    }

    public static enum TiledTextureKeys{
        Letters_Board,
        Letters_Hud,
        Letters_Rogue,
        Anim_Diamond,
        Anim_Explosion,
        Gate
    }

    public enum FontKeys{
        alcubierre,
        alcubierreSmall,
        belepotan,
        litsansLarge,
        kelvetica,
        litsans,
        roboto,
        robotoTiny
    }

    private Rectangle sceneShadow;

    public GraphicsManager(GameActivity gameActivity, int themeHud, int themeMap){
        this.gameActivity = gameActivity;
        this.themeHud = themeHud;
        this.themeMap = themeMap;
        this.skins = new ArrayList<>();
        for(Player p : gameActivity.getPlayerManager().getPlayers()){
            skins.add(p.getLetterTileSkin());
        }
    }

    public void initiate(){
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(BASE_PATH);
        loadCommonGraphics();
        loadHudTextures();
        loadTextures();
        letterFactory = new LetterFactory(gameActivity, skins);
        spriteBatchManager = new SpriteBatchManager(gameActivity, themeMap);
    }

    //region Getters

    public List<SkinLetter> getSkins() {
        return skins;
    }

    public LetterFactory getLetterFactory(){
        return this.letterFactory;
    }

    public TextureRegion GetTextureRegion(TextureKeys key){
        return mapTextures.get(key);
    }

    public TiledTextureRegion GetTiledTextureRegion(TiledTextureKeys key){
        return mapKeyTiledTextures.get(key);
    }

    public Font getFont(FontKeys key){
        return getFont(key, getFontMainColor(themeHud));
    }

    public Font getFont(FontKeys key, int color){
        switch (key){
            case alcubierre:
                return getFontTexture("font/alcubierre.ttf", Constant.FONT_MEDIUM, color, 256);
            case alcubierreSmall:
                return getFontTexture("font/alcubierre.ttf", Constant.FONT_SMALL, color,  256);
            case belepotan:
                return getFontTexture("font/belepotan.ttf", Constant.FONT_MEDIUM, color,  256);
            case litsansLarge:
                return getFontTexture("font/litsans.ttf", Constant.FONT_LARGE, color,  512);
            case litsans:
                return getFontTexture("font/litsans.ttf", Constant.FONT_SMALL_MEDIUM_BRIDGE, color,    256);
            case kelvetica:
                return getFontTexture("font/kelvetica.ttf", Constant.FONT_MEDIUM, color,  256);
            case roboto:
                return getFontTexture("font/roboto.ttf", Constant.FONT_MEDIUM, color,  256);
            case robotoTiny:
                return getFontTexture("font/roboto.ttf", Constant.FONT_SMALL, color,  256);
        }
        return null;
    }

    public TextureRegion LoadDynamicTextureRegion(String assetDirectoryPath, String assetFileName, int textureWidth, int textureHeight){
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(assetDirectoryPath);
        BitmapTextureAtlas tile_TextureAtlas = new BitmapTextureAtlas(gameActivity.getTextureManager(), textureWidth, textureHeight, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegion output = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tile_TextureAtlas, gameActivity, assetFileName, 0, 0);
        tile_TextureAtlas.load();
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(BASE_PATH); //Return to default
        return output;
    }

    public Rectangle getSceneShadow(){
        if(sceneShadow == null){
            sceneShadow = new Rectangle(Constant.CAMERA_1920 / 2, Constant.CAMERA_1080 / 2,
                    Constant.CAMERA_1920, Constant.CAMERA_1080, gameActivity.getVertexBufferObjectManager());
            sceneShadow.setAlpha(0.75f);
            sceneShadow.setColor(0.0f, 0.0f, 0.0f);
            sceneShadow.setZIndex(-1);
        }
        return sceneShadow;
    }

    //endregion

    //region Loading

    private void loadCommonGraphics(){
        String dir = "common/";
        mapKeyTiledTextures.put(TiledTextureKeys.Anim_Diamond, getTiledTextureRegionThemed(dir + "diamond_grey.png",
                Constant.DIAMOND_BOARD_WIDTH * 15, Constant.DIAMOND_BOARD_HEIGHT, 15, 1));
        mapKeyTiledTextures.put(TiledTextureKeys.Anim_Explosion, getTiledTextureRegionThemed(dir + "fire.png",
                Constant.TILE_SIZE * 4, Constant.TILE_SIZE * 6, 4, 6));
        mapTextures.put(TextureKeys.Detonator, getTextureRegion(dir + "detonator.png", Constant.DETONATOR_HEIGHT_WIDTH));
        mapTextures.put(TextureKeys.WallDynamite, getTextureRegion(dir + "dynamite_crate.jpg", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.Diamond, getTextureRegion(dir + "diamond.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.DiamondHallow, getTextureRegion(dir + "diamond_hallow.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.Trophy, getTextureRegion(dir + "trophy.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.TrophyHallow, getTextureRegion(dir + "trophy_hallow.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.IconTurns, getTextureRegion(dir + "ic_moves.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.IconPoints, getTextureRegion(dir + "ic_points.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.IconTime, getTextureRegion(dir + "ic_time.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.Clock, getTextureRegion(dir + "clock.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.ClockHand, getTextureRegion(dir + "clock_hand.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.Border, getTextureRegion(dir + "border.jpg", Constant.TILE_SIZE, Constant.TILE_SIZE / 2));
        mapTextures.put(TextureKeys.BorderCorner, getTextureRegion(dir + "border_corner.jpg", Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2));
        mapTextures.put(TextureKeys.User_CPU, getTextureRegion(dir + "ic_user_cpu.jpg", Constant.ICON_96));
        mapTextures.put(TextureKeys.User, getTextureRegion(dir + "ic_user.png", Constant.ICON_96));
        mapTextures.put(TextureKeys.LetterSack, getTextureRegion(dir + "letter_sack.png", Constant.LETTER_SACK));
        mapTextures.put(TextureKeys.SpeechBubble, getTextureRegion(dir + "speech_bubble.png", Constant.SPEECH_BUBBLE_WIDTH, Constant.SPEECH_BUBBLE_HEIGHT));
        mapTextures.put(TextureKeys.Hourglass, getTextureRegion(dir + "hourglass.png", Constant.HOURGLASS_WIDTH_HEIGHT));
        mapTextures.put(TextureKeys.HourglassSand, getTextureRegion(dir + "hourglass_sand.png", Constant.HOURGLASS_SAND_WIDTH_HEIGHT));
        mapTextures.put(TextureKeys.BonusClock, getTextureRegion(dir + "bonus_clock.png", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.BonusMoves, getTextureRegion(dir + "bonus_move.png", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.BonusPoints, getTextureRegion(dir + "bonus_points.png", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.BonusSack, getTextureRegion(dir + "bonus_sack.png", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.Shuffle, getTextureRegion(dir + "ic_shuffle.png", Constant.ICON_48));
        mapTextures.put(TextureKeys.BonusClockMini, getTextureRegion(dir + "ic_time_mini.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.BonusMovesMini, getTextureRegion(dir + "ic_moves_mini.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.BonusPointsMini, getTextureRegion(dir + "ic_points_mini.png", Constant.ICON_SMALL_64));
        mapTextures.put(TextureKeys.BonusSackMini, getTextureRegion(dir + "ic_sack_mini.png", Constant.ICON_SMALL_64));
    }

    private void loadHudTextures(){
        String dir = "hud/" + themeHud + "/";
        mapTextures.put(TextureKeys.Btn_Tick, getTextureRegion(dir + "btn_tick.png", Constant.HUD_BUTTON_LONG_WIDTH, Constant.HUD_BUTTON_HEIGHT));
        mapTextures.put(TextureKeys.Btn_Cancel, getTextureRegion(dir + "btn_cancel.png", Constant.HUD_BUTTON_WIDTH));
        mapTextures.put(TextureKeys.Btn_Undo, getTextureRegion(dir + "btn_undo.png", Constant.HUD_BUTTON_WIDTH));
        mapTextures.put(TextureKeys.Btn_Menu, getTextureRegion(dir + "btn_menu.png", Constant.HUD_BUTTON_WIDTH));
        mapTextures.put(TextureKeys.Btn_Info, getTextureRegion(dir + "btn_info.png", Constant.HUD_BUTTON_WIDTH));
        mapTextures.put(TextureKeys.HudControllerBase, getTextureRegion(dir + "controller.png", Constant.HUD_CONTROLLER_BASE));
        mapTextures.put(TextureKeys.HudBackground, getTextureRegion(dir + "hud_bg.jpg", Constant.HUD_WIDTH, Constant.CAMERA_1080));
        mapTextures.put(TextureKeys.HudControllerKnob, getTextureRegion(dir + "controller_stick.png", Constant.HUD_CONTROLLER_KNOB));
        mapTextures.put(TextureKeys.HudControllerBackground, getTextureRegion(dir + "controller_stick_bg.png", Constant.HUD_CONTROLLER_BACKGROUND));
        mapTextures.put(TextureKeys.TileRack, getTextureRegion(dir + "tile_rack.png", Constant.CAMERA_1920, Constant.TILE_RACK_HEIGHT));
        mapTextures.put(TextureKeys.HudTextShadow, getTextureRegion(dir + "hud_shadowbar.png", Constant.CAMERA_1920 - Constant.HUD_WIDTH, Constant.HUD_TEXT_SHADOW_HEIGHT));
    }

    private void loadTextures(){
        String dir = "theme/" + themeMap + "/";
        mapTextures.put(TextureKeys.TileMap, getTextureRegion(dir + "tile_map.jpg", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.TileEndzone, getTextureRegion(dir + "tile_endzone.jpg", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.TileArrowUp, getTextureRegion(dir + "arrow_up.png", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.TileHighlight, getTextureRegion(dir + "highlight.png", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.TileWall, getTextureRegion(dir + "tile_wall.jpg", Constant.TILE_SIZE));
        mapKeyTiledTextures.put(TiledTextureKeys.Gate, getTiledTextureRegionThemed(dir + "gate.png",
                Constant.TILE_SIZE * 3, Constant.TILE_SIZE * 3, 3, 3));
        mapTextures.put(TextureKeys.Bridge_hole, getTextureRegion(dir + "bridge_hole.jpg", Constant.TILE_SIZE));
        mapTextures.put(TextureKeys.Bridge, getTextureRegion(dir + "bridge.png", Constant.TILE_SIZE, Constant.BRIDGE_HEIGHT));
    }

    //endregion

    //region Rendering

    /**
     * Render all the board tiles and graphics.
     * Sprites are loaded efficiently in 3 runs:
     * Run 1: Utilize SpriteBatch class, to load map + wall + endzone tiles
     * Run 2: Render individual sprites except letter tiles i.e. children sprites of map tiles
     * Run 3: Utilize SpriteGroup to render letter tiles
     */
    public void renderLevel(GameActivity gameActivity) {

        //#1. Render the sprite batch tiles
        spriteBatchManager.renderBaseTiles();
        
        //#2. Render individual sprite tiles
        for(int row = 0; row < gameActivity.getLevelManager().getLevelStaticData().getRows(); row++){
            String r = gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal().get(row);
            for(int col = 0; col < r.length(); col++){
                Character tileCode = r.charAt(col);
                if(Character.isLetter(tileCode) || tileCode.equals(TileCodes.MapTile.getCharacter()) ||
                                                   tileCode.equals(TileCodes.EndZone.getCharacter()) ||
                                                   tileCode.equals(TileCodes.WallTile.getCharacter())){
                    //Letters will be rendered in the next run (SpriteGroup)
                    //Map + EndZone + Wall Tiles were rendered in the previous run (SpriteBatch)
                    continue;
                }
                attachTileChildToScene(tileCode, row, col);
            }
        }

        //#3. Render the letters (SpriteGroup)
        for(int row = 0; row < gameActivity.getLevelManager().getLevelStaticData().getRows(); row++){
            String r = gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal().get(row);
            for(int col = 0; col < r.length(); col++){
                Character tileCode = r.charAt(col);
                if(Character.isLetter(tileCode)){
                    boolean isRogueTile = Character.isUpperCase(tileCode);
                    Player owner = isRogueTile ? null : gameActivity.getLevelManager().getTileOwnerUsingLevelStaticData(row, col);
                    float[] xy = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), row, col);
                    TileLetter tileLetter = gameActivity.getGraphicsManager().getLetterFactory().createLetterBoard(
                            isRogueTile ? gameActivity.getPlayerManager().getPlayers().get(0).getLetterTileSkin() : owner.getLetterTileSkin(),
                            tileCode,
                            xy[0],
                            xy[1],
                            isRogueTile ? LetterState.ROGUE : LetterState.LOCKED,
                            owner);
                    if(isRogueTile){
                        gameActivity.getLevelManager().addRogueTile(tileLetter);
                    }
                }
            }
        }

        //Finally... PowerUps
        for(PowerUpLink p : gameActivity.getLevelManager().getLevelStaticData().getPowerUpLinks()){
            gameActivity.getScene().attachTileChild(p.getXy()[1], p.getXy()[0], new PowerUp(gameActivity, p));
        }
    }

    private void attachTileChildToScene(Character tileCode, int row, int col){
        if(tileCode.equals(TileCodes.Diamond.getCharacter())){
            if(!gameActivity.getLevelManager().getSessionStatistics().getDiamondIds().contains(diamondId)){
                //only attach the diamond if we haven't acquired it recently
                Diamond diamond = new Diamond(gameActivity, diamondId);
                diamond.animate(100, true);
                diamondId++;
                gameActivity.getScene().attachTileChild(row, col, diamond);
            }
        }else if(tileCode.equals(TileCodes.Detonator.getCharacter())){
            gameActivity.getScene().attachTileChild(row, col, new Detonator(gameActivity));
        }else if(tileCode.equals(TileCodes.DetonatorCrate.getCharacter())){
            gameActivity.getScene().attachTileChild(row, col, new DetonatorCrate(gameActivity));
        }else if(tileCode.equals(TileCodes.Gate.getCharacter())){
            Gate gate = new Gate(gameActivity, row, col);
            gameActivity.getScene().attachTileChild(row, col, gate);
            gameActivity.getLevelManager().addGate(gate);
        }else if(tileCode.equals(TileCodes.Bridge.getCharacter())){
            Bridge bridge = new Bridge(gameActivity, row, col);
            gameActivity.getScene().attachTileChild(row, col, bridge);
            gameActivity.getLevelManager().addBridge(bridge);
        }
    }

    //endregion

    //region private

    private TextureRegion getTextureRegion(String imageFileName, int heightWidth){
        return getTextureRegion(imageFileName, heightWidth, heightWidth).deepCopy();
    }

    private TiledTextureRegion getTiledTextureRegionThemed(String file, int width, int height, int columns, int rows){
        if(!Util.isAssetExist(gameActivity, BitmapTextureAtlasTextureRegionFactory.getAssetBasePath() + file)){
            file = getFallbackPath(file);
        }
        BitmapTextureAtlas tex = new BitmapTextureAtlas(gameActivity.getTextureManager(), width, height, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion ret = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(tex, gameActivity, file, 0, 0, columns, rows);
        tex.load();
        return ret;
    }

    private String getFallbackPath(String imageFileName) {
        File f = new File(imageFileName);
        StringBuilder sb = new StringBuilder(f.getParent());
        if(!sb.toString().endsWith("/")){
            sb.append("/");
        }
        String output = new File(sb.toString().replaceAll("/[0-9]/", "/0/"), f.getName()).getAbsolutePath();
        return output.startsWith("/") ? output.substring(1) : output;
    }

    private TiledTextureRegion getTiledTextureRegion(String imageFileName, int width, int height, int columns, int rows){
        BitmapTextureAtlas tex = new BitmapTextureAtlas(gameActivity.getTextureManager(), width, height, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion ret = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(tex, gameActivity, imageFileName, 0, 0, columns, rows);
        tex.load();
        return ret;
    }

    private TextureRegion getTextureRegion(String file, int width, int height){
        if(!Util.isAssetExist(gameActivity, BitmapTextureAtlasTextureRegionFactory.getAssetBasePath() + file)){
            file = getFallbackPath(file);
        }
        BitmapTextureAtlas tile_TextureAtlas = new BitmapTextureAtlas(gameActivity.getTextureManager(), width, height, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tile_TextureAtlas, gameActivity, file, 0, 0);
        tile_TextureAtlas.load();
        return textureRegion;
    }

    private Font getFontTexture(String fontName, int fontSize, int textColor, int heightWidth){
        return getFontTexture(fontName, fontSize, textColor, heightWidth, heightWidth);
    }

    private Font getFontTexture(String fontName, int fontSize, int textColor, int width, int height){
        BitmapTextureAtlas FontTexture = new BitmapTextureAtlas(gameActivity.getTextureManager(), width, height, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        Font mFont = new Font(gameActivity.getFontManager(), FontTexture, Typeface.createFromAsset(gameActivity.getAssets(), fontName), fontSize, true, textColor);
        gameActivity.getTextureManager().loadTexture(FontTexture);
        mFont.load();
        return mFont;
    }

    private static int getFontMainColor(int themeId){
        switch (themeId){
            default:
            case 0:
                return Color.WHITE;
        }
    }

    //endregion

}
