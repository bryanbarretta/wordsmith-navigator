package bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Serialized Parcel Level object. Wraps static and player data into one object.
 * Created by Bryan on 20/08/2018.
 */

public class Level implements Serializable, Parcelable {

    private LevelStaticData levelStaticData;
    private LevelPlayerData levelPlayerData;

    public Level(LevelStaticData levelStaticData, LevelPlayerData levelPlayerData){
        this.levelStaticData = levelStaticData;
        this.levelPlayerData = levelPlayerData;
    }

    public LevelStaticData getLevelStaticData() {
        return levelStaticData;
    }

    /**
     * @return null if this level is locked
     */
    public LevelPlayerData getLevelPlayerData() {
        return levelPlayerData;
    }

    protected Level(Parcel in) {
        this.levelStaticData = (LevelStaticData) in.readValue(LevelStaticData.class.getClassLoader());
        this.levelPlayerData = (LevelPlayerData) in.readValue(LevelPlayerData.class.getClassLoader());
    }

    public static final Creator<Level> CREATOR = new Creator<Level>() {
        @Override
        public Level createFromParcel(Parcel in) {
            return new Level(in);
        }

        @Override
        public Level[] newArray(int size) {
            return new Level[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(levelStaticData);
        dest.writeValue(levelPlayerData);
    }

}
