package bbgames.ie.wordsmithnavigator.GameObjects.Camera;

import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;

import org.andengine.engine.camera.SmoothCamera;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.CameraSpeed;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.CameraZoomLevel;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.HudSide;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 07/05/2016.
 */
public class GameCamera extends SmoothCamera{

    private static final int MS_INTERVAL_CAMERA_MOVING_CHECK = 100;

    private GameActivity gameActivity;
    private float focusX;
    private float focusY;
    private Handler handlerMoveCheck;
    private boolean lockCameraUntilAnimationFinished = false;

    public GameCamera(GameActivity gameActivity)
    {
        super(gameActivity.getLevelManager().getLevelStaticData().getBounds().getTopLeft()[0],
                gameActivity.getLevelManager().getLevelStaticData().getBounds().getTopLeft()[1],
                Constant.CAMERA_1920,
                Constant.CAMERA_1080,
                CameraSpeed.getCameraSpeed(gameActivity.getApplicationManager()).getMs(),
                CameraSpeed.getCameraSpeed(gameActivity.getApplicationManager()).getMs(),
                0.f);
        this.gameActivity = gameActivity;
        focusX = gameActivity.getLevelManager().getLevelStaticData().getBounds().getTopLeft()[0];
        focusY = gameActivity.getLevelManager().getLevelStaticData().getBounds().getTopLeft()[1];
        setBoundsEnabled(true);
        setCameraBoundsForLevel();
        this.SetZoom(CameraZoomLevel.getCameraZoomLevel(gameActivity.getApplicationManager()).getValue());
    }

    private void setCameraBoundsForLevel(){
        float offsetZoom = CameraZoomLevel.getCameraZoomLevel(gameActivity.getApplicationManager()).getValue();
        float offsetTileRack = Constant.TILE_SIZE / offsetZoom;
        float bottomY = 0 - Constant.BORDER - offsetTileRack;
        float leftX =   0 - Constant.BORDER;
        float topY =    gameActivity.getLevelManager().getLevelStaticData().getBounds().getTopRight()[1]
                        - Constant.BORDER
                        + Constant.TILE_SIZE;
        float rightX =  gameActivity.getLevelManager().getLevelStaticData().getBounds().getTopRight()[0]
                        - Constant.BORDER
                        + Constant.TILE_SIZE;
        if(HudSide.getHudSide(gameActivity.getApplicationManager()).getSide() == Gravity.RIGHT){
            rightX += Constant.HUD_WIDTH / offsetZoom;
        }else {
            leftX -= Constant.HUD_WIDTH / offsetZoom;
        }
        SetBounds(leftX, bottomY, rightX, topY);
    }

    public void SetZoom(float zoomLevel){
        setZoomFactorDirect(zoomLevel);
        setCenterDirect(focusX, focusY);
    }

    public void SetSpeed(int actualSpeed){
        setMaxVelocity(actualSpeed, actualSpeed);
    }

    public void SetBounds(float bottomLeftX, float bottomLeftY, float topRightX, float topRightY){
        setBounds(bottomLeftX, bottomLeftY, topRightX, topRightY);
    }

    public void panToXY(float x, float y, boolean lockCameraUntilAnimationFinished, Runnable onMovementFinished){
        float startingX = this.getCenterX();
        float startingY = this.getCenterY();
        setCenter(x, y);
        this.lockCameraUntilAnimationFinished = lockCameraUntilAnimationFinished;
        if(handlerMoveCheck == null){
            handlerMoveCheck = new Handler(Looper.getMainLooper());
        }
        handlerMoveCheck.removeCallbacksAndMessages(null);
        checkIfCameraStillMoving(startingX, startingY, onMovementFinished);
    }

    public void panToXY(int[] xy, boolean lockCameraUntilAnimationFinished, Runnable onMovementFinished) {
        float[] pXY = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), xy[1], xy[0]);
        panToXY(pXY[0], pXY[1], lockCameraUntilAnimationFinished, onMovementFinished);
    }

    private void checkIfCameraStillMoving(final float previousX, final float previousY, final Runnable onMovementFinished){
        handlerMoveCheck.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(GameCamera.this.getCenterX() == previousX && GameCamera.this.getCenterY() == previousY){
                    lockCameraUntilAnimationFinished = false;
                    onMovementFinished.run();
                }else {
                    checkIfCameraStillMoving(GameCamera.this.getCenterX(), GameCamera.this.getCenterY(), onMovementFinished);
                }
            }
        }, MS_INTERVAL_CAMERA_MOVING_CHECK);
    }

    //region all methods that move the camera (either slowly or directly)

    /**
     * This will pan to x,y at this.cameraMovement speed
     * @param pCenterX
     * @param pCenterY
     */
    @Override
    public void setCenter(float pCenterX, float pCenterY) {
        if(!lockCameraUntilAnimationFinished) {
            super.setCenter(pCenterX, pCenterY);
        }
    }

    /**
     * This will instantly set the camera to x,y
     * @param pCenterX
     * @param pCenterY
     */
    @Override
    public void setCenterDirect(float pCenterX, float pCenterY) {
        if(!lockCameraUntilAnimationFinished) {
            super.setCenterDirect(pCenterX, pCenterY);
        }
    }

    public void setCenterDirect(int[] xy) {
        float[] pXY = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), xy[1], xy[0]);
        setCenterDirect(pXY[0], pXY[1]);
    }

    //endregion

}
