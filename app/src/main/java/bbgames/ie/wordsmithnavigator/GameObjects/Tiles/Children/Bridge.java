package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.BridgeLockTurns;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 26/08/2018.
 */
public class Bridge extends Sprite implements iTileChild {

    public static final int MS_EXPLOSION_FRAME_DURATION = 40;

    private final int row, col;
    private Boolean mIsVerticallyAligned;
    private GameActivity gameActivity;
    private BridgeLockTurns mTurnsLock;
    private int mCurrentTurnsRemaining;

    private Sprite bridge;
    private Text text;

    /**
     * @param gameActivity
     */
    public Bridge(GameActivity gameActivity, int row, int col) {
        super(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2, gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Bridge_hole), gameActivity.getVertexBufferObjectManager());
        this.gameActivity = gameActivity;
        this.row = row;
        this.col = col;
        this.mTurnsLock = gameActivity.getLevelManager().getTurnsLock(new int[]{col, row});
        this.mCurrentTurnsRemaining = mTurnsLock.getOriginalTurns();
        bridge = new Sprite(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Bridge),
                gameActivity.getVertexBufferObjectManager());
        attachChild(bridge);
        setTurns(mCurrentTurnsRemaining);
        if(!isVerticallyAligned()){
            setRotation(90f);
            text.setRotation(-90f);
        }
        this.setCullingEnabled(true);
    }

    @Override
    public boolean isBlocker() {
        return mCurrentTurnsRemaining <= 0;
    }

    @Override
    public Character getTileCode() {
        return TileCodes.Bridge.getCharacter();
    }

    @Override
    public Integer[] getColumnRow() {
        return new Integer[]{col, row};
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.Bridge;
    }

    /**
     * called when a turn has incremented
     * @param turns
     */
    public void setTurns(int turns) {
        if(gameActivity.getGraphicsManager().getLetterFactory().containsLetter(row, col)){
            removeBridge(true);
            return;
        }
        mCurrentTurnsRemaining = turns;
        if(text == null){
            text = new Text(getWidth() / 2, getHeight() / 2,
                    gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.litsans, Constant.COLOR_WHITE),
                    turns == 0 ? "" : Util.toNDecimalPlaces(turns, 2),
                    2,
                    gameActivity.getVertexBufferObjectManager());
            attachChild(text);
            return;
        }
        text.setText(turns == 0 ? "" : Util.toNDecimalPlaces(turns, 2));
    }

    public BridgeLockTurns getTurnsLock() {
        return mTurnsLock;
    }

    public int getCurrentTurnsRemaining() {
        return mCurrentTurnsRemaining;
    }

    /**
     * Explode the bridge, leaving this tile impassable.
     */
    public void explodeAnimation() {
        final AnimatedSprite fire = new AnimatedSprite(getX(), getY(),
                gameActivity.getGraphicsManager().GetTiledTextureRegion(GraphicsManager.TiledTextureKeys.Anim_Explosion),
                this.getVertexBufferObjectManager());
        gameActivity.getScene().attachChild(fire);
        fire.animate(MS_EXPLOSION_FRAME_DURATION, false, new AnimatedSprite.IAnimationListener() {
            @Override
            public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
                                           int pInitialLoopCount) {
            }
            @Override
            public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
                                                int pRemainingLoopCount, int pInitialLoopCount) {
            }
            @Override
            public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
                                                int pOldFrameIndex, int pNewFrameIndex) {
                if(pNewFrameIndex == 15){
                    removeBridge(false);
                }
            }
            @Override
            public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
                gameActivity.runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.getRubbish().RecycleSprite(fire);
                    }
                });
            }
        });
    }

    //region private

    /**
     * Finish the bridge animation
     * @param passable if true, the player has placed a letter in time. If false, this map tile is forever blocked
     */
    private void removeBridge(boolean passable){
        if(passable){
            gameActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gameActivity.getScene().detachTileChild(row, col, Bridge.this);
                    gameActivity.getLevelManager().getBridges().remove(Bridge.this);
                }
            });
        }else {
            //Block this path!
            mCurrentTurnsRemaining = 0;                           //isBlocker() will now return false, so letters cannot be placed on this tile
            gameActivity.getScene().unregisterTouchArea(this); //Touching this tile will do nothing
            bridge.setVisible(false);
            gameActivity.getLevelManager().getBridges().remove(Bridge.this);
        }
    }

    private boolean isVerticallyAligned(){
        if(this.mIsVerticallyAligned != null){
            return this.mIsVerticallyAligned;
        }
        Character cLeft = BoardUtils.getAdjacentCharacter(gameActivity.getLevelManager().getLayoutModified(),
                new int[]{col, row}, Direction.WEST);
        if(TileCodes.getPlayableTileCodes().contains(cLeft)){
            //The tile to the left is playable, so the left tile is our anchor: horizontal
            mIsVerticallyAligned = true;
        }else {
            mIsVerticallyAligned = false;
        }
        return mIsVerticallyAligned;
    }

    //endregion
}
