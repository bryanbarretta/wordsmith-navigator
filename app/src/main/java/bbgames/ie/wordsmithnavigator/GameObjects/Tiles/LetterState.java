package bbgames.ie.wordsmithnavigator.GameObjects.Tiles;

/**
 * Correspond to the different sprite textures a letter can have
 * Created by Bryan on 15/09/2018.
 */
public enum LetterState {

    ENABLED,
    DISABLED,
    PLAYING,
    RED,
    ROGUE,
    LOCKED,     //Same as enabled but no touch event
    HIDDEN;     //Invisible

    public boolean isEnabled(){
        return LetterState.this.equals(ENABLED);
    }

}
