package bbgames.ie.wordsmithnavigator.GameObjects.AI.Word;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import bbgames.ie.wordsmithnavigator.Constants.TileCodes;

/**
 * Represents each tile along the line
 */
public class HintBoardTile {

    private int id;
    private int[] xy;
    private Character tileCode;
    private Character lockedLetter;
    private boolean canPlayAnyLetterHere;
    private boolean isTouchingAdjacentLetters;
    private HashMap<Character, String> mapLetterWord;

    public HintBoardTile(@NonNull int id, @NonNull Character tileCode, @NonNull int[] xy) {
        this.id = id;
        this.tileCode = tileCode;
        this.xy = xy;
        this.lockedLetter = null;
        this.canPlayAnyLetterHere = false;
        this.mapLetterWord = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public Character getTileCode(){
        return tileCode;
    }

    public int[] getXy() {
        return xy;
    }

    public void setLockedLetter(Character c){
        this.lockedLetter = c;
    }

    public void setCanPlayAnyLetterHere(boolean b){
        this.canPlayAnyLetterHere = b;
    }

    public void addPotentialLetterAndWord(Character potentialLetter, String potentialWord){
        mapLetterWord.put(potentialLetter, potentialWord);
    }

    public Character getLockedLetter(){
        return lockedLetter;
    }

    public boolean isCanPlayAnyLetterHere(){
        return canPlayAnyLetterHere;
    }

    public HashMap<Character, String> getMapLetterWord() {
        return mapLetterWord;
    }

    /**
     * This tile has a map of potential letters to potential words.
     * @param potentialWord Given the potential word
     * @return the corresponding potential letter
     */
    public Character getPotentialLetter(String potentialWord) {
        for(Map.Entry<Character, String> entry : mapLetterWord.entrySet()){
            if(entry.getValue().equals(potentialWord)){
                return entry.getKey();
            }
        }
        return null;
    }

    public void setIsTouchingAdjacentLetters(boolean isTouchingAdjacentLetters) {
        this.isTouchingAdjacentLetters = isTouchingAdjacentLetters;
    }

    public boolean isTouchingAdjacentLetters(){
        return isTouchingAdjacentLetters;
    }

    public boolean isPlayable() {
        return TileCodes.getPlayableTileCodes().contains(getTileCode());
    }
}
