package bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules;

/**
 * Logic class for underlying Gate-PointLock mechanism
 */
public class GateLockPoints {

    private final int[] xyGate;
    private final int mOriginalPoints;

    /**
     * @param key "[gateX, gateY]->[points]"      e.g. "[3,4]->[40]"
     */
    public GateLockPoints(String key){
        String[] parts = key.trim().split("->");
        String detonator = parts[0].trim();
        xyGate = new int[]{
                Integer.valueOf(detonator.substring(1, detonator.indexOf(",")).trim()),
                Integer.valueOf(detonator.substring(detonator.indexOf(",") + 1, detonator.length()-1))
        };
        mOriginalPoints = Integer.parseInt(parts[1].trim().substring(1, parts[1].trim().indexOf("]")));
    }

    public int[] getXYGate() {
        return xyGate;
    }

    public int getOriginalPoints() {
        return mOriginalPoints;
    }
}
