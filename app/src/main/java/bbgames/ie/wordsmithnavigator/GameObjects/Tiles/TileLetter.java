package bbgames.ie.wordsmithnavigator.GameObjects.Tiles;

import android.os.Handler;
import android.util.Log;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.sprite.batch.SpriteGroup;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.util.GLState;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.GameObjects.GameScene;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Represents a letter tile sprite (TiledTextureSprite)
 * Rogue Tiles should be instantiated with [Owner: null, LetterState: ROGUE, skinID: any]
 * Created by Bryan on 15/09/2018.
 */
public class TileLetter extends TiledSprite implements iTileChild {

    private final String LOGTAG = getClass().getSimpleName();

    protected GameActivity gameActivity;
    protected SkinLetter skin;
    protected LetterState state;
    protected Character letter;
    protected Text textLetter, textPoints;
    protected int points;
    protected Player owner;

    private Handler mHandlerFlashingRed;
    private Runnable mRunnableFlashingRed;

    public TileLetter(float pX,
                      float pY,
                      GameActivity gameActivity,
                      TiledTextureRegion tiledTextureRegion,
                      SkinLetter skin,
                      Character letter,
                      Player owner){
        super(pX, pY, tiledTextureRegion, gameActivity.getVertexBufferObjectManager());
        this.gameActivity = gameActivity;
        this.skin = skin;
        setLetter(letter);
        setOwner(owner);
    }

    public boolean isHudAttached(){
        if(getParent() != null && getParent() instanceof SpriteGroup){
            return getParent().getParent() != null && getParent().getParent() instanceof GameHud;
        }
        return false;
    }

    public boolean isBoardAttached(){
        if(getParent() != null && getParent() instanceof SpriteGroup){
            return getParent().getParent() != null && getParent().getParent() instanceof GameScene;
        }
        return false;
    }

    //region public functions

    public void flashRed(Runnable onFinishedRunnabled){
        if(getState().equals(LetterState.RED)){
            return;
        }
        if(mHandlerFlashingRed != null){
            if(mRunnableFlashingRed != null){
                mHandlerFlashingRed.removeCallbacks(mRunnableFlashingRed);
                mRunnableFlashingRed = null;
            }
            mHandlerFlashingRed = null;
        }
        final int originalStateId = getState().ordinal();
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gameActivity.getHUD().setState(GameHud.STATE.LOCKED);
                setState(LetterState.RED);
                mHandlerFlashingRed = new Handler();
                mRunnableFlashingRed = new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gameActivity.getHUD().setState(GameHud.STATE.ACTIVE_BUTTONS);
                                setState(LetterState.values()[originalStateId]); //This will reset the color
                                if(onFinishedRunnabled != null){
                                    onFinishedRunnabled.run();
                                }
                            }
                        });
                    }
                };
                mHandlerFlashingRed.postDelayed(mRunnableFlashingRed, 1000);
            }
        });
    }

    public void updateState(Player player){
        if(gameActivity.getPlayerManager().getActivePlayer().getId() == player.getId()){
            if(gameActivity.getPlayerManager().getActivePlayer().isHumanUser()){
                setState(LetterState.ENABLED);
            }else {
                setState(LetterState.DISABLED);
            }
        }else {
            setState(LetterState.HIDDEN);
        }
    }

    //endregion

    //region getters and setters

    public void setLetter(Character letter){
        this.letter = letter;
        this.points = Util.getPointsForLetter(letter);
        drawText();
    }

    public void setState(LetterState state) {
        if(this.state != null && this.state == state){
            return;
        }
        this.state = state;
        this.setVisible(!state.equals(LetterState.HIDDEN));
        int index = state.ordinal() >= 5 ? LetterState.ENABLED.ordinal() : state.ordinal();
        this.setCurrentTileIndex(index);
        this.textPoints.setColor(skin.getColor(index));
        this.textLetter.setColor(skin.getColor(index));
        log("Set State");
    }

    public void setOwner(Player owner){
        this.owner = owner;
    }

    public LetterState getState() {
        return state;
    }

    public Character getLetter() {
        return letter;
    }

    public int getPoints() {
        return points;
    }

    public Player getOwner() {
        return owner;
    }

    public SkinLetter getSkin() {
        return skin;
    }

    //endregion

    //region private

    private void drawText(){
        if(textLetter == null) {
            textLetter = new Text(Constant.TILE_SIZE / 2,
                    Constant.TILE_SIZE / 2,
                    gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.roboto),
                    letter.toString().toLowerCase(),
                    gameActivity.getVertexBufferObjectManager());
            attachChild(textLetter);
        }else {
            textLetter.setText(letter.toString().toLowerCase());
        }
        if(textPoints == null) {
            textPoints = new Text(Constant.TILE_SIZE - Constant.TILE_POINTS_OFFSET,
                    Constant.TILE_SIZE - Constant.TILE_POINTS_OFFSET,
                    gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.robotoTiny),
                    points + "", 2,
                    gameActivity.getVertexBufferObjectManager());
            attachChild(textPoints);
        }else {
            textPoints.setText(points + "");
        }
    }

    public void log(String info){
        Log.i(LOGTAG, "[" + letter + "]@[" + getX() + ", " + getY() + "]:\t" + state.name() +
                "\t| Owner: " + (owner == null ? "NULL" : owner.getName()) +
                "\t| Attached to: " + (isHudAttached() ? "HUD" : (isBoardAttached() ? "Board" : "NULL")) +
                "\t| \"" + info + "\"");
    }

    //endregion

    @Override
    public boolean isBlocker() {
        return false;
    }

    @Override
    public Character getTileCode() {
        return letter;
    }

    @Override
    public Integer[] getColumnRow() {
        if(isBoardAttached()) {
            int[] ret = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{getX(), getY()});
            return new Integer[]{ret[0], ret[1]};
        }else {
            return null;
        }
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.Letter;
    }

    @Override
    protected void preDraw(final GLState pGLState, final Camera pCamera)
    {
        super.preDraw(pGLState, pCamera);
        pGLState.enableDither();
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if(gameActivity.getLevelManager().getFlagValue(LevelManager.StateFlag.ARROW_DIRECTION_BEING_SET, false)){
            return false; //Your finger is setting the arrow direction, so you can't touch these hud letter tiles during this process
        }
        if(!gameActivity.getPlayerManager().getActivePlayer().isHumanUser()){
            return false; //This is not your tile!
        }
        if(state.equals(LetterState.LOCKED)){
            return true; //This tile is locked on the board, do nothing
        }
        if(state.equals(LetterState.HIDDEN)){
            return false; //This tile is hidden, so you aren't touching it
        }
        if(state.equals(LetterState.DISABLED)){
            return true; //This tile is visible but locked, so you can touch it and it does nothing
        }
        if(isHudAttached() && state.equals(LetterState.ENABLED)){
            if(pSceneTouchEvent.isActionDown()){
                if(gameActivity.getLevelManager().getFocusTile() == null){
                    gameActivity.showToast(gameActivity.getString(R.string.tapMapTile));
                    return true;
                }else {
                    gameActivity.getHUD().moveLetterFromHudToBoard(this);
                }
            }else {
                return true;
            }
        }
        return false;
    }

}
