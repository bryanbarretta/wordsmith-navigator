package bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Activities.LevelCompleteActivity;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.IntentKeys;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.LetterTileOnBoard;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.BridgeLockTurns;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.GateLockPoints;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Animation.AnimationManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Bridge;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Diamond;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Gate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Highlight;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.PowerUp;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.FocusTile;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Dynamic data about the level that changes as the user plays the level.
 * It is initiated every time a player starts a level and disposed of when they exit it.
 *  - Data (ModifiedLayout)
 *  - UI objects
 * Created by Bryan on 19/08/2018.
 */

public class LevelManager {

    private final String LOGTAG = getClass().getSimpleName();

    public enum StateFlag {
        ARROW_DIRECTION_BEING_SET,
        USER_IS_CREATING_WORD
    }

    //Ui
    private GameActivity gameActivity;
    private HashMap<StateFlag, Boolean>        flags = new HashMap<>();
    private ArrayList<Gate>                    gates = new ArrayList<>();
    private ArrayList<Bridge>                  bridges = new ArrayList<>();
    private ArrayList<TileLetter>         rogueTiles = new ArrayList<>();
    private HintWord currentWord;
    private FocusTile focusTile;

    //Data
    private final int levelId;
    private LevelStaticData levelStaticData;
    private LevelPlayerData levelPlayerData;
    private ArrayList<String> layoutModified;
    private HashMap<Integer, SessionStatistics> sessionStatistics;
    private Integer rowCount;

    public LevelManager(GameActivity gameActivity) {
        this.gameActivity = gameActivity;
        this.levelId = gameActivity.getLevelId();
        this.levelStaticData = gameActivity.getApplicationManager().getLevelFactory().getLevelStaticData(levelId);
        this.levelPlayerData = gameActivity.getApplicationManager().getLevelFactory().getLevelPlayerData(levelId);
        this.layoutModified = new ArrayList<>(levelStaticData.getLayoutOriginal());
        this.sessionStatistics = new HashMap<>();
        for(int id : levelPlayerData.getMapIdDiamondUnlocked().keySet()){
            Boolean diamondUnlocked = levelPlayerData.getMapIdDiamondUnlocked().get(id);
            if(diamondUnlocked != null && diamondUnlocked == true){
                getSessionStatistics(0).getDiamondIds().add(id);
            }
        }
        this.levelPlayerData.incrementPlays();
        this.levelPlayerData.save(gameActivity, levelId);
    }

    //region UI

    //-------------------------
    // FUNCTIONALITY
    //-------------------------

    /**
     * Move the letter tile occupying the map tile specified from the board back onto the active players hud
     */
    public void moveLetterFromBoardToHud(int row, int col){

        float[] pXY = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), row, col);

        //Update board UI
        gameActivity.getGraphicsManager().getLetterFactory().detachLetter(row, col);
        //When moving a letter from the board back to the hud, the focus tile should display an arrow only if it is the last active letter being returned
        boolean attachArrow = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().size() == 1;
        setFocusTile(new FocusTile(new int[]{col, row}, getFocusTile().getDirection()), attachArrow);
        gameActivity.getCamera().setCenterDirect(pXY[0], pXY[1]);

        //Update hud UI
        if(gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard() == null ||
           gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().isEmpty()){
            Util.throwException(gameActivity, "hudLetterTilesOnBoard is null or empty");
        }
        int iHudLetter = -1;
        for(int i = 0; i < gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().size(); i++){
            LetterTileOnBoard hltob = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().get(i);
            if(hltob.getBoardColRow()[0] == col && hltob.getBoardColRow()[1] == row){
                iHudLetter = i;
                break;
            }
        }
        if(iHudLetter == -1){
            Util.throwException(gameActivity, "Unable to find iHudLetter index!");
        }
        gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().
                get(iHudLetter).
                getHudLetterTile().
                setState(LetterState.ENABLED);
        gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().remove(iHudLetter);

        //Update word
        while (true){
            if(getCurrentWord().getLetters().isEmpty()){
                break;
            }
            HintLetter letterToRemove;
            if(BoardUtils.isReverseDirection(getFocusTile().getDirection())){
                letterToRemove = getCurrentWord().getFirstLetter();
            }else {
                letterToRemove = getCurrentWord().getLastLetter();
            }
            getCurrentWord().removeLetter(letterToRemove);
            if(letterToRemove.isLockedOnBoard()){
                //This letter was locked on the board (and thus will remain on the board),
                //so keep going as we have not actually undone the last letter played
            }else {
                break;
            }
        }
        setCurrentWord(getCurrentWord());

        if(!gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().isEmpty()){
            boolean wordExists = gameActivity.getApplicationManager().getDatabaseGateway().isWordExists(
                    getCurrentWord().toString());
            gameActivity.getHUD().setButtonTickEnabled(wordExists);
            setFocusTile(focusTile, false);
        }
    }

    public boolean submitCurrentWord(){
        return submitCurrentWord(false);
    }

    /**
     * @param undoLastLetterPlayedIfWordIsInvalid: if true, and the word is invalid, the last letter played will be returned to the HUD
     *                                             similar to if btnUndo was pressed.
     * @return if the current word was successful
     */
    public boolean submitCurrentWord(boolean undoLastLetterPlayedIfWordIsInvalid){
        if(currentWord == null){
            return false;
        }
        if(!gameActivity.getApplicationManager().getDatabaseGateway().isWordExists(currentWord.toString())){
            gameActivity.getHUD().setState(GameHud.STATE.ACTIVE_BUTTONS, true, getPlayedHudTileLetters());
            gameActivity.showToast(String.format(gameActivity.getString(R.string.word_does_not_exist), currentWord));
            return false;
        }
        if(!BoardUtils.verifyPlacedLetterTiles(gameActivity, undoLastLetterPlayedIfWordIsInvalid)){
            return false;
        }
        gameActivity.getScene().SetEnabled(false);
        gameActivity.getHUD().setState(GameHud.STATE.LOCKED, true, getPlayedHudTileLetters());

        int points = Util.getPointsForWord(currentWord.toString());
        getSessionStatistics().addWord(currentWord.toString(), points);
        if(!gameActivity.getPlayerManager().getActivePlayer().getPlayerType().equals(PlayerType.CPU)) {
            gameActivity.getPlayerManager().getActivePlayer().addPlayedWord(currentWord);
        }
        setFlagValue(LevelManager.StateFlag.USER_IS_CREATING_WORD, false);
        gameActivity.getHUD().HideSpeechBubble();
        clearFocusTile();
        updateMapUiAndFlags(points);
        setCurrentWord(null);
        return true;
    }

    /**
     * Call this to completely clear the current word and update the UI accordingly
     */
    public void clearCurrentWord(){
        ArrayList<LetterTileOnBoard> letterTilesOnBoard = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard();

        while (!gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().isEmpty()){
            int i = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().size() - 1;
            LetterTileOnBoard l = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard().get(i);
            moveLetterFromBoardToHud(l.getBoardColRow()[1], l.getBoardColRow()[0]);
        }

        //reattach letter tiles to hud
        for(LetterTileOnBoard letterTileOnBoard : letterTilesOnBoard){
            letterTileOnBoard.getHudLetterTile().setState(LetterState.ENABLED);
        }
        letterTilesOnBoard.clear();

        setCurrentWord(null);
        setFlagValue(LevelManager.StateFlag.USER_IS_CREATING_WORD, false);
        gameActivity.getHUD().HideSpeechBubble();
        gameActivity.getScene().detachHighlights();
        int[] xyFocus = gameActivity.getLevelManager().getFocusTile().getXy();
        Direction direction = gameActivity.getLevelManager().getFocusTile().getDirection();
        gameActivity.getLevelManager().clearFocusTile();
        gameActivity.getScene().touchMapTile(xyFocus[1], xyFocus[0], direction);
    }

    /**
     * @return the HUD TileLetter objects of the currently played letters on the HUD rack (they will have state HIDDEN)
     */
    public TileLetter[] getPlayedHudTileLetters(){
        ArrayList<LetterTileOnBoard> hltob = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard();
        TileLetter[] playedHudTiles = new TileLetter[hltob.size()];
        int i = 0;
        for(LetterTileOnBoard ltob : hltob){
            playedHudTiles[i] = ltob.getHudLetterTile();
            i++;
        }
        return playedHudTiles;
    }

    //-------------------------
    // ADD/UPDATE
    //-------------------------

    /**
     * @param currentWord The current word as-is being played by the active player. Pass null to reset (i.e. clear current word)
     */
    public void setCurrentWord(HintWord currentWord) {
        this.currentWord = currentWord;
        if(currentWord == null){
            gameActivity.getHUD().HideSpeechBubble();
        }else {
            gameActivity.getHUD().ShowSpeechBubble(currentWord.toString());
        }
    }

    //-------------------------
    // GETTERS
    //-------------------------

    public TileLetter getLetterTile(int row, int column){
        return gameActivity.getGraphicsManager().getLetterFactory().getLetter(row, column);
    }

    public ArrayList<Gate> getGates() {
        return gates;
    }

    public ArrayList<Bridge> getBridges() {
        return bridges;
    }

    /**
     * Get focus tile  data (map tile and direction)
     * @return Null if the active player has not yet placed a focus tile by touching a map tile
     */
    public FocusTile getFocusTile(){
        return focusTile;
    }

    /**
     * The current word being played by the active player. Note it may not yet be a valid word
     * @return Null if they have not yet placed a letter. Otherwise the current word (i.e. what you see in the speech bubble)
     */
    public HintWord getCurrentWord(){
        if(currentWord == null || currentWord.getLetters().isEmpty()) {
            setCurrentWord(BoardUtils.getPreviousLettersForFocusedTile(gameActivity));
        }
        return currentWord;
    }

    //endregion

    //region Data

    //-------------------------
    // ADD/UPDATE
    //-------------------------


    public void updateLayoutModified(int[] xy, TileCodes updatedCode, boolean uniqueCode){
        LevelUtils.updateLayout(levelStaticData.getLayoutOriginal(), layoutModified, xy, updatedCode, uniqueCode);
    }

    public void updateLayoutModified(int[] xy, Character updatedCode){
        LevelUtils.updateLayout(levelStaticData.getLayoutOriginal(), layoutModified, xy, updatedCode);
    }

    /**
     * Set a flag value
     * @param flag
     * @param value
     */
    public void setFlagValue(LevelManager.StateFlag flag, boolean value) {
        flags.put(flag, value);
    }

    /**
     * @param newFocusTile is set when the active player touches an active map tile. Pass null to reset
     * @return false if there was no update (i.e. its the same tile), true if there is a new focus tile
     */
    public boolean setFocusTile(FocusTile newFocusTile, boolean attachArrow) {

        //#0) Pre check:
        if(this.focusTile != null && this.focusTile.equals(newFocusTile)){
            return false;   //The current focus tile is the same as the new focus tile. No work to do.
        }

        //#1) UI: Clean up old focus tile + highlights:
        if(this.focusTile != null) {
            gameActivity.getScene().detachHighlights();
            //The layout modified
            if(gameActivity.getPlayerManager().getActivePlayer().isHumanUser()){
                LevelUtils.revertLayoutModified(levelStaticData.getLayoutOriginal(), getLayoutModified(), focusTile.getXy(), false);
            }
        }

        //#2) Data: Update focus tile variable
        this.focusTile = newFocusTile;

        //#3) UI: Add the new focus tile + highlights
        if(this.focusTile != null) {
            if (gameActivity.getPlayerManager().getActivePlayer().isHumanUser()) {
                //The layout modified
                updateLayoutModified(newFocusTile.getXy(), TileCodes.StartingZone, true);
            }
            //The focus tile
            Highlight highlight = new Highlight(gameActivity);
            if (attachArrow) {
                highlight.showArrow(focusTile.getDirection());
            }
            gameActivity.getScene().attachTileChild(newFocusTile.getXy()[1], newFocusTile.getXy()[0], highlight);
            //The highlighted tiles
            ArrayList<int[]> xyPlayableTiles = focusTile.getSubFocusTileXYs(gameActivity.getLevelManager().getLayoutModified());
            for (int[] xy : xyPlayableTiles){
                gameActivity.getScene().attachTileChild(xy[1], xy[0], new Highlight(gameActivity));
            }
            if (!attachArrow) {
                float[] newPXY = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), focusTile.getXy()[1], focusTile.getXy()[0]);
                gameActivity.getCamera().setCenterDirect(newPXY[0], newPXY[1]);
            }
            if (!gameActivity.getPlayerManager().getActivePlayer().isHumanUser()) {
                setFlagValue(LevelManager.StateFlag.ARROW_DIRECTION_BEING_SET, false);
            }
        }

        return true;
    }

    /**
     * Sets focus tile to null
     */
    public void clearFocusTile(){
        setFocusTile((FocusTile) null, false);
    }

    //-------------------------
    // GETTERS
    //-------------------------

    /**
     * @param flag see Flags enum
     * @return Null if the flag has not yet been set, otherwise the flag value
     */
    public Boolean getFlagValue(LevelManager.StateFlag flag){
        return flags.containsKey(flag) ? flags.get(flag) : null;
    }

    public Boolean getFlagValue(LevelManager.StateFlag flag, boolean defaultValue){
        return flags.containsKey(flag) ? flags.get(flag) : defaultValue;
    }

    public ArrayList<String> getLayoutModified() {
        return layoutModified;
    }

    public LevelStaticData getLevelStaticData(){
        return levelStaticData;
    }

    public LevelPlayerData getLevelPlayerData(){
        return levelPlayerData;
    }

    public GateLockPoints getPointsLock(int[] xy) {
        for(GateLockPoints pl : levelStaticData.getPointsLocks()){
            if(Util.equal(pl.getXYGate(), xy)){
                return pl;
            }
        }
        return null;
    }

    public BridgeLockTurns getTurnsLock(int[] xy){
        for(BridgeLockTurns l : levelStaticData.getTurnsLocks()){
            if(Util.equal(l.getXYBridge(), xy)){
                return l;
            }
        }
        return null;
    }

    public SessionStatistics getSessionStatistics(){
        return getSessionStatistics(gameActivity.getPlayerManager().getActivePlayer().getId());
    }

    public SessionStatistics getSessionStatistics(int playerId){
        if(!sessionStatistics.containsKey(playerId)){
            sessionStatistics.put(playerId, new SessionStatistics(playerId));
        }
        return sessionStatistics.get(playerId);
    }

    public int getTileCodeCount(TileCodes tileCode){
        Character c = tileCode.getCharacter();
        int total = 0;
        for(String row : layoutModified){
            total += (row.length() - row.replace(c.toString(), "").length());
        }
        return total;
    }

    public int getPlayableTileCodeCount(){
        TileCodes.getPlayableTileCodes();
        int total = 0;
        for(Character c : TileCodes.getPlayableTileCodes()){
            total += getTileCodeCount(TileCodes.getTileCode(c));
        }
        return total;
    }

    /**
     * Who owns the letter tile at row,col?
     * Only use levelStaticData, as the level has not been rendered yet
     */
    public Player getTileOwnerUsingLevelStaticData(int row, int col) {
        if(gameActivity.getPlayerManager().getPlayers().size() == 1){
            return gameActivity.getPlayerManager().getPlayers().get(0);
        }
        int[] xy = new int[]{col, row};
        ArrayList<String> layout = gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal();
        for(Player player : gameActivity.getPlayerManager().getPlayers()){
            int[] playerXy = LevelUtils.getFirstXYof(layout, player.getPlayerType().getTileCode());
            ArrayList<int[]> xyPlayersBoardLetters = LevelUtils.getLetterTilesTouchingLetter(layout, playerXy);
            for (int[] bxy : xyPlayersBoardLetters){
                if(Util.equal(xy, bxy)){
                    return player;
                }
            }
        }
        return null;
    }

    public int getRowCount(){
        if(rowCount == null){
            rowCount = layoutModified.size();
        }
        return rowCount;
    }

    public void addBridge(Bridge bridge) {
        bridges.add(bridge);
    }

    public void addGate(Gate gate) {
        gates.add(gate);
    }

    public void addRogueTile(TileLetter tileLetter){
        rogueTiles.add(tileLetter);
    }

    //endregion

    //region private methods

    /**
     * Helper method used when a word is successfully submitted. Updates the map and player status, and triggers end turn transition animation
     * Updates Map:
     *  - Update the status of each map tile
     *  - Update the layoutModified string array in level properties
     *  - Update players data (list of their owned letter tiles on board and their diamond tally)
     * Updates Flags:
     *  - Finish Level | Detonator Tiles | Rogue Tiles
     * @param points Need to pass this for the point incrementing animation. That animation handles the underlying points update
     */
    private void updateMapUiAndFlags(int points){

        boolean flagFinishLevel = false;
        ArrayList<int[]> flagDetonatorXYs = new ArrayList<>();
        HashMap<TileLetter, PowerUpLink> flagPowerUps = new HashMap<>();

        //Add each letter placed this turn to layout modified...
        for(LetterTileOnBoard hltob : gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard()){
            int row = hltob.getBoardColRow()[1];
            int col = hltob.getBoardColRow()[0];

            //Analyze underlying tile
            int[] xy = new int[]{col, row};
            Character underlyingChar = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), xy);
            if(underlyingChar.equals(TileCodes.EndZone.getCharacter())){
                flagFinishLevel = true;
            }
            else if(underlyingChar.equals(TileCodes.Detonator.getCharacter())){
                flagDetonatorXYs.add(xy);
                gameActivity.getScene().detachTileChild(row, col, TileChildType.Detonator);
            }
            else if(underlyingChar.equals(TileCodes.Diamond.getCharacter())){
                iTileChild child = gameActivity.getScene().getTileChild(row, col, TileChildType.Gem);
                if(child != null){
                    Diamond d = (Diamond) child;
                    getSessionStatistics().addDiamondId(d.GetDiamondId());
                    gameActivity.getScene().detachTileChild(row, col, d);
                }
            }
            else {
                ArrayList<iTileChild> children = gameActivity.getScene().getTileChildren(xy);
                for(iTileChild child : children){
                    if(child.getTileChildType().equals(TileChildType.PowerUps)){
                        TileLetter tileLetter = gameActivity.getGraphicsManager().getLetterFactory().getLetter(row, col);
                        PowerUp powerUp = (PowerUp) child;
                        flagPowerUps.put(tileLetter, powerUp.getPowerUpLink());
                        gameActivity.getScene().detachTileChild(row, col, powerUp);
                    }
                }
            }

            //Update layout modified with placed letter
            TileLetter l = gameActivity.getGraphicsManager().getLetterFactory().getLetter(row, col);
            l.setState(LetterState.LOCKED);
            gameActivity.getPlayerManager().getActivePlayer().updateListXYLetterTilesOnBoard(new int[]{col, row});
            gameActivity.getLevelManager().updateLayoutModified(new int[]{col, row}, l.getLetter());
        }

        endTurnAnimations(currentWord.toString(), points, flagFinishLevel, flagDetonatorXYs, getRogueTilesToTransform(), flagPowerUps);
    }

    /**
     * We want to transform a rogue tile at the end of this turn if any of the played letters touch the rogue tile
     * @return the list of rogue tiles that will be transformed due to a played letter bordering at least one of them
     */
    private ArrayList<int[]> getRogueTilesToTransform(){
        ArrayList<int[]> output = new ArrayList<>();
        ArrayList<LetterTileOnBoard> playedLetters = gameActivity.getPlayerManager().getActivePlayer().getHudLetterTilesOnBoard();
        for(TileLetter r : rogueTiles){
            int[] xyRogueLetter = LevelUtils.getColumnRow(levelStaticData.getRows(), new float[]{r.getX(), r.getY()});
            for(LetterTileOnBoard l : playedLetters){
                if(LevelUtils.isAdjacent(xyRogueLetter, l.getBoardColRow())){
                    //A played letter is bordering a rogue letter; Transform all the rogue letters!
                    ArrayList<int[]> rtt = LevelUtils.getXYsOfTouchingRogueTilesRecursively(getLayoutModified(), xyRogueLetter);
                    for(int[] rxy : rtt){
                        if(!Util.contains(output, rxy)){
                            output.add(rxy);
                        }
                    }
                }
            }
        }
        //Remove rogue tiles from member array that are about to be transformed
        ArrayList<TileLetter> rogueTilesToRemove = new ArrayList<>();
        for(int[] xy : output){
            for(TileLetter r : rogueTiles){
                int[] xyRogueLetter =  LevelUtils.getColumnRow(levelStaticData.getRows(), new float[]{r.getParent().getX(), r.getParent().getY()});
                if(!rogueTilesToRemove.contains(r) && Util.equal(xy, xyRogueLetter)){
                    rogueTilesToRemove.add(r);
                }
            }
        }
        rogueTiles.removeAll(rogueTilesToRemove);
        return output;
    }

    /**
     * move over 1 or more letter tiles in the arrow direction
     */
    public void hopLetterTilesFromFocusedTile(){
        if(focusTile == null){
            Util.throwException(gameActivity, "hopLetterTilesFromFocusedTile has no focus!");
        }
        HintWord potentialWord = getCurrentWord();
        int[] nextXY = LevelUtils.incrementXY(focusTile.getXy(), focusTile.getDirection());
        Character nextTile = LevelUtils.getCharacterAt(gameActivity, nextXY);
        //Keep hopping while we are moving onto map tiles
        while (nextTile != null){
            if(TileCodes.getBlockTileCodes().contains(nextTile)){
                break;
            }
            if(Character.isLetter(nextTile)){
                HintLetter hl = new HintLetter(focusTile.getXy(), nextTile, true, false);
                //Add the letter to iteratedLetters and move on to the next tile in the focused direction
                if(focusTile.getDirection() == Direction.SOUTH || focusTile.getDirection() == Direction.EAST){
                    potentialWord.addLetter(hl);
                }else {
                    potentialWord.addLetter(hl, true);
                }
                nextXY = LevelUtils.incrementXY(nextXY, focusTile.getDirection());
                nextTile = LevelUtils.getCharacterAt(gameActivity, nextXY);
            }else {
                //Empty mapTile: Update current word & Make this the focus tile. Exit method here
                setCurrentWord(potentialWord);
                boolean isHumanUser = gameActivity.getPlayerManager().getActivePlayer().isHumanUser();
                boolean isWordExist = gameActivity.getApplicationManager().getDatabaseGateway().isWordExists(getCurrentWord().toString());
                gameActivity.getHUD().setState(isHumanUser ? GameHud.STATE.ACTIVE_BUTTONS : GameHud.STATE.LOCKED);
                gameActivity.getHUD().setButtonTickEnabled(isWordExist);
                setFocusTile(new FocusTile(nextXY, focusTile.getDirection()), false);
                return;
            }
        }
        setCurrentWord(potentialWord);
        submitCurrentWord();
    }

    //region Turn Transition logic and animations

    /**
     * First part of turn transition: Ending the current players turn.
     * These animations include:
     *  - Detonating any crates if the player touched a detonator
     *  - Transforming any rogue letter tiles if a player touched them
     *  - animate the submitted word on screen, and show total word/point count being incremented + any gems/trophies
     * @param word
     * @param points earned this turn
     * @param flagFinishLevel
     * @param flagDetonatorMapTiles
     * @param flagRogueTiles
     * @param flagPowerUps
     */
    private void endTurnAnimations(String word, final int points, final boolean flagFinishLevel, ArrayList<int[]> flagDetonatorMapTiles, ArrayList<int[]> flagRogueTiles, HashMap<TileLetter, PowerUpLink> flagPowerUps){
        gameActivity.getHUD().GetClock().stopTimer();
        AnimationManager.Builder animationBuilder = new AnimationManager.Builder(gameActivity);
        if(flagDetonatorMapTiles != null && !flagDetonatorMapTiles.isEmpty())
            animationBuilder.queueExplodeDetonateCrates(flagDetonatorMapTiles);
        if(flagRogueTiles != null && !flagRogueTiles.isEmpty())
            animationBuilder.queueTransformRogueTilesToLetterTiles(flagRogueTiles);

        if(gates.size() > 0){
            animationBuilder.queueGatePointsUpdate(points);
        }
        if(bridges.size() > 0){
            animationBuilder.queueBridgeTurnsUpdate();
        }

        animationBuilder.queueSuccessfulWordAnimation(word, points, flagPowerUps);
        if(!flagFinishLevel) {
            animationBuilder.queueReplenishUsedHudTilesAnimation(flagPowerUps);
        }
        animationBuilder.execute(true, new Runnable() {
            @Override
            public void run() {
                if(flagFinishLevel){
                    gameActivity.setFlag(GameActivity.FLAGS.LEVEL_COMPLETE, true);
                    getSessionStatistics().setTotalSeconds(gameActivity.getHUD().GetTotalSeconds());
                    Intent i = new Intent(gameActivity, LevelCompleteActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(IntentKeys.SESSION_STATISTICS.name(), getSessionStatistics(gameActivity.getPlayerManager().getPlayer(PlayerType.USER).getId()));
                    i.putExtras(bundle);
                    i.putExtra(IntentKeys.GAME_MODE_ORDINAL.name(), gameActivity.getGameMode().ordinal());
                    i.putExtra(IntentKeys.LEVEL_ID.name(), levelId);
                    gameActivity.startActivity(i);
                    gameActivity.finish();
                }else {
                    gameActivity.getHUD().getHudFloatingElements().unstickyItems();
                    transitionTurnAnimations();
                }
            }
        });
    }

    /**
     * Second part of turn transition: Starting the next players turn.
     * These animations include:
     *  - Panning the camera to the next players last played word
     *  - Shifting the current players tile rack off screen and the next players tile rack on screen
     */
    private void transitionTurnAnimations(){
        if(gameActivity.getPlayerManager().getPlayers().size() <= 1){
            Integer[] nextXY = gameActivity.getPlayerManager().getActivePlayer().getNextTileToPanTo();
            if(nextXY == null){
                gameActivity.getCamera().setCenterDirect(gameActivity.getPlayerManager().getActivePlayer().getXYLastLetterTilePlayed());
            }else {
                gameActivity.getCamera().setCenterDirect(Util.toPrimitive(nextXY));
                gameActivity.getPlayerManager().getActivePlayer().setNextTileToPanTo(null);
            }
            gameActivity.getPlayerManager().startNewTurn();
        }else {
            Player previousPlayer = gameActivity.getPlayerManager().getActivePlayer();
            gameActivity.getPlayerManager().updateCurrentPlayer();
            Player currentPlayer = gameActivity.getPlayerManager().getActivePlayer();

            AnimationManager.Builder builder = new AnimationManager.Builder(gameActivity);
            builder.queueCameraToNextWord(currentPlayer);
            builder.queueSwapPlayersHud(previousPlayer, currentPlayer);
            builder.execute(false, new Runnable() {
                @Override
                public void run() {
                    gameActivity.getHUD().RefreshHud();
                    gameActivity.getPlayerManager().startNewTurn();
                }
            });
        }
    }

    //endregion

    //endregion

}
