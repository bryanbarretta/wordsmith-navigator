package bbgames.ie.wordsmithnavigator.GameObjects.Graphics.SpriteFactory;

import android.util.Pair;

import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.batch.SpriteGroup;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.list.SmartList;

import java.util.HashMap;
import java.util.List;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 15/09/2018.
 */

public class LetterFactory {

    private final static int TILED_TEXTURE_COLUMNS = 5;
    private final static int TILED_TEXTURE_ROWS    = 1;
    private final static int CAPACITY = 5000;
    private final static TextureOptions TEXTURE_OPTIONS = TextureOptions.BILINEAR_PREMULTIPLYALPHA;
    private final static String PATH_TEXTURE = "letters/%d/tile_letters.png";

    private GameActivity gameActivity;

    private HashMap<Integer, TiledTextureRegion>         mapSkinIdTextures;
    private HashMap<Integer, Pair<SpriteGroup, Boolean>> mapSkinIdSpriteGroupBoard;
    private HashMap<Integer, Pair<SpriteGroup, Boolean>> mapSkidIdSpriteGroupHUD;
    private HashMap<Pair<Integer, Integer>, TileLetter> mapXYLetter; //Map of [col/row] and tileLetter

    public LetterFactory(GameActivity gameActivity, List<SkinLetter> skins){
        this.gameActivity = gameActivity;
        this.mapXYLetter = new HashMap<>();
        init(skins);
    }

    public TileLetter createLetterBoard(SkinLetter skin, Character letter, float pX, float pY, LetterState state, Player owner){
        return createLetter(true, skin, letter, pX, pY, state, owner);
    }

    public TileLetter createLetterHUD(SkinLetter skin, Character letter, float pX, float pY, LetterState state, Player owner){
        return createLetter(false, skin, letter, pX, pY, state, owner);
    }

    public void detachLetter(int row, int col) {
        detachLetter(getLetter(row, col));
    }

    public void detachLetter(TileLetter tile) {
        if(tile.isBoardAttached()){
            gameActivity.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    mapSkinIdSpriteGroupBoard.get(tile.getSkin().getId()).first.detachChild(tile);
                }
            });
            int[] colRow = LevelUtils.getColumnRow(gameActivity.getLevelManager().getLayoutModified().size(), new float[]{tile.getX(), tile.getY()});
            mapXYLetter.remove(new Pair<>(colRow[0], colRow[1]));
        }
        if(tile.isHudAttached()){
            mapSkidIdSpriteGroupHUD.get(tile.getSkin().getId()).first.detachChild(tile);
        }
    }

    //region private

    private void init(List<SkinLetter> skins) {
        mapSkinIdSpriteGroupBoard = new HashMap<>();
        mapSkidIdSpriteGroupHUD =   new HashMap<>();
        mapSkinIdTextures =         new HashMap<>();
        int pWidth = Constant.TILE_SIZE * TILED_TEXTURE_COLUMNS;
        int pHeight = Constant.TILE_SIZE * TILED_TEXTURE_ROWS;
        for(SkinLetter skin : skins){
            String dir = String.format(PATH_TEXTURE, skin.getId());
            if(!Util.isAssetExist(gameActivity, BitmapTextureAtlasTextureRegionFactory.getAssetBasePath() + dir)){
                throw new RuntimeException("LetterFactory could not find the file at: " + (BitmapTextureAtlasTextureRegionFactory.getAssetBasePath() + dir));
            }
            BitmapTextureAtlas atlas = new BitmapTextureAtlas(gameActivity.getTextureManager(), pWidth, pHeight, TEXTURE_OPTIONS);
            TiledTextureRegion texture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(atlas, gameActivity, dir, 0, 0, TILED_TEXTURE_COLUMNS, TILED_TEXTURE_ROWS);
            atlas.load();

            SpriteGroup spriteGroupBoard = new SpriteGroup(0f, 0f, atlas, CAPACITY, gameActivity.getVertexBufferObjectManager()){
                @Override
                protected boolean onUpdateSpriteBatch() {
                    return false;
                }
                @Override
                protected void onManagedUpdate(float pSecondsElapsed) {
                    super.onManagedUpdate(pSecondsElapsed);
                    final SmartList<IEntity> children = this.mChildren;
                    if(children != null) {
                        final int childCount = children.size();
                        for(int i = 0; i < childCount; i++) {
                            this.drawWithoutChecks((Sprite)children.get(i));
                        }
                        submit();
                    }
                }
            };
            mapSkinIdSpriteGroupBoard.put(skin.getId(), new Pair<>(spriteGroupBoard, false));

            SpriteGroup spriteGroupHUD = new SpriteGroup(0f, 0f, atlas, CAPACITY, gameActivity.getVertexBufferObjectManager()){
                @Override
                protected boolean onUpdateSpriteBatch() {
                    return false;
                }
                @Override
                protected void onManagedUpdate(float pSecondsElapsed) {
                    super.onManagedUpdate(pSecondsElapsed);
                    final SmartList<IEntity> children = this.mChildren;
                    if(children != null) {
                        final int childCount = children.size();
                        for(int i = 0; i < childCount; i++) {
                            this.drawWithoutChecks((Sprite)children.get(i));
                        }
                        submit();
                    }
                }
            };
            mapSkidIdSpriteGroupHUD.put(skin.getId(), new Pair<>(spriteGroupHUD, false));

            mapSkinIdTextures.put(skin.getId(), texture);
        }
    }

    private TileLetter createLetter(boolean isBoard, SkinLetter skin, Character letter, float pX, float pY, LetterState state, Player owner){
        TileLetter tile = new TileLetter(pX, pY, gameActivity, mapSkinIdTextures.get(skin.getId()), skin, letter, owner);
        if(isBoard){
            Pair<SpriteGroup, Boolean> v = mapSkinIdSpriteGroupBoard.get(skin.getId());
            if(v.second == false){
                gameActivity.getScene().attachChild(v.first);
                v.first.setZIndex(TileChildType.Letter.getZIndex());
                v.first.setChildrenVisible(true);
                mapSkinIdSpriteGroupBoard.put(skin.getId(), new Pair<>(v.first, true));
                gameActivity.getScene().sortChildren();
            }
            mapSkinIdSpriteGroupBoard.get(skin.getId()).first.attachChild(tile);
            gameActivity.getScene().registerTouchArea(tile);
            int[] colRow = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{pX, pY});
            mapXYLetter.put(new Pair<>(colRow[0], colRow[1]), tile);
        }else {
            Pair<SpriteGroup, Boolean> v = mapSkidIdSpriteGroupHUD.get(skin.getId());
            if(v.second == false){
                gameActivity.getHUD().attachChild(v.first);
                v.first.setChildrenVisible(true);
                mapSkidIdSpriteGroupHUD.put(skin.getId(), new Pair<>(v.first, true));
            }
            mapSkidIdSpriteGroupHUD.get(skin.getId()).first.attachChild(tile);
            gameActivity.getHUD().registerTouchArea(tile);
        }
        tile.setState(state);
        return tile;
    }

    public boolean containsLetter(int row, int col) {
        Pair<Integer, Integer> xy = new Pair<>(col, row);
        return mapXYLetter.containsKey(xy);
    }

    public TileLetter getLetter(int row, int col) {
        Pair<Integer, Integer> xy = new Pair<>(col, row);
        if(!mapXYLetter.containsKey(xy)){
            return null;
        }
        return mapXYLetter.get(xy);
    }

    //endregion

}
