package bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects;

import android.util.Log;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.AStar.Node;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class PathFinderOption {

    private int[] xy;
    private Direction direction;
    private ArrayList<HintWord> words;
    private int pathScore;
    private int[] nextPathTurnXY;
    private Direction nextPathTurnDirection;
    private String logData;

    public PathFinderOption(int[] xy, Direction direction, ArrayList<HintWord> words, int pathScore,
                            int[] nextPathTurnXY, Direction nextDirection){
        this.xy = xy;
        this.direction = direction;
        this.words = words;
        this.pathScore = pathScore;
        this.nextPathTurnXY = nextPathTurnXY;
        this.nextPathTurnDirection = nextDirection;
    }

    public int[] getXy() {
        return xy;
    }

    public Direction getDirection() {
        return direction;
    }

    public ArrayList<HintWord> getWords() {
        return words;
    }

    public int getPathScore() {
        return pathScore;
    }

    public int[] getNextPathTurnXY() {
        return nextPathTurnXY;
    }

    public String getLogDataAStar(){
        return logData;
    }

    public Direction getNextPathTurnDirection(){
        return nextPathTurnDirection;
    }

    //Maybe use this again, but path score is better for the moment
    @Deprecated
    public static double generateAverageDistanceToEndzone(ArrayList<HintWord> answers, Node[][] grid, int[] endXY) {
        double score = 0;
        ArrayList<int[]> xyMapTilesFromEndZoneToCurrentTile = new ArrayList<>();

        //populate xyMapTilesFromEndZoneToCurrentTile
        Node current = grid[endXY[1]][endXY[0]];
        while (current.getParent() != null){
            xyMapTilesFromEndZoneToCurrentTile.add(new int[]{current.getCol(), current.getRow()});
            current = current.getParent();
        }
        xyMapTilesFromEndZoneToCurrentTile.add(new int[]{current.getCol(), current.getRow()});

        //rate each answer by their tile closest to xyMapTilesFromEndZoneToCurrentTile[0]
        for(HintWord answer : answers){
            int[] xyStart = answer.getLetters().get(0).getXY();
            int[] xyEnd = answer.getLetters().get(answer.getLetters().size() - 1).getXY();
            Integer iStart = Util.indexOf(xyStart, xyMapTilesFromEndZoneToCurrentTile);
            Integer iEnd = Util.indexOf(xyEnd, xyMapTilesFromEndZoneToCurrentTile);
            if(iStart == null && iEnd == null){
                Log.d(PathFinderOption.class.getSimpleName(), answer.toString() + " is not registering on xyMapTilesFromEndZoneToCurrentTile!");
                continue;
            }
            score += Math.min(iStart == null ? 1000 : iStart, iEnd == null ? 1000 : iEnd);
        }
        score /= (double) answers.size();
        return score;
    }

    public void setWords(ArrayList<HintWord> words) {
        this.words = words;
    }

    public void logData(String logData) {
        this.logData = logData;
    }
}
