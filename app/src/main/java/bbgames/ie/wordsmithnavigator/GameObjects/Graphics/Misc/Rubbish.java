package bbgames.ie.wordsmithnavigator.GameObjects.Graphics.Misc;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import java.util.ArrayList;

/**
 * Created by Bryan on 09/05/2016.
 * Holds sprites that need to be recycled and recycle them when it can
 */
public class Rubbish {

    private ArrayList<Sprite> spritesToBeRemoved;
    private ArrayList<Text> textsToBeRemoved;

    public Rubbish(){
        spritesToBeRemoved = new ArrayList<>();
        textsToBeRemoved = new ArrayList<>();
    }

    public void RecycleSprite(Sprite sprite){
        spritesToBeRemoved.add(sprite);
    }

    public void RecycleText(Text text){
        textsToBeRemoved.add(text);
    }

    public ArrayList<Sprite> getSpritesToBeRemoved() {
        return spritesToBeRemoved;
    }

    public ArrayList<Text> getTextsToBeRemoved() {
        return textsToBeRemoved;
    }
}
