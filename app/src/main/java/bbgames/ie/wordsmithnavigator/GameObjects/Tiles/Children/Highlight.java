package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.entity.sprite.Sprite;
import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class Highlight extends Sprite implements iTileChild {

    private GameActivity gameActivity;
    private Sprite arrow;
    private Integer[] colRow;

    public Highlight(GameActivity gameActivity) {
        super(Constant.TILE_SIZE/2, Constant.TILE_SIZE/2,
                gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.TileHighlight),
                gameActivity.getVertexBufferObjectManager());
        setCullingEnabled(true);
        this.gameActivity = gameActivity;
    }

    @Override
    public boolean isBlocker() {
        return false;
    }

    @Override
    public Character getTileCode() {
        return arrow != null ? TileCodes.StartingZone.getCharacter() : TileCodes.Invisible.getCharacter();
    }

    public void showArrow(Direction direction){
        if(arrow == null) {
            arrow = new Sprite(getX(), getY(),
                    gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.TileArrowUp),
                    gameActivity.getVertexBufferObjectManager());
            attachChild(arrow);
        }
        arrow.resetRotationCenter();
        if(direction.equals(Direction.NORTH)){
            arrow.setRotation(0f);
        }
        if(direction.equals(Direction.EAST)){
            arrow.setRotation(90f);
        }
        if(direction.equals(Direction.SOUTH)){
            arrow.setRotation(180f);
        }
        if(direction.equals(Direction.WEST)){
            arrow.setRotation(270f);
        }
    }

    public void setArrowVisible(boolean visible) {
        if(arrow != null) {
            arrow.setVisible(visible);
        }
    }

    @Override
    public Integer[] getColumnRow() {
        if(colRow == null){
            int[] a = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{getX(), getY()});
            colRow = new Integer[]{a[0], a[1]};
        }
        return colRow;
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.Highlight;
    }

}
