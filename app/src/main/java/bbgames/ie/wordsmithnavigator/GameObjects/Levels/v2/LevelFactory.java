package bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.Database.LevelUserData;
import bbgames.ie.wordsmithnavigator.Settings.Settings;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Loads all the levels static and player data
 * Created by Bryan on 19/08/2018.
 */

public class LevelFactory {

    private ApplicationManager applicationManager;
    private HashMap<Integer, LevelStaticData> mapIdLevelStaticData;
    private HashMap<Integer, LevelPlayerData> mapIdLevelPlayerData;

    public LevelFactory(AssetManager assets, ApplicationManager applicationManager) {
        this.applicationManager = applicationManager;
        try {
            loadStaticLevelData(assets);
            loadPlayerLevelData(applicationManager.getDatabaseGateway());
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    //region get level objects

    public LevelStaticData getLevelStaticData(int id){
        return mapIdLevelStaticData.get(id);
    }

    public LevelPlayerData getLevelPlayerData(int id){
        return mapIdLevelPlayerData.get(id);
    }

    public ArrayList<Level> getLevels(int from, int to) {
        ArrayList<Level> levels = new ArrayList<>();
        for(int i = from; i < to; i++){
            levels.add(new Level(getLevelStaticData(i), getLevelPlayerData(i)));
        }
        return levels;
    }

    //endregion

    //region get info about level progress

    public int getLatestUnlockedLevelId(){
        return mapIdLevelPlayerData.size();
    }

    public int getTotalUnlockedStarCount(){
        int res = 0;
        for(int i = 1; i <= getLatestUnlockedLevelId(); i++){
            for(Boolean u : mapIdLevelPlayerData.get(i).getMapIdStarUnlocked().values()){
                res += (u != null && u == true) ? 1 : 0;
            }
        }
        return res;
    }

    public int getTotalLevelCount() {
        return mapIdLevelStaticData.size();
    }

    //endregion

    //region functions

    /**
     * Unlock the level of the supplied id
     * @param levelId
     */
    public void unlockLevel(int levelId) {
        if(!mapIdLevelPlayerData.containsKey(levelId)){
            LevelPlayerData l = new LevelPlayerData(mapIdLevelStaticData.get(levelId));
            mapIdLevelPlayerData.put(levelId, l);
            l.save(applicationManager, levelId);
        }
    }

    /**
     * Refresh the mapIdLevelPlayerData value for levelId, loading the value from the db
     * @param databaseGateway
     * @param levelId
     */
    public void refreshLevelPlayerData(DatabaseGateway databaseGateway, int levelId) {
        LevelUserData data = databaseGateway.getUserLevelData(levelId);
        if(data != null){
            mapIdLevelPlayerData.put(levelId, new LevelPlayerData(data.getUserData()));
        }
    }

    //endregion

    //region private load methods

    private void loadStaticLevelData(AssetManager assets) throws IOException {
        mapIdLevelStaticData = new HashMap<>();
        ArrayList<String> arrLevel = new ArrayList<>();
        InputStream inputStream = assets.open("levels_" + new Settings(applicationManager).getLanguage().getAbbreviation().toLowerCase() + ".txt");
        String line;
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( inputStream ) );
        while((line = bufferedReader.readLine()) != null )
        {
            line = line.trim();
            if(line.isEmpty()){
                continue;
            }
            else if(line.startsWith("#")){
                continue;
            }
            else {
                if(line.startsWith("[")){
                    String key = line.substring(1, line.trim().indexOf("]"));
                    if(android.text.TextUtils.isDigitsOnly(key) && !arrLevel.isEmpty()){
                        //Assign current level to list, reset and start new level
                        LevelStaticData levelStaticData = LevelStaticData.newInstance(arrLevel);
                        mapIdLevelStaticData.put(levelStaticData.getId(), levelStaticData);
                        arrLevel = new ArrayList<>();
                    }
                }
                arrLevel.add(line);
            }
        }
        if(!arrLevel.isEmpty()){
            LevelStaticData levelStaticData = LevelStaticData.newInstance(arrLevel);
            mapIdLevelStaticData.put(levelStaticData.getId(), levelStaticData);
        }
    }

    private void loadPlayerLevelData(DatabaseGateway databaseGateway) {
        mapIdLevelPlayerData = new HashMap<>();
        ArrayList<LevelUserData> data = databaseGateway.getUserLevelData();
        for(LevelUserData l : data){
            mapIdLevelPlayerData.put(l.getLevelId(), new LevelPlayerData(l.getUserData()));
        }
    }

    //endregion

}
