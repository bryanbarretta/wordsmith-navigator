package bbgames.ie.wordsmithnavigator.GameObjects.Operations;

import java.io.Serializable;
import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Holds information about the users session for this level:
 * - Words played
 * - Points
 * - Seconds
 * - Diamonds achieved
 * Created by Bryan on 17/07/2016.
 */
public class SessionStatistics implements Serializable{

    private int playerId;
    private ArrayList<WordAndPoints> submittedWords;
    private double totalPoints;
    private int totalSeconds;
    private ArrayList<Integer> diamondIds;

    private WordAndPoints bestWord;
    private WordAndPoints longestWord;

    public SessionStatistics(int playerId){
        this.playerId = playerId;
        submittedWords = new ArrayList<>();
        diamondIds = new ArrayList<>();
        totalPoints = 0;
    }

    //region setters

    public void addWord(String currentWord) {
        addWord(currentWord, Util.getPointsForWord(currentWord));
    }

    public void addWord(String currentWord, int points){
        WordAndPoints word = new WordAndPoints(currentWord, points);
        submittedWords.add(word);
        totalPoints += points;

        evaluateBestWord(word);
        evaluateLongestWord(word);
    }

    public void addDiamondId(int diamondId){
        diamondIds.add(diamondId);
    }

    public void setTotalSeconds(int totalSeconds) {
        this.totalSeconds = totalSeconds;
    }

    //endregion

    //region getters

    public WordAndPoints getBestWord(){
        return bestWord;
    }

    public WordAndPoints getLongestWord(){
        return longestWord;
    }

    public double getAverageWordLength(){
        double totalSizeAllWords = 0;
        for(WordAndPoints w : submittedWords){
            totalSizeAllWords += w.getWord().length();
        }
        return totalSizeAllWords / (double)submittedWords.size();
    }

    public double getAverageWordPoints(){
        return totalPoints / (double)submittedWords.size();
    }

    public ArrayList<WordAndPoints> getSubmittedWords(){
        return submittedWords;
    }

    public double getTotalPoints(){
        return totalPoints;
    }

    public int getTotalSeconds() {
        if(totalSeconds < 1){
            return 9999;
        }
        return totalSeconds;
    }

    public int getAverageSeconds(){
        return getTotalSeconds() / getSubmittedWords().size();
    }

    public ArrayList<Integer> getDiamondIds(){
        return diamondIds;
    }

    //endregion

    //region private

    private void evaluateBestWord(WordAndPoints candidateWord){
        if(bestWord == null){
            bestWord = candidateWord;
            return;
        }
        if(candidateWord.getPoints() > bestWord.getPoints()){
            bestWord = candidateWord;
        }
    }

    private void evaluateLongestWord(WordAndPoints candidateWord){
        if(longestWord == null){
            longestWord = candidateWord;
            return;
        }
        if(candidateWord.getWord().length() > longestWord.getWord().length()){
            longestWord = candidateWord;
        }
    }

    public String getLevelScore(int levelId) {
        //((moves * avg. length) + points) * 1.level
        double t = (getSubmittedWords().size() * getAverageWordLength());
        t += getTotalPoints();

        int levelDec = (levelId - (levelId % 10)) / 10;
        int secondRoof = 30 + levelDec; //levels 0-9 = 30 secs, 10-19 = 40 secs etc.
        int secondBonus = secondRoof - getTotalSeconds();
        t += secondBonus;

        double levelBonus = levelId < 100 ? (t / (100 - levelId)) : (t * (1 + (((levelId - 100) / 100.0))));
        t += levelBonus;

        return Math.round(t) + "";
    }

    //endregion

    public class WordAndPoints implements Serializable{
        private String word;
        private int points;

        public WordAndPoints(String word, int points){
            this.word = word;
            this.points = points;
        }

        public String getWord() {
            return word;
        }

        public int getPoints() {
            return points;
        }

        @Override
        public String toString() {
            return word + " (" + points + ")";
        }
    }

}
