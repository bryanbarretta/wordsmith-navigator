package bbgames.ie.wordsmithnavigator.GameObjects.Graphics.Sprites;

import android.os.Handler;
import android.os.Looper;

import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.sprite.Sprite;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;

/**
 * Created by Bryan on 08/07/2018.
 */

public class Hourglass extends Sprite {

    private final int SAND_SPEC_COUNT = 3;
    private final float DURATION_ANIMATION_SECONDS = 1.5f;

    private ArrayList<Sprite> sandSpecs = new ArrayList<>();

    private float xStartingSandSpec = Constant.HOURGLASS_WIDTH_HEIGHT / 2;
    private float yStartingSandSpec = 145.0f;
    private float yEndingSandSpec = 60.0f;
    private LoopEntityModifier modifier =
            new LoopEntityModifier(new MoveYModifier(DURATION_ANIMATION_SECONDS, yStartingSandSpec, yEndingSandSpec));
    private Handler handler = new Handler(Looper.getMainLooper());

    public Hourglass(float pX, float pY, GameActivity gameActivity) {
        super(pX, pY, gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Hourglass), gameActivity.getVertexBufferObjectManager());
        for(int i = 0; i < SAND_SPEC_COUNT; i++){
            sandSpecs.add(new Sprite(xStartingSandSpec, yStartingSandSpec,
                    gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HourglassSand),
                    gameActivity.getVertexBufferObjectManager()));
            attachChild(sandSpecs.get(i));
        }
        setVisible(true);
    }

    @Override
    public void setVisible(boolean pVisible) {
        super.setVisible(pVisible);
        if(pVisible){
            startAnimation();
        }else {
            endAnimation();
        }
    }

    private void startAnimation(){
        endAnimation();
        for (final Sprite s : sandSpecs){
            float delay_seconds = sandSpecs.indexOf(s) * (DURATION_ANIMATION_SECONDS / SAND_SPEC_COUNT);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    s.registerEntityModifier(new LoopEntityModifier(modifier.deepCopy()));
                }
            }, (long) (delay_seconds * 1000));
        }
    }

    private void endAnimation(){
        for (Sprite s : sandSpecs){
            s.clearEntityModifiers();
            s.setY(yStartingSandSpec);
        }
    }

}
