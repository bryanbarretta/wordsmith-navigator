package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.camera.hud.controls.DigitalOnScreenControl;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;

import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.Sprites.Hourglass;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 16/02/2018.
 */

public class HudBuilder {

    public static void createBackground(GameHud wunderHUD){
        wunderHUD.hudbg = new Sprite(
                wunderHUD.hudBounds.getCenterX(),
                wunderHUD.hudBounds.getCenterY(),
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudBackground),
                wunderHUD.gameActivity.getVertexBufferObjectManager())
        {
            @Override
            public boolean onAreaTouched(TouchEvent pTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                return true;
            }
        };
        wunderHUD.controllerBackground = new Sprite(
                wunderHUD.hudBounds.getCenterX(),
                Constant.TILE_SIZE + Constant.ICON_SMALL_64 + (Constant.HUD_CONTROLLER_BACKGROUND / 2) - Constant.HUD_PADDING,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudControllerBackground),
                wunderHUD.gameActivity.getVertexBufferObjectManager());
        wunderHUD.attachChild(wunderHUD.hudbg);
        wunderHUD.registerTouchArea(wunderHUD.hudbg);
        wunderHUD.attachChild(wunderHUD.controllerBackground);
        wunderHUD.setOnAreaTouchTraversalFrontToBack();
    }

    public static DigitalOnScreenControl createController(final GameHud wunderHUD){
        return new DigitalOnScreenControl(
                wunderHUD.controllerBackground.getX(),
                wunderHUD.controllerBackground.getY(),
                wunderHUD.gameActivity.getCamera(),
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudControllerBase),
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudControllerKnob),
                0.1f,
                true,
                wunderHUD.gameActivity.getVertexBufferObjectManager(),
                new BaseOnScreenControl.IOnScreenControlListener() {
                    @Override
                    public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl, final float pValueX, final float pValueY) {
                        if(wunderHUD.controllerEnabled){
                            wunderHUD.gameActivity.getCamera().setCenter(
                                    wunderHUD.gameActivity.getCamera().getCenterX() + (pValueX * 200),
                                    wunderHUD.gameActivity.getCamera().getCenterY() + (pValueY * 200));
                        }
                    }
                })
        {
            @Override
            public boolean onSceneTouchEvent(TouchEvent pSceneTouchEvent) {
                if(pSceneTouchEvent.isActionUp()){
                    wunderHUD.gameActivity.getLevelManager().setFlagValue(LevelManager.StateFlag.ARROW_DIRECTION_BEING_SET, false);
                }
                return super.onSceneTouchEvent(pSceneTouchEvent);
            }
        };
    }

    public static void createTileRack(GameHud wunderHUD) {
        wunderHUD.spriteTileRack = new Sprite(
                Constant.CAMERA_1920 /2,
                Constant.TILE_RACK_HEIGHT/2,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.TileRack),
                wunderHUD.gameActivity.getVertexBufferObjectManager());
        wunderHUD.attachChild(wunderHUD.spriteTileRack);
        wunderHUD.registerTouchArea(wunderHUD.spriteTileRack);
    }

    public static void createSpeechBubble(GameHud wunderHUD){
        wunderHUD.speechBubble = new SpeechBubble(wunderHUD.gameActivity,
                Constant.CAMERA_1920 - Constant.HUD_WIDTH - (Constant.SPEECH_BUBBLE_WIDTH / 2),
                Constant.CAMERA_1080 - (Constant.SPEECH_BUBBLE_HEIGHT / 2));
        wunderHUD.speechBubble.setVisible(false);
        wunderHUD.attachChild(wunderHUD.speechBubble);
    }

    public static void assignTileRackLetterCoordinates(GameHud wunderHUD){
        float tileSpaceWidth = Constant.CAMERA_1920 / wunderHUD.GetHudLetterCapacity();
        for(int i = 0; i < wunderHUD.GetHudLetterCapacity(); i++){
            float centerX = (i * tileSpaceWidth) + (tileSpaceWidth/2);
            float centerY = Constant.TILE_SIZE / 2;
            wunderHUD.mapTileRackLetterCoordinates.put(i, new float[]{centerX, centerY});
        }
    }

    public static void createSideHudControls(final GameHud wunderHUD){

        //region Player Name + Icon

        wunderHUD.imgPlayer = new Sprite(Constant.HUD_PADDING_SMALL + (Constant.ICON_STANDARD_128 / 2),
                Constant.CAMERA_1080 - (Constant.HUD_PADDING + (Constant.ICON_STANDARD_128 / 2)),
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.User),
                wunderHUD.gameActivity.getVertexBufferObjectManager());
        wunderHUD.hudbg.attachChild(wunderHUD.imgPlayer);

        int totalImageWidth = (Constant.HUD_PADDING_SMALL * 2) + Constant.ICON_STANDARD_128;
        wunderHUD.textName = new Text(((Constant.HUD_WIDTH - totalImageWidth) / 2) + totalImageWidth,
                wunderHUD.imgPlayer.getY() - Constant.HUD_PADDING_SMALL,
                wunderHUD.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.alcubierre),
                "---", 9, wunderHUD.gameActivity.getVertexBufferObjectManager());
        wunderHUD.hudbg.attachChild(wunderHUD.textName);

        //endregion

        //region clock

        float yClock = wunderHUD.imgPlayer.getY() - (Constant.ICON_STANDARD_128 / 2) - (Constant.HUD_PADDING_SMALL * 2) - (Constant.ICON_SMALL_64 / 2);
        float xClock = (Constant.HUD_WIDTH / 2) + (Constant.ICON_SMALL_64 / 2);
        wunderHUD.initialiseWunderClockManager((int)xClock, (int)yClock);

        //endregion

        //region btnTick

        float btnTickX = Constant.HUD_WIDTH / 2;
        float btnTickY = wunderHUD.controllerBackground.getY() - Constant.HUD_PADDING - (Constant.HUD_BUTTON_HEIGHT / 2);
        wunderHUD.btnTick = new ButtonSprite(btnTickX, btnTickY,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Btn_Tick),
                wunderHUD.gameActivity.getVertexBufferObjectManager()){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                wunderHUD.btnTickPressed();
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        wunderHUD.registerTouchArea(wunderHUD.btnTick);
        wunderHUD.hudbg.attachChild(wunderHUD.btnTick);

        //endregion

        //region btnUndo

        float btnUndoX = (Constant.HUD_WIDTH / 4) + 10;
        float btnUndoY = wunderHUD.controllerBackground.getY() + (Constant.HUD_BUTTON_HEIGHT / 2);
        wunderHUD.btnUndo = new ButtonSprite(btnUndoX, btnUndoY,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Btn_Undo),
                wunderHUD.gameActivity.getVertexBufferObjectManager()){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionDown()){
                    wunderHUD.btnUndoPressed();
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        wunderHUD.registerTouchArea(wunderHUD.btnUndo);
        wunderHUD.hudbg.attachChild(wunderHUD.btnUndo);

        //endregion

        //region btnCancel

        float btnCancelX = btnUndoX + (Constant.HUD_WIDTH / 2) - 15;
        float btnCancelY = btnUndoY;
        wunderHUD.btnCancel = new ButtonSprite(btnCancelX, btnCancelY,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Btn_Cancel),
                wunderHUD.gameActivity.getVertexBufferObjectManager()){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                wunderHUD.btnXPressed();
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        wunderHUD.registerTouchArea(wunderHUD.btnCancel);
        wunderHUD.hudbg.attachChild(wunderHUD.btnCancel);

        //endregion

        wunderHUD.setButtonTickEnabled(false);
        wunderHUD.setButtonXEnabled(false);
        wunderHUD.setButtonUndoEnabled(false);

        //region btnMenu + btnInfo

        float yBtnInfoMenu = yClock - (Constant.ICON_SMALL_64 / 2) - (Constant.HUD_PADDING * 2) - (Constant.HUD_BUTTON_HEIGHT / 2);

        wunderHUD.btnMenu = new ButtonSprite(btnCancelX, yBtnInfoMenu,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Btn_Menu),
                wunderHUD.gameActivity.getVertexBufferObjectManager()){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionDown()){
                    wunderHUD.btnMenuPressed();
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        wunderHUD.btnMenu.setCurrentTileIndex(0);
        wunderHUD.registerTouchArea(wunderHUD.btnMenu);
        wunderHUD.hudbg.attachChild(wunderHUD.btnMenu);

        wunderHUD.btnInfo = new ButtonSprite(btnUndoX, yBtnInfoMenu,
                wunderHUD.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Btn_Info),
                wunderHUD.gameActivity.getVertexBufferObjectManager()){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionDown()){
                    wunderHUD.btnInfoPressed();
                }
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        wunderHUD.btnInfo.setCurrentTileIndex(0);
        wunderHUD.registerTouchArea(wunderHUD.btnInfo);
        wunderHUD.hudbg.attachChild(wunderHUD.btnInfo);

        //endregion

        //region lock and lockText

        float yTopContainer = btnUndoY + (wunderHUD.btnUndo.getHeight() / 2);
        float yBottomContainer = btnTickY - (wunderHUD.btnTick.getHeight() / 2);
        wunderHUD.hourglass = new Hourglass(Constant.HUD_WIDTH / 2,
                yTopContainer - (Constant.HOURGLASS_WIDTH_HEIGHT / 2), wunderHUD.gameActivity);
        wunderHUD.hudbg.attachChild(wunderHUD.hourglass);

        wunderHUD.textLock = new Text(wunderHUD.hourglass.getX(),
                yBottomContainer + (((wunderHUD.hourglass.getY() - (Constant.HOURGLASS_WIDTH_HEIGHT / 2)) - yBottomContainer) / 2),
                wunderHUD.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.alcubierre),
                wunderHUD.gameActivity.getString(R.string.please_wait), 50, wunderHUD.gameActivity.getVertexBufferObjectManager());
        wunderHUD.hudbg.attachChild(wunderHUD.textLock);

        //endregion

    }
}
