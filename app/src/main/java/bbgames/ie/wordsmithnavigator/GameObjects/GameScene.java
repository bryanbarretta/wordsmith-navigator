package bbgames.ie.wordsmithnavigator.GameObjects;

import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.input.touch.TouchEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.HUD.GameHud;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Bridge;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Detonator;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.DetonatorCrate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Diamond;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Gate;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.Highlight;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children.PowerUp;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.FocusTile;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 11/05/2016.
 */
public class GameScene extends Scene {

    private GameActivity gameActivity;
    private boolean enabled = true;

    private IOnSceneTouchListener onSceneTouchListener = new IOnSceneTouchListener() {
        @Override
        public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
            if(!enabled){
                return false;
            }
            if(gameActivity.getLevelManager().getFlagValue(LevelManager.StateFlag.USER_IS_CREATING_WORD, false) == true){
                return false;
            }

            float[] closestTileCenterXY = LevelUtils.getClosestTileCenterXY(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
            int[]   colRow = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(),
                                                     new float[]{closestTileCenterXY[0], closestTileCenterXY[1]});
            Direction direction = getLatestDirection(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());

            switch(pSceneTouchEvent.getAction()) {
                case (MotionEvent.ACTION_DOWN) :
                    Character tile = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), colRow);
                    if(TileCodes.getBlockTileCodes().contains(tile) || Character.isLetter(tile)){
                        return true;         //Pressing down on a wall or letter does nothing
                    }
                    if(mapRowColTileChild.containsKey(new Pair<>(colRow[1], colRow[0]))){
                        ArrayList<iTileChild> children = mapRowColTileChild.get(new Pair<>(colRow[1], colRow[0]));
                        for(iTileChild child : children){
                            if(child.isBlocker()){
                                return true; //Pressing down on a blocker (e.g. closed gate, crate etc.) does nothing
                            }
                        }
                    }
                    Log.d(LOGTAG, String.format("> Touching map tile [%d, %d | %s]", colRow[0], colRow[1], direction.name()));
                    touchMapTile(colRow[1], colRow[0], direction);
                    return true;
                case (MotionEvent.ACTION_MOVE) :
                    if(!gameActivity.getLevelManager().getFlagValue(LevelManager.StateFlag.ARROW_DIRECTION_BEING_SET, false)){
                        return true; //A move that was initiated from ACTION_DOWN on a non-playable tile such as a wall: do nothing
                    }
                    FocusTile focusTile = gameActivity.getLevelManager().getFocusTile();
                    if(focusTile.getDirection().equals(direction)){
                        return true; //Would be redundant continuing
                    }
                    FocusTile newFocusTile = new FocusTile(focusTile.getXy(), direction);
                    Log.d(LOGTAG, String.format("> Setting new direction [%d, %d | %s]", newFocusTile.getXy()[0], newFocusTile.getXy()[1], newFocusTile.getDirection().name()));
                    gameActivity.getLevelManager().setFocusTile(newFocusTile, true);
                    return true;
                case (MotionEvent.ACTION_UP) :
                    gameActivity.getLevelManager().setFlagValue(LevelManager.StateFlag.ARROW_DIRECTION_BEING_SET, false);
                    return true;
                case (MotionEvent.ACTION_CANCEL) :
                    return true;
                case (MotionEvent.ACTION_OUTSIDE) :
                    return true;
                default :
                    return true;
            }
        }
    };

    private HashMap<Pair<Integer, Integer>, ArrayList<iTileChild>> mapRowColTileChild = new HashMap<>();

    public GameScene(final GameActivity gameActivity){
        this.gameActivity = gameActivity;
        setBackground(new Background(0.2f, 0.4f, 0.7f));
        setOnSceneTouchListener(onSceneTouchListener);
        registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                gameActivity.cleanupScreen();
            }
            @Override
            public void reset() {
            }
        });
    }

    public void SetEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * attach the sprite on the scene at row/col position
     * NOTE: the pX and pY value of the sprite is set in this method based on the row/col
     */
    public void attachTileChild(int row, int col, iTileChild child){
        Pair<Integer, Integer> rowCol = new Pair<>(row, col);

        IEntity childEntity = (IEntity) child;

        //Set the pX pY of the sprite
        float[] xy = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(), row, col);
        childEntity.setX(xy[0]);
        childEntity.setY(xy[1]);
        //Update the layout
        gameActivity.getLevelManager().updateLayoutModified(new int[]{col, row}, TileCodes.getTileCode(child.getTileCode()), false);
        //Add the sprite to the map
        if(!mapRowColTileChild.containsKey(rowCol)){
            mapRowColTileChild.put(rowCol, new ArrayList<>());
        }
        mapRowColTileChild.get(rowCol).add(child);
        //Attach the sprite to the scene
        attachChild(childEntity);
        childEntity.setZIndex(child.getTileChildType().getZIndex());
        registerTouchArea(childEntity);
        try {
            sortChildren();
        }catch (Exception e){
            Log.e(LOGTAG, e.getMessage());
        }
    }

    /**
     * Remove the sprite on the scene at row|col position
     * @return true if a sprite was successfully removed
     */
    public void detachTileChild(int row, int col, iTileChild tileChild){
        Pair<Integer, Integer> rowCol = new Pair<>(row, col);
        if(mapRowColTileChild.containsKey(rowCol)){
            ArrayList<iTileChild> children = mapRowColTileChild.get(rowCol);
            if(!children.contains(tileChild)){
                //Nothing to remove
                return;
            }
            int[] xy = new int[]{col, row};
            TileCodes codeAfterDetach = getCodeAfterDetaching(xy, tileChild);
            //Update the layout
            gameActivity.getLevelManager().updateLayoutModified(xy, codeAfterDetach, false);
            //Remove from the map
            children.remove(tileChild);
            mapRowColTileChild.put(rowCol, children);
            //Remove from the scene
            IEntity childEntity = (IEntity) tileChild;
            unregisterTouchArea(childEntity);
            gameActivity.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    detachChild(childEntity);
                }
            });
        }
    }

    public void detachTileChild(int row, int col, TileChildType tileChildType){
        iTileChild tileChild = getTileChild(row, col, tileChildType);
        if(tileChild == null){
            return;
        }
        detachTileChild(row, col, tileChild);
    }

    public void detachTileChild(iTileChild tileChild) {
        detachTileChild(tileChild.getColumnRow()[1], tileChild.getColumnRow()[0], tileChild);
    }

    /**
     * Detach all instances of the supplied tileChildType in the game
     * @param tileChildType
     */
    public void detachTileChildren(TileChildType... tileChildType) {
        List<TileChildType> childrenToRemove = Arrays.asList(tileChildType);
        for (Map.Entry<Pair<Integer, Integer>, ArrayList<iTileChild>> entry : mapRowColTileChild.entrySet()){
            for (TileChildType childToRemove : childrenToRemove){
                detachTileChild(entry.getKey().first, entry.getKey().second, childToRemove);
            }
        }
    }

    /**
     * Removes all highlights + arrow
     */
    public void detachHighlights(){
        gameActivity.getScene().detachTileChildren(TileChildType.Arrow, TileChildType.Highlight);
    }

    private TileCodes getCodeAfterDetaching(int[] xy, iTileChild tileChild) {
        if(tileChild instanceof Highlight){
            ArrayList<iTileChild> children = getTileChildren(xy);
            for(iTileChild child : children){
                if(child.getTileChildType().equals(TileChildType.PowerUps)){
                    return TileCodes.PowerUp;
                }
            }
            return TileCodes.getTileCode(LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), xy));
        }
        Character originalTile = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLevelStaticData().getLayoutOriginal(), xy);
        TileCodes output = TileCodes.getTileCode(originalTile);
        if(tileChild instanceof Detonator && output.equals(TileCodes.Detonator)){
            output = TileCodes.MapTile;
        }
        if(tileChild instanceof DetonatorCrate && output.equals(TileCodes.DetonatorCrate)){
            output = TileCodes.MapTile;
        }
        if(tileChild instanceof Bridge && output.equals(TileCodes.Bridge)){
            output = TileCodes.MapTile;
        }
        if(tileChild instanceof Gate && output.equals(TileCodes.Gate)){
            output = TileCodes.MapTile;
        }
        if(tileChild instanceof Diamond && output.equals(TileCodes.Diamond)){
            output = TileCodes.MapTile;
        }
        if(tileChild instanceof PowerUp && output.equals(TileCodes.PowerUp)){
            output = TileCodes.MapTile;
        }
        return output;
    }

    /**
     * Get the sprite on the scene at row|col position
     * @return null if the specified tileChildType does not exist at this position
     */
    public iTileChild getTileChild(int row, int col, TileChildType tileChildType) {
        ArrayList<iTileChild> children = getTileChildren(row, col);
        for(iTileChild child : children){
            if(tileChildType.equals(child.getTileChildType())){
                return child;
            }
        }
        return null;
    }

    public ArrayList<iTileChild> getTileChildren(int[] xy){
        return getTileChildren(xy[1], xy[0]);
    }

    public ArrayList<iTileChild> getTileChildren(int row, int col){
        return getTileChildren(new Pair<>(row, col));
    }

    public ArrayList<iTileChild> getTileChildren(Pair<Integer, Integer> rowCol){
        if(!mapRowColTileChild.containsKey(rowCol)){
            return new ArrayList<>();
        }
        return mapRowColTileChild.get(rowCol);
    }

    //region onTouchHandler methods

    private final String LOGTAG = getClass().getSimpleName();

    private Direction getLatestDirection(float newX, float newY){
        if(gameActivity.getLevelManager().getFocusTile() == null){
            float[] cXY = LevelUtils.getClosestTileCenterXY(newX, newY);
            gameActivity.getLevelManager().setFocusTile(new FocusTile(
                    LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), cXY),
                    Direction.NORTH), true);
        }
        LevelUtils.getClosestTileCenterXY(newX, newY);
        float[] fOldPXY = LevelUtils.getXY(gameActivity.getLevelManager().getRowCount(),
                                           gameActivity.getLevelManager().getFocusTile().getXy()[1],
                                           gameActivity.getLevelManager().getFocusTile().getXy()[0]);
        Direction d = BoardUtils.getDirectionRelativeToXY(fOldPXY[0], fOldPXY[1] , newX, newY);
        Log.d(LOGTAG, "Latest Direction: " + d.name() + "\t[" + fOldPXY[0] + ", " + fOldPXY[1] + " : " + newX + ", " + newY + "]");
        return d;
    }

    public void touchMapTile(int row, int col, Direction direction){
        gameActivity.getHUD().setState(gameActivity.getPlayerManager().getActivePlayer().isHumanUser() ?
                GameHud.STATE.ACTIVE_CONTROLLER :
                GameHud.STATE.LOCKED);
        FocusTile newFocusTile = new FocusTile(new int[]{col, row}, direction);
        gameActivity.getLevelManager().setFocusTile(newFocusTile, true);
        gameActivity.getLevelManager().setFlagValue(LevelManager.StateFlag.ARROW_DIRECTION_BEING_SET, true);
    }

    //endregion

}
