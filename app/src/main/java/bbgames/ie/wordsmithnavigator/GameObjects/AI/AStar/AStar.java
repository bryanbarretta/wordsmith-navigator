package bbgames.ie.wordsmithnavigator.GameObjects.AI.AStar;

import android.util.Log;
import android.util.Pair;

import java.util.*;

import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * A Star Algorithm
 *
 * @author Marcelo Surriabre
 * @version 2.1, 2017-02-23
 */
public class AStar {

    //All nodes for the map, divided into [rows][columns]. Their variables are updated live during the algorithm
    private Node[][] searchArea;

    //Open list of nodes we still have to evaluate, their position in the queue is determined by comparator.
    //A node is added to this queue the first time we encounter it. Poll() will take the node from the head of the queue.
    //When a node has been evaluated from all sides (top, bottom, left, right), it is removed from openList and added to closedSet.
    private PriorityQueue<Node> openList;

    //Once a node is added to closetSet, it has been fully evaluated from 4 sides and will not be evaluated again.
    private Set<Node> closedSet;

    //Starting node
    private Node initialNode;

    //Target node
    private Node finalNode;

    //Populated at the end of findPath()
    private List<Node> finalPath;

    private PenaltyManager penaltyManager;
    private AIDifficulty difficulty;
    private int[] startXY;
    private int[] endXY;

    public AStar(ArrayList<String> layout, int[] startXY, int[] endXY){
        this(layout, startXY, endXY, AIDifficulty.NORMAL);
    }

    public AStar(ArrayList<String> layout, int[] startXY, int[] endXY, AIDifficulty difficulty){
        this.startXY = startXY;
        this.endXY = endXY;
        this.difficulty = difficulty;
        setInitialNode(new Node(startXY[1], startXY[0]));
        setFinalNode(new Node(endXY[1], endXY[0]));
        this.searchArea = new Node[layout.size()][layout.get(0).length()];
        this.openList = new PriorityQueue<Node>(11, new Comparator<Node>() {
            @Override
            public int compare(Node node0, Node node1) {
                return Integer.compare(node0.getF(), node1.getF());
            }
        });
        setNodes();
        this.closedSet = new HashSet<>();
        //set blocks
        for(int y = 0; y < layout.size(); y++){
            String row = layout.get(y);
            for(int x = 0; x < row.length(); x++){
                Character tile = row.charAt(x);
                if(TileCodes.getBlockTileCodes().contains(tile)){
                    setBlock(y, x);
                }
                if(Character.isLetter(tile)){
                    setLetter(y, x);
                }
            }
        }
    }

    private void setNodes() {
        for (int i = 0; i < searchArea.length; i++) {
            for (int j = 0; j < searchArea[0].length; j++) {
                Node node = new Node(i, j);
                node.calculateHeuristic(getFinalNode());
                this.searchArea[i][j] = node;
            }
        }
    }

    /**
     * executes findPath(Direction embarkDirection) x 4 times; one for each direction
     * @return the best of the 4 routes (lowest score)
     */
    public List<Node> findPath(){
        ArrayList<List<Node>> routes = new ArrayList<>();
        for(Direction direction : Direction.values()){
            routes.add(findPath(direction));
        }
        List<Node> optimalRoute = null;
        Integer  optimalScore = null;
        for (List<Node> route : routes){
            if(route == null)
                continue;
            int score = getPathScore(route);
            if(optimalScore == null || score < optimalScore){
                optimalRoute = route;
                optimalScore = score;
            }
        }
        return optimalRoute;
    }

    /**
     * @param embarkDirection This is the direction we will go from the initial node
     */
    public List<Node> findPath(Direction embarkDirection) {
        penaltyManager = new PenaltyManager(embarkDirection);
        openList.add(initialNode);
        while (!isEmpty(openList)) {
            Node currentNode = openList.poll();
            closedSet.add(currentNode);
            if (isFinalNode(currentNode)) {
                addAdjacentNodes(currentNode); //TODO: Remove?
                finalPath = getPath(currentNode);
                return finalPath;
            } else {
                addAdjacentNodes(currentNode);
            }
            logNodeData(NodeMetric.final_points, currentNode);
        }
        return new ArrayList<Node>();
    }

    private List<Node> getPath(Node currentNode) {
        List<Node> path = new ArrayList<Node>();
        path.add(currentNode);
        Node parent;
        while ((parent = currentNode.getParent()) != null) {
            path.add(0, parent);
            currentNode = parent;
        }
        return path;
    }

    private void addAdjacentNodes(Node currentNode) {
        addAdjacentUpperRow(currentNode);
        addAdjacentMiddleRow(currentNode);
        addAdjacentLowerRow(currentNode);
    }

    private void addAdjacentLowerRow(Node currentNode) {
        int row = currentNode.getRow();
        int col = currentNode.getCol();
        int lowerRow = row + 1;
        if (lowerRow < getSearchArea().length) {
            checkNode(currentNode, col, lowerRow);
        }
    }

    private void addAdjacentMiddleRow(Node currentNode) {
        int row = currentNode.getRow();
        int col = currentNode.getCol();
        int middleRow = row;
        if (col - 1 >= 0) {
            checkNode(currentNode, col - 1, middleRow);
        }
        if (col + 1 < getSearchArea()[0].length) {
            checkNode(currentNode, col + 1, middleRow);
        }
    }

    private void addAdjacentUpperRow(Node currentNode) {
        int row = currentNode.getRow();
        int col = currentNode.getCol();
        int upperRow = row - 1;
        if (upperRow >= 0) {
            checkNode(currentNode, col, upperRow);
        }
    }

    /**
     * @param currentNode src node
     * @param col of adjacent node
     * @param row of adjacent node
     */
    private void checkNode(Node currentNode, int col, int row) {

        Node adjacentNode = getSearchArea()[row][col];

        int cost = penaltyManager.getPenaltyCost(adjacentNode, currentNode, searchArea);

        if (!adjacentNode.isBlock() && !getClosedSet().contains(adjacentNode)) { //adjacentNode is not a block tile, nor is it a tile we have already added to the path
            if (!getOpenList().contains(adjacentNode)) {
                //First time evaluating this node
                adjacentNode.setNodeData(currentNode, cost);
                getOpenList().add(adjacentNode);
            } else {
                //We have evaluated this node before, now check its penalty cost in this context, and update it if its better
                boolean changed = adjacentNode.checkBetterPath(currentNode, cost);
                if (changed) {
                    // adjacentNode has a new parent (currentNode) as it has a lower penalty cost
                    // Remove and Add the changed node, so that the PriorityQueue can sort again its contents with the modified "finalCost" value of the modified node
                    getOpenList().remove(adjacentNode);
                    getOpenList().add(adjacentNode);
                }
            }
        }
    }

    private boolean isFinalNode(Node currentNode) {
        return currentNode.equals(finalNode);
    }

    private boolean isEmpty(PriorityQueue<Node> openList) {
        return openList.size() == 0;
    }

    private void setBlock(int row, int col) {
        this.searchArea[row][col].setBlock(true);
    }

    private void setLetter(int row, int col) {
        this.searchArea[row][col].setLetter(true);
    }

    public Node getInitialNode() {
        return initialNode;
    }

    public void setInitialNode(Node initialNode) {
        this.initialNode = initialNode;
    }

    public Node getFinalNode() {
        return finalNode;
    }

    public void setFinalNode(Node finalNode) {
        this.finalNode = finalNode;
    }

    public Node[][] getSearchArea() {
        return searchArea;
    }

    public void setSearchArea(Node[][] searchArea) {
        this.searchArea = searchArea;
    }

    public PriorityQueue<Node> getOpenList() {
        return openList;
    }

    public void setOpenList(PriorityQueue<Node> openList) {
        this.openList = openList;
    }

    public Set<Node> getClosedSet() {
        return closedSet;
    }

    public void setClosedSet(Set<Node> closedSet) {
        this.closedSet = closedSet;
    }

    private class PenaltyManager {

        /**
         * Penalties:
         * HV (10 points) - standard penalty cost, needed for algorithm to work
         * LS (20 points) - Long Straight penalty; for each additional letter in the same direction after an ideal word length (based on difficulty), penalty is COST_LS * surplusLetterCount
         * TN (30 points) - turn cost penalty, designed to promote long straight paths (and avoid 'stair' pattern paths)
         * AL (50 points) - adjacent letter cost, try to avoid paths that will run parallel to letters
         * ED (500 points)- embark direction cost, apply this if the path from the initial node is not in the direction specified (embarkDirection)
         */
        private int COST_HV = 10;
        private int COST_LS = 20;
        private int COST_TN = 30;
        private int COST_AL = 200;
        private int COST_ED = 500;

        private Direction embarkDirection;

        public PenaltyManager(Direction embarkDirection) {
            this.embarkDirection = embarkDirection;
        }

        /**
         * Returns the penalty cost to apply to 'toNode' passed in. For context, a 'fromNode' must be supplied (which will be one of the adjacent nodes).
         * @param toNode we are evaluating the penalty that will be applied to toNode
         * @param fromNode this node gives us the context from which the penalty will be determined
         * @param searchArea
         * @return the penalty that will be applied to toNode
         * Example:
         * 0	0	0	0	0	0	0
         * 0	15	0	0	0	0	0
         * 15	0*	13	0	0	0!	0
         * 0	15	0	0	0	0	0
         * 0	0	0	0	0	0	0
         * 0	0	0	0	0	0	0
         * Where -> [0*] is the initial node and [0!] is the final node. In this scenario, [0*] is the fromNode.
         *          Passing the 4 adjacent nodes into this method as 'toNode' returns [13] for the node to the right, [15] for the other 3.
         */
        public int getPenaltyCost(Node toNode, Node fromNode, Node[][] searchArea){
            int cost = 0;
            cost += getHorizontalVerticalPenaltyCost();
            cost += getLongStraightPenaltyCost(toNode, fromNode);
            cost += getTurnPenaltyCost(toNode, fromNode);
            cost += getAdjacentLetterCost(toNode, fromNode, searchArea);
            cost += getEmbarkDirectionCost(toNode, fromNode);
            return cost;
        }

        private int getHorizontalVerticalPenaltyCost(){
            return COST_HV;
        }

        private int getLongStraightPenaltyCost(Node toNode, Node fromNode) {
            Direction direction = LevelUtils.getDirection(toNode.getCol(), toNode.getRow(), fromNode.getCol(), fromNode.getRow());
            double desiredWordLength = (double)(difficulty.getMaxWordLength() + difficulty.getMinWordLength()) / 2;
            int wordLength = 1; //toNode to fromNode
            Node currentNode = fromNode;
            while (currentNode.getParent() != null){
                Direction d = LevelUtils.getDirection(currentNode.getCol(), currentNode.getRow(), currentNode.getParent().getCol(), currentNode.getParent().getRow());
                if(!d.equals(direction)){
                    //A turn has occurred, break the while loop
                    break;
                }
                currentNode = currentNode.getParent();
                wordLength++;
            }
            if(wordLength <= desiredWordLength){
                return 0;
            }
            return (int)(wordLength - desiredWordLength) * COST_LS;
        }

        private int getTurnPenaltyCost(Node toNode, Node fromNode){
            if(fromNode.getParent() == null){
                //Not enough information to determine if a turn has occurred
                return 0;
            }
            Node prevFromNode = fromNode.getParent();
            Direction direction1 = LevelUtils.getDirection(prevFromNode.getCol(), prevFromNode.getRow(), fromNode.getCol(), fromNode.getRow());
            Direction direction2 = LevelUtils.getDirection(fromNode.getCol(), fromNode.getRow(), toNode.getCol(), toNode.getRow());
            if(direction1.equals(direction2)){
                return 0;
            }else {
                return COST_TN;
            }
        }

        private int getAdjacentLetterCost(Node toNode, Node fromNode, Node[][] searchArea) {
            Direction direction = LevelUtils.getDirection(fromNode.getCol(), fromNode.getRow(), toNode.getCol(), toNode.getRow());
            //Get the next node after toNode (using direction); if its a letter, punish toNode
            int row = toNode.getRow();
            int col = toNode.getCol();
            switch (direction){
                case NORTH: row--; break;
                case SOUTH: row++; break;
                case EAST:  col++; break;
                case WEST:  col--; break;
            }
            if(row >= 0 && row < searchArea.length){
                if(col >= 0 && col < searchArea[row].length){
                    if(searchArea[row][col].isLetter()){
                        return COST_AL;
                    }
                }
            }
            return 0;
        }

        private int getEmbarkDirectionCost(Node toNode, Node fromNode){
            if(fromNode.equals(initialNode)){
                Direction direction = LevelUtils.getDirection(fromNode.getCol(), fromNode.getRow(), toNode.getCol(), toNode.getRow());
                if(!embarkDirection.equals(direction)){
                    return COST_ED;
                }
            }
            return 0;
        }
    }

    //region logging

    private boolean logToLogcat = false;
    private boolean logToSystem = false;

    public void setLogToLogcat(boolean logToLogcat) {
        this.logToLogcat = logToLogcat;
    }

    public void setLogToSystem(boolean logToSystem) {
        this.logToSystem = logToSystem;
    }

    public String getLogSummary() {
        StringBuilder sb = new StringBuilder();
        for(Node[] row : searchArea){
            for(Node node : row){
                if(node == null || node.isBlock()){
                    sb.append("---- ");
                }else {
                    sb.append((node.getF() > 9999 ? "9999" : String.format("%04d", node.getF())) + " ");
                }
            }
            sb.append("\n");
        }
        return String.format("*** AStar Summary ***\nPath: %s to %s\nFinal Score: %s\n%s",
                initialNode.toString(),
                finalNode.toString(),
                getPathScore(finalPath),
                sb.toString());
    }

    private enum NodeMetric {
        penalty,
        final_points,
        heuristic
    }

    private int iCycle = 0; //For logging

    private void logNodeData(NodeMetric nodeMetric, Node currentNode){
        if(logToLogcat == false && logToSystem == false){
            return;
        }
        iCycle ++;
        log("\n[" + iCycle + "] " + nodeMetric.name() + ": CurrentNode: " + currentNode.toString() + " ParentNode: " + (currentNode.getParent() == null ? "" : currentNode.getParent().toString())
                + "\nClosedSet: " + closedSet.toString()
                + "\nOpenList : " + openList.toString());
        for(Node[] row : searchArea){
            StringBuilder sb = new StringBuilder();
            for(Node node : row){
                switch (nodeMetric){
                    case penalty: sb.append(node.getP()); break;
                    case final_points: sb.append(node.getF()); break;
                    case heuristic: sb.append(node.getH()); break;
                }
                if(node.equals(initialNode)){
                    sb.append(">");
                }
                if(node.equals(finalNode)){
                    sb.append("!");
                }
                if(node.equals(currentNode)){
                    sb.append("*");
                }
                sb.append("\t");
            }
            log(sb.toString());
        }
        log(currentNode + "'s parent is now: " + (currentNode.getParent() == null ? "" : currentNode.getParent().toString()));
    }

    private void log(String log){
        if(logToLogcat){
            Log.d(getClass().getSimpleName(), log);
        }
        if(logToSystem){
            System.out.println(log);
        }
    }

    //endregion

    public static int getPathScore(List<Node> path){
        if(path == null || path.isEmpty()){
            return -1;
        }
        int score = 0;
        for(Node n : path){
            score += n.getF();
        }
        return score;
    }

    public static Pair<int[], Direction> getXyDirectionFirstTurn(List<Node> path){
        if(path == null || path.size() == 0){
            return null;
        }
        if(path.size() == 1){
            //Means the word is adjacent to the endzone, so just a 1-letter play will win
            return new Pair<>(new int[]{path.get(0).getCol(), path.get(0).getRow()}, null);
        }
        Direction direction = LevelUtils.getDirection(path.get(0).getCol(), path.get(0).getRow(), path.get(1).getCol(), path.get(1).getRow());
        for(Node n : path){
            if(n.getParent() == null){
                continue;
            }
            Direction d = LevelUtils.getDirection(n.getParent().getCol(), n.getParent().getRow(), n.getCol(), n.getRow());
            if(!direction.equals(d)){
                return new Pair<>(new int[]{n.getParent().getCol(), n.getParent().getRow()}, d);
            }
        }
        //If there is no turn (i.e. straight path to the endzone) just return the last path node
        Node lastN = path.get(path.size()-1);
        return new Pair<>(new int[]{lastN.getCol(), lastN.getRow()}, null);
    }

}