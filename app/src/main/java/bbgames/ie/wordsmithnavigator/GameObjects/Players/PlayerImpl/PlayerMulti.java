package bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerImpl;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 17/06/2017.
 */

public class PlayerMulti extends Player {

    public PlayerMulti(int id, PlayerType playerType, GameActivity gameActivity, int[] xyStart, SkinLetter letterSkin, SkinHUD themePlayer) {
        super(id, playerType, gameActivity.getString(R.string.opponent), gameActivity, xyStart, letterSkin, themePlayer, GraphicsManager.TextureKeys.User);
    }

}
