package bbgames.ie.wordsmithnavigator.GameObjects.Players;

import bbgames.ie.wordsmithnavigator.Constants.TileCodes;

/**
 * Enum representation of the player and mapping their tilecode
 */

public enum PlayerType{

    USER(TileCodes.StartingZone),
    MULTI_PLAYER_USER(TileCodes.StartingZoneP2),
    CPU(TileCodes.StartingZoneCPU),
    ROGUE(null);

    private TileCodes tileCode;

    PlayerType(TileCodes tileCode){
        this.tileCode = tileCode;
    }

    public TileCodes getTileCode(){
        return tileCode;
    }

    public static PlayerType getPlayerType(Character c){
        if(c.equals(USER.getTileCode().getCharacter()))
            return USER;
        if(c.equals(MULTI_PLAYER_USER.getTileCode().getCharacter()))
            return MULTI_PLAYER_USER;
        if(c.equals(CPU.getTileCode().getCharacter()))
            return CPU;
        if(c == null)
            return ROGUE;
        return null;
    }
}
