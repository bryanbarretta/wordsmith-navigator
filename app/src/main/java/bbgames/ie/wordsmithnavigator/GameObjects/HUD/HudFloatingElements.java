package bbgames.ie.wordsmithnavigator.GameObjects.HUD;

import android.os.Handler;
import android.os.Looper;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;
import java.util.HashMap;

import javax.microedition.khronos.opengles.GL10;

import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Hud items that float over the main area of the screen (above the hud letters on the rack):
 *  - Turns taken so far
 *  - Total points so far
 *  - Total seconds so far
 *  - Gems + Trophies
 *  - Letter Sack
 */
public class HudFloatingElements {

    private static final int PX_BONUS_TEXT_HEIGHT_OVER_NORMAL_TEXT = 100;

    private GameHud hud;

    private final float timeMoveDurationSeconds = 0.3f;
    private final float timeDisplayDurationSeconds = 2.0f;
    private final int x = (Constant.CAMERA_1920 - Constant.HUD_WIDTH) / 2;
    private final int yShadowBarRaised =  Constant.TILE_SIZE + Constant.HUD_TEXT_SHADOW_PADDING_BOTTOM + (Constant.HUD_TEXT_SHADOW_HEIGHT / 2);
    private final int yShadowBarLowered = - Constant.TILE_SIZE;
    private final Runnable runnableDescend = new Runnable() {
        @Override
        public void run() {
            if(!currentState.equals(State.Stickied)) {
                currentModifier = createMoveYModifier(yShadowBarRaised, yShadowBarLowered, true);
                shadowBar.registerEntityModifier(currentModifier);
                currentState = State.Descending;
            }
        }
    };

    public float[] getLetterSackXY() {
        return new float[]{spriteLetterSack.getX(), spriteLetterSack.getY()};
    }

    private enum State {
        Lowered,
        Ascending,
        Displayed,
        Descending,
        Stickied
    }
    private Handler handler = new Handler(Looper.getMainLooper());
    private State currentState;
    private MoveYModifier currentModifier;

    private Sprite shadowBar;   //Parent for all sprites, so this is the only sprite that needs to be moved

    private Sprite spriteTurns, spritePoints, spriteTime, spriteLetterSack;
    private ArrayList<Sprite> spriteDiamonds;
    private Text   textTurns, textPoints, textTime;
    private Text   textTurnsLabel, textPointsLabel;
    private HashMap<Integer, TextureRegion> mapPlayerIdShadowTextureRegion = new HashMap<>();
    private Text   textSuccessfulWord, textAddedTurn, textAddedPoints,
                                       textAddedTurnBonus, textAddedPointsBonus, textAddedSecondsBonus;

    private int X_ITEM_GAP = 225;
    private int X_ANCHOR_LEFT_ICON_TURNS = Constant.HUD_PADDING;
    private int X_ANCHOR_LEFT_TEXT_TURNS = X_ANCHOR_LEFT_ICON_TURNS + Constant.ICON_SMALL_64 + Constant.HUD_PADDING_BIG;
    private int X_ANCHOR_LEFT_ICON_POINTS = X_ANCHOR_LEFT_TEXT_TURNS + X_ITEM_GAP;
    private int X_ANCHOR_LEFT_TEXT_POINTS = X_ANCHOR_LEFT_ICON_POINTS + Constant.ICON_SMALL_64 + Constant.HUD_PADDING_BIG;
    private int X_ANCHOR_LEFT_ICON_CLOCK = X_ANCHOR_LEFT_TEXT_POINTS + X_ITEM_GAP;
    private int X_ANCHOR_LEFT_TEXT_CLOCK = X_ANCHOR_LEFT_ICON_CLOCK + Constant.ICON_SMALL_64 + Constant.HUD_PADDING_BIG;
    private int X_ANCHOR_LEFT_ICON_DIAMONDS = X_ANCHOR_LEFT_ICON_CLOCK + X_ITEM_GAP + (X_ITEM_GAP / 2);

    public HudFloatingElements(GameHud hud){
        this.hud = hud;
        setup();
    }

    public void animate(boolean lowerItemsAfterDisplaying){
        updateDiamondData();
        if(currentState == State.Stickied){
            //btnInfo does nothing while items are stickied on screen
            return;
        }
        else if(currentState == State.Displayed){
            //btnInfo pressed while the floating items are already displayed: Keep them up for msDisplayedDuration starting now
            removeModifier();
            if(lowerItemsAfterDisplaying) {
                handler.postDelayed(runnableDescend, (int) (timeDisplayDurationSeconds * 1000));
            }else {
                this.currentState = State.Stickied;
            }
        }
        else if(currentState == State.Ascending){
            if(!lowerItemsAfterDisplaying){
                //The app has called animate() [Not the user] while it is already ascending.
                //Marking the current state as stickied now
                currentState = State.Stickied;
            }
            return; //Do nothing, its already ascending
        }
        else if(currentState == State.Descending){
            //btnInfo pressed while the floating items are being lowered: Restart the full animation from the current Y
            removeModifier();
            float secondsDurationToRise = timeMoveDurationSeconds * getFractionOfAnimationUp(shadowBar.getY());
            currentModifier = createMoveYModifier(shadowBar.getY(), yShadowBarRaised, lowerItemsAfterDisplaying);
            shadowBar.registerEntityModifier(currentModifier);
            currentState = State.Ascending;
            if(lowerItemsAfterDisplaying) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Now i am displayed, I will post a lower runnable after msDisplayDuration has elapsed
                        handler.post(runnableDescend);
                    }
                }, Math.round(1000 * (timeDisplayDurationSeconds + secondsDurationToRise)));
            }
        }else if(currentState == State.Lowered){
            //A full animation
            removeModifier();
            currentModifier = createMoveYModifier(shadowBar.getY(), yShadowBarRaised, lowerItemsAfterDisplaying);
            shadowBar.registerEntityModifier(currentModifier);
            currentState = State.Ascending;
            if(lowerItemsAfterDisplaying) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Now i am displayed, I will post a lower runnable after msDisplayDuration has elapsed
                        handler.post(runnableDescend);
                    }
                }, Math.round(1000 * (timeDisplayDurationSeconds + timeMoveDurationSeconds)));
            }
        }
    }

    private void updateDiamondData() {
        int i = 0;
        boolean isHumanUser = hud.gameActivity.getPlayerManager().getActivePlayer().isHumanUser();
        for(int id : hud.gameActivity.getLevelManager().getLevelPlayerData().getMapIdDiamondUnlocked().keySet()){
            boolean unlocked = hud.gameActivity.getLevelManager().getSessionStatistics().getDiamondIds().contains(id);
            spriteDiamonds.get(i).setVisible(isHumanUser && unlocked);
            i++;
        }
    }

    //This will raise the hud items and sticky them on screen until unstickyItems() is called
    public void stickyItems(){
        animate(false);
    }

    public void unstickyItems(){
        currentState = State.Displayed;
        runnableDescend.run();
    }

    public void setTextTurns(int turns){
        textTurns.setText("" + turns);
        textTurns.setPosition(X_ANCHOR_LEFT_TEXT_TURNS + (textTurns.getWidth() / 2), textTurns.getY());
        textTurnsLabel.setPosition(textTurns.getX() + (textTurns.getWidth() / 2) + Constant.HUD_PADDING_BIG + (textTurnsLabel.getWidth() / 2), textTurnsLabel.getY());
    }

    public void setTextPoints(int points){
        textPoints.setText("" + points);
        textPoints.setPosition(X_ANCHOR_LEFT_TEXT_POINTS + (textPoints.getWidth() / 2), textPoints.getY());
        textPointsLabel.setPosition(textPoints.getX() + (textPoints.getWidth() / 2) + Constant.HUD_PADDING_BIG + (textPointsLabel.getWidth() / 2), textTurnsLabel.getY());
    }

    public void setTimeSeconds(int seconds){
        setTimeSeconds(Util.convertTotalSecondsToStringMMSS(seconds));
    }

    public void setTimeSeconds(String txt) {
        textTime.setText(txt);
        textTime.setPosition(X_ANCHOR_LEFT_TEXT_CLOCK + (textTime.getWidth() / 2), textTime.getY());
    }

    public Text getTextSuccessfulWord(){
        return textSuccessfulWord;
    }

    public void incrementPoints(final int points, final int bonusPoints) {
        final float yStartingPosition = yShadowBarRaised + Constant.HUD_PADDING + textPoints.getHeight();
        textAddedPoints.setText("+" + String.format("%02d", points));
        textAddedPoints.setPosition(textAddedPoints.getX(), yStartingPosition);
        textAddedPoints.setAlpha(0f);
        textAddedPoints.setVisible(true);
        textAddedPoints.registerEntityModifier(generateAddedTextModifierAnimation(textAddedPoints, new Runnable() {
            @Override
            public void run() {
                hud.AddPointsToScore(points + bonusPoints);
            }
        }));
        if(bonusPoints != 0){
            textAddedPointsBonus.setText((bonusPoints > 0 ? "+" : "-") + String.format("%02d", bonusPoints));
            textAddedPointsBonus.setPosition(textAddedPoints.getX(), yStartingPosition + PX_BONUS_TEXT_HEIGHT_OVER_NORMAL_TEXT);
            textAddedPointsBonus.setAlpha(0f);
            textAddedPointsBonus.setVisible(true);
            textAddedPointsBonus.registerEntityModifier(generateAddedTextModifierAnimation(textAddedPointsBonus, null));
        }else {
            textAddedPointsBonus.setVisible(false);
        }
    }

    public void incrementTurns(final int bonusTurns) {
        final float yStartingPosition = yShadowBarRaised + Constant.HUD_PADDING + textPoints.getHeight();
        textAddedTurn.setPosition(textAddedTurn.getX(), yStartingPosition);
        textAddedTurn.setAlpha(0f);
        textAddedTurn.setVisible(true);
        textAddedTurn.registerEntityModifier(generateAddedTextModifierAnimation(textAddedTurn, new Runnable() {
            @Override
            public void run() {
                hud.IncrementMoves(bonusTurns);
            }
        }));
        if(bonusTurns != 0) {
            textAddedTurnBonus.setText((bonusTurns > 0 ? "-" : "+") + bonusTurns);
            textAddedTurnBonus.setPosition(textAddedTurnBonus.getX(), yStartingPosition + PX_BONUS_TEXT_HEIGHT_OVER_NORMAL_TEXT);
            textAddedTurnBonus.setAlpha(0f);
            textAddedTurnBonus.setVisible(true);
            textAddedTurnBonus.registerEntityModifier(generateAddedTextModifierAnimation(textAddedTurnBonus, null));
        }else {
            textAddedTurnBonus.setVisible(false);
        }
    }

    public void incrementSeconds(final int bonusSeconds) {
        if(bonusSeconds != 0){
            final float yStartingPosition = yShadowBarRaised + Constant.HUD_PADDING + textTime.getHeight();
            textAddedSecondsBonus.setText((bonusSeconds > 0 ? "-" : "+") + bonusSeconds);
            textAddedSecondsBonus.setPosition(textTime.getX(), yStartingPosition);
            textAddedSecondsBonus.setAlpha(0f);
            textAddedSecondsBonus.setVisible(true);
            textAddedSecondsBonus.registerEntityModifier(generateAddedTextModifierAnimation(textAddedSecondsBonus, new Runnable() {
                @Override
                public void run() {
                    hud.addBonusSeconds(bonusSeconds);
                }
            }));
        }else {
            textAddedSecondsBonus.setVisible(false);
        }
    }

    /**
     * Call when switching players: Updates text values and color, and shadow bar color
     * @param player
     */
    public void setPlayer(Player player){
        if(!mapPlayerIdShadowTextureRegion.containsKey(player.getId())){
            if (player.getThemePlayer().ordinal() == hud.gameActivity.getGraphicsManager().GetDefaultThemeHudId()) {
                mapPlayerIdShadowTextureRegion.put(player.getId(), hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudTextShadow));
            }else {
                mapPlayerIdShadowTextureRegion.put(player.getId(), hud.gameActivity.getGraphicsManager().LoadDynamicTextureRegion(
                    "gfx/hud/" + player.getThemePlayer().ordinal() + "/",
                    "hud_shadowbar.png",
                    Constant.CAMERA_1920 - Constant.HUD_WIDTH, Constant.HUD_TEXT_SHADOW_HEIGHT)
                );
            }
        }
        shadowBar.setTextureRegion(mapPlayerIdShadowTextureRegion.get(player.getId()));
        int textColor = hud.gameActivity.getResources().getColor(player.getThemePlayer().getHudTextColor());
        textTurns.setColor(textColor);
        textPoints.setColor(textColor);
        textTime.setColor(textColor);
        textTurnsLabel.setColor(textColor);
        textPointsLabel.setColor(textColor);
        setTextTurns(player.getTotalMoves());
        setTextPoints(player.getTotalPoints());
        setTimeSeconds(player.getTotalSeconds());
    }

    public float getYRaised(){
        return yShadowBarRaised;
    }

    //region private helpers

    private void setup(){

        int shadowBarWidth = Constant.CAMERA_1920 - Constant.HUD_WIDTH;
        int yIcon = Constant.HUD_TEXT_SHADOW_HEIGHT / 2;
        int yText = yIcon - 4;
        int ySmallText = yText - 13;

        shadowBar = new Sprite(x, yShadowBarLowered,
                hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.HudTextShadow),
                hud.gameActivity.getVertexBufferObjectManager());
        currentState = State.Lowered;

        spriteTurns =  new Sprite(X_ANCHOR_LEFT_ICON_TURNS + (Constant.ICON_SMALL_64 / 2), yIcon,
                hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.IconTurns),
                hud.gameActivity.getVertexBufferObjectManager());
        spritePoints = new Sprite(X_ANCHOR_LEFT_ICON_POINTS + (Constant.ICON_SMALL_64 / 2), yIcon,
                hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.IconPoints),
                hud.gameActivity.getVertexBufferObjectManager());
        spriteTime =   new Sprite(X_ANCHOR_LEFT_ICON_CLOCK + (Constant.ICON_SMALL_64 / 2), yIcon,
                hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.IconTime),
                hud.gameActivity.getVertexBufferObjectManager());
        spriteLetterSack = new Sprite(shadowBarWidth - Constant.HUD_PADDING - (Constant.LETTER_SACK / 2),
                (Constant.LETTER_SACK / 2),
                hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.LetterSack),
                hud.gameActivity.getVertexBufferObjectManager());
        shadowBar.attachChild(spriteTurns);
        shadowBar.attachChild(spritePoints);
        shadowBar.attachChild(spriteTime);
        shadowBar.attachChild(spriteLetterSack);

        spriteDiamonds = new ArrayList<>();
        int i = 0;
        for(int id : hud.gameActivity.getLevelManager().getLevelPlayerData().getMapIdDiamondUnlocked().keySet()){
            float x = X_ANCHOR_LEFT_ICON_DIAMONDS + (Constant.ICON_SMALL_64 / 2) +
                    (i * Constant.ICON_SMALL_64) + (i * Constant.HUD_PADDING_SMALL);
            Sprite diamond = new Sprite(x, shadowBar.getHeight() / 2,
                    hud.gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Diamond),
                    hud.gameActivity.getVertexBufferObjectManager());
            shadowBar.attachChild(diamond);
            diamond.setVisible(hud.gameActivity.getLevelManager().getSessionStatistics().getDiamondIds().contains(id));
            spriteDiamonds.add(diamond);
            i++;
        }

        //region texts

        textTurns = new Text(X_ANCHOR_LEFT_TEXT_TURNS, yText,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica,
                        hud.gameActivity.getResources().getColor(R.color.white)),
                "0", 3, hud.gameActivity.getVertexBufferObjectManager());
        textTurnsLabel = new Text(X_ANCHOR_LEFT_TEXT_TURNS, ySmallText,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.alcubierreSmall,
                        hud.gameActivity.getResources().getColor(R.color.white)),
                hud.gameActivity.getString(R.string.turns).toUpperCase(),
                hud.gameActivity.getString(R.string.turns).length(), hud.gameActivity.getVertexBufferObjectManager());
        shadowBar.attachChild(textTurns);
        shadowBar.attachChild(textTurnsLabel);
        setTextTurns(0);

        textPoints = new Text(X_ANCHOR_LEFT_TEXT_POINTS, yText,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica,
                        hud.gameActivity.getResources().getColor(R.color.white)),
                "0", 4, hud.gameActivity.getVertexBufferObjectManager());
        textPointsLabel = new Text(X_ANCHOR_LEFT_TEXT_POINTS, ySmallText,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.alcubierreSmall,
                        hud.gameActivity.getResources().getColor(R.color.white)),
                hud.gameActivity.getString(R.string.points).toUpperCase(),
                hud.gameActivity.getString(R.string.points).length(), hud.gameActivity.getVertexBufferObjectManager());
        shadowBar.attachChild(textPoints);
        shadowBar.attachChild(textPointsLabel);
        setTextPoints(0);

        textTime = new Text(X_ANCHOR_LEFT_TEXT_CLOCK, yText,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica,
                        hud.gameActivity.getResources().getColor(R.color.white)),
                "00:00", 6, hud.gameActivity.getVertexBufferObjectManager());
        shadowBar.attachChild(textTime);
        setTimeSeconds(0);

        textSuccessfulWord = new Text(hud.getHudSceneWindow().getCenterX(), hud.getHudSceneWindow().getCenterY(),
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.litsansLarge, Constant.COLOR_GREEN_LIGHT),
                "", 20, hud.gameActivity.getVertexBufferObjectManager());
        textSuccessfulWord.setVisible(false);
        hud.attachChild(textSuccessfulWord);

        textAddedTurn = new Text(textTurns.getX(), shadowBar.getY(),
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica, Constant.COLOR_GREEN_LIGHT),
                "+1", 2, hud.gameActivity.getVertexBufferObjectManager());
        textAddedTurn.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        textAddedTurn.setVisible(false);
        textAddedTurn.setZIndex(3);
        hud.attachChild(textAddedTurn);

        textAddedTurnBonus = new Text(textTurns.getX(), shadowBar.getY() + PX_BONUS_TEXT_HEIGHT_OVER_NORMAL_TEXT,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica, Constant.COLOR_YELLOW),
                "+1", 2, hud.gameActivity.getVertexBufferObjectManager());
        textAddedTurnBonus.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        textAddedTurnBonus.setVisible(false);
        textAddedTurnBonus.setZIndex(3);
        hud.attachChild(textAddedTurnBonus);

        textAddedPoints = new Text(textPoints.getX(), shadowBar.getY(),
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica, Constant.COLOR_GREEN_LIGHT),
                "+10", 3, hud.gameActivity.getVertexBufferObjectManager());
        textAddedPoints.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        textAddedPoints.setVisible(false);
        textAddedPoints.setZIndex(3);
        hud.attachChild(textAddedPoints);

        textAddedPointsBonus = new Text(textPoints.getX(), shadowBar.getY() + PX_BONUS_TEXT_HEIGHT_OVER_NORMAL_TEXT,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica, Constant.COLOR_YELLOW),
                "+10", 3, hud.gameActivity.getVertexBufferObjectManager());
        textAddedPointsBonus.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        textAddedPointsBonus.setVisible(false);
        textAddedPointsBonus.setZIndex(3);
        hud.attachChild(textAddedPointsBonus);

        textAddedSecondsBonus = new Text(textTime.getX(), shadowBar.getY() + PX_BONUS_TEXT_HEIGHT_OVER_NORMAL_TEXT,
                hud.gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.kelvetica, Constant.COLOR_YELLOW),
                "-30", 5, hud.gameActivity.getVertexBufferObjectManager());
        textAddedSecondsBonus.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        textAddedSecondsBonus.setVisible(false);
        textAddedSecondsBonus.setZIndex(3);
        hud.attachChild(textAddedSecondsBonus);

        //endregion

        hud.attachChild(shadowBar);
    }

    private float getFractionOfAnimationUp(float y) {
        int offset = Math.abs(yShadowBarLowered);
        float max = offset + yShadowBarRaised;       //e.g. 520
        float current = offset + y;         //e.g. 190
        return current / max;               //e.g. return 190 / 520 = 0.36538...
    }

    private void removeModifier(){
        shadowBar.clearEntityModifiers();
        currentModifier = null;
        handler.removeCallbacksAndMessages(null);
    }

    private MoveYModifier createMoveYModifier(final float fromY, final float toY, final boolean lowerItemsAfterDisplaying){
        return new MoveYModifier(timeMoveDurationSeconds, fromY, toY){
            @Override
            protected void onModifierFinished(IEntity pItem) {
                super.onModifierFinished(pItem);
                if(fromY > toY){
                    currentState = State.Lowered;
                }else if(!currentState.equals(State.Stickied)){
                    currentState = lowerItemsAfterDisplaying ? State.Displayed : State.Stickied;
                }
            }
        };
    }

    private AlphaModifier generateAddedTextModifierAnimation(final Text text, final Runnable onDisplayFinishedRunnable){
        final float sFadeIn = 0.5f;
        final float sDisplay = 0.5f;
        final float sLowerAndFadeOut = 0.5f;
        return new AlphaModifier(sFadeIn, 0f, 1f){
            @Override
            protected void onModifierFinished(IEntity pItem) {
                super.onModifierFinished(pItem);
                //Now display the text
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(onDisplayFinishedRunnable != null) {
                            onDisplayFinishedRunnable.run();
                        }
                        //Finally, lower + fadeout
                        text.registerEntityModifier(new ParallelEntityModifier(
                                new MoveYModifier(sLowerAndFadeOut, text.getY(), yShadowBarRaised),
                                new AlphaModifier(sLowerAndFadeOut, 1f, 0f)
                        ){
                            @Override
                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                super.onModifierFinished(pModifier, pItem);
                                text.setVisible(false);
                            }
                        });
                    }
                }, (long) (sDisplay * 1000));
            }
        };
    }

    //endregion

}
