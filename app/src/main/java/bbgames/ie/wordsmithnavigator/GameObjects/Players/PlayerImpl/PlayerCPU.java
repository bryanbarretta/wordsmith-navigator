package bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerImpl;

import android.os.Handler;

import org.joda.time.DateTime;

import java.util.UUID;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.TileSpeed;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPUWordEngine;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 17/06/2017.
 */

public class PlayerCPU extends Player {

    //CPU variables
    private CPUWordEngine cpuWordEngine;
    private HintWord cpuNextWord;

    private UUID optimalPathTaskId;
    private DateTime dtLoadOptimalPaths;
    private static final int MS_MIN_DELAY_CPU_TURN = 1000;  //CPU will take at least 1 second to make a move, and try again every second
    private static final int MS_MAX_DELAY_CPU_TURN = 20000; //CPU will forfeit if it cannot find a solution within 20 seconds
    private static final int MS_MIN_CPU_THINKING_TIME = 10000; //From the end of CPU's last turn until it plays its next word must be 10 seconds minimum
    private Handler handlerPlayWordCPU;

    public PlayerCPU(int id, PlayerType playerType, GameActivity gameActivity, int[] xyStart, SkinLetter letterSkin, SkinHUD themePlayer) {
        super(id, playerType, gameActivity.getString(R.string.cpu), gameActivity, xyStart, letterSkin, themePlayer, GraphicsManager.TextureKeys.User_CPU);
        this.cpuWordEngine = new CPUWordEngine(gameActivity) {
            @Override
            public void onWordReady(HintWord word) {
                cpuNextWord = word;
            }
        };
    }

    public void LoadOptimalPaths(){
        if(optimalPathTaskId != null){
            gameActivity.getApplicationManager().getAsyncTaskHandler().deleteQueuedTasks(optimalPathTaskId);
        }
        optimalPathTaskId = UUID.randomUUID();
        dtLoadOptimalPaths = DateTime.now();
        cpuWordEngine.findNextWord(Util.getApplicationManager(gameActivity).getAsyncTaskHandler(),
                getPlayedWords(),
                gameActivity.getLevelManager().getLayoutModified(),
                getTargetXY(),
                getHudLetters(),
                gameActivity.getDifficulty(),
                gameActivity.getDifficulty().getMinFrequencyWord(Util.getApplicationManager(gameActivity).getLanguage()));
    }

    public void AutomateTurn(){
        long msPlayWordIn = 0;
        long msSinceLoadOptimalPaths = DateTime.now().minus(dtLoadOptimalPaths.getMillis()).getMillis();
        if(msSinceLoadOptimalPaths < MS_MIN_CPU_THINKING_TIME){
            msPlayWordIn = (MS_MIN_CPU_THINKING_TIME - msSinceLoadOptimalPaths) - MS_MIN_DELAY_CPU_TURN;
        }
        final long msDelay = msPlayWordIn;
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(handlerPlayWordCPU == null){
                    handlerPlayWordCPU = new Handler();
                }
                handlerPlayWordCPU.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cpuBeginTurn(MS_MIN_DELAY_CPU_TURN, MS_MAX_DELAY_CPU_TURN);
                    }
                }, msDelay);
            }
        });
    }

    private void cpuBeginTurn(final long delay, final long maxDelay){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if(cpuPlayWord()){
                    return;
                }else {
                    if((maxDelay - delay) <= 0)
                        Util.showOkDialog(gameActivity, "CPU says: I can't think of a word! I forfeit!");
                    else
                        cpuBeginTurn(delay, maxDelay - delay);
                }
            }
        };
        handlerPlayWordCPU.postDelayed(r, delay);
    }

    private boolean cpuPlayWord(){
        if(cpuNextWord == null){
            return false;
        }else {
            gameActivity.getPlayerManager().getActivePlayer().addPlayedWord(cpuNextWord);
            gameActivity.getHUD().moveWordFromHudToBoard(cpuNextWord, TileSpeed.getTileSpeed(gameActivity.getApplicationManager()).getMs());
            cpuNextWord = null;
            return true;
        }
    }

}
