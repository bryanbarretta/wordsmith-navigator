package bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger;

import org.andengine.util.TextUtils;

import java.util.HashMap;

/**
 * Created by Bryan on 16/06/2018.
 */

public class CpuTurnLog {

    private final HashMap<LogType, String> mapFieldValues;

    public enum LogType{
        Layout("Layout"),
        PlayedWords("Played Words"),
        TargetXy("Target XY"),
        DepartureKeys("Departure Keys"),
        AStar("AStar"),
        AStarErrors("AStar Errors"),
        PfoEvaluation("PFO Evaluation"),
        Answer("Answer"),
        General("General");

        String displayName;

        LogType(String val){
            this.displayName = val;
        }

        @Override
        public String toString() {
            return displayName;
        }

        public static LogType get(String displayName){
            for(LogType l : LogType.values()){
                if(l.displayName.equals(displayName)){
                    return l;
                }
            }
            return null;
        }
    }

    public CpuTurnLog(){
        this(new HashMap<LogType, String>());
    }

    public CpuTurnLog(HashMap<LogType, String> mapFieldValues){
        this.mapFieldValues = mapFieldValues;
    }

    public HashMap<LogType, String> getMapFieldValues() {
        return mapFieldValues;
    }

    @Override
    public String toString() {
        return "[" + TextUtils.join(" | ", mapFieldValues.keySet()) + "]";
    }
}
