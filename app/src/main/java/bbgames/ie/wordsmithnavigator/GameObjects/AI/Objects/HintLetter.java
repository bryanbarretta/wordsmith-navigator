package bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.BoardUtils;
import bbgames.ie.wordsmithnavigator.Constants.Direction;

/**
 * Used for hints. Tells the game which letters of the hint words need to be played and where.
 * Contains advanced data about each words letters such as row/column and letter status.
 */

public class HintLetter {

    private static final float SCORE_TARGET_TILE = 10f;
    private int row;
    private int column;
    private Character letter;
    private boolean isLockedOnBoard;
    private boolean isFocusTile;
    private float desirabilityScore = -1;

    public HintLetter(int[] xy, Character letter, boolean isLockedOnBoard, boolean isFocusTile){
        this(xy[0], xy[1], letter, isLockedOnBoard, isFocusTile);
    }

    public HintLetter(int column, int row, Character letter, boolean isLockedOnBoard, boolean isFocusTile){
        this.row = row;
        this.column = column;
        this.letter = Character.toLowerCase(letter);
        this.isLockedOnBoard = isLockedOnBoard;
        this.isFocusTile = isFocusTile;
    }

    public HintLetter(HintLetter h){
        this(h.getColumn(), h.getRow(), h.getLetter(), h.isLockedOnBoard(), h.isFocusTile());
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Character getLetter() {
        return letter;
    }

    public boolean isLockedOnBoard() {
        return isLockedOnBoard;
    }

    public boolean isFocusTile(){
        return isFocusTile;
    }

    public void setLetter(Character letter){
        this.letter = Character.toLowerCase(letter);
    }

    public void setLockedOnBoard(boolean isLockedOnBoard){
        this.isLockedOnBoard = isLockedOnBoard;
    }

    public void setFocusTile(boolean isFocusTile) {
        this.isFocusTile = isFocusTile;
    }

    public int[] getXY() {
        return new int[]{column, row};
    }

    /**
     * Analyze the letter on the map, and the adjacent tiles.
     * A desirability score of ranges from 0.0f to 1.0f and is based on the 4 adjacent tiles (up to 0.25f for each side)
     * Example:
     * ...........-
     * .....-.....-
     * ...........-
     * .....MEET..-
     * .......^...-
     * ...........-
     * Evaluating the letter M: N: is a map tile (+0.10f), and this map tile has adjacent (E)map(+0.05f), (N)wall(+0.0f), (W)map(+0.05f)
     *                          S: is a map tile (+0.10f), and this map tile has 3 adjacent map tiles (+0.15f)
     *                          W: is a map tile (+0.10f), and this map tile has 3 adjacent map tiles (+0.15f)
     *                          E: is a non-playable tile (+0.0f)
     *   Total Desirability Score: 0.7f
     */
    public void setDesirabilityScore(ArrayList<String> layout) {
        if(TileCodes.getTargetTileCodes().contains(BoardUtils.getCharacterAtXY(layout, getXY()))){
            this.desirabilityScore = SCORE_TARGET_TILE;
            return;
        }
        float score = 0.0f;
        for(Direction direction : Direction.values()){
            int[] xyAdjacent = BoardUtils.getAdjacentTileXY(layout, getXY(), direction);
            Character tileAdjacent = BoardUtils.getCharacterAtXY(layout, xyAdjacent);
            int adjacentLetterCount = 0;
            if(LevelUtils.isPlayableTile(tileAdjacent)){
                score += 0.10f;
                Character c1 = BoardUtils.getAdjacentCharacter(layout, xyAdjacent, direction);
                Character c2 = BoardUtils.getAdjacentCharacter(layout, xyAdjacent, BoardUtils.getInvertedDirection_NorthOrWest(direction));
                Character c3 = BoardUtils.getAdjacentCharacter(layout, xyAdjacent, BoardUtils.getOppositeDirection(BoardUtils.getInvertedDirection_NorthOrWest(direction)));
                score += LevelUtils.isPlayableTile(c1) ? 0.05f : 0f;
                score += LevelUtils.isPlayableTile(c2) ? 0.05f : 0f;
                score += LevelUtils.isPlayableTile(c3) ? 0.05f : 0f;
                adjacentLetterCount += c1 == null ? 0 : (Character.isLetter(c1) ? 1 : 0);
                adjacentLetterCount += c2 == null ? 0 : (Character.isLetter(c2) ? 1 : 0);
                adjacentLetterCount += c3 == null ? 0 : (Character.isLetter(c3) ? 1 : 0);
                if(adjacentLetterCount > 1){
                    //Avoid letters touching 2 letters or more
                    this.desirabilityScore = -1f;
                    return;
                }
            }
        }
        this.desirabilityScore = score;
    }

    public void setDesirabilityScore(float desirabilityScore){
        this.desirabilityScore = desirabilityScore;
    }

    public float getDesirabilityScore(){
        if(desirabilityScore == SCORE_TARGET_TILE){
            return desirabilityScore;
        }
        if(desirabilityScore > 1f){
            desirabilityScore = 1f;
        }
        return desirabilityScore;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof HintLetter){
            HintLetter other = (HintLetter) o;
            return row == other.row &&
                    column == other.column &&
                    letter == other.letter &&
                    isLockedOnBoard == other.isLockedOnBoard &&
                    isFocusTile == other.isFocusTile;
        }else {
            return false;
        }
    }
}
