package bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.Star;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * This data is loaded from the database
 * Level data regarding the players progress on this level. Values change before/after a player plays the level.
 *  - Stars
 *  - Gems
 *  - IsLocked
 *  - IsComplete
 * Created by Bryan on 19/08/2018.
 */
public class LevelPlayerData implements Serializable{

    private static final String key_plays   = "plays";
    private static final String key_complete= "complete";
    private static final String key_unlocked= "unlocked";
    private static final String key_stars   = "star";
    private static final String key_diamonds= "diamond";

    private int plays = 0;
    private boolean isComplete = false;
    private boolean isUnlocked = false;
    private HashMap<Integer, Boolean> mapIdStarUnlocked = new HashMap<>();
    private HashMap<Integer, Boolean> mapIdDiamondUnlocked = new HashMap<>();

    /**
     * Blank canvas for creating a new level's player data
     * @param levelStaticData
     */
    public LevelPlayerData(LevelStaticData levelStaticData){
        this.plays = 0;
        this.isComplete = false;
        this.isUnlocked = true;
        for(Star s : levelStaticData.getStars().values()){
            mapIdStarUnlocked.put(s.getId(), false);
        }
        for(int i = 0; i < levelStaticData.getDiamondCount(); i++){
            mapIdDiamondUnlocked.put(i, false);
        }
    }

    /**
     * Used for loading a level's player data from the database
     * @param jsonString
     */
    public LevelPlayerData(String jsonString){
        try {
            initialise(jsonString);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void initialise(String jsonString) throws JSONException {
        JSONObject j = new JSONObject(jsonString);
        plays = j.getInt(key_plays);
        isComplete = j.getBoolean(key_complete);
        isUnlocked = j.getBoolean(key_unlocked);
        JSONArray jStars = j.getJSONArray(key_stars);
        if(jStars != null && jStars.length() > 0){
            for(int i = 0; i < jStars.length(); i++){
                JSONObject obj = new JSONObject(jStars.get(i).toString());
                int id = Integer.parseInt(obj.keys().next());
                boolean val = obj.getBoolean(String.valueOf(id));
                mapIdStarUnlocked.put(id, val);
            }
        }
        JSONArray jDiamonds = j.getJSONArray(key_diamonds);
        if(jDiamonds != null && jDiamonds.length() > 0){
            for(int i = 0; i < jDiamonds.length(); i++){
                JSONObject obj = new JSONObject(jDiamonds.get(i).toString());
                int id = Integer.parseInt(obj.keys().next());
                boolean val = obj.getBoolean(String.valueOf(id));
                mapIdDiamondUnlocked.put(id, val);
            }
        }
    }

    /**
     * Save the current state to database
     */
    public void save(Context context, int levelId) {
        Util.getApplicationManager(context).getDatabaseGateway().addUpdateLevelPlayerData(levelId, toJSONObject().toString());
    }

    public JSONObject toJSONObject() {
        JSONObject j = new JSONObject();
        JSONArray listStars = new JSONArray();
        for(int i = 0; i < mapIdStarUnlocked.size(); i++){
            listStars.put("{\"" + i + "\":" + mapIdStarUnlocked.get(i) + "}");
        }
        JSONArray listDiamonds = new JSONArray();
        for(int i = 0; i < mapIdDiamondUnlocked.size(); i++){
            listDiamonds.put("{\"" + i + "\":" + mapIdDiamondUnlocked.get(i) + "}");
        }
        try {
            j.put(key_plays, plays);
            j.put(key_complete, isComplete);
            j.put(key_unlocked, isUnlocked);
            j.put(key_stars, listStars);
            j.put(key_diamonds, listDiamonds);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return j;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public HashMap<Integer, Boolean> getMapIdStarUnlocked() {
        return mapIdStarUnlocked;
    }

    public HashMap<Integer, Boolean> getMapIdDiamondUnlocked() {
        return mapIdDiamondUnlocked;
    }

    public int getPlays() {
        return plays;
    }

    public void setPlays(int plays) {
        this.plays = plays;
    }

    public void updateDiamonds(SessionStatistics sessionStatistics) {
        for (int id : sessionStatistics.getDiamondIds()){
            mapIdDiamondUnlocked.put(id, true);
        }
    }

    public void updateStars(LevelStaticData levelStaticData, SessionStatistics sessionStatistics) {
        for (int i = 0; i < levelStaticData.getStars().size(); i++){
            Star s = levelStaticData.getStar(i);
            boolean isUnlocked = mapIdStarUnlocked.get(s.getId());
            if(isUnlocked){
                continue;
            }
            if(s.isCriteriaMet(sessionStatistics)){
                mapIdStarUnlocked.put(s.getId(), true);
            }else{
                //If star 1 conditions not met, star 2 and 3 cannot be unlocked. etc.
                break;
            }
        }
    }

    public void setCompleted(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public void incrementPlays() {
        this.plays++;
    }

}
