package bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger;

import org.joda.time.DateTime;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.PathFinderOption;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;

/**
 * Optional logger to record the logic behind every decision the CPU makes
 * Created by Bryan on 16/06/2018.
 */

public class CpuLogger implements CpuLoggerInterface{

    public static String LOG_DIVIDER = "\n-----------------------------------------\n";
    private GameActivity gameActivity;
    private CpuGameLogs gameLogs;
    private boolean loggingEnabled = false;

    public CpuLogger(GameActivity gameActivity){
        this.gameActivity = gameActivity;
        loggingEnabled = Boolean.parseBoolean(gameActivity.getApplicationManager()
                .getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.cpu_logs_enabled, "false"));
        this.gameLogs = new CpuGameLogs(generateGameName(gameActivity), loggingEnabled);
        if(loggingEnabled){
            enableWriteToDatabase();
        }
    }

    //region private

    private String generateGameName(GameActivity gameActivity) {
        return DateTime.now() + " - LV. " + gameActivity.getLevelManager().getLevelStaticData().getId();
    }

    private int getTurn(){
        return gameActivity.getPlayerManager().getPlayer(PlayerType.CPU).getPlayedWords().size();
    }

    //endregion

    public boolean enableWriteToDatabase(){
        return gameLogs.enableWriteToDatabase(gameActivity.getApplicationManager().getDatabaseGateway());
    }

    @Override
    public boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    public void logPlayedWords(ArrayList<HintWord> playedWords){
        gameLogs.log(getTurn(), CpuTurnLog.LogType.PlayedWords, android.text.TextUtils.join(", ", playedWords));
    }

    public void logTargetXy(int[] targetXY){
        gameLogs.log(getTurn(), CpuTurnLog.LogType.TargetXy, "[" + targetXY[0] + ", " + targetXY[1] + "]");
    }

    public void logDepartureKeys(ArrayList<String> departureKeys) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.DepartureKeys, android.text.TextUtils.join(", ", departureKeys));
    }

    public void logAStarData(ArrayList<PathFinderOption> pathFinderOptions) {
        String data = "Found " + pathFinderOptions.size() + " Path Finder Options.\n";
        for(PathFinderOption p : pathFinderOptions){
            data += "\n" + pathFinderOptions.indexOf(p) + ".\n" + p.getLogDataAStar();
        }
        gameLogs.log(getTurn(), CpuTurnLog.LogType.AStar, data);
    }

    public void logAStarErrors(String errorLogs) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.AStarErrors, errorLogs);
    }

    public void logPathFinderOptionEvaluation(String log) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.PfoEvaluation, log);
    }

    public void logAnswer(HintWord answer) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.Answer, answer.toString());
    }

    public void logLayout() {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.Layout, LevelUtils.getLevelAsString(gameActivity.getLevelManager().getLayoutModified()));
    }

    @Override
    public void log(String log) {
        gameLogs.log(getTurn(), CpuTurnLog.LogType.General, log);
    }
}
