package bbgames.ie.wordsmithnavigator.GameObjects.Tiles.Children;

import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.TileChildType;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Graphics.GraphicsManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.iTileChild;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

/**
 * Created by Bryan on 08/09/2018.
 */

public class PowerUp extends Sprite implements iTileChild {

    private final static float ANIM_SECONDS = 10f;
    private final static float ANIM_START = 0;
    private final static float ANIM_END = 360;
    private Integer[] colRow;
    private GameActivity gameActivity;
    private final PowerUpLink powerUpLink;

    public PowerUp(GameActivity gameActivity, PowerUpLink powerUpLink){
        super(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                gameActivity.getGraphicsManager().GetTextureRegion(getSprite(powerUpLink.getType())),
                gameActivity.getVertexBufferObjectManager());
        this.powerUpLink = powerUpLink;
        this.gameActivity = gameActivity;

        LoopEntityModifier mod = new LoopEntityModifier(new RotationModifier(ANIM_SECONDS, ANIM_START, ANIM_END));
        this.registerEntityModifier(mod);

        String valueText = null;
        switch (powerUpLink.getType()){
            case POINTS:
                valueText = powerUpLink.getValue() + "x"; break;
            case MOVE:
            case TIME:
                valueText = String.valueOf(powerUpLink.getValue());
        }
        LoopEntityModifier modRev = new LoopEntityModifier(new RotationModifier(ANIM_SECONDS, ANIM_END, ANIM_START));
        if(valueText != null) {
            Text txtValue = new Text(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                    gameActivity.getGraphicsManager().getFont(GraphicsManager.FontKeys.litsans, powerUpLink.getType().getColor()),
                    valueText,
                    gameActivity.getVertexBufferObjectManager());
            txtValue.registerEntityModifier(modRev);
            attachChild(txtValue);
        }else {
            Sprite sprite = new Sprite(Constant.TILE_SIZE / 2, Constant.TILE_SIZE / 2,
                    gameActivity.getGraphicsManager().GetTextureRegion(GraphicsManager.TextureKeys.Shuffle),
                    gameActivity.getVertexBufferObjectManager());
            sprite.registerEntityModifier(modRev);
            attachChild(sprite);
        }
    }

    public PowerUpLink getPowerUpLink() {
        return powerUpLink;
    }

    @Override
    public boolean isBlocker() {
        return false;
    }

    @Override
    public Character getTileCode() {
        return TileCodes.PowerUp.getCharacter();
    }

    public static GraphicsManager.TextureKeys getSprite(PowerUpLink.Type type){
        switch (type){
            default:
            case POINTS:
                return GraphicsManager.TextureKeys.BonusPoints;
            case MOVE:
                return GraphicsManager.TextureKeys.BonusMoves;
            case LETTER_SACK_JUMBLE:
                return GraphicsManager.TextureKeys.BonusSack;
            case TIME:
                return GraphicsManager.TextureKeys.BonusClock;
        }
    }

    public static GraphicsManager.TextureKeys getMiniSprite(PowerUpLink.Type type){
        switch (type){
            default:
            case POINTS:
                return GraphicsManager.TextureKeys.BonusPointsMini;
            case MOVE:
                return GraphicsManager.TextureKeys.BonusMovesMini;
            case LETTER_SACK_JUMBLE:
                return GraphicsManager.TextureKeys.BonusSackMini;
            case TIME:
                return GraphicsManager.TextureKeys.BonusClockMini;
        }
    }

    @Override
    public Integer[] getColumnRow() {
        if(colRow == null){
            int[] a = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(), new float[]{getX(), getY()});
            colRow = new Integer[]{a[0], a[1]};
        }
        return colRow;
    }

    @Override
    public TileChildType getTileChildType() {
        return TileChildType.PowerUps;
    }
}
