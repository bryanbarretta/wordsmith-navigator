package bbgames.ie.wordsmithnavigator.Files;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Manages reading and writing files from/to internal/external storage for this app
 * Created by Bryan on 16/09/2018.
 */

public class FileManager {

    private final String LOGTAG = getClass().getSimpleName();
    private final static int COMPRESSION_QUALITY = 100;
    private Context context;

    public FileManager(Context context) {
        this.context = context;
    }

    /**
     * Save a bitmap to the device. Note the bitmap is not recycled.
     * @param bitmap that will be saved
     * @param name of the saved file (extension not necessary)
     * @param isPng true? file save as png. false? file saved as bitmap
     * @param dir select relevant enum value to save to that directory
     * @param subDirectoryId this is the ID of the theme e.g. 4 would save to wordnav/hud/4/file.png
     * @return true if the save operation was successful
     */
    public boolean save(Bitmap bitmap, String name, boolean isPng, FileDir dir, int subDirectoryId){
        String extension = isPng ? ".png" : ".jpg";
        if(!name.toLowerCase().endsWith(extension)){
            name += extension;
        }
        String path = dir.getAbsolutePath(context);
        if(!(path.endsWith(File.separator + subDirectoryId) || path.endsWith(File.separator + subDirectoryId + File.separator))){
            path += File.separator + subDirectoryId;
        }
        File f = new File(path, name);
        if(f.exists()){
            f.delete();
        }
        if(!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY, out);
            out.flush();
            out.close();
            Log.d(LOGTAG, "File saved successfully: " + f.getAbsolutePath() + " [" + getFileSizeKB(f) + " kb]");
            return true;
        } catch (Exception e) {
            Log.w(LOGTAG, e.getMessage());
            return false;
        }
    }

    /**
     * Load a bitmap from the device.
     * @param name of the saved file (extension not necessary)
     * @param isPng true? will search for file ending with '.png'. false? ending with '.jpg'. null? ending with anything
     * @param dir select relevant enum value to load from that directory
     * @param subDirectoryId this is the ID of the theme e.g. 4 would load from wordnav/hud/4/file.png
     * @return null if the bitmap does not exist. Otherwise return
     */
    public Bitmap getBitmap(String name, Boolean isPng, FileDir dir, int subDirectoryId){
        File file = exists(name, isPng, dir, subDirectoryId);
        if(file == null){
            Log.w(LOGTAG, "Unable to find file: " + dir.getAbsolutePath(context) + "/" + subDirectoryId + "/" + name);
            return null;
        }
        return getBitmap(file);
    }

    public Bitmap getBitmap(File fileBitmap){
        if(fileBitmap == null){
            Log.w(LOGTAG, "Unable to find file!");
            return null;
        }
        InputStream is;
        try {
            is = new FileInputStream(fileBitmap);
            Bitmap output = BitmapFactory.decodeStream(is);
            is.close();
            Log.d(LOGTAG, "File found: " + fileBitmap.getAbsolutePath() + " [" + getFileSizeKB(fileBitmap) + " kb]");
            return output;
        } catch (Exception e) {
            Log.w(LOGTAG, "Unable to find file " + fileBitmap.getName() + ": " + e.getMessage());
            return null;
        }
    }

    /**
     * Check if a file exists and return it if it does
     * @param name of the saved file (extension not necessary)
     * @param isPng true? will search for file ending with '.png'. false? ending with '.jpg'. null? ending with anything
     * @param dir select relevant enum value to load from that directory
     * @param subDirectoryId this is the ID of the theme e.g. 4 would load from wordnav/hud/4/file.png
     * @return the file if it exists. Otherwise return null
     */
    public File exists(String name, Boolean isPng, FileDir dir, int subDirectoryId){
        String extension = isPng == null ? "" : isPng == true ? ".png" : ".jpg";
        if(!extension.isEmpty() && !name.toLowerCase().endsWith(extension)){
            name += extension;
        }
        String path = dir.getAbsolutePath(context);
        if(!(path.endsWith(File.separator + subDirectoryId) || path.endsWith(File.separator + subDirectoryId + File.separator))){
            path += File.separator + subDirectoryId;
        }
        File file = null;
        if(!extension.isEmpty()){
            if(new File(path, name).exists()) {
                file = new File(path, name);
            }
        }else {
            File folder = new File(path);
            if(!folder.exists()){
                return null;
            }
            File[] children = folder.listFiles();
            if(children == null || children.length == 0){
                return null;
            }
            for(File f : children){
                if(f.getName().toLowerCase().startsWith(name.toLowerCase())){
                    file = f;
                    break;
                }
            }
        }
        return file;
    }

    /**
     * @param file
     * @return the size of the file in kilobytes
     */
    private long getFileSizeKB(File file){
        if(file == null || !file.exists()){
            return 0;
        }
        return file.length() / 1024;
    }
}
