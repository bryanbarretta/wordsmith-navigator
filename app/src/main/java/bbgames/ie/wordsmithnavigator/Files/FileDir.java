package bbgames.ie.wordsmithnavigator.Files;

import android.content.Context;

import java.io.File;

/**
 * Created by Bryan on 16/09/2018.
 */

public enum  FileDir {

    HUD("hud"),
    Letters("letters"),
    Theme("theme");

    public static final String appFolder = "wordnav";

    private String folder;

    FileDir(String folder){
        this.folder = folder;
    }

    public String getFolderName(){
        return folder;
    }

    public String getAbsolutePath(Context context){
        return context.getFilesDir() + File.separator + appFolder + File.separator + folder;
    }

}
