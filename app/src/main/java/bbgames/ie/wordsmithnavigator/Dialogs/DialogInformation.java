package bbgames.ie.wordsmithnavigator.Dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import bbgames.ie.wordsmithnavigator.Controls.ViewInformationPage;
import bbgames.ie.wordsmithnavigator.Dialogs.BaseDialogs.BaseDialog;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Dialog with previous/next buttons to scroll through pages argument. Last page has a finish button.
 */
public abstract class DialogInformation extends BaseDialog {

    private ArrayList<ViewInformationPage> pages;
    private int iActivePage = 0;

    public DialogInformation(Context context,
                             String title,
                             ArrayList<ViewInformationPage> pages) {
        super(context,
                null,
                title,
                createContainer(context),
                null,
                context.getString(R.string.next),
                null,
                context.getString(R.string.back),
                null,
                null,
                null);
        this.iActivePage = 0;
        this.pages = pages;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPage(iActivePage - 1);
            }
        });
        setPage(this.iActivePage);
    }

    private void setPage(int toPage){
        iActivePage = toPage;
        LinearLayout container = (LinearLayout)viewBody;
        container.setWeightSum(10);
        container.removeAllViews();
        container.addView(pages.get(iActivePage).getImage());
        container.addView(pages.get(iActivePage).getScrollableText());
        refreshDialog();
    }

    /**
     * Refreshes UI
     */
    private void refreshDialog(){
        boolean isFirstPage = iActivePage == 0;
        final boolean isLastPage = iActivePage == pages.size() - 1;
        btnNo.setVisibility(isFirstPage ? View.INVISIBLE : View.VISIBLE);
        btnYes.setText(getContext().getString(isLastPage ? R.string.done : R.string.next));
        btnYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(isLastPage){
                    DialogInformation.this.dismiss();
                    onDialogClosed();
                }else {
                    setPage(iActivePage + 1);
                }
            }
        });
    }

    private static LinearLayout createContainer(Context ctx){
        int p = Util.getDimensValueInPixels(ctx, R.dimen.padding_8dp_border);
        LinearLayout container = new LinearLayout(ctx);
        container.setOrientation(LinearLayout.HORIZONTAL);
        container.setPadding(p, p, p, p);
        container.setWeightSum(2);
        return container;
    }

    public abstract void onDialogClosed();
}
