package bbgames.ie.wordsmithnavigator.Dialogs;

import android.app.ProgressDialog;
import android.content.Context;

import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 18/08/2018.
 */

public class DialogLoading extends ProgressDialog {

    public DialogLoading(Context context, String message) {
        super(context);
        this.setTitle(context.getString(R.string.loading));
        this.setMessage(message);
        this.setIcon(R.drawable.ic_launcher);
        this.setCancelable(false);
    }

}
