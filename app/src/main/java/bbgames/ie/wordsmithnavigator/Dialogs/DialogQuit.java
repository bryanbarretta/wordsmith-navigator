package bbgames.ie.wordsmithnavigator.Dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;

import bbgames.ie.wordsmithnavigator.Dialogs.BaseDialogs.BaseDialog;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;

/**
 * Created by Bryan on 06/08/2018.
 */

public class DialogQuit extends BaseDialog {

    public DialogQuit(@NonNull final Activity activity) {
        this(activity, null);
    }

    public DialogQuit(@NonNull final Activity activity, Runnable onNoRunnable) {
        super(activity,
                null,
                activity.getString(R.string.quit),
                null,
                activity.getString(R.string.do_you_want_to_quit),
                activity.getString(R.string.quit),
                new Runnable() {
                    @Override
                    public void run() {
                        activity.finish();
                    }
                },
                activity.getString(R.string.cancel),
                onNoRunnable,
                null,
                null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GraphicUtils.buttonColor(btnYes, getContext().getResources().getColor(R.color.red_400));
        btnYes.setTextColor(getContext().getResources().getColor(R.color.white));
    }
}
