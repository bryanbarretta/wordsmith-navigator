package bbgames.ie.wordsmithnavigator.Dialogs.BaseDialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 16/09/2018.
 */

public class BaseDialog extends Dialog {

    private RelativeLayout header, body;
    private LinearLayout footer;
    private TextView tvTitle;
    private ImageView ivTitle;
    private Typeface tfHeader, tfBody;
    private int colorWhite, colorBlack;
    private Integer imgTitle;
    private String txtTitle;
    private String txtBody;
    private String txtYes;
    private Runnable runnableYes;
    private String txtNo;
    private Runnable runnableNo;
    private String txtNeutral;
    private Runnable runnableNeutral;

    protected View viewBody;
    protected Button btnNo, btnNeutral, btnYes;

    public BaseDialog(@NonNull Context context,
                      Integer imgTitle, //If imgTitle and txtTitle are both null, header will be invisible
                      String txtTitle,
                      View viewBody,    //Will be displayed as the body
                      String txtBody,   //Will be displayed as the body if viewBody is null
                      String txtYes,    //If a buttons String and nullable are null, it will not be shown
                      Runnable runnableYes,
                      String txtNo,
                      Runnable runnableNo,
                      String txtNeutral,
                      Runnable runnableNeutral) {
        super(context);
        this.imgTitle = imgTitle;
        this.txtTitle = txtTitle;
        this.viewBody = viewBody;
        this.txtBody = txtBody;
        this.txtYes = txtYes;
        this.runnableYes = runnableYes;
        this.txtNo = txtNo;
        this.runnableNo = runnableNo;
        this.txtNeutral = txtNeutral;
        this.runnableNeutral = runnableNeutral;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_template);
        init();
    }

    @Override
    public void show() {
        super.show();
        int fraction = viewBody == null ? 6 : 9;
        int maxWidth = Util.getApplicationManager(getContext()).getScreenWidthPixels();
        int maxHeight = Util.getApplicationManager(getContext()).getScreenHeightPixels();
        getWindow().setLayout((maxWidth / 10) * fraction, (maxHeight / 10) * fraction);
    }

    private void init(){
        colorBlack = getContext().getResources().getColor(R.color.primary_darker);
        colorWhite = getContext().getResources().getColor(R.color.tertiary_brighter);
        tfHeader = Fonts.defaultTitleDialog(getContext());
        tfBody = Fonts.defaultBodyDialog(getContext());

        header = (RelativeLayout) findViewById(R.id.header);
        tvTitle = (TextView) findViewById(R.id.txtTitle);
        ivTitle = (ImageView) findViewById(R.id.imgTitle);
        body = (RelativeLayout) findViewById(R.id.body);
        footer = (LinearLayout) findViewById(R.id.footer);
        btnNo = (Button) findViewById(R.id.btnNo);
        btnNeutral = (Button) findViewById(R.id.btnNeutral);
        btnYes = (Button) findViewById(R.id.btnYes);

        //Header
        if(imgTitle == null && txtTitle == null){
            header.setVisibility(View.GONE);
            findViewById(R.id.div1).setVisibility(View.GONE);
        }else {
            if(imgTitle != null){
                ivTitle.setImageResource(imgTitle);
            }
            if(txtTitle != null){
                tvTitle.setTypeface(tfHeader);
                tvTitle.setText(txtTitle);
            }
        }

        //Body
        if(viewBody != null){
            body.addView(viewBody, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }else if(txtBody != null){
            TextView txt = new TextView(getContext());
            txt.setText(txtBody);
            txt.setTextColor(colorWhite);
            txt.setTypeface(tfBody);
            txt.setTextSize(Util.getDimensValueInScaledDensityPixels(getContext(), R.dimen.font_normal));
            body.addView(txt);
        }

        //Footer
        initFooterButton(btnYes, txtYes, runnableYes);
        initFooterButton(btnNo, txtNo, runnableNo);
        initFooterButton(btnNeutral, txtNeutral, runnableNeutral);
        int numButtons = (txtYes != null ? 1 : 0) + (txtNo != null ? 1 : 0) + (txtNeutral != null ? 1 : 0);
        footer.setWeightSum(numButtons == 3 ? 3 : 2);
    }

    private void initFooterButton(Button btn, String txt, final Runnable runnable){
        if(txt != null){
            btn.setText(txt);
            GraphicUtils.buttonColor(btn, colorWhite);
            btn.setTextColor(colorBlack);
            btn.setTypeface(tfHeader);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(runnable != null) {
                        runnable.run();
                    }
                    BaseDialog.this.dismiss();
                }
            });
        }else {
            btn.setVisibility(View.GONE);
        }
    }
}
