package bbgames.ie.wordsmithnavigator.Dialogs;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Dialogs.BaseDialogs.BaseDialog;
import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.Star;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.LevelMapGenerator;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelStaticData;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 06/08/2018.
 */

public class DialogLevelDetails extends BaseDialog {

    public DialogLevelDetails(@NonNull final Activity activity,
                              @NonNull final LevelStaticData levelStaticData,
                              @NonNull final Runnable onYesRunnable) {
        super(activity,
                null,
                String.format(activity.getString(R.string.level_n), levelStaticData.getId() + ""),
                generateBody(activity, levelStaticData, activity instanceof GameActivity),
                null,
                activity.getString((activity instanceof GameActivity) ? R.string.start : R.string.play),
                onYesRunnable,
                activity.getString((activity instanceof GameActivity) ? R.string.quit : R.string.cancel),
                new Runnable() {
                    @Override
                    public void run() {
                        if(activity instanceof GameActivity){
                            activity.finish();
                        }
                    }
                },
                null,
                null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GraphicUtils.buttonColor(btnYes, getContext().getResources().getColor(R.color.green_400));
        btnYes.setTextColor(getContext().getResources().getColor(R.color.white));
        if(getContext() instanceof GameActivity){
            GraphicUtils.buttonColor(btnNo, getContext().getResources().getColor(R.color.red_400));
            btnNo.setTextColor(getContext().getResources().getColor(R.color.white));
        }
    }

    public static View generateBody(Activity activity, LevelStaticData levelStaticData, boolean isGameActivity){
        View background = Util.inflateView(activity, R.layout.template_img_with_llcontainer);
        Typeface tf = Fonts.defaultBodyDialog(activity);
        //Left
        ImageView imgMap = (ImageView) background.findViewById(R.id.imgMap);
        new LevelMapGenerator(levelStaticData).GenerateMap(imgMap, null, true);
        //Right
        LinearLayout container = (LinearLayout) background.findViewById(R.id.container);
        addRules(container, activity, levelStaticData, tf, isGameActivity);
        Star.generateStarDetailsView(container, activity, levelStaticData.getId(), tf);
        return background;
    }

    private static void addRules(LinearLayout container, Activity activity, LevelStaticData levelStaticData, Typeface tf, boolean isGameActivity){
        if(isGameActivity && ((GameActivity)activity).getPlayerManager().getPlayers().size() > 1){
            container.addView(generateRuleTextView(activity, tf, String.format(activity.getString(
                    R.string.goes_first),
                    ((GameActivity)activity).getPlayerManager().getActivePlayer().getName())));
        }else {
            if (levelStaticData.getPlayerCount() == 1) {
                container.addView(generateRuleTextView(activity, tf, activity.getString(R.string.single_player)));
            } else {
                container.addView(generateRuleTextView(activity, tf, String.format(activity.getString(R.string.n_players), levelStaticData.getPlayerCount())));
            }
        }
        Integer secondsPerTurn = levelStaticData.getClockSecondsPerTurn();
        if(secondsPerTurn != null){
            container.addView(generateRuleTextView(activity, tf, String.format(activity.getString(R.string.seconds_per_turn), secondsPerTurn)));
        }else {
            container.addView(generateRuleTextView(activity, tf, activity.getString(R.string.unlimitied_time)));
        }
        if(!levelStaticData.getExcludedLetters().isEmpty()){
            container.addView(generateRuleTextView(activity, tf, String.format(activity.getString(R.string.excluded_letters), levelStaticData.getExcludedLetters().toUpperCase())));
        }
    }

    private static TextView generateRuleTextView(Activity activity, Typeface tf, String text){
        TextView t = new TextView(activity);
        int p = Util.getDimensValueInPixels(activity, R.dimen.padding_4dp_border);
        t.setPadding(p, p, p, p);
        t.setTypeface(tf);
        t.setTextColor(activity.getResources().getColor(R.color.tertiary_brighter));
        t.setText("\u2022 " + text);
        t.setTextSize(Util.getDimensValueInScaledDensityPixels(activity, R.dimen.font_normal));
        return t;
    }

}
