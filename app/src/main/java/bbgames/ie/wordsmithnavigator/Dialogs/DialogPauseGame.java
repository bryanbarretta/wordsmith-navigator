package bbgames.ie.wordsmithnavigator.Dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.TileSpeed;
import bbgames.ie.wordsmithnavigator.Controls.AdapterListViewHints;
import bbgames.ie.wordsmithnavigator.Dialogs.BaseDialogs.BaseDialog;
import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Word.HintManager;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.LevelMapGenerator;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Created by Bryan on 07/08/2018.
 */

public class DialogPauseGame extends BaseDialog {

    public DialogPauseGame(final GameActivity gameActivity) {
        super(gameActivity,
                null,
                null,
                generateBody(gameActivity),
                null,
                gameActivity.getString(R.string.resume),
                null,
                gameActivity.getString(R.string.quit),
                new Runnable() {
                    @Override
                    public void run() {
                        new DialogQuit(gameActivity, new Runnable() {
                            @Override
                            public void run() {
                                new DialogPauseGame(gameActivity).show();
                            }
                        }).show();
                    }
                },
                gameActivity.getString(R.string.play_hint),
                null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnNeutral.setEnabled(false);
        GraphicUtils.buttonColor(btnNo, getContext().getResources().getColor(R.color.red_400));
        btnNo.setTextColor(getContext().getResources().getColor(R.color.white));
    }

    public Button getNeutralButton(){
        return btnNeutral;
    }

    private static View generateBody(final GameActivity gameActivity){
        final View background = Util.inflateView(gameActivity, R.layout.popup_pause_game);
        Typeface tf = Fonts.defaultFun(gameActivity);

        //Left hand pane...
        final ImageView imgMap = (ImageView) background.findViewById(R.id.imgMap);
        LevelMapGenerator lmg = new LevelMapGenerator(gameActivity.getLevelManager());
        if(gameActivity.getLevelManager().getFocusTile() != null &&
           gameActivity.getLevelManager().getFocusTile().getDirection() != null){
            lmg.setArrowDirection(gameActivity.getLevelManager().getFocusTile().getDirection());
        }
        lmg.GenerateMap(imgMap, null, true);

        //Right hand pane...
        final LinearLayout container = (LinearLayout) background.findViewById(R.id.container);

        //Initial hint button
        if(gameActivity.getLevelManager().getLevelStaticData().getHardcodedLetters().isEmpty()) {
            View vRowHintButton = Util.inflateView(gameActivity, R.layout.row_hint_button);
            Button btnHint = (Button) vRowHintButton.findViewById(R.id.btnHint);
            btnHint.setTypeface(tf);
            btnHint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    container.removeAllViews();
                    buttonHintClicked(gameActivity, imgMap, container);
                }
            });
            TextView txtHint = (TextView) vRowHintButton.findViewById(R.id.txtHintInfo);
            txtHint.setText(String.format(gameActivity.getString(R.string.hints_remaining), gameActivity.getSettings().getHintCount() + ""));
            txtHint.setTypeface(tf);
            container.addView(vRowHintButton);
        }

        return background;
    }

    private static void buttonHintClicked(final GameActivity gameActivity, final ImageView imgMap, LinearLayout container){
        if(gameActivity.getLevelManager().getFocusTile() == null){
            gameActivity.showToast(gameActivity.getString(R.string.tapMapTileHint));
            return;
        }
        if(gameActivity.getSettings().getHintCount() < 1){
            gameActivity.showToast("TODO: need to get more hints");
            //TODO:
            return;
        }
        final ListView lvValidWords;
        final AdapterListViewHints adapterListViewHints;
        final LevelMapGenerator levelMapGenerator = new LevelMapGenerator(gameActivity.getLevelManager());
        if(gameActivity.getLevelManager().getFocusTile() != null &&
           gameActivity.getLevelManager().getFocusTile().getDirection() != null){
            levelMapGenerator.setArrowDirection(gameActivity.getLevelManager().getFocusTile().getDirection());
        }

        gameActivity.getSettings().decrementHintCount();
        RelativeLayout mapHintsContainer = (RelativeLayout) ((LayoutInflater)gameActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.body_map_hints, null);
        adapterListViewHints = new AdapterListViewHints(gameActivity);
        lvValidWords = (ListView) mapHintsContainer.findViewById(R.id.popup_listview);
        lvValidWords.setAdapter(adapterListViewHints);
        lvValidWords.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                adapterListViewHints.setSelectedIndex(i);
                getButtonHint(gameActivity).setText(gameActivity.getString(R.string.play) + " " + adapterListViewHints.getSelectedWord().toString());
                levelMapGenerator.GenerateMap(imgMap, adapterListViewHints.getSelectedWord(), true);
            }
        });

        getButtonHint(gameActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playHintWord(gameActivity, adapterListViewHints.getSelectedWord());
            }
        });

        container.addView(mapHintsContainer);

        HintManager hintManager = new HintManager(gameActivity.getCpuLogicLogger()) {
            @Override
            public void onWordsFound(ArrayList<HintWord> hints) {
                onHintListReady(gameActivity, adapterListViewHints, levelMapGenerator, imgMap, hints);
            }
        };
        hintManager.findWordsAsynchronously(gameActivity);
        startLoadingHintList(gameActivity);
    }

    private static void playHintWord(final GameActivity gameActivity, final HintWord word){
        if(gameActivity.getDialogPauseGame() != null){
            gameActivity.getDialogPauseGame().dismiss();
        }
        gameActivity.lockScreen();
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gameActivity.getHUD().moveWordFromHudToBoard(word,
                        TileSpeed.getTileSpeed(gameActivity.getApplicationManager()).getMs());
            }
        });
    }

    private static final int ID_SPINNER_VIEW = 1;

    private static void startLoadingHintList(GameActivity gameActivity){
        TextView txtWordsFound = (TextView)gameActivity.getDialogPauseGame().findViewById(R.id.txtFoundWords);
        txtWordsFound.setVisibility(View.VISIBLE);
        txtWordsFound.setText(gameActivity.getString(R.string.searching));
        ProgressBar progressBar = new ProgressBar(gameActivity, null, android.R.attr.progressBarStyleLarge);
        progressBar.setId(ID_SPINNER_VIEW);
        progressBar.setIndeterminate(true);
        progressBar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ((LinearLayout)gameActivity.getDialogPauseGame().findViewById(R.id.container)).addView(progressBar);
    }

    private static void onHintListReady(GameActivity gameActivity,
                                        AdapterListViewHints adapterListViewHints,
                                        LevelMapGenerator levelMapGenerator,
                                        ImageView imgMap,
                                        ArrayList<HintWord> validWords){

        TextView txtFoundWords = (TextView)gameActivity.getDialogPauseGame().findViewById(R.id.txtFoundWords);
        txtFoundWords.setTextColor(gameActivity.getResources().getColor(R.color.green_200));
        txtFoundWords.setVisibility(View.VISIBLE);
        txtFoundWords.setText(
                String.format(gameActivity.getString(R.string.x_words_found), validWords.size()+"")
        );

        adapterListViewHints.setWords(validWords);

        if(adapterListViewHints.getCount() == 0){
            Toast.makeText(gameActivity, "TODO: No hint words found!", Toast.LENGTH_SHORT).show();
        }else {
            adapterListViewHints.setSelectedIndex(0);
            levelMapGenerator.GenerateMap(imgMap, adapterListViewHints.getSelectedWord(), true);
            getButtonHint(gameActivity).setEnabled(true);
            getButtonHint(gameActivity).setText(gameActivity.getString(R.string.play) + " " + adapterListViewHints.getSelectedWord());
        }
    }

    private static Button getButtonHint(GameActivity gameActivity){
        return gameActivity.getDialogPauseGame().getNeutralButton();
    }

}
