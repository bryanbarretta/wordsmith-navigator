package bbgames.ie.wordsmithnavigator.Dialogs;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Controls.ViewInformationPage;

/**
 * Created by Bryan on 14/08/2018.
 */

public class DialogManager {

    private static final String prefix = "help";
    private static final String div = "_";

    /**
     * Show all the relevant pre game dialogs in order (Skips dialogs not necessary)
     */
    public static void showLevelDialogs(final GameActivity gameActivity){
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDialogInformation(gameActivity, new Runnable() {
                    @Override
                    public void run() {
                        showDialogFlipCoin(gameActivity, new Runnable() {
                            @Override
                            public void run() {
                                if(gameActivity.getPlayerManager().getActivePlayer() == null){
                                    gameActivity.getPlayerManager().setActivePlayer(0);
                                }
                                showDialogLevelDetails(gameActivity);
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Help dialog giving level info if the level has any
     */
    private static void showDialogInformation(final GameActivity gameActivity, final Runnable onFinishedRunnable){
        if(Boolean.parseBoolean(gameActivity.getApplicationManager().getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.help_dialogs_enabled, "true")) == false){
            onFinishedRunnable.run();
            return;
        }
        String full_prefix = prefix + div + gameActivity.getLevelManager().getLevelStaticData().getId() + div;
        ArrayList<ViewInformationPage> pages = new ArrayList<>();
        int i = 1;
        while (true){
            String res_name = full_prefix + i;
            int str_id = gameActivity.getResources().getIdentifier(res_name, "string", gameActivity.getPackageName());
            int img_id = gameActivity.getResources().getIdentifier(res_name, "drawable", gameActivity.getPackageName());
            if(str_id > 0 && img_id > 0){
                pages.add(new ViewInformationPage(gameActivity, img_id, str_id));
            }else {
                break;
            }
            i++;
        }
        if(pages.isEmpty()){
            onFinishedRunnable.run();
            return;
        }
        int title_id = gameActivity.getResources().getIdentifier(full_prefix + "title", "string", gameActivity.getPackageName());
        new DialogInformation(gameActivity, gameActivity.getString(title_id), pages) {
            @Override
            public void onDialogClosed() {
                onFinishedRunnable.run();
            }
        }.show();
    }

    /**
     * The flip coin dialog to decide which player goes first
     */
    private static void showDialogFlipCoin(final GameActivity gameActivity, final Runnable onFinishedRunnable){
        if(gameActivity.getPlayerManager().getPlayers().size() > 1){
            new DialogFlipCoin(gameActivity) {
                @Override
                public void onFlipFinished() {
                    dialog.dismiss();
                    onFinishedRunnable.run();
                }
            }.show();
        }else {
            onFinishedRunnable.run();
        }
    }

    /**
     * The final dialog, showing the map, level rules, stars, and start button
     */
    private static void showDialogLevelDetails(final GameActivity gameActivity){
        DialogLevelDetails dialogLevelDetails = new DialogLevelDetails(gameActivity, gameActivity.getLevelManager().getLevelStaticData(), new Runnable() {
            @Override
            public void run() {
                gameActivity.getPlayerManager().startGame();
            }
        });
        dialogLevelDetails.setCancelable(false);
        dialogLevelDetails.show();
    }

}
