package bbgames.ie.wordsmithnavigator.Dialogs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.SkinHUD;
import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * Dialog that appears at the start of the GameActivity.
 * Displays level title, map, rules and 2 buttons (Start | Quit)
 * If there is 2 players, this dialog first shows a 'spin the coin to choose who goes first' view before the above view.
 * Created by Bryan on 11/05/2018.
 */

public abstract class DialogFlipCoin extends AlertDialog.Builder{

    private GameActivity gameActivity;
    protected AlertDialog dialog;
    private Runnable runnableFlipCoinAnimation;

    private LinearLayout body;

    private boolean player1GoesFirst = true;

    public DialogFlipCoin(GameActivity gameActivity) {
        super(gameActivity, AlertDialog.THEME_HOLO_DARK);
        this.gameActivity = gameActivity;
        generateBody(gameActivity.getPlayerManager().getPlayers().get(0).getThemePlayer(),
                gameActivity.getPlayerManager().getPlayers().get(1).getThemePlayer());
        setTitle(R.string.flip_coin_first_go);
        setView(body);
    }

    private void generateBody(SkinHUD color1, SkinHUD color2){
        Typeface tf = Fonts.defaultFun(gameActivity);
        body = (LinearLayout) Util.inflateView(getContext(), R.layout.template_img_with_llcontainer);

        Drawable[] coins1 = color1.getCoinDrawables(getContext()); //Index 2 is the front facing frame
        Drawable[] coins2 = color2.getCoinDrawables(getContext()); //Index 2 is the front facing frame
        Drawable coinSide = gameActivity.getResources().getDrawable(R.drawable.coin_side);
        final Drawable[] spin1to2 = new Drawable[]{ coins1[3], coins1[4], coinSide, coins2[0], coins2[1], coins2[2]}; //ends coin2 face up
        final Drawable[] spin2to1 = new Drawable[]{ coins2[3], coins2[4], coinSide, coins1[0], coins1[1], coins1[2]}; //ends coin1 face up

        //Coin
        final int animLength = 30;
        ImageView img = (ImageView) body.findViewById(R.id.imgMap);
        final AnimationDrawable animCoin = new AnimationDrawable();
        animCoin.addFrame(coins1[2], animLength); //0
        animCoin.setOneShot(true);
        img.setImageDrawable(animCoin);

        //Avatars beside coin
        final LinearLayout llPlayerOne = (LinearLayout) Util.inflateView(getContext(), R.layout.row_coin);
        ImageView imgPlayerOne = (ImageView) llPlayerOne.findViewById(R.id.imgCoin);
        TextView txtPlayerOne = (TextView) llPlayerOne.findViewById(R.id.txtCoin);
        imgPlayerOne.setImageDrawable(coins1[2]);
        txtPlayerOne.setText(gameActivity.getPlayerManager().getPlayers().get(0).getName());
        txtPlayerOne.setTypeface(tf);
        ((LinearLayout)body.findViewById(R.id.container)).addView(llPlayerOne);

        final LinearLayout llPlayerTwo = (LinearLayout) Util.inflateView(getContext(), R.layout.row_coin);
        ImageView imgPlayerTwo = (ImageView) llPlayerTwo.findViewById(R.id.imgCoin);
        TextView txtPlayerTwo = (TextView) llPlayerTwo.findViewById(R.id.txtCoin);
        imgPlayerTwo.setImageDrawable(coins2[2]);
        txtPlayerTwo.setText(gameActivity.getPlayerManager().getPlayers().get(1).getName());
        txtPlayerTwo.setTypeface(tf);
        ((LinearLayout)body.findViewById(R.id.container)).addView(llPlayerTwo);

        runnableFlipCoinAnimation = new Runnable() {
            boolean isFlipping = false;
            @Override
            public void run() {
                if(isFlipping){
                    return;
                }
                isFlipping = true;
                player1GoesFirst = new Random().nextBoolean();
                int numSpins = 3 + new Random().nextInt(3);
                for(int i = 0; i < numSpins; i++){
                    for(int f = 0; f < spin1to2.length; f++){
                        animCoin.addFrame(spin1to2[f], animLength);
                    }
                    for(int f = 0; f < spin2to1.length; f++){
                        animCoin.addFrame(spin2to1[f], animLength);
                    }
                }
                //now player 1 is face up
                if(!player1GoesFirst){
                    for(int f = 0; f < spin1to2.length; f++){
                        animCoin.addFrame(spin1to2[f], animLength);
                    }
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Util.getApplicationManager(gameActivity).getSoundManager().playSound(R.raw.coin_drop);
                        AnimatorSet animationSet = new AnimatorSet();
                        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(player1GoesFirst ? llPlayerTwo : llPlayerOne, "alpha",  1f, 0f);
                        fadeOut.setDuration(2000);
                        animationSet.play(fadeOut);
                        animationSet.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                gameActivity.getPlayerManager().setActivePlayer(gameActivity.getPlayerManager().getPlayers().get(player1GoesFirst ? 0 : 1).getId());
                                gameActivity.getCamera().setCenterDirect(gameActivity.getPlayerManager().getActivePlayer().getMiddleLetterOfLastWordPlayed().getXY());
                                onFlipFinished();
                            }
                        });
                        animationSet.start();
                    }
                }, animCoin.getNumberOfFrames() * animLength);
                Util.getApplicationManager(gameActivity).getSoundManager().playSound(R.raw.coin_spin);
                animCoin.start();
            }
        };

        //Button
        setPositiveButton(R.string.flip_coin, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    public abstract void onFlipFinished();

    @Override
    public AlertDialog show() {
        dialog = super.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                runnableFlipCoinAnimation.run();
            }
        });
        int fontSize = Util.getDimensValueInScaledDensityPixels(getContext(), R.dimen.font_big);
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(fontSize);
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(Fonts.defaultSerious(getContext()), Typeface.BOLD);
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.secondary));
        return dialog;
    }

}
