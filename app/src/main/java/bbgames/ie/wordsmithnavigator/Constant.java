package bbgames.ie.wordsmithnavigator;

/**
 * Created by Bryan on 07/05/2016.
 */
public class Constant {

    public static final int CAMERA_1920 = 1920;
    public static final int CAMERA_1080 = 1080;

    public static final int TILE_SIZE = 190;
    public static final int TILE_POINTS_OFFSET = 40; //The distance from the center of the points text to the top right corner of the tile
    public static final int TILE_RACK_HEIGHT = 112;
    public static final int BORDER = 95;

    public static final int FONT_SMALL = 35;
    public static final int FONT_SMALL_MEDIUM_BRIDGE = 55;
    public static final int FONT_MEDIUM = 70;
    public static final int FONT_LARGE = 140;

    public static final int HUD_WIDTH = 450;
    public static final int HUD_CONTROLLER_BACKGROUND = 380; //Controller / 2 buttons (flips)
    public static final int HUD_CONTROLLER_BASE = 360;
    public static final int HUD_CONTROLLER_KNOB = 190;
    public static final int HUD_TEXT_SHADOW_HEIGHT = 120;
    public static final int HUD_TEXT_SHADOW_PADDING_BOTTOM = 20;
    public static final int HUD_DIAMOND_HEIGHT = 60;
    public static final int HUD_DIAMOND_WIDTH = 77;
    public static final int HUD_BUTTON_LONG_WIDTH = 400;
    public static final int HUD_BUTTON_WIDTH = 190;
    public static final int HUD_BUTTON_HEIGHT = 190;
    public static final int HUD_PADDING = 15;
    public static final int HUD_PADDING_BIG = 20;
    public static final int HUD_PADDING_SMALL = 10;
    public static final int HOURGLASS_WIDTH_HEIGHT = 245;
    public static final int HOURGLASS_SAND_WIDTH_HEIGHT = 7;
    public static final int LETTER_SACK = 140;
    public static final int SPEECH_BUBBLE_WIDTH = 730;
    public static final int SPEECH_BUBBLE_LEFT_PADDING = 11;
    public static final int SPEECH_BUBBLE_RIGHT_PADDING = 106;
    public static final int SPEECH_BUBBLE_CENTER_NATURAL_X = SPEECH_BUBBLE_LEFT_PADDING +
            ((SPEECH_BUBBLE_WIDTH - SPEECH_BUBBLE_RIGHT_PADDING) / 2);
    public static final int SPEECH_BUBBLE_HEIGHT = 179;

    public static final int ICON_STANDARD_128 = 128;
    public static final int ICON_96 = 96;
    public static final int ICON_SMALL_64 = 64;
    public static final int ICON_32 = 32;
    public static final int ICON_48 = 48;

    public static final int DIAMOND_BOARD_HEIGHT = 126;
    public static final int DIAMOND_BOARD_WIDTH = 180;

    public static final int DETONATOR_HEIGHT_WIDTH = 180;
    public static final int BRIDGE_HEIGHT = 140;

    public static final int COLOR_GREEN_LIGHT = 0xFF97E779;
    public static final int COLOR_BLUE_LIGHT = 0xFFDCF6FA;
    public static final int COLOR_BLUE = 0xFF58BDD8;
    public static final int COLOR_BROWN = 0xFFC79453;
    public static final int COLOR_GREEN = 0xFF42AB40;
    public static final int COLOR_LT_GREEN = 0xFFA5D6A7;
    public static final int COLOR_GRAY = 0xFF727272;
    public static final int COLOR_WHITE = 0xFFFFFFFF;
    public static final int COLOR_BLACK = 0xFF000000;
    public static final int COLOR_YELLOW = 0xFFF7DF2D;
    public static final int COLOR_YELLOW_DARK = 0xFFCAC41D;
    public static final int COLOR_RED = 0xFFF01F1F;

}
