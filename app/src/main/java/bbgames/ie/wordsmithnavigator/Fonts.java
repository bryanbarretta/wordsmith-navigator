package bbgames.ie.wordsmithnavigator;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Bryan on 02/03/2018.
 */

public class Fonts {

    public static Typeface defaultSerious(Context context){
        return Typeface.createFromAsset(context.getAssets(), "font/clearsans.ttf");
    }

    public static Typeface defaultFun(Context context){
        return Typeface.createFromAsset(context.getAssets(), "font/litsans.ttf");
    }

    public static Typeface defaultMonospace(Context context){
        return Typeface.createFromAsset(context.getAssets(), "font/inconsolata.ttf");
    }

    public static Typeface defaultTitle(Context context){
        return Typeface.createFromAsset(context.getAssets(), "font/aquawax.ttf");
    }

    public static Typeface defaultTitleDialog(Context context){
        return Typeface.createFromAsset(context.getAssets(), "font/litsans.ttf");
    }

    public static Typeface defaultBodyDialog(Context context){
        return Typeface.createFromAsset(context.getAssets(), "font/clearsans.ttf");
    }

}
