package bbgames.ie.wordsmithnavigator.Settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import org.andengine.util.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Constants.LoggingModes;
import bbgames.ie.wordsmithnavigator.Constants.SupportedLanguages;

/**
 * Created by Bryan on 08/05/2016.
 */
public class Settings {

    private static final String SHARED_PREFERENCES_NAME = "WUNDER_WORDS_SETTINGS_1";
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Context context;

    public Settings(Context context){
        this.context = context;
        prefs = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
    }

    public static Locale getLocale(){
        return Locale.ENGLISH;
    }

    private enum SettingKeys {
        Language,
        WordDefinitionsLimitPerCategory,
        HintCount,
        LoggingMode
    }

    public SupportedLanguages getLanguage(){
        String l;
        if(prefs.contains(SettingKeys.Language.name())){
            l = prefs.getString(SettingKeys.Language.name(), SupportedLanguages.ENGLISH_UK.getAbbreviation());
        }else {
            l = Locale.getDefault().getLanguage();
            if(!Arrays.asList(SupportedLanguages.values()).contains(l)){   //If local language is not supported
                l = SupportedLanguages.ENGLISH_UK.getAbbreviation();  //Default language is english
            }
        }
        return SupportedLanguages.getLanguageByAbbr(l);
    }

    public int getWordDefinitionsLimitPerCategory(){
        return prefs.getInt(SettingKeys.WordDefinitionsLimitPerCategory.name(), 3);
    }

    public int getHintCount(){
        return prefs.getInt(SettingKeys.HintCount.name(), 99);
    }

    public boolean setLanguage(ApplicationManager applicationManager, SupportedLanguages language){
        return setLanguage(applicationManager, language.getAbbreviation());
    }

    public boolean setLanguage(ApplicationManager applicationManager, String abbr){
        if(!getLanguage().getAbbreviation().equals(abbr)) {
            editor.putString(SettingKeys.Language.name(), abbr);
            editor.commit();
            Locale locale = new Locale(abbr);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            applicationManager.getApplicationContext().getResources().updateConfiguration(config, null);
            applicationManager.loadDictionary(context, true);
            return true;
        }
        return false;
    }

    public void setLoggingMode(LoggingModes... modes){
        editor.putString(SettingKeys.LoggingMode.name(), TextUtils.join(' ', modes));
        editor.commit();
    }

    public void setWordDefinitionsLimitPerCategory(int limit){
        editor.putInt(SettingKeys.WordDefinitionsLimitPerCategory.name(), limit);
        editor.commit();
    }

    public void incrementHintCount(int numHintsToAdd){
        int c = getHintCount();
        editor.putInt(SettingKeys.HintCount.name(), c + numHintsToAdd);
        editor.commit();
    }

    public void decrementHintCount(){
        int c = getHintCount();
        editor.putInt(SettingKeys.HintCount.name(), c - 1);
        editor.commit();
    }

}
