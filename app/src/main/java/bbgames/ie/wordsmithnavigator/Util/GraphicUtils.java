package bbgames.ie.wordsmithnavigator.Util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 02/03/2018.
 */

public class GraphicUtils {

    public static void buttonColor(Button button, int color){
        button.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
    }

    public static void buttonDrawableLeft(final Button button, final Drawable drawable, int color){
        buttonDrawableLeft(button, drawable, color, null, null);
    }

    public static void buttonDrawableLeft(final Button button, final Drawable drawable, int color, Integer buttonHeight, Integer buttonPadding){
        drawable.setColorFilter(new LightingColorFilter(color, color));
        if(buttonHeight == null || buttonPadding == null){
            ViewTreeObserver vto = button.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    button.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = button.getMeasuredHeight();
                    drawable.setBounds(0, 0, height - (button.getPaddingLeft() * 4), height - (button.getPaddingBottom() * 4));
                    button.setCompoundDrawables(drawable, null, null, null);
                }
            });
        }else {
            drawable.setBounds(0, 0, buttonHeight - (buttonPadding * 2), buttonHeight - (buttonPadding * 2));
            button.setCompoundDrawables(drawable, null, null, null);
        }
    }

    public static void buttonRoundWithDrawableTop(final Button button, final Drawable drawable, int color, Integer buttonHeight, Integer buttonPadding){
        drawable.setColorFilter(new LightingColorFilter(color, color));
        if(buttonHeight == null || buttonPadding == null){
            ViewTreeObserver vto = button.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    button.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = button.getMeasuredHeight();
                    int width = button.getMeasuredWidth();
                    int ic_size = width / 3;
                    drawable.setBounds(0,
                            height / 5,
                            ic_size,
                            (height / 5) + ic_size);
                    button.setCompoundDrawables(null, drawable, null, null);
                }
            });
        }else {
            drawable.setBounds(0, 0, buttonHeight - (buttonPadding * 2), buttonHeight - (buttonPadding * 2));
            button.setCompoundDrawables(null, drawable, null, null);
        }
    }

    public static ImageView createIcon(Context context, Drawable drawable){
        return createIcon(context, drawable, context.getResources().getDimensionPixelSize(R.dimen.icon_small), context.getResources().getDimensionPixelSize(R.dimen.icon_small));
    }

    public static ImageView createIcon(Context context, Drawable drawable, int pxWidth, int pxHeight){
        ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(drawable);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(pxWidth, pxHeight));
        return imageView;
    }

    public static Bitmap getBitmap(byte[] byteArray) {
        if(byteArray == null)
        {
            return null;
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray , 0, byteArray.length);
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, blob);
        return bitmap;
    }

    public static Bitmap getBitmap(Drawable drawable, int w, int h) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap getBitmapFromAssets(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            return null;
        }
        return bitmap;
    }

    /**
     * Merge supplied bitmaps into one bitmap. The output bitmap will be one line of the inputted images.
     * @return
     */
    public static Bitmap combineBitmapsHorizontally(Bitmap... input){
        List<Bitmap> bitmaps = Arrays.asList(input);
        int width = 0;
        int height = 0;
        for(Bitmap b : bitmaps){
            width += b.getWidth();
            if(b.getHeight() > height){
                height = b.getHeight();
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        int x = 0;
        for(Bitmap b : bitmaps){
            canvas.drawBitmap(b, x, 0, null);
            x += b.getWidth();
        }
        return bitmap;
    }

    public static void textviewDrawableLeft(final TextView textView, final Drawable drawable, int color){
        textviewDrawableLeft(textView, drawable, color, null, null);
    }

    public static void textviewDrawableLeft(final TextView textView, final Drawable drawable, int color, Integer textViewHeight, Integer textViewPadding){
        drawable.setColorFilter(new LightingColorFilter(color, color));
        if(textViewHeight == null || textViewPadding == null){
            ViewTreeObserver vto = textView.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    textView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = textView.getMeasuredHeight();
                    drawable.setBounds(0, 0, height - (textView.getPaddingLeft() * 4), height - (textView.getPaddingBottom() * 4));
                    textView.setCompoundDrawables(drawable, null, null, null);
                }
            });
        }else {
            drawable.setBounds(0, 0, textViewHeight - (textViewPadding * 2), textViewHeight - (textViewPadding * 2));
            textView.setCompoundDrawables(drawable, null, null, null);
        }
    }

    public static void scalePadViewContent(View v, int pxViewDiameter) {
        int pxPadding = pxViewDiameter / 4;
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.height = pxViewDiameter;
        params.width = pxViewDiameter;
        v.setLayoutParams(params);
        v.setPadding(pxPadding, pxPadding, pxPadding, pxPadding);
    }

    /**
     * transform a views height or width so they are both the same
     * @param v
     * @param useHeight: If true, make the width match the height. If false, make the height match the width
     * @param scalePaddingAfter: if true, calls scalePadViewContent after measuring
     */
    public static void scaleSquareView(final View v, final boolean useHeight, final boolean scalePaddingAfter){
        ViewTreeObserver vto = v.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams params = v.getLayoutParams();
                if(useHeight){
                    params.width = v.getMeasuredHeight();
                    params.height = v.getMeasuredHeight();
                }else {
                    params.width = v.getMeasuredWidth();
                    params.height = v.getMeasuredWidth();
                }
                v.setLayoutParams(params);
                if(scalePaddingAfter && v instanceof ImageButton){
                    scalePadViewContent(v, params.height);
                }
            }
        });
    }
}
