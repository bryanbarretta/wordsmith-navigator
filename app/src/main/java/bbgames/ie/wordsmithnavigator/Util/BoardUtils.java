package bbgames.ie.wordsmithnavigator.Util;

import android.util.Log;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.LetterState;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 11/05/2016.
 * Carries out calculations for the board; Provides board/word information given a subset of information
 */
public class BoardUtils {

    /**
     * Used e.g to get the direction the user is touching relative to a tile.
     * If the source is a tile on the second row, and the user is touching the tile above it, return will be Direction.UP
     * @param sourceX The coordinates of the tile
     * @param sourceY
     * @param offsetX The coordinates of the area being touched
     * @param offsetY
     * @return
     */
    public static Direction getDirectionRelativeToXY(float sourceX, float sourceY, float offsetX, float offsetY){
        float[] touch = new float[]{offsetX, offsetY};
        float[] top =   new float[]{sourceX, sourceY + Constant.TILE_SIZE/2};
        float[] bottom =new float[]{sourceX, sourceY - Constant.TILE_SIZE/2};
        float[] left =  new float[]{sourceX - Constant.TILE_SIZE/2, sourceY};
        float[] right = new float[]{sourceX + Constant.TILE_SIZE/2, sourceY};

        ArrayList<float[]> points = new ArrayList<float[]>();
        points.add(top);
        points.add(bottom);
        points.add(left);
        points.add(right);

        float[] closest = getClosestCoordinatesToPoint(touch, points);

        if(closest.equals(top)){
            return Direction.NORTH;
        }
        if(closest.equals(bottom)){
            return Direction.SOUTH;
        }
        if(closest.equals(left)){
            return Direction.WEST;
        }
        if(closest.equals(right)){
            return Direction.EAST;
        }
        throw new RuntimeException(String.format("getDirectionRelativeToXY: Unexpected result. Params = %f, %f, %f, %f",
                sourceX, sourceY, offsetX, offsetY));
    }

    public static Direction getDirectionRelativeToRowColumns(float sourceRow, float sourceColumn, float offsetRow, float offsetColumn){
        if(sourceRow < offsetRow){
            return Direction.SOUTH;
        }
        if(sourceRow > offsetRow){
            return Direction.NORTH;
        }
        if(sourceColumn < offsetColumn){
            return Direction.EAST;
        }
        return Direction.WEST;
    }

    public static Direction getOppositeDirection(Direction direction){
        if(direction.equals(Direction.SOUTH)){
            return Direction.NORTH;
        }
        if(direction.equals(Direction.NORTH)){
            return Direction.SOUTH;
        }
        if(direction.equals(Direction.EAST)){
            return Direction.WEST;
        }
        if(direction.equals(Direction.WEST)){
            return Direction.EAST;
        }
        throw new RuntimeException("getOppositeDirection: Invalid direction passed: " + direction);
    }

    public static Direction getInvertedDirection_NorthOrWest(Direction direction){
        switch (direction){
            case EAST:
            case WEST:
                return Direction.NORTH;
            case NORTH:
            case SOUTH:
            default:
                return Direction.WEST;
        }
    }

    public static boolean isReverseDirection(Direction direction){
        return direction == Direction.NORTH || direction == Direction.WEST;
    }

    /**
     * Analyzes the tile behind the current focused map tile.
     * If this tile has a letter, returns the word that letter is part of.
     */
    public static HintWord getPreviousLettersForFocusedTile(GameActivity gameActivity){
        if(gameActivity.getLevelManager().getFocusTile() == null){
            return new HintWord();
        }

        int[] previousXY = LevelUtils.incrementXY(gameActivity.getLevelManager().getFocusTile().getXy(),
                getOppositeDirection(gameActivity.getLevelManager().getFocusTile().getDirection()));
        Character previousTile = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), previousXY);
        ArrayList<HintLetter> previousLetters = new ArrayList<>();
        while (previousTile != null && Character.isLetter(previousTile)){
            previousLetters.add(0, new HintLetter(previousXY, previousTile,true,false));
            previousXY = LevelUtils.incrementXY(previousXY, getOppositeDirection(gameActivity.getLevelManager().getFocusTile().getDirection()));
            previousTile = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), previousXY);
        }
        return new HintWord(previousLetters);
    }

    /**
     * Checks that all the placed letter tiles on this round do not inadvertently form a bad word
     * @return
     */
    public static boolean verifyPlacedLetterTiles(GameActivity gameActivity, boolean undoLastLetterPlayedIfWordIsInvalid){
        if(gameActivity.getHUD().getHudLetterTilesOnBoard() == null || gameActivity.getHUD().getHudLetterTilesOnBoard().isEmpty()){
            Util.throwException(gameActivity, "verifyPlacedLetterTiles but no hud letters on Board!");
        }
        if(gameActivity.getLevelManager().getFocusTile() == null ||
           gameActivity.getLevelManager().getFocusTile().getDirection() == null) {
            Util.throwException(gameActivity, "verifyPlacedLetterTiles but no focusedTileArrowDirection!");
        }
        TileLetter letterOfInterest = gameActivity.getGraphicsManager().getLetterFactory().getLetter(
                gameActivity.getHUD().getHudLetterTilesOnBoard().get(0).getBoardColRow()[1],
                gameActivity.getHUD().getHudLetterTilesOnBoard().get(0).getBoardColRow()[0]);
        boolean wordIsHorizontal = gameActivity.getLevelManager().getFocusTile().getDirection().equals(Direction.EAST) ||
                                   gameActivity.getLevelManager().getFocusTile().getDirection().equals(Direction.WEST);
        return verifyLetterTilesMakeValidWord(gameActivity, letterOfInterest, wordIsHorizontal, true, false, undoLastLetterPlayedIfWordIsInvalid);
    }

    public static Character getCharacterAtXY(ArrayList<String> layout, int[] xy){
        if(xy == null){
            return null;
        }
        int x = xy[0];
        int y = xy[1];
        if(y >= layout.size()){
            return null;
        }
        String row = layout.get(y);
        if(x >= row.length()){
            return null;
        }
        return row.charAt(x);
    }

    public static Character getAdjacentCharacter(ArrayList<String> layout, int[] xySourceChar, Direction direction){
        if(xySourceChar == null){
            return null;
        }
        int x = xySourceChar[0];
        int y = xySourceChar[1];
        try {
            if(x == 0 && direction.equals(Direction.WEST) ||
                    x == (layout.get(0).length() - 1) && direction.equals(Direction.EAST) ||
                    y == 0 && direction.equals(Direction.NORTH) ||
                    y == (layout.size() - 1) && direction.equals(Direction.SOUTH)){
                return null;
            }
            switch (direction){
                case NORTH:
                    return layout.get(y - 1).charAt(x);
                case SOUTH:
                    return layout.get(y + 1).charAt(x);
                case WEST:
                    return layout.get(y).charAt(x - 1);
                case EAST:
                    return layout.get(y).charAt(x + 1);
            }
        }catch (IndexOutOfBoundsException e){
            Log.e("getAdjacentCharacter", "Expected - Could not get character " + direction.name() + " of position " + x + ", " + y + ":\n" + e.getMessage());
            return null;
        }catch (Exception e){
            Log.e("getAdjacentCharacter", "Unexpected - Could not get character " + direction.name() + " of position " + x + ", " + y + ":\n" + e.getMessage());
            return null;
        }
        Log.e("getAdjacentCharacter", "Should not have reached here");
        return null;
    }

    public static int[] getAdjacentTileXY(ArrayList<String> layout, int[] xySourceChar, Direction direction){
        if(direction.equals(Direction.WEST)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0] - 1, xySourceChar[1]};
            }
            return null;
        }else if(direction.equals(Direction.EAST)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0] + 1, xySourceChar[1]};
            }
            return null;
        }else if(direction.equals(Direction.NORTH)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0], xySourceChar[1] - 1};
            }
            return null;
        }else if(direction.equals(Direction.SOUTH)){
            if(getAdjacentCharacter(layout, xySourceChar, direction) != null){
                return new int[]{xySourceChar[0], xySourceChar[1] + 1};
            }
            return null;
        }
        return null;
    }

    //region Private methods

    /**
     * Given a mapTile, ensure the word is valid.
     * @param sourceTile
     * @param wordIsHorizontal : true = Word is flat, false = word is written vertically on the board
     * @param verifyTouchingLetterTiles : While traversing letter tiles, if true, this will also analyse the letter tiles touching these letters
     * @param isConnectedToPlacedLetters : Pass in false by default. If the first offset word is touching placed letters, this will remain true ensuring connection is satisfied
     * @param undoLastLetterPlayedIfWordIsInvalid
     * @return true if the letter tiles make a valid word
     */
    private static boolean verifyLetterTilesMakeValidWord(GameActivity gameActivity, TileLetter sourceTile, boolean wordIsHorizontal, boolean verifyTouchingLetterTiles, boolean isConnectedToPlacedLetters, boolean undoLastLetterPlayedIfWordIsInvalid){

        if(!sourceTile.isBoardAttached()){
            throw new RuntimeException("verifyLetterTilesMakeValidWord needs a sourceTile that is attached to the board");
        }

        //region 1. Go to the start of this word

        Direction directionToTraverse = wordIsHorizontal ? Direction.WEST : Direction.NORTH;
        TileLetter letterOfInterest = sourceTile;
        int[] currentColRow = LevelUtils.getColumnRow(gameActivity.getLevelManager().getRowCount(),
                          new float[] {letterOfInterest.getX(), letterOfInterest.getY()});
        while (true){
            int[] nCR = LevelUtils.incrementXY(currentColRow, directionToTraverse);
            TileLetter nTile = gameActivity.getGraphicsManager().getLetterFactory().getLetter(nCR[1], nCR[0]);
            if(nTile == null){
                break;
            }
            currentColRow = nCR;
            letterOfInterest = nTile;
        }

        //endregion

        //region 2: traverse the word. Verify word is touching at least one letter tile, and that any touching letters do not form invalid words

        directionToTraverse = BoardUtils.getOppositeDirection(directionToTraverse);
        Direction directionToMonitor1 = null;
        Direction directionToMonitor2 = null;
        if(directionToTraverse == Direction.EAST){
            directionToMonitor1 = Direction.SOUTH;
            directionToMonitor2 = Direction.NORTH;
        }else if(directionToTraverse == Direction.SOUTH){
            directionToMonitor1 = Direction.WEST;
            directionToMonitor2 = Direction.EAST;
        }else {
            Util.throwException(gameActivity, "verifyLetterTilesMakeValidWord WTF");
        }

        //Traverse
        String word = "";
        PlayerType playerWhoseLetterTileIsBeingTouched = null;
        ArrayList<TileLetter> listTileLettersTraversed = new ArrayList<>();
        while (true){
            if(verifyTouchingLetterTiles){
                int[] oXY1 = LevelUtils.incrementXY(currentColRow, directionToMonitor1);
                int[] oXY2 = LevelUtils.incrementXY(currentColRow, directionToMonitor2);
                Character offsetTile1 = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), oXY1);
                Character offsetTile2 = LevelUtils.getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), oXY2);
                boolean hasTouchingWord = (offsetTile1 != null && Character.isLetter(offsetTile1)) ||
                                          (offsetTile2 != null && Character.isLetter(offsetTile2));
                if(hasTouchingWord){
                    if(verifyLetterTilesMakeValidWord(gameActivity, letterOfInterest, !wordIsHorizontal, false, isConnectedToPlacedLetters, undoLastLetterPlayedIfWordIsInvalid)){
                        isConnectedToPlacedLetters = true;
                    }else {
                        return false;
                    }
                }
            }

            TileLetter tileLetter = letterOfInterest;
            if(tileLetter.getOwner() != null && !tileLetter.getOwner().equals(gameActivity.getPlayerManager().getActivePlayer())){
                playerWhoseLetterTileIsBeingTouched = tileLetter.getOwner().getPlayerType();
            }
            listTileLettersTraversed.add(tileLetter);
            word += tileLetter.getLetter();
            if(!isConnectedToPlacedLetters && (tileLetter instanceof TileLetter && tileLetter.getState().equals(LetterState.LOCKED))){
                isConnectedToPlacedLetters = true;
            }
            currentColRow = LevelUtils.incrementXY(currentColRow, directionToTraverse);
            letterOfInterest = gameActivity.getGraphicsManager().getLetterFactory().getLetter(currentColRow[1], currentColRow[0]);
            if(letterOfInterest == null){
                break;
            }
        }
        if(!isConnectedToPlacedLetters){
            String errorMsg = (playerWhoseLetterTileIsBeingTouched != null && playerWhoseLetterTileIsBeingTouched.equals(PlayerType.ROGUE)) ?
                    gameActivity.getString(R.string.placed_letters_touching_rogue_tiles_only) :
                    gameActivity.getString(R.string.placed_letters_isolated);
            alertInvalidWord(gameActivity, listTileLettersTraversed, errorMsg, undoLastLetterPlayedIfWordIsInvalid);
            return false;
        }
        if(playerWhoseLetterTileIsBeingTouched != null && !playerWhoseLetterTileIsBeingTouched.equals(PlayerType.ROGUE)) {
            alertInvalidWord(gameActivity, listTileLettersTraversed, String.format(
                    gameActivity.getString(R.string.word_is_touching_other_players_letters),
                    word, playerWhoseLetterTileIsBeingTouched.name()), undoLastLetterPlayedIfWordIsInvalid);
            return false;
        }
        else if(gameActivity.getApplicationManager().getDatabaseGateway().isWordExists(word)){
            return true;
        }else {
            alertInvalidWord(gameActivity,
                    listTileLettersTraversed,
                    String.format(gameActivity.getString(verifyTouchingLetterTiles ?
                        R.string.word_does_not_exist :
                        R.string.word_touching_your_word_does_not_exist), word.toUpperCase()), undoLastLetterPlayedIfWordIsInvalid);
            return false;
        }

        //endregion
    }

    private static float[] getClosestCoordinatesToPoint(float[] sourceXY, ArrayList<float[]> listOffsetXYs){
        double shortestDistance = 20000;
        float[] shortestPoint = null;
        for(float[] xy : listOffsetXYs)
        {
            double dist = getDistanceBetweenTwoPoints(xy, sourceXY);
            if (dist <= shortestDistance)
            {
                shortestDistance = dist;
                shortestPoint = xy;
            }
        }
        return shortestPoint;
    }

    private static double getDistanceBetweenTwoPoints(float[] p1, float[] p2){
        float x1 = p1[0];
        float x2 = p2[0];
        float y1 = p1[1];
        float y2 = p2[1];
        return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
    }

    private static void alertInvalidWord(GameActivity gameActivity, ArrayList<TileLetter> invalidLetterTiles, String message, boolean undoLastLetterPlayedIfWordIsInvalid){
        for (TileLetter l : invalidLetterTiles){
            l.flashRed(undoLastLetterPlayedIfWordIsInvalid ? new Runnable() {
                @Override
                public void run() {
                    if(invalidLetterTiles.indexOf(l) == 0) {
                        //This if check is just a way to ensure btnUndo action is only take once
                        gameActivity.getHUD().btnUndoPressed();
                    }
                }
            } : null);
        }
        gameActivity.showToast(message);
    }

    //endregion

}
