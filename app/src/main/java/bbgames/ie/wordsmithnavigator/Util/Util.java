package bbgames.ie.wordsmithnavigator.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.Constants.SpecialCharacters;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Constants.Direction;

/**
 * Created by Bryan on 07/05/2016.
 */
public class Util {

    public static boolean contains(final ArrayList<int[]> parent, final int[] child){
        for(final int[] item : parent){
            if(Arrays.equals(item, child)){
                return true;
            }
        }
        return false;
    }

    public static boolean isAssetExist(Context context, String pathInAssets){
        AssetManager mg = context.getAssets();
        boolean exists = false;
        InputStream is = null;
        try {
            is = mg.open(pathInAssets);
            exists = true;
        } catch (IOException ex) {
            exists = false;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.e(Util.class.getCanonicalName(), "isAssetExist failed to close InputStream: " + e.getMessage());
                }
            }
        }
        return exists;
    }

    public static int getPointsForWord(String word){
        int points = 0;
        for (Character c : word.toCharArray()){
            points += getPointsForLetter(c);
        }
        return points;
    }

    public static int getPointsForLetter(char l)
    {
        switch (Character.toUpperCase(l))
        {
            case 'A': return 1;
            case 'B': return 3;
            case 'C': return 3;
            case 'D': return 2;
            case 'E': return 1;
            case 'F': return 4;
            case 'G': return 2;
            case 'H': return 4;
            case 'I': return 1;
            case 'J': return 8;
            case 'K': return 5;
            case 'L': return 1;
            case 'M': return 3;
            case 'N': return 1;
            case 'O': return 1;
            case 'P': return 3;
            case 'Q': return 10;
            case 'R': return 1;
            case 'S': return 1;
            case 'T': return 1;
            case 'U': return 1;
            case 'V': return 4;
            case 'W': return 4;
            case 'X': return 8;
            case 'Y': return 4;
            case 'Z': return 10;
        }
        return 0;
    }

    public static boolean isCharacterVowel(Character character){
        character = Character.toLowerCase(character);
        return Arrays.asList(getVowels()).contains(character);
    }

    public static boolean isCharacterConsonant(Character character){
        character = Character.toLowerCase(character);
        return Arrays.asList(getConsonants()).contains(character);
    }

    public static Character[] getVowels(){
        return new Character[]{'a', 'e', 'i', 'o', 'u'};
    }

    public static Character[] getConsonants(){
        return new Character[]{'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
    }

    public static int getDimensValueInPixels(Context context, int resourceIdDimen){
        return (int) context.getResources().getDimension(resourceIdDimen);
    }

    public static int getDimensValueInDensityPixels(Context context, int resourceIdDimen){
        return (int) (getDimensValueInPixels(context, resourceIdDimen) / context.getResources().getDisplayMetrics().density);
    }

    public static int getDimensValueInScaledDensityPixels(Context context, int resourceIdDimen){
        return (int) (getDimensValueInPixels(context, resourceIdDimen) / context.getResources().getDisplayMetrics().scaledDensity);
    }

    public static String convertTotalSecondsToStringMMSS(int totalSeconds){
        String s = String.format("%02d", totalSeconds%60);
        String m = String.format("%02d", (totalSeconds - (totalSeconds%60))/60);
        return m + ":" + s;
    }

    public static int convertMMSStoSeconds(String mmss){
        String[] parts = mmss.split(":");
        int seconds = Integer.parseInt(parts[1]);
        seconds += (Integer.parseInt(parts[0]) * 60);
        return seconds;
    }

    public static DateTime getUTCNow() {
        return DateTime.now(DateTimeZone.UTC);
    }

    public static void throwException(Context context, String e){
        throwException(context, new Exception(e));
    }

    public static void throwException(Context context, Exception e){
        if(context instanceof GameActivity){
            throwException((GameActivity)context, e);
        }else {
            try {
                Activity activity = (Activity) context;
                ApplicationManager applicationManager = (ApplicationManager) activity.getApplication();
                throwException(applicationManager.getDatabaseGateway(), e);
            }catch (Exception e2){
                throwException(new DatabaseGateway(context), e);
            }
        }
    }

    public static void throwException(GameActivity gameActivity, Exception e){
        throwException(gameActivity.getApplicationManager().getDatabaseGateway(), e);
    }

    public static void throwException(DatabaseGateway databaseGateway, Exception e){
        databaseGateway.addUpdateException(e);
        throw new RuntimeException(e);
    }

    public static void showOkDialog(Context context, String message){
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }



    public static void showYesNoDialog(Context context, String title, String message, String yes, String no,
                                       final Runnable runnableYes, final Runnable runnableNo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if(title != null) builder.setTitle(title);
        if(message != null) builder.setMessage(message);
        builder.setPositiveButton(yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(runnableYes != null)
                    runnableYes.run();
            }
        });
        builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(runnableNo != null)
                    runnableNo.run();
            }
        });
        builder.show();
    }

    public static double getFactor(double mostFreq) {
        int numDigits = String.valueOf(mostFreq).length();
        String f = "1";
        for(int i = 1; i < numDigits; i++){
            f += "0";
        }
        f += ".0";
        return Double.valueOf(f);
    }

    public static String getBaseWord(String word) {
        if(!SpecialCharacters.containsSpecialCharacters(word)){
            return word;
        }
        String baseWord = "";
        for(Character c : word.toCharArray()){
            baseWord += SpecialCharacters.getBaseCharacter(c);
        }
        return baseWord;
    }

    public static byte[] getByteArrayFromBitmap(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] a = stream.toByteArray();
        return a;
    }

    public static String getBase64(Drawable d) {
        return getBase64(GraphicUtils.getBitmap(d, d.getBounds().width(), d.getBounds().height()));
    }

    public static String getBase64(Bitmap b) {
        String base64 = Util.getBase64(Util.getByteArrayFromBitmap(b));
        return base64;
    }

    public static String getBase64(byte[] image){
        return Base64.encodeToString(image, Base64.DEFAULT);
    }

    public static Integer indexOf(int[] target, ArrayList<int[]> array) {
        for(int i = 0; i < array.size(); i++){
            if(array.get(i)[0] == target[0] && array.get(i)[1] == target[1]){
                return i;
            }
        }
        return null;
    }

    public static ApplicationManager getApplicationManager(Context context) {
        return (ApplicationManager)context.getApplicationContext();
    }

    public static boolean isNullOrBlank(Character character) {
        return character == null || character == ' ';
    }

    public static boolean equal(Integer[] intArray1, Integer[] intArray2) {
        if(intArray1 == null && intArray2 == null){
            return true;
        }
        if(intArray1 == null || intArray2 == null){
            return false;
        }
        if(intArray1.length != intArray2.length){
            return false;
        }
        for(int i = 0; i < intArray1.length; i++){
            if(intArray1[i] != intArray2[i]){
                return false;
            }
        }
        return true;
    }

    public static boolean equal(int[] intArray1, int[] intArray2) {
        if(intArray1 == null && intArray2 == null){
            return true;
        }
        if(intArray1 == null || intArray2 == null){
            return false;
        }
        if(intArray1.length != intArray2.length){
            return false;
        }
        for(int i = 0; i < intArray1.length; i++){
            if(intArray1[i] != intArray2[i]){
                return false;
            }
        }
        return true;
    }

    public static boolean isRogueTile(Character c) {
        return Character.isLetter(c) && Character.isUpperCase(c);
    }

    public static boolean isBoardLetterTile(Character c) {
        return Character.isLetter(c) && Character.isLowerCase(c);
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static ArrayList<int[]> getLetterXYs(ArrayList<HintWord> words) {
        ArrayList<int[]> output = new ArrayList<>();
        for(HintWord w : words){
            ArrayList<int[]> xys = getLetterXYs(w);
            for(int[] xy : xys){
                if(!Util.contains(output, xy)){
                    output.add(xy);
                }
            }
        }
        return output;
    }

    public static ArrayList<int[]> getLetterXYs(HintWord playedWord) {
        ArrayList<int[]> output = new ArrayList<>();
        for(HintLetter l : playedWord.getLetters()){
            output.add(l.getXY());
        }
        return output;
    }

    public static HintWord getWordFromUnorderedLetters(DatabaseGateway databaseGateway, ArrayList<String> layout, ArrayList<int[]> unorderedLettersXY) {
        if(unorderedLettersXY.isEmpty()){
            return null;
        }
        if(unorderedLettersXY.size() == 1){
            return new HintWord(new HintLetter(unorderedLettersXY.get(0)[0], unorderedLettersXY.get(0)[1],
                BoardUtils.getCharacterAtXY(layout, unorderedLettersXY.get(0)), true, false));
        }
        boolean isHorizontal = unorderedLettersXY.get(0)[1] == unorderedLettersXY.get(1)[1];
        int[] startingXY = unorderedLettersXY.get(0);
        while (true){
            Character c = BoardUtils.getAdjacentCharacter(layout, startingXY, isHorizontal ? Direction.WEST : Direction.NORTH);
            if(c != null && Character.isLetter(c)){
                startingXY = new int[]{startingXY[0] - (isHorizontal ? 1 : 0) , startingXY[1] - (isHorizontal ? 0 : 1)};
            }else {
                break;
            }
        }
        ArrayList<HintLetter> letters = new ArrayList<>();
        letters.add(new HintLetter(startingXY[0], startingXY[1], BoardUtils.getCharacterAtXY(layout, startingXY), true, false));
        while (true){
            startingXY = new int[]{startingXY[0] + (isHorizontal ? 1 : 0) , startingXY[1] + (isHorizontal ? 0 : 1)};
            Character c = BoardUtils.getCharacterAtXY(layout, startingXY);
            if(c != null && Character.isLetter(c)){
                letters.add(new HintLetter(startingXY[0], startingXY[1],
                        BoardUtils.getCharacterAtXY(layout, startingXY), true, false));
            }else {
                break;
            }
        }
        HintWord word = new HintWord(letters);
        if(databaseGateway.isWordExists(word.toString())){
            return new HintWord(word, databaseGateway);
        }else {
            String wordAsString = new StringBuilder(word.toString()).reverse().toString();
            if(databaseGateway.isWordExists(wordAsString)){
                HintWord reversedWord = new HintWord(new ArrayList<HintLetter>());
                for(HintLetter l : word.getLetters()){
                    reversedWord.addLetter(l, true);
                }
                return word;
            }else {
                throw new RuntimeException(String.format("There is a word [%s] on the board that is not in the dictionary!\n%s",
                        wordAsString, LevelUtils.getLevelAsString(layout)));
            }
        }
    }

    public static View inflateView(Context context, int layoutId) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layoutId, null, false);
    }

    public static String toNDecimalPlaces(double number, int n) {
        if(n < 1){
            return ((int)number)+"";
        }
        String f = "#.";
        for(int j = 0; j < n; j++){
            f += "#";
        }
        return new DecimalFormat(f).format(number);
    }

    public static Activity getActivity(Context ctx) {
        return (Activity) ctx;
    }

    public static int countMatches(String line, Character character) {
        return line.length() - line.replace(character.toString(), "").length();
    }

    public static int getConsonantsCount(String hudLetters) {
        int ret = 0;
        ArrayList<Character> consonants = new ArrayList<Character>();
        Arrays.asList(getConsonants());
        for(Character c : hudLetters.toCharArray()){
            if(consonants.contains(c)){
                ret++;
            }
        }
        return ret;
    }

    public static int getVowelsCount(String hudLetters) {
        int ret = 0;
        ArrayList<Character> vowels = new ArrayList<Character>();
        Arrays.asList(getVowels());
        for(Character c : hudLetters.toCharArray()){
            if(vowels.contains(c)){
                ret++;
            }
        }
        return ret;
    }

    //region Primitive conversions

    public static int[] toPrimitive(Integer[] input) {
        int[] newArray = new int[input.length];
        int i = 0;
        for (Integer value : input) {
            newArray[i++] = value == null ? null : (int)value;
        }
        return newArray;
    }

    public static Integer[] fromPrimitive(int[] input) {
        Integer[] newArray = new Integer[input.length];
        int i = 0;
        for (int value : input) {
            newArray[i++] = Integer.valueOf(value);
        }
        return newArray;
    }
    //Arrays.stream( data ).boxed().toArray( Integer[]::new );

    //endregion
}

