package bbgames.ie.wordsmithnavigator.Util;

import android.util.Log;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.Constant;
import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.Position;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.AStar.AStar;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.Player;
import bbgames.ie.wordsmithnavigator.GameObjects.Tiles.TileLetter;

/**
 * Mainly for functions that work with the level layouts (levels_en_uk.txt)
 */
public class LevelUtils {

    public static int getLevelWidth(ArrayList<String> level){
        int w = level.get(0).length();
        for(String s : level){
            if(s.length() > w){
                w = s.length();
            }
        }
        return w;
    }

    public static Integer[][] getBlockedXYTiles(ArrayList<String> layout) {
        ArrayList<Integer[]> xyArr = new ArrayList<>();
        int totalWidth = getLevelWidth(layout);
        ArrayList<Character> blockers = TileCodes.getBlockTileCodes();
        int x = 0;
        int y = 0;
        for(String s : layout){
            for(Character c : s.toCharArray()){
                if(blockers.contains(c)){
                    xyArr.add(new Integer[]{x, y});
                }
                x++;
            }
            //fill out any blanks
            for(; x < totalWidth; x++){
                xyArr.add(new Integer[]{x, y});
            }
            x = 0;
            y++;
        }
        return xyArr.toArray(new Integer[0][]);
    }

    public static int[] getFirstXYof(ArrayList<String> layout, TileCodes tileCode){
        return getFirstXYof(layout, tileCode.getCharacter());
    }

    public static int[] getFirstXYof(ArrayList<String> layout, Character target){
        int x = 0;
        int y = 0;
        for (String s : layout){
            for(Character c : s.toCharArray()){
                if(c.equals(target)){
                    return new int[]{x, y};
                }
                x++;
            }
            x = 0;
            y++;
        }
        return null;
    }

    public static ArrayList<int[]> getPlayableTilesTouchingALetter(ArrayList<String> layout, int[] letterTileXY){
        ArrayList<int[]> letterTiles = getLetterTilesTouchingLetter(layout, letterTileXY);
        ArrayList<int[]> result = new ArrayList<>();
        for(int[] l : letterTiles){
            ArrayList<int[]> touchingPlayableTiles = getXYsOfTouchingPlayableTiles(layout, l);
            for(int[] tpt : touchingPlayableTiles){
                if(!Util.contains(result, tpt)){
                    result.add(tpt);
                }
            }
        }
        return result;
    }

    public static ArrayList<int[]> getLetterTilesTouchingLetter(ArrayList<String> layout, int[] letterTileXY){
        ArrayList<int[]> result = new ArrayList<>();
        result.add(letterTileXY);
        return getLetterTilesTouchingLetter(layout, letterTileXY, result);
    }

    public static ArrayList<int[]> getLetterTilesTouchingLetter(ArrayList<String> layout, int[] letterTileXY, ArrayList<int[]> listSoFar){
        ArrayList<int[]> xYofTouchingLetters = getXYsOfAdjacentLetters(layout, letterTileXY);
        for (int[] l : xYofTouchingLetters){
            if(!Util.contains(listSoFar, l)){
                listSoFar.add(l);
                getLetterTilesTouchingLetter(layout, l, listSoFar);
            }
        }
        return listSoFar;
    }

    public static String getLevelAsString(ArrayList<String> layout){
        String s = "";
        for(String l : layout){
            s += l + "\n";
        }
        return s;
    }

    //Returns a list of xy coordinates of letters that are adjacent to sourceXY tile
    public static ArrayList<int[]> getXYsOfAdjacentLetters(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> l = new ArrayList<>();
        Character cE = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.EAST);
        Character cW = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.WEST);
        Character cN = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.NORTH);
        Character cS = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.SOUTH);
        if(cE != null && Character.isLetter(cE)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.EAST));
        }
        if(cW != null && Character.isLetter(cW)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.WEST));
        }
        if(cN != null && Character.isLetter(cN)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.NORTH));
        }
        if(cS != null && Character.isLetter(cS)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.SOUTH));
        }
        return l;
    }

    public static ArrayList<int[]> getXYsOfTouchingPlayableTiles(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> l = new ArrayList<>();
        Character cE = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.EAST);
        Character cW = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.WEST);
        Character cN = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.NORTH);
        Character cS = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.SOUTH);
        if(cE != null && TileCodes.getPlayableTileCodes().contains(cE)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.EAST));
        }
        if(cW != null && TileCodes.getPlayableTileCodes().contains(cW)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.WEST));
        }
        if(cN != null && TileCodes.getPlayableTileCodes().contains(cN)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.NORTH));
        }
        if(cS != null && TileCodes.getPlayableTileCodes().contains(cS)){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.SOUTH));
        }
        return l;
    }

    public static ArrayList<int[]> getXYsOfAdjacentTiles(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> l = new ArrayList<>();
        Character cE = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.EAST);
        Character cW = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.WEST);
        Character cN = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.NORTH);
        Character cS = BoardUtils.getAdjacentCharacter(layout, sourceXY, Direction.SOUTH);
        if(cE != null){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.EAST));
        }
        if(cW != null){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.WEST));
        }
        if(cN != null){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.NORTH));
        }
        if(cS != null){
            l.add(BoardUtils.getAdjacentTileXY(layout, sourceXY, Direction.SOUTH));
        }
        return l;
    }

    public static ArrayList<int[]> getXYsOfTouchingTilesOfTypeRecursively(ArrayList<String> layout, int[] sourceXY, TileCodes ofType){
        return getXYsOfTouchingTilesOfTypeRecursively(layout, sourceXY, ofType, null);
    }

    public static ArrayList<int[]> getXYsOfTouchingRogueTilesRecursively(ArrayList<String> layout, int[] sourceXY) {
        return getXYsOfTouchingTilesOfTypeRecursively(layout, sourceXY, null, true);
    }

    public static ArrayList<int[]> getXYsOfTouchingLetterTilesRecursively(ArrayList<String> layout, int[] sourceXY) {
        return getXYsOfTouchingTilesOfTypeRecursively(layout, sourceXY, null, false);
    }

    public static Character getCharacterAt(ArrayList<String> layout, int[] sourceXY) {
        try {
            if (layout.size() >= sourceXY[1]) {
                String row = layout.get(sourceXY[1]);
                if (row.length() >= sourceXY[0]) {
                    return row.charAt(sourceXY[0]);
                }
            }
        }catch (IndexOutOfBoundsException oob){
            return null;
        }
        return null;
    }

    public static Character getCharacterAt(GameActivity gameActivity, int[] sourceXY) {
        TileLetter l = gameActivity.getGraphicsManager().getLetterFactory().getLetter(sourceXY[1], sourceXY[0]);
        if(l != null){
            return l.getLetter();
        }else {
            return getCharacterAt(gameActivity.getLevelManager().getLayoutModified(), sourceXY);
        }
    }

    /**
     * Gets the xy's of all the possible playable tiles the player can use.
     * Filters out any playable tiles not accessible due to walls/non-playable etc. tile boundaries
     */
    public static ArrayList<int[]> getXYsOfAvailablePlayableTilesToPlayer(ArrayList<String> layout, int[] sourceXY){
        ArrayList<int[]> output = new ArrayList<>();
        ArrayList<int[]> xyQueue = new ArrayList<>();      //A list of xys we have yet to evaluate
        ArrayList<int[]> xyEvaluated = new ArrayList<>();  //A list of xys we have already evaluated
        xyQueue.add(sourceXY);
        while(!xyQueue.isEmpty()){
            int[] xy = xyQueue.get(0);
            if(!Util.contains(xyEvaluated, xy)){
                //We haven't evaluated this tile yet
                Character xyCode = getCharacterAt(layout, xy);
                if(LevelUtils.isPlayableTile(xyCode) && !Util.contains(output, xy)){
                    output.add(xy);
                }
                ArrayList<int[]> touchingXys = LevelUtils.getXYsOfAdjacentTiles(layout, xy);
                for(int[] touchingXy : touchingXys){
                    if(!LevelUtils.isBlockingTile(getCharacterAt(layout, xy)) && !Util.contains(xyEvaluated, touchingXy)){
                        //Add it to queue if we have not already evaluated this xy
                        xyQueue.add(touchingXy);
                    }
                }
                xyEvaluated.add(xy);
            }
            xyQueue.remove(0);
        }
        return output;
    }

    /**
     * Given the players current xy, returns the xy the player should be aiming to reach next.
     * Normally the ENDZONE is the target. Make sure we are aiming for the endzone on our side.
     * Sometimes we may have other targets first as well e.g. detonator, points etc.
     * As a result, the result of this method may be different each turn
     */
    public static int[] getOptimalTargetXY(ArrayList<String> layout, int[] sourceXY){
        //Target priority:
        //#1: Detonator
        //#2. Endzone
        //#3. No target (i.e. aiming to reach high points to open a gate) = NULL
        ArrayList<int[]> availableXys = getXYsOfAvailablePlayableTilesToPlayer(layout, sourceXY);
        int[] optimalXY = null;
        ArrayList<int[]> endZoneXYs = new ArrayList<>();
        for (int[] xy : availableXys){
            Character tilecode = LevelUtils.getCharacterAt(layout, xy);
            if(TileCodes.Detonator.getCharacter() == tilecode){
                optimalXY = xy;
                break;
            }
            if(TileCodes.EndZone.getCharacter() == tilecode){
                optimalXY = xy;
                endZoneXYs.add(xy);
            }
        }
        Integer bestScore = null;
        for (int[] endXy : endZoneXYs){
            int score = AStar.getPathScore(new AStar(layout, sourceXY, endXy).findPath());
            if(bestScore == null || score < bestScore){
                bestScore = score;
                optimalXY = endXy;
            }
        }
        return optimalXY;
    }

    /**
     * 'Near' means in the 8 tiles surrounding the source tile i.e. E, NE, N, NW, W, SW, S, SE
     * 'Adjacent' means in the 4 tiles directly touching the source tile i.e. N, S, E, W
     */
    public static boolean isNearSpecificTileCode(ArrayList<String> layout, int[] sourceXY, TileCodes specificTileCode){
        for(Position p : Position.values()){
            Character c = getCharacterAt(layout, p.getXy(sourceXY));
            if(c != null && c.equals(specificTileCode.getCharacter())){
                return true;
            }
        }
        return false;
    }

    public static boolean isNearBoardLetter(ArrayList<String> layout, int[] sourceXY){
        for(Position p : Position.values()){
            Character c = getCharacterAt(layout, p.getXy(sourceXY));
            if(c != null && Character.isLetter(c)){
                return true;
            }
        }
        return false;
    }

    public static boolean isAdjacentToBoardLetter(ArrayList<String> layout, int[] sourceXY){
        Position[] ps = new Position[]{Position.TOP, Position.BOTTOM, Position.LEFT, Position.RIGHT};
        for(Position p : ps){
            Character c = getCharacterAt(layout, p.getXy(sourceXY));
            if(c != null && Character.isLetter(c)){
                return true;
            }
        }
        return false;
    }

    /**
     * @return null if there is no playable tile in this direction, else the first playable map tile in this direction
     */
    public static int[] getNextPlayableMapTileXY(ArrayList<String> layout, int[] sourceXY, Direction direction) {
        while (true){
            sourceXY = BoardUtils.getAdjacentTileXY(layout, sourceXY, direction);
            if(sourceXY == null){
                return null;
            }
            Character c = BoardUtils.getCharacterAtXY(layout, sourceXY);
            if(c == null || TileCodes.getBlockTileCodes().contains(c)){
                return null;
            }
            if(TileCodes.getPlayableTileCodes().contains(c)){
                return sourceXY;
            }
        }
    }

    public static Direction getDirection(int sourceX, int sourceY, int targetX, int targetY){
        if(sourceX < targetX){
            return Direction.EAST;
        }
        if(sourceX > targetX){
            return Direction.WEST;
        }
        if(sourceY < targetY){
            return Direction.SOUTH;
        }
        if(sourceY > targetY){
            return Direction.NORTH;
        }
        return null;
    }

    public static ArrayList<String> applyHintWordToLevel(ArrayList<String> layout, HintWord answer) {
        for (HintLetter l : answer.getLetters()){
            String oldRow = layout.get(l.getRow());
            String newRow = oldRow.substring(0, l.getColumn()) + l.getLetter().toString().toUpperCase() + oldRow.substring(l.getColumn()+1);
            layout.set(l.getRow(), newRow);
        }
        return layout;
    }

    public static boolean isPlayableTile(Character tile) {
        return tile != null && TileCodes.getPlayableTileCodes().contains(tile);
    }

    private static boolean isBlockingTile(Character tile) {
        return tile != null && TileCodes.getBlockTileCodes().contains(tile);
    }

    public static HintWord getInitialBoardWord(DatabaseGateway databaseGateway, int[] playerStartingXY, ArrayList<String> level){
        ArrayList<int[]> unorderedLettersXY = LevelUtils.getLetterTilesTouchingLetter(level, playerStartingXY);
        if(Util.contains(unorderedLettersXY, playerStartingXY) && !Character.isLetter(LevelUtils.getCharacterAt(level, playerStartingXY))){
            unorderedLettersXY.remove(playerStartingXY);
        }
        return Util.getWordFromUnorderedLetters(databaseGateway, level, unorderedLettersXY);
    }

    public static int[] getColumnRow(int rowCount, float[] pxXY) {
        int x = (Math.round(pxXY[0]) / Constant.TILE_SIZE);
        int y = (Math.round(pxXY[1]) / Constant.TILE_SIZE);
        y = (rowCount - 1) - y;
        if(x < 0 || y < 0){
            throw new RuntimeException("getColumnRow is returning negative values!");
        }
        return new int[]{x, y};
    }

    /**
     * Provided the current layout contain a StartingZone character, returns the best direction to embark from.
     * @return null if the current layout does not contain a StartingZone character
     */
    public static Direction generateFocusTileDirection(ArrayList<String> layout) {
        int[] xyFocusTile = getFirstXYof(layout, TileCodes.StartingZone.getCharacter());
        if(xyFocusTile == null){
            return null;
        }
        if(xyFocusTile[0] > 0){ //check to the left
            if(Character.isLetter(layout.get(xyFocusTile[1]).charAt(xyFocusTile[0]-1))){
                return Direction.EAST;
            }
        }
        if(xyFocusTile[1] > 0){ //check above
            if(Character.isLetter(layout.get(xyFocusTile[1]-1).charAt(xyFocusTile[0]))){
                return Direction.SOUTH;
            }
        }
        if(xyFocusTile[0] < layout.get(xyFocusTile[1]).length() - 1){ //check to the right
            if(Character.isLetter(layout.get(xyFocusTile[1]).charAt(xyFocusTile[0]+1))){
                return Direction.WEST;
            }
        }
        if(xyFocusTile[1] < layout.size() - 1){ //check below
            if(Character.isLetter(layout.get(xyFocusTile[1]+1).charAt(xyFocusTile[0]))){
                return Direction.NORTH;
            }
        }
        return null;
    }

    /**
     * @return true if the 2 letters are touching each other NORTH, SOUTH, EAST or WEST
     */
    public static boolean isAdjacent(int[] xyRogueLetter, int[] xyPlayedLetter) {
        if(xyRogueLetter[0] == xyPlayedLetter[0] - 1 && xyRogueLetter[1] == xyPlayedLetter[1]){
            return true;
        }
        if(xyRogueLetter[0] == xyPlayedLetter[0] + 1 && xyRogueLetter[1] == xyPlayedLetter[1]){
            return true;
        }
        if(xyRogueLetter[1] == xyPlayedLetter[1] - 1 && xyRogueLetter[0] == xyPlayedLetter[0]){
            return true;
        }
        if(xyRogueLetter[1] == xyPlayedLetter[1] + 1 && xyRogueLetter[0] == xyPlayedLetter[0]){
            return true;
        }
        return false;
    }

    public static String getRowOrColumnAsString(boolean isRow, ArrayList<String> layout, int[] xy){
        if(isRow){
            return LevelUtils.getRowAsString(layout, xy[1]);
        }else {
            return LevelUtils.getColumnAsString(layout, xy[0]);
        }
    }

    public static String getRowAsString(ArrayList<String> layout, int y) {
        if(y >= layout.size()){
            return null;
        }
        return layout.get(y);
    }

    public static String getColumnAsString(ArrayList<String> layout, int x) {
        if(x >= layout.get(0).length()){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for(String row : layout){
            sb.append(row.charAt(x));
        }
        return sb.toString();
    }

    /**
     * @param row
     * @param col
     * @return the pixel X and Y value of the center of the tile at [col, row].
     */
    public static float[] getXY(int rowCount, int row, int col) {
        float offset = Constant.TILE_SIZE / 2; //So it is at the center of the tile
        float py = ((rowCount - 1) - row) * Constant.TILE_SIZE;
        float px = col * Constant.TILE_SIZE;
        return new float[]{px + offset, py + offset};
    }

    /**
     * @param xy
     * @param direction
     * @return the xy of the next tile in the specified direction originating from the specified xy
     */
    public static int[] incrementXY(int[] xy, Direction direction) {
        int[] output = new int[2];
        if(direction.isHorizontal()){
            int nextX = direction.isBackwards() ? xy[0] - 1 : xy[0] + 1;
            output[0] = nextX;
            output[1] = xy[1];
        }else {
            int nextY = direction.isBackwards() ? xy[1] - 1 : xy[1] + 1;
            output[0] = xy[0];
            output[1] = nextY;
        }
        if(output[0] < 0 || output[1] < 0){
            Log.w(LevelUtils.class.getSimpleName(), "incrementXY is returning negative values");
        }
        return output;
    }

    /**
     * Given a pixel X and Y, return the pixel X and Y of the closest tile's center anchor point
     * e.g. pX = 97.123, pY = 0.413     | return {95.0, 95.0}
     * e.g. pX = 191.044, pY = 189.75   | return {285.0, 95.0}
     */
    public static float[] getClosestTileCenterXY(float pX, float pY) {
        float excessX = pX % Constant.TILE_SIZE;    //e.g. 2.123  OR 96.044
        float excessY = pY % Constant.TILE_SIZE;    //e.g. 95.413 OR 94.75
        float tileEndX = pX - excessX;              //e.g. 190.0  OR 190.0
        float tileEndY = pY - excessY;              //e.g. 0.0    OR 190.0
        return new float[]{tileEndX + (Constant.TILE_SIZE / 2), tileEndY + (Constant.TILE_SIZE / 2)};
    }

    /**
     * Given a column row (xy), return the nearest played letter column row to that tile in any playable direction
     */
    public static Integer[] getClosestPlayedLetterTo(Player player, ArrayList<String> layout, Integer[] sourceXY) {
        //First pass: see if the sourceXY is touching any playable tiles, thats a quick win
        ArrayList<int[]> output = LevelUtils.getXYsOfTouchingPlayableTiles(layout, new int[]{sourceXY[0], sourceXY[1]});
        if(output != null && output.size() > 0){
            return new Integer[]{output.get(0)[0], output.get(0)[1]};
        }
        //Second pass: Use AStar to determine closest tile
        Integer bestScore = null;
        Integer[] bestXY = null;
        ArrayList<int[]> scannedXys = new ArrayList<>();
        for(HintWord word : player.getPlayedWords()){
            for (HintLetter l : word.getLetters()){
                if(Util.contains(scannedXys, l.getXY())){
                    continue;
                }
                int[] xy = l.getXY();
                AStar aStar = new AStar(layout, xy, new int[]{sourceXY[0], sourceXY[1]}, AIDifficulty.IMPOSSIBLE);
                int score = AStar.getPathScore(aStar.findPath());
                if(bestScore == null || score < bestScore) {
                    bestScore = score;
                    bestXY = new Integer[]{xy[0], xy[1]};
                }
            }
        }
        return bestXY;
    }

    //region private

    /**
     * Get a list of xy's of all touching tiles to source of type supplied.
     * Recursively continues in all directions until there is no more tiles of type supplied adjacent.
     * @param layout
     * @param sourceXY
     * @param ofType
     * @param overrideLetter1Rogue0Board null by default.
     *                                       True:  previous param ofType is ignored; returns touching Rogue Letter tiles recursively
     *                                       False: previous param ofType is ignored; returns touching Letter tiles recursively
     */
    private static ArrayList<int[]> getXYsOfTouchingTilesOfTypeRecursively(ArrayList<String> layout, int[] sourceXY, TileCodes ofType, Boolean overrideLetter1Rogue0Board){
        ArrayList<int[]> output = new ArrayList<>();
        ArrayList<int[]> xyQueue = new ArrayList<>();      //A list of xys we have yet to evaluate
        ArrayList<int[]> xyEvaluated = new ArrayList<>();  //A list of xys we have already evaluated
        xyQueue.add(sourceXY);
        while(!xyQueue.isEmpty()){
            int[] xy = xyQueue.get(0);
            if(!Util.contains(xyEvaluated, xy)){
                //We haven't evaluated this tile yet
                Character xyCode = getCharacterAt(layout, xy);
                boolean addToOutput;
                if(overrideLetter1Rogue0Board == null) {
                    addToOutput = xyCode.equals(ofType.getCharacter());
                }else {
                    if(overrideLetter1Rogue0Board){
                        addToOutput = Util.isRogueTile(xyCode);
                    }else {
                        addToOutput = !Util.isBoardLetterTile(xyCode);
                    }
                }
                if(addToOutput && !Util.contains(output, xy)){
                    output.add(xy);
                }
                ArrayList<int[]> touchingXys = LevelUtils.getXYsOfAdjacentTiles(layout, xy);
                for(int[] touchingXy : touchingXys){
                    Character c = LevelUtils.getCharacterAt(layout, touchingXy);
                    boolean addToQueue;
                    if(overrideLetter1Rogue0Board == null) {
                        addToQueue = c.equals(ofType.getCharacter());
                    }else {
                        if(overrideLetter1Rogue0Board){
                            addToQueue = Util.isRogueTile(c);
                        }else {
                            addToQueue = !Util.isBoardLetterTile(c);
                        }
                    }
                    if(addToQueue && !Util.contains(xyEvaluated, touchingXy)){
                        //Add it to queue if we have not already evaluated this xy
                        xyQueue.add(touchingXy);
                    }
                }
                xyEvaluated.add(xy);
            }
            xyQueue.remove(0);
        }
        return output;
    }

    public static int[] isWordOnEndzone(ArrayList<String> layoutOriginal, HintWord answer){
        return isWordOnTileCode(layoutOriginal, answer, TileCodes.EndZone);
    }

    public static int[] isWordOnDetonator(ArrayList<String> layoutOriginal, HintWord answer){
        return isWordOnTileCode(layoutOriginal, answer, TileCodes.Detonator);
    }

    /**
     * @return the x,y of the letter that has been placed on the specified tileCode.
     *         null if the tileCode is not covered by any of the answer's letters.
     */
    public static int[] isWordOnTileCode(ArrayList<String> layoutOriginal, HintWord answer, TileCodes tileCode) {
        if(answer == null){
            return null;
        }
        for(HintLetter l : answer.getLetters()){
            Character originalChar = LevelUtils.getCharacterAt(layoutOriginal, l.getXY());
            if(originalChar != null && tileCode.getCharacter().equals(originalChar)){
                return l.getXY();
            }
        }
        return null;
    }

    /**
     * Updates the layout modified data if allowed
     * @param xy where the new character will be written
     * @param updatedCode new character to write
     * @param uniqueCode means there can only be 1 instance of this tile on the board.
     *                   If true, revert any other instances of this tile with the underlying tile code
     */
    public static void updateLayout(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, int[] xy, TileCodes updatedCode, boolean uniqueCode){
        if(xy == null || xy.length != 2 || updatedCode == null){
            Log.e(LevelUtils.class.getSimpleName(), "Validation failed!");
            return;
        }
        if(updatedCode.equals(TileCodes.Invisible)){
            //Invisible codes don't affect the layout
            return;
        }
        String before = layoutModified.get(xy[1]);
        if(before.charAt(xy[0]) == TileCodes.WallTile.getCharacter() || Character.isLetter(before.charAt(xy[0]))){
            Log.e(LevelUtils.class.getSimpleName(), "cannot overwrite Wall [-] or Letter");
            return;
        }
        char[] beforeChars = before.toCharArray();
        beforeChars[xy[0]] = updatedCode.getCharacter();
        layoutModified.set(xy[1], String.valueOf(beforeChars));
        if(uniqueCode){
            enforceCodeExistsOnlyAtXY(layoutOriginal, layoutModified, updatedCode, xy, uniqueCode);
        }
    }

    public static void updateLayout(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, int[] xy, Character updatedCode){
        if(xy == null || xy.length != 2 || updatedCode == null){
            Log.e(LevelUtils.class.getSimpleName(), "Validation failed!");
            return;
        }
        String before = layoutModified.get(xy[1]);
        if(before.charAt(xy[0]) == TileCodes.WallTile.getCharacter() || Character.isLetter(before.charAt(xy[0]))){
            Log.e(LevelUtils.class.getSimpleName(), "cannot overwrite Wall [-] or Letter");
            return;
        }
        char[] beforeChars = before.toCharArray();
        beforeChars[xy[0]] = updatedCode;
        layoutModified.set(xy[1], String.valueOf(beforeChars));
    }

    /**
     * Revert the code at xy in layout modified (that is, make it equal to the corresponding layout original value).
     * NOTE: StartingZone is reverted to a map tile
     * @param xy
     * @param uniqueCode
     */
    public static void revertLayoutModified(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, int[] xy, boolean uniqueCode){
        Character original = LevelUtils.getCharacterAt(layoutOriginal, xy).charValue();
        /**
         * There are some exceptions when reverting to the original layout character.
         * Depending on events in the game we may not want the initial original value...
         */
        ArrayList<Character> exceptions = new ArrayList<>();
        //Players starting zone is a map tile. So when they focus somewhere else, the original starting zone is the same as any other map tile.
        exceptions.addAll(TileCodes.getPlayerStartingZoneTileCodes());
        //Gates, detonator crates are essentially map tiles. Once the player can focus on them, that means they are gone.
        exceptions.addAll(TileCodes.getBlockTileCodes());
        if(exceptions.contains(original)){
            original = TileCodes.MapTile.getCharacter();
        }
        updateLayout(layoutOriginal, layoutModified, xy, TileCodes.getTileCode(original), uniqueCode);
    }

    /**
     * Given a letter on the board, return all words that use that specific letter (either locked or temporary)
     */
    public static ArrayList<HintWord> getWordsOfBoardLetter(DatabaseGateway databaseGateway,
                                                            ArrayList<String> layout,
                                                            int[] xyBoardLetter,
                                                            HintWord answerPlayedThisTurn){
        final boolean yes = true;
        ArrayList<HintWord> output = new ArrayList<>();
        ArrayList<HintLetter> word1Letters = new ArrayList<HintLetter>(){
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.EAST, answerPlayedThisTurn, yes, yes));}
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.WEST, answerPlayedThisTurn, yes, yes));}
        };
        ArrayList<HintLetter> word2Letters = new ArrayList<HintLetter>(){
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.NORTH, answerPlayedThisTurn, yes, yes));}
            {addAll(getAdjoiningLettersInDirection(layout, xyBoardLetter, Direction.SOUTH, answerPlayedThisTurn, yes, yes));}
        };
        if(!word1Letters.isEmpty()){
            word1Letters.add(new HintLetter(xyBoardLetter, BoardUtils.getCharacterAtXY(layout, xyBoardLetter), yes, false));
            output.add(Util.getWordFromUnorderedLetters(databaseGateway, layout, Util.getLetterXYs(new HintWord(word1Letters))));
        }
        if(!word2Letters.isEmpty()){
            word2Letters.add(new HintLetter(xyBoardLetter, BoardUtils.getCharacterAtXY(layout, xyBoardLetter), yes, false));
            output.add(Util.getWordFromUnorderedLetters(databaseGateway, layout, Util.getLetterXYs(new HintWord(word2Letters))));
        }
        return output;
    }

    /**
     * Given a board letter xy, get any board letters touching it in a specific direction (starting with the nearest | either locked or temporary)
     */
    public static ArrayList<HintLetter> getAdjoiningLettersInDirection(ArrayList<String> layout,
                                                                       int[] xyBoardLetter,
                                                                       Direction direction,
                                                                       HintWord answerPlayedThisTurn,
                                                                       boolean includeBoardLetterTiles,
                                                                       boolean includeRogueLetterTiles){
        ArrayList<HintLetter> output = new ArrayList<>();
        int[] nextXy = BoardUtils.getAdjacentTileXY(layout, xyBoardLetter, direction);
        Character nextC = BoardUtils.getCharacterAtXY(layout, nextXy);
        if(nextC == null){
            return output;
        }
        while (nextC != null){
            HintLetter l = null;
            for(HintLetter aL : answerPlayedThisTurn.getLetters()){
                if(Util.equal(aL.getXY(), nextXy)){
                    l = new HintLetter(aL);
                    break;
                }
            }
            if(l == null) {
                //If letter at nextXy isn't part of answerPlayedThisTurn, means its locked on the board
                l = new HintLetter(nextXy, nextC, true, false);
            }
            if((includeBoardLetterTiles && Util.isBoardLetterTile(nextC)) ||
               (includeRogueLetterTiles && Util.isRogueTile(nextC))){
                output.add(l);
            }else {
                break;
            }
            nextXy = BoardUtils.getAdjacentTileXY(layout, nextXy, direction);
            nextC = BoardUtils.getCharacterAtXY(layout, nextXy);
        }
        return output;
    }

    //endregion

    //region private

    private static void enforceCodeExistsOnlyAtXY(ArrayList<String> layoutOriginal, ArrayList<String> layoutModified, TileCodes updatedCode, int[] xy, boolean uniqueCode){
        for(int r = 0; r < layoutModified.size(); r++){
            String row = layoutModified.get(r);
            for(int c = 0; c < row.length(); c++){
                Character character = row.charAt(c);
                if(character == updatedCode.getCharacter()){
                    //An instance of our updated character. If this is not the specified xy, revert it
                    int[] _xy = new int[]{c, r};
                    if(!Util.equal(xy, _xy)) {
                        revertLayoutModified(layoutOriginal, layoutModified, xy, uniqueCode);
                    }
                }
            }
        }
    }

    //endregion
}
