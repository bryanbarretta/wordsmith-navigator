package bbgames.ie.wordsmithnavigator.Util;

public class LogUtils {

    private static final int LENGTH_BLOCK_LINE = 76;

    public static String getBlockWithText(Character blockSymbol, String text){
        return getBlockWithText(blockSymbol, text, false);
    }

    public static String getBlockWithText(Character blockSymbol, String text, boolean isDoubleLineBorder){
        int spaces = LENGTH_BLOCK_LINE - text.length() - 4;
        int spacesL;
        int spacesR;
        if(spaces % 2 == 0){
            spacesL = spacesR = (spaces / 2);
        }else {
            spacesL = ((spaces - 1) / 2);
            spacesR = ((spaces + 1) / 2);
        }
        String fullSymbolLine = new String(new char[LENGTH_BLOCK_LINE]).replace('\0', blockSymbol);
        String doubleSymbol = blockSymbol + "" + blockSymbol;
        StringBuilder output = new StringBuilder();
        output.append(fullSymbolLine + "\n");
        if(isDoubleLineBorder){
            output.append(fullSymbolLine + "\n");
        }
        output.append(doubleSymbol + new String(new char[spacesL]).replace('\0', ' ') + text
                + new String(new char[spacesR]).replace('\0', ' ') + doubleSymbol);
        output.append("\n" + fullSymbolLine);
        if(isDoubleLineBorder){
            output.append("\n" + fullSymbolLine);
        }
        return output.toString();
    }

}
