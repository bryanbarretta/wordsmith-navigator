package bbgames.ie.wordsmithnavigator.Util;

import android.content.Context;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.ApplicationManager;
import bbgames.ie.wordsmithnavigator.R;

public class UserLevelUtils {

    public static int getCurrentUserTotalPoints(ApplicationManager applicationManager){
        return Integer.parseInt(applicationManager.getSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.user_total_points, "0"));
    }

    public static void setCurrentUserTotalPoints(ApplicationManager applicationManager, int newTotal){
        applicationManager.setSharedPreferenceValue(ApplicationManager.SHARED_PREF_KEY.user_total_points, newTotal+"");
    }

    public static ArrayList<LevelUpData> getLevelUpDataForProgressBar(int fromTotalPoints, int toTotalPoints){
        ArrayList<LevelUpData> luds = new ArrayList<>();
        ArrayList<PairLevelPoints> kvpLvPoints = UserLevelUtils.getRelevantLevelUpLvPointThresholds(fromTotalPoints, toTotalPoints);
        if(kvpLvPoints.size() < 2){
            //Lv. 200+
            return luds;
        }
        for(int i = 0; i < kvpLvPoints.size(); i++){
            if(i >= kvpLvPoints.size() - 1){
                //Finished
                break;
            }
            PairLevelPoints min = kvpLvPoints.get(i);
            PairLevelPoints max = kvpLvPoints.get(i + 1);
            int curr = fromTotalPoints > min.getPoints() && fromTotalPoints < max.getPoints() ? fromTotalPoints : min.getPoints();
            int target = toTotalPoints > min.getPoints() && toTotalPoints < max.getPoints() ? toTotalPoints : max.getPoints();
            luds.add(new LevelUpData(min, max, curr, target));
        }
        return luds;
    }

    public static String getUserSkillName(Context context, int forLevelNumber){
        int RANKS_PER_SKILL = 5;
        int rank = forLevelNumber % RANKS_PER_SKILL;
        int base = (forLevelNumber - rank) / RANKS_PER_SKILL;
        String roman;
        switch (rank){
            case 0 : roman = "I"; break;
            case 1 : roman = "II"; break;
            case 2 : roman = "III"; break;
            case 3 : roman = "IV"; break;
            case 4 : roman = "V"; break;
            default: roman = "?";
        }
        return getUserLevelSkillNames(context).get(base) + " - " + roman;
    }

    //region private

    static ArrayList<Integer> userLevelPointTargets;
    static ArrayList<String> userLevelSkillNames;

    /**
     * Lv       0 - 1 -  2 -  3 -  4 -  5 - etc.
     * @return [0, 101, 203, 306, 410, 515, etc. ....]
     */
    private static ArrayList<Integer> getUserLevelPointTargets(){
        if(userLevelPointTargets == null) {
            userLevelPointTargets = new ArrayList<>();
            userLevelPointTargets.add(0); //First level min
            for (int i = 1; i <= 200; i++) {
                userLevelPointTargets.add(userLevelPointTargets.get(i - 1) + (i * 5) + i);
            }
        }
        return userLevelPointTargets;
    }

    private static ArrayList<String> getUserLevelSkillNames(Context context){
        if(userLevelSkillNames == null){
            userLevelSkillNames = new ArrayList<>();
            String[] sa = context.getResources().getStringArray(R.array.user_level_skill_names);
            for(String s : sa){
                userLevelSkillNames.add(s);
            }
        }
        return userLevelSkillNames;
    }

    /**
     * Given a from (before level) and to (after level complete) user points total, returns an int pair array of relevant
     * user level number (key) & point thresholds (value) for levelling up. This will be a minimum of 2 pairs : the min
     * and max progress bar values for the current level. If the user is going to level up, an additional pair is returned
     * for each additional level. So you may have [minLv1, maxLv1, maxLv2]
     * @param fromPoints e.g. 300
     * @param toPoints   e.g. 310
     * @return e.g. [<2, 203>, <3, 306>, <4, 410>]
     */
    private static ArrayList<PairLevelPoints> getRelevantLevelUpLvPointThresholds(int fromPoints, int toPoints){
        ArrayList<PairLevelPoints> output = new ArrayList<>();
        //Get the one before from, after to, and any in between
        boolean gotBeforeFrom = false;
        ArrayList<Integer> arr = getUserLevelPointTargets();
        for(int lv = 0; lv < arr.size(); lv ++){
            int points = arr.get(lv);
            boolean passedMinLevel = points >= fromPoints;
            boolean beforeMaxLevel = points <= toPoints;
            if(passedMinLevel){
                if(!gotBeforeFrom) {
                    //before from
                    gotBeforeFrom = true;
                    if (lv > 0) {
                        output.add(new PairLevelPoints(lv - 1, getUserLevelPointTargets().get(lv - 1)));
                    }else {
                        output.add(new PairLevelPoints(0, 0));
                        continue;
                    }
                }
                if(beforeMaxLevel){
                    output.add(new PairLevelPoints(lv, points));
                }
            }else {
                continue; //Not reached level range yet
            }
            if(!beforeMaxLevel){
                output.add(new PairLevelPoints(lv, points));
                break;
            }
        }
        return output;
    }

    //endregion

    public static class PairLevelPoints {
        private int level;
        private int points;

        public PairLevelPoints(int level, int points) {
            this.level = level;
            this.points = points;
        }

        public int getLevel() {
            return level;
        }

        public int getPoints() {
            return points;
        }
    }

    public static class LevelUpData {

        private PairLevelPoints min;
        private PairLevelPoints max;
        private int curr;
        private int target;

        public LevelUpData(PairLevelPoints min, PairLevelPoints max, int curr, int target) {
            this.min = min;
            this.max = max;
            this.curr = curr;
            this.target = target;
        }

        //Adjusted methods adheres to where min of progress bar is 0
        public int getMaxAdjusted(){
            return this.max.getPoints() - this.min.getPoints();
        }

        public int getCurrAdjusted(){
            return this.curr - this.min.getPoints();
        }

        public int getTargetAdjusted(){
            return this.target - this.min.getPoints();
        }

        public int getNextLevelPointsAdjusted(){
            return this.max.points - this.min.getPoints();
        }

        //Each point takes 50 ms
        public long getDurationMs(){
            return (getTargetAdjusted() - getCurrAdjusted()) * 50;
        }

        public int getFromLevel(){
            return min.level;
        }

        public int getToLevel(){
            return max.level;
        }

        @Override
        public String toString() {
            return "LevelUpData{" +
                    "min=" + min.getPoints() +
                    ", max=" + max.getPoints() +
                    ", curr=" + curr +
                    ", target=" + target +
                    ", getMaxAdjusted()=" + getMaxAdjusted() +
                    ", getCurrAdjusted()=" + getCurrAdjusted() +
                    ", getTargetAdjusted()=" + getTargetAdjusted() +
                    ", getDurationMs()=" + getDurationMs() +
                    '}';
        }
    }

}
