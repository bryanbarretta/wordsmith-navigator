package bbgames.ie.wordsmithnavigator.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuGameLogs;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuTurnLog;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;

/**
 * Created by Bryan on 06/08/2016.
 */
public abstract class AdapterListViewCpuLogLevel extends BaseExpandableListAdapter{

    private Context context;
    private CpuGameLogs cpuGameLogs;

    public AdapterListViewCpuLogLevel(Context context, CpuGameLogs cpuGameLogs){
        this.context = context;
        this.cpuGameLogs = cpuGameLogs;
    }

    @Override
    public int getGroupCount() {
        return cpuGameLogs.getTurnLogs().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return cpuGameLogs.getTurnLogs().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View rowView, ViewGroup parent) {
        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_cpu_log_level, null);
        }

        int turn = groupPosition + 1;
        final CpuTurnLog turnLog = cpuGameLogs.getTurnLogs().get(turn);
        TextView txtTitle = (TextView)rowView.findViewById(R.id.txtTurn);
        Button btnExpandCollapse = (Button)rowView.findViewById(R.id.btnExpandCollapse);

        String answer = turnLog.getMapFieldValues().containsKey(CpuTurnLog.LogType.Answer) ?
                turnLog.getMapFieldValues().get(CpuTurnLog.LogType.Answer) : "???";

        txtTitle.setText(turn + ". \"" + answer + "\"");
        GraphicUtils.buttonColor(btnExpandCollapse, context.getResources().getColor(isExpanded ? R.color.red_700 : R.color.theme_navy_1b));
        btnExpandCollapse.setText(isExpanded ? "Collapse" : "Expand");
        btnExpandCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onExpandCollapsedPressed(groupPosition, isExpanded);
            }
        });
        return rowView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new LinearLayout(context);
        }
        int turn = groupPosition + 1;
        LinearLayout ll = (LinearLayout)convertView;
        ll.removeAllViews();
        ll.setOrientation(LinearLayout.VERTICAL);
        CpuTurnLog turnLog = cpuGameLogs.getTurnLogs().get(turn);
        for(CpuTurnLog.LogType logType : CpuTurnLog.LogType.values()){
            if(!turnLog.getMapFieldValues().containsKey(logType)){
                continue;
            }
            LinearLayout row = new LinearLayout(context);
            row.setOrientation(LinearLayout.HORIZONTAL);
            row.setWeightSum(5);
            row.addView(generateTextView(logType.toString(), 4, false));
            row.addView(generateTextView(turnLog.getMapFieldValues().get(logType), 1, true));
            ll.addView(row);
        }
        return ll;
    }

    private View generateTextView(final String txt, int weight, boolean clickable){
        final TextView t = new TextView(context);
        t.setTextColor(0xFF73fa75);
        t.setTypeface(Typeface.MONOSPACE);
        int p = (int) context.getResources().getDimension(R.dimen.padding_title_border);
        t.setPadding(p, p, p, p);
        t.setText(txt);
        t.setGravity(Gravity.LEFT | Gravity.TOP);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.weight = weight;
        t.setLayoutParams(lp);
        if(clickable) {
            t.setHorizontallyScrolling(true);
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        return t;
    }

    public abstract void onExpandCollapsedPressed(int groupPosition, boolean isExpanded);

}
