package bbgames.ie.wordsmithnavigator.Controls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.GraphicUtils;

/**
 * Created by Bryan on 06/08/2016.
 */
public abstract class AdapterListViewCpuLogLevelSelect extends BaseAdapter{

    private Context context;
    private ArrayList<String> levels;

    public AdapterListViewCpuLogLevelSelect(Context context, ArrayList<String> levels){
        this.context = context;
        this.levels = levels;
    }

    @Override
    public int getCount() {
        return levels.size();
    }

    @Override
    public Object getItem(int position) {
        return levels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {

        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_cpu_log_level_select, null);
        }

        TextView txtLevel = (TextView)rowView.findViewById(R.id.txtLevel);
        Button btnDelete = (Button)rowView.findViewById(R.id.btnDelete);
        Button btnView = (Button)rowView.findViewById(R.id.btnView);

        final String level = levels.get(position);

        txtLevel.setText(level);
        GraphicUtils.buttonColor(btnDelete, context.getResources().getColor(R.color.red_700));
        GraphicUtils.buttonColor(btnView, context.getResources().getColor(R.color.indigo_500));

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteSelected(level);
            }
        });
        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onViewSelected(level);
            }
        });

        return rowView;
    }

    public abstract void onDeleteSelected(String level);
    public abstract void onViewSelected(String level);
}
