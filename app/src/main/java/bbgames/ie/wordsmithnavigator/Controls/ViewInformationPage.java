package bbgames.ie.wordsmithnavigator.Controls;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

/**
 * 1920 x 1080... Ratio = 16:9 (where 1 = 120px)
 *
 * -------------------------------
 * |              |              |
 * |              |              |  50:50 img:text
 * |              |              |
 * |         1920 x 1080         |  Ideal img dimensions = 960:1080 [8:9]
 * |              |              |                         480:540
 * |              |              |
 * |              |              |
 * -------------------------------
 *
 */

public class ViewInformationPage {

    private ImageView image;
    private ScrollView scrollView;
    private TextView text;

    public ViewInformationPage(Context context, int resIdImage, int resIdString) {
        setup(context, resIdImage, resIdString);
    }

    private void setup(Context context, int resIdImage, int resIdString){
        image = new ImageView(context);
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        image.setImageDrawable(context.getResources().getDrawable(resIdImage));
        LinearLayout.LayoutParams lpImage = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        lpImage.weight = 4;
        lpImage.rightMargin = Util.getDimensValueInPixels(context, R.dimen.padding_4dp_border);
        image.setLayoutParams(lpImage);

        text = new TextView(context);
        text.setText(context.getString(resIdString));
        text.setTypeface(Fonts.defaultSerious(context));
        text.setTextSize(Util.getDimensValueInScaledDensityPixels(context, R.dimen.font_normal));
        text.setTextColor(context.getResources().getColor(R.color.white));

        scrollView = new ScrollView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = 6;
        scrollView.setLayoutParams(params);
        scrollView.addView(text);
    }

    public ImageView getImage() {
        return image;
    }

    public View getScrollableText() {
        return scrollView;
    }
}
