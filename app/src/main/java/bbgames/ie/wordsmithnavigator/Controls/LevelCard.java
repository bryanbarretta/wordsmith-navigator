package bbgames.ie.wordsmithnavigator.Controls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.GameObjects.Levels.LevelMapGenerator;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.Level;
import bbgames.ie.wordsmithnavigator.R;

/**
 * Created by Bryan on 06/08/2018.
 */

public class LevelCard extends LinearLayout {

    private Level level;
    private LinearLayout view;
    private TextView txtLevelNum;
    private ImageView imgDiamond, imgCpu, imgMap;
    private ImageView imgStar1, imgStar2, imgStar3;

    public LevelCard(Context context, Level level) {
        super(context);
        this.level = level;
        setup();
    }

    private void setup(){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = (LinearLayout) inflater.inflate(R.layout.card_level, null);
        addView(view);

        if(level.getLevelPlayerData() == null){
            findViewById(R.id.card).setBackground(getContext().getResources().getDrawable(R.drawable.shape_grey));
        }

        txtLevelNum = (TextView) view.findViewById(R.id.txtLevelNumber);
        imgDiamond = (ImageView) view.findViewById(R.id.imgDiamond);
        imgCpu = (ImageView) view.findViewById(R.id.imgCpu);
        imgMap = (ImageView) view.findViewById(R.id.imgMap);
        imgStar1 = (ImageView) view.findViewById(R.id.star1);
        imgStar2 = (ImageView) view.findViewById(R.id.star2);
        imgStar3 = (ImageView) view.findViewById(R.id.star3);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.weight = 1;
        setLayoutParams(lp);

        txtLevelNum.setText(level.getLevelStaticData().getId() + "");
        if(level.getLevelPlayerData() == null){
            imgStar1.setVisibility(GONE);
            imgStar2.setVisibility(GONE);
            imgStar3.setVisibility(GONE);
        }else {
            new LevelMapGenerator(level.getLevelStaticData()).GenerateMap(imgMap, null, level.getLevelStaticData().getHardcodedLetters().isEmpty());
            ArrayList<ImageView> stars = new ArrayList<ImageView>(){{add(imgStar1); add(imgStar2); add(imgStar3);}};
            for(int i = 0; i < 3; i++){
                if(i >= level.getLevelPlayerData().getMapIdStarUnlocked().size()){
                    stars.get(i).setVisibility(GONE);
                }else {
                    Boolean u = level.getLevelPlayerData().getMapIdStarUnlocked().get(i);
                    if(u != null && u == true) {
                        stars.get(i).setImageResource(R.drawable.ic_star);
                    }
                }
            }
            if(level.getLevelPlayerData().getMapIdDiamondUnlocked().isEmpty()){
                //Leave view invisible
            }else {
                imgDiamond.setVisibility(VISIBLE);
                int unlocked = 0;
                for(Boolean d : level.getLevelPlayerData().getMapIdDiamondUnlocked().values()){
                    unlocked += (d != null && d == true) ? 1 : 0;
                }
                if(unlocked == level.getLevelPlayerData().getMapIdDiamondUnlocked().size()){
                    imgDiamond.setImageResource(R.drawable.ic_diamond);
                }else if(unlocked > 0){
                    imgDiamond.setImageResource(R.drawable.ic_diamond_half);
                }
            }
            if(level.getLevelStaticData().getPlayerCount() > 1){
                imgCpu.setVisibility(VISIBLE);
            }
        }
    }

}
