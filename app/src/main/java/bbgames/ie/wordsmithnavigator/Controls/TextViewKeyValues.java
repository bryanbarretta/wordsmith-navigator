package bbgames.ie.wordsmithnavigator.Controls;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.LinearLayout;
import android.widget.TextView;

import bbgames.ie.wordsmithnavigator.Fonts;
import bbgames.ie.wordsmithnavigator.R;
import bbgames.ie.wordsmithnavigator.Util.Util;

public class TextViewKeyValues extends LinearLayout {

    private static class TextPiece {

        private String text;
        private Typeface font;
        private int color;
        private Integer bgColor;
        private int additionalTypefaceStyle; //e.g. Typeface.BOLD_ITALIC / Typeface.BOLD / Typeface.ITALIC / Typeface.NORMAL
        private float fontSize;

        public TextPiece(String text, Typeface font, int color, Integer bgColor, int additionalTypefaceStyle, float fontSize) {
            this.text = text;
            this.font = font;
            this.color = color;
            this.bgColor = bgColor;
            this.additionalTypefaceStyle = additionalTypefaceStyle;
            this.fontSize = fontSize;
        }
    }

    public static class TextPieceBuilder{

        private Context context;
        private String text;
        private Typeface font;
        private Integer color;
        private Integer bgColor;
        private Float fontSize;
        private Integer additionalTypefaceStyle; //e.g. Typeface.BOLD_ITALIC / Typeface.BOLD / Typeface.ITALIC / Typeface.NORMAL

        public TextPieceBuilder(Context context){
            this.context = context;
        }

        public TextPieceBuilder text(String text){
            this.text = text;
            return this;
        }

        public TextPieceBuilder font(Typeface font){
            this.font = font;
            return this;
        }

        public TextPieceBuilder fontSize(Float fontSize){
            this.fontSize = fontSize;
            return this;
        }

        public TextPieceBuilder color(Integer color){
            this.color = color;
            return this;
        }

        public TextPieceBuilder bgColor(Integer bgColor){
            this.bgColor = bgColor;
            return this;
        }

        public TextPieceBuilder additionalTypefaceStyle(Integer additionalTypefaceStyle){
            this.additionalTypefaceStyle = additionalTypefaceStyle;
            return this;
        }

        private TextPiece build(){
            return new TextPiece(this.text,
                    this.font != null ? this.font : Fonts.defaultFun(context),
                    this.color != null ? this.color : context.getResources().getColor(R.color.white),
                    this.bgColor,
                    this.additionalTypefaceStyle,
                    this.fontSize != null ? this.fontSize : Util.getDimensValueInScaledDensityPixels(context, R.dimen.font_normal));
        }

    }

    public static class Builder {

        private Context context;
        private TextPieceBuilder tpKey, tpValue1, tpValue2;

        public Builder(Context context){
            this.context = context;
        }

        public TextPieceBuilder key(){
            if(tpKey == null){
                tpKey = new TextPieceBuilder(context);
            }
            return tpKey;
        }

        public TextPieceBuilder value1(){
            if(tpValue1 == null){
                tpValue1 = new TextPieceBuilder(context);
            }
            return tpValue1;
        }

        public TextPieceBuilder value2(){
            if(tpValue2 == null){
                tpValue2 = new TextPieceBuilder(context);
            }
            return tpValue2;
        }

        public TextViewKeyValues build(){
            TextPiece tKey = tpKey == null ? null : tpKey.build();
            TextPiece tValue1 = tpValue1 == null ? null : tpValue1.build();
            TextPiece tValue2 = tpValue2 == null ? null : tpValue2.build();
            return new TextViewKeyValues(context, tKey, tValue1, tValue2);
        }

    }

    TextPiece tKey;
    TextPiece tValue1;
    TextPiece tValue2;

    private TextViewKeyValues(Context context, TextPiece tKey, TextPiece tValue1, TextPiece tValue2) {
        super(context);
        this.tKey = tKey;
        this.tValue1 = tValue1;
        this.tValue2 = tValue2;
        setup(tKey, tValue1, tValue2);
        refresh(tKey, tValue1, tValue2);
    }


    private TextView textViewKey;
    private TextView textViewValue1;
    private TextView textViewValue2;

    private void setup(TextPiece tKey, TextPiece tValue1, TextPiece tValue2){
        inflate(getContext(), R.layout.textview_key_values, this);
        textViewKey = (TextView) findViewById(R.id.llKey);
        textViewValue1 = (TextView) findViewById(R.id.llValue1);
        textViewValue2 = (TextView) findViewById(R.id.llValue2);
        if(tKey.bgColor != null){
            textViewKey.setBackgroundColor(tKey.bgColor);
        }
        if(tValue1.bgColor != null){
            textViewValue1.setBackgroundColor(tValue1.bgColor);
        }
        if(tValue2.bgColor != null){
            textViewValue2.setBackgroundColor(tValue2.bgColor);
        }
    }

    private void refresh(TextPiece tKey, TextPiece tValue1, TextPiece tValue2){
        textViewKey.setText(tKey.text);
        textViewKey.setTextColor(tKey.color);
        textViewKey.setTypeface(tKey.font, tKey.additionalTypefaceStyle);
        textViewKey.setTextSize(tKey.fontSize);

        if(tValue1 == null){
            textViewValue1.setVisibility(GONE);
        }else {
            textViewValue1.setText(tValue1.text);
            textViewValue1.setTextColor(tValue1.color);
            textViewValue1.setTypeface(tValue1.font, tValue1.additionalTypefaceStyle);
            textViewValue1.setTextSize(tValue1.fontSize);
        }
        if(tValue2 == null){
            textViewValue2.setVisibility(GONE);
        }else {
            textViewValue2.setText(tValue2.text);
            textViewValue2.setTextColor(tValue2.color);
            textViewValue2.setTypeface(tValue2.font, tValue2.additionalTypefaceStyle);
            textViewValue2.setTextSize(tValue2.fontSize);
        }

        LayoutParams lp1 = (LayoutParams) textViewKey.getLayoutParams();
        LayoutParams lp2 = (LayoutParams) textViewValue1.getLayoutParams();
        LayoutParams lp3 = (LayoutParams) textViewValue2.getLayoutParams();
        if(tValue1.text == null && tValue2.text == null){
            lp1.weight = 5.0f;
            textViewKey.setLayoutParams(lp1);
        }else if(tValue2.text == null){
            lp1.weight = 2.5f;
            lp2.weight = 2.5f;
            textViewKey.setLayoutParams(lp1);
            textViewValue1.setLayoutParams(lp1);
        }else if(tValue1.text == null){
            lp1.weight = 4.0f;
            lp3.weight = 1.0f;
            textViewKey.setLayoutParams(lp1);
            textViewValue2.setLayoutParams(lp3);
        }
    }

}
