package bbgames.ie.wordsmithnavigator.Controls;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import bbgames.ie.wordsmithnavigator.*;
import bbgames.ie.wordsmithnavigator.Activities.GameActivity;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;

/**
 * Created by Bryan on 06/08/2016.
 */
public class AdapterListViewHints extends BaseAdapter {

    private String hexColorBlue, hexColorGreen, hexColorWhite;
    private GameActivity gameActivity;
    private ArrayList<HintWord> words;
    private int selectedIndex = -1;
    private Typeface tfText;

    public AdapterListViewHints(GameActivity gameActivity){
        this.gameActivity = gameActivity;
        this.words = new ArrayList<>();
        this.tfText = Fonts.defaultMonospace(gameActivity);
    }

    public void setWords(ArrayList<HintWord> words){
        this.words = words;
        this.notifyDataSetChanged();
    }

    public void setSelectedIndex(int selectedIndex){
        this.selectedIndex = selectedIndex;
        this.notifyDataSetChanged();
    }

    public HintWord getSelectedWord() {
        return selectedIndex >= 0 ? words.get(selectedIndex) : null;
    }

    @Override
    public int getCount() {
        return words.size();
    }

    @Override
    public Object getItem(int i) {
        return words.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int index, View rowView, ViewGroup viewGroup) {
        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater) gameActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_hint_word, null);
        }
        rowView.setBackgroundColor(selectedIndex == index ?
                gameActivity.getResources().getColor(R.color.blue_gray_6) :
                Color.TRANSPARENT);

        final HintWord word = words.get(index);
        TextView txtWord = (TextView) rowView.findViewById(R.id.txtName);
        TextView txtPoints = (TextView)rowView.findViewById(R.id.txtPoints);
        TextView txtLength = (TextView)rowView.findViewById(R.id.txtLength);

        txtWord.setTypeface(tfText);
        txtLength.setText("" + word.toString().length());
        txtPoints.setText("" + word.getPoints());

        StringBuilder html = new StringBuilder();
        for(int i = 0; i < word.getLetters().size(); i++){
            HintLetter c = word.getLetters().get(i);
            Character displayCharacter = word.getDictionaryEntry().getDisplayWord() == null ? c.getLetter() :
                    word.getDictionaryEntry().getDisplayWord().charAt(i);
            html.append("<font color=" + getCharacterColor(c) + ">" + displayCharacter + "</font>");
        }
        txtWord.setText(Html.fromHtml(html.toString()));

        return rowView;
    }

    private String getCharacterColor(HintLetter c){
        if(c.isFocusTile()) {
            if(hexColorBlue == null) {
                hexColorBlue = gameActivity.getResources().getString(0 + R.color.blue_100).replace("#ff", "#");
            }
            return hexColorBlue;
        }
        else if(c.isLockedOnBoard()) {
            if (hexColorWhite == null) {
                hexColorWhite = gameActivity.getResources().getString(0 + R.color.white).replace("#ff", "#");
            }
            return hexColorWhite;
        }
        else {
            if(hexColorGreen == null) {
                hexColorGreen = gameActivity.getResources().getString(0 + R.color.green_200).replace("#ff", "#");
            }
            return hexColorGreen;
        }
    }

}
