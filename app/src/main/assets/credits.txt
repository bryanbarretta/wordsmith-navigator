----------------------------
--          ICONS         --
----------------------------

All sourced images fall under the CC0 Creative Commons (Free for commercial use, No attribution required). Websites and links provided below.

Pixabay
"Images and Videos on Pixabay are released under Creative Commons CC0. To the extent possible under law, uploaders of Pixabay have waived their copyright and related or neighboring rights to these Images and Videos. You are free to adapt and use them for commercial purposes without attributing the original author or source. Although not required, a link back to Pixabay is appreciated."

- https://pixabay.com/en/crate-wood-box-archive-1143427/
- https://pixabay.com/en/goal-closed-input-door-double-door-819975/

Snipstock
"Images on Snipstock are released under Creative Commons CC0. Images are sourced from the public domain where uploaders have waived their copyright and related or neighboring rights to these Images. You are free to adapt and use them for commercial purposes without attributing the original author or source. Although not required, a link back to Snipstock is appreciated."

- https://www.snipstock.com/image/png-images-dynamite-5-png-32676

Flaticon
"Our contents may be used for free inserting the attribution:
 - In personal and commercial projects.
 - In websites, presentations, printable elements and so on.
 - Totally or partially, using the full design or modifying it as well as integrated in your own designs."

- [User] Icon made by user_149071 from www.flaticon.com
- [Camera] Icon made by Linh Pham from www.flaticon.com
- [Directions] Icon made by Dave Gandy from www.flaticon.com
