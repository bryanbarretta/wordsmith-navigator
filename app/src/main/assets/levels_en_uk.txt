#   [1] = Level Number. Any further text on this line becomes hardcoded letters. i.e. the players hud letters in this order on this level
#
#   Special Keys:
#   # = Sentences beginning with # are ignored as comments
#   [ = This key is used to indicate a condition or something unrelated to the map layout.
#           [7] = Level number, in this example it means Level 7. The name can come after this
#           [*] = Condition required to achieve a trophy
#           [!] [3,4]->[8,8] ...would mean the detonator at 3,4 explodes crate at 8,8 (and touching crates)
#           [%] [3,4]->[30]  ...would mean the gate at 3,4 needs 30 points to open
#           [S] 30 ...would mean 30 seconds per turn (running out of time loses the match). If this is not included clock counts up.
#           [P] [2|3,0][3|5,2] ...would mean 2x Points bonus at 3,0 & 3x Points bonus at 5,2
#           [T] [5|3,4] ...would mean 5 second bonus at 3,4 (i.e. -5 seconds)
#           [M] [1|2,7] ...would mean 1 move bonus at 2,7 (i.e. next move does not increment move counter)
#           [L] [3,7] ...would mean letter sack jumble at 3,7
#
#   NOTE: If adding a new key, update loadLevels() in LevelManager
#
#   Map Layout Keys:
#   0 = starting focus
#   1 = finish zone
#   2 = double points tile
#   3 = triple points tile
#   . = regular tile
#   - = wall tile
#   ^ = grey diamond
#   ! = detonator
#   + = wall dynamite
#   $ = CPU Starting focus
#   % = gate
#   = = gate

[1]

---------
-s0.....-
-t......-
-a-11111-
-r......-
-t......-
---------

[*] <= 2 moves
[*] <= 10 sec
[*] >= 25 points

[2]

---------
-b0.!...-
-o......-
-m......-
-b++++++-
-s+11111-
---------

[*] <= 2 moves
[*] <= 10 sec
[*] >= 25 points
[!] [4,1]->[2,5]

[3]

----------
-s%111111-
-c--------
-o.......-
-r0......-
-e.......-
--.......-
-........-
-........-
----------

[*] <= 3 moves
[*] <= 20 sec
[*] >= 40 points
[%] [2,1]->[15]

[4]

----------
-t.......-
-i.......-
-m.......-
-e.......-
-r0...----
--....111-
-.....111-
-.....111-
----------

[*] <= 3 moves
[*] <= 12 sec
[*] >= 50 points
[S] = 30

[5]

----T111111
----I------
----L-.....
----E-.....
r0...-.....
o-----.....
g..........
u..........
e..........

[*] <= 2 moves
[*] <= 30 sec
[*] >= 60 points

[6] !aiouy

-e-11111-
-x-11111-
-c......-
-l0.....-
-u......-
-d......-
-e......-

[*] <= 2 moves
[*] <= 10 sec
[*] >= 25 points

[7]

b......
r......
i......
d......
g0.....
e......
---=---
....111
....111
....111

[*] <= 3 moves
[*] <= 20 sec
[*] >= 40 points
[=] [3,6]->[5]

[8]

p......
o......
w......
e......
r0.....
---....
....111
^...111
....111

[*] <= 3 moves
[*] <= 20 sec
[*] >= 40 points
[P] [2|3,0][3|5,2]
[T] [5|3,4]
[M] [1|2,7]
[L] [3,7]

[9]

----rogue----
......0......
.............
.............
.............
.........-+++
....TILES-111
....--!---111

[!] [6,7]->[10,5]
[*] <= 7 moves
[*] <= 70 sec
[*] >= 70 points

[10] ditchnorms

------N--
------A--
------V--
w0......1
------G--
------A--
------T--
------O--
------R--

[*] <= 1 moves
[S] = 30

[11]

-------......
layout-....!.
.....0-.--...
..........-..
......-I-----
......-N-...^
++++++-G.....
111111---....
111111---....

[!] [11,1]->[0,6]
[*] <= 6 moves
[*] <= 35 sec
[*] >= 60 points

[12]

locked-....
..0...%....
......-....
......-....
......--%--
......-1111
......-1111
......-1111

[%] [6,1]->[20]
[%] [8,4]->[40]
[*] <= 6 moves
[*] <= 30 sec
[*] >= 70 points

[13]

--------enemy--
......b-....$..
......o-.......
......r------..
......d-.......
......e-.......
.....0r-..-----
.....---.......
.....-C-.......
........-----..
.....-O-.......
.....-S-.......
.....-S1111----
.....-I1111111-
.....-N1111111-
.....-G1111111-

[*] <= 4 moves
[*] <= 45 sec
[*] >= 65 points

[14]

the
0..
111

[*] <= 2 moves
[*] <= 10 sec
[*] >= 25 points

#patient-clueless
#0......-.......$
#.......-........
#.......-.......-
#.....++----....!
#.....+^-1+-.....
#.....++-++-.....
#1111111-........
#1111111-........
#1111111-........
#[!] [15,4]->[6,4]
#[!] [15,4]->[9,5]
#[*] <= 3 moves
#[*] <= 20 sec
#[*] >= 60 points
#[S] = 30