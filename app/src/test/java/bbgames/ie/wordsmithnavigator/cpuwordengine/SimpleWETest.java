package bbgames.ie.wordsmithnavigator.cpuwordengine;

import org.junit.Test;
import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.cpuwordengine.base.BaseWETest;

/**
 * NOTE: To fix "missing manifest"/null dictionary error:
 *       Click dropdown left of Run button
 *       Edit Configurations...
 *       Under Configuration tab, click File button to right of Working Directory field
 *       Click MODULE_DIR and run tests again
 * Created by Bryan on 04/05/2017.
 */

public class SimpleWETest extends BaseWETest {

    @Override
    public boolean isTestAssertionEnabled() {
        return true;
    }

    @Override
    public boolean isPrintTestResultsEnabled() {
        return false;
    }

    @Override
    public boolean isLogCpuLoggerViaSystemOutPrintEnabled() {
        return false;
    }

    @Test
    public void testSimpleEast() throws InterruptedException {
        testSimpleEast(AIDifficulty.VERY_EASY, "some");
        testSimpleEast(AIDifficulty.EASY,      "some");
        testSimpleEast(AIDifficulty.NORMAL,    "tongue");
        testSimpleEast(AIDifficulty.HARD,      "tongue");
        testSimpleEast(AIDifficulty.VERY_HARD, "toughen");
        testSimpleEast(AIDifficulty.IMPOSSIBLE,"toughen");
    }

    @Test
    public void testSimpleSouth() throws InterruptedException {
        testSimpleSouth(AIDifficulty.VERY_EASY, "some");
        testSimpleSouth(AIDifficulty.EASY,      "some");
        testSimpleSouth(AIDifficulty.NORMAL,    "samuel");
        testSimpleSouth(AIDifficulty.HARD,      "samuel");
        testSimpleSouth(AIDifficulty.VERY_HARD, "samuel");
        testSimpleSouth(AIDifficulty.IMPOSSIBLE,"shoeman");
    }

    @Test
    public void testSimpleWest() throws InterruptedException {
        testSimpleWest(AIDifficulty.VERY_EASY, "has");
        testSimpleWest(AIDifficulty.EASY,      "means");
        testSimpleWest(AIDifficulty.NORMAL,    "amount");
        testSimpleWest(AIDifficulty.HARD,      "amount");
        testSimpleWest(AIDifficulty.VERY_HARD, "hangout");
        testSimpleWest(AIDifficulty.IMPOSSIBLE,"hangout");
    }

    @Test
    public void testSimpleNorth() throws InterruptedException {
        testSimpleNorth(AIDifficulty.VERY_EASY, "has");
        testSimpleNorth(AIDifficulty.EASY,      "means");
        testSimpleNorth(AIDifficulty.NORMAL,    "holmes");
        testSimpleNorth(AIDifficulty.HARD,      "angelus");
        testSimpleNorth(AIDifficulty.VERY_HARD, "manholes");
        testSimpleNorth(AIDifficulty.IMPOSSIBLE,"manholes");
    }

    //region methods

    private void testSimpleEast(AIDifficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-T0.............1-");
        layout.add("-E..............1-");
        layout.add("-S..............1-");
        layout.add("-T..............1-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(1, 0, 't', true, false));
        playedWord.addLetter(new HintLetter(1, 1, 'e', true, false));
        playedWord.addLetter(new HintLetter(1, 2, 's', true, false));
        playedWord.addLetter(new HintLetter(1, 3, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                LevelUtils.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    private void testSimpleSouth(AIDifficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-TEST-");
        layout.add("-0...-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-1111-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(0, 0, 't', true, false));
        playedWord.addLetter(new HintLetter(1, 0, 'e', true, false));
        playedWord.addLetter(new HintLetter(2, 0, 's', true, false));
        playedWord.addLetter(new HintLetter(3, 0, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                LevelUtils.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    private void testSimpleWest(AIDifficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-1.............0T-");
        layout.add("-1..............E-");
        layout.add("-1..............S-");
        layout.add("-1..............T-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(16, 0, 't', true, false));
        playedWord.addLetter(new HintLetter(16, 1, 'e', true, false));
        playedWord.addLetter(new HintLetter(16, 2, 's', true, false));
        playedWord.addLetter(new HintLetter(16, 3, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                LevelUtils.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    private void testSimpleNorth(AIDifficulty difficulty, String expectedAnswer) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-1111-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-....-");
        layout.add("-0...-");
        layout.add("-TEST-");
        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(0, 13, 't', true, false));
        playedWord.addLetter(new HintLetter(1, 13, 'e', true, false));
        playedWord.addLetter(new HintLetter(2, 13, 's', true, false));
        playedWord.addLetter(new HintLetter(3, 13, 't', true, false));
        HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                LevelUtils.getFirstXYof(layout, TileCodes.EndZone.getCharacter()),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer));
    }

    //endregion
}
