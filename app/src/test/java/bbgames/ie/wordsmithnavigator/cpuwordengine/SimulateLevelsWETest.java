package bbgames.ie.wordsmithnavigator.cpuwordengine;

import org.andengine.util.TextUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.DetonatorLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.GateLockPoints;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.Level;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelFactory;
import bbgames.ie.wordsmithnavigator.GameObjects.Misc.LetterSack;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;
import bbgames.ie.wordsmithnavigator.TestedLevelSummary;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.LogUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;
import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;
import bbgames.ie.wordsmithnavigator.cpuwordengine.base.BaseWETest;

/**
 * This test suite will go through every level and ensure the CPU won't get stuck.
 */
public class SimulateLevelsWETest extends BaseWETest {

    private static final boolean LOG_CPU_LOGGER_LOGIC = true;

    ArrayList<Level> levels;

    private static final String LOG_ASTERISKS = "****************************************************************************";
    private static final String LOG_TICKS     = "✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓";
    private static final String LOG_FAILED_LV = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    ArrayList<TestedLevelSummary> levelSummaries;

    @Test
    public void testLevels() throws InterruptedException {
        System.out.println("Preparing...");
        levelSummaries = new ArrayList<>();
        LevelFactory lf = new LevelFactory(activity.getAssets(), Util.getApplicationManager(activity));
        levels = lf.getLevels(1, lf.getTotalLevelCount());
        ArrayList<AIDifficulty> difficultiesToTest = new ArrayList<AIDifficulty>(){
            {add(AIDifficulty.VERY_EASY);}
            {add(AIDifficulty.EASY);}
            {add(AIDifficulty.NORMAL);}
            {add(AIDifficulty.HARD);}
            {add(AIDifficulty.VERY_HARD);}
            {add(AIDifficulty.IMPOSSIBLE);}
        };
        int levelToTest = -1; //specify the level num tested. -1 will test all levels
        ArrayList<PlayerType> playersToTest = new ArrayList<PlayerType>(){
            {add(PlayerType.USER);}
            {add(PlayerType.CPU);}
        };
        logTestPreview(levelToTest, levels, difficultiesToTest, playersToTest);
        executeTestLevels(difficultiesToTest, playersToTest, levelToTest);
        logTestResults();
    }

    //region private

    private void executeTestLevels(ArrayList<AIDifficulty> difficultiesToTest, ArrayList<PlayerType> playersToTest, int levelToTest){
        for(Level level : levels){
            boolean levelEvaluated = false;
            for(PlayerType player : playersToTest) {
                for(AIDifficulty difficulty : difficultiesToTest) {
                    if (levelToTest > 0) {
                        if(level.getLevelStaticData().getId() == levelToTest) {
                            levelEvaluated = executeTestSpecificLevel(difficulty, player, level);
                        }else {
                            continue;
                        }
                    }else {
                        levelEvaluated = executeTestSpecificLevel(difficulty, player, level);
                    }
                }
            }
            if(levelEvaluated) {
                logBreak();
            }
        }
    }

    private boolean executeTestSpecificLevel(AIDifficulty difficulty, PlayerType player, Level level){
        ArrayList<String> layoutModified = new ArrayList<>(level.getLevelStaticData().getLayoutOriginal());
        if(!LevelUtils.getLevelAsString(layoutModified).contains(player.getTileCode().getCharacter().toString())){
            return false;
        }
        logLevelTitle(level, difficulty, player);
        String logGateInfo = "";
        if(!level.getLevelStaticData().getPointsLocks().isEmpty()){
            for(GateLockPoints g : level.getLevelStaticData().getPointsLocks()){
                logGateInfo += (logGateInfo.isEmpty() ? "" : " | ") + g.getOriginalPoints();
            }
        }
        int maxNumLetters = 10;
        int[] currentXY = LevelUtils.getFirstXYof(layoutModified, player.getTileCode());
        LetterSack letterSack = new LetterSack(level.getLevelStaticData(), 204963L); //Got nice seed running testLetterSacks
        String hudLetters = "";
        for(int i = 0; i < maxNumLetters; i++){
            hudLetters += letterSack.getLetterBalancingVowelsAndConsonants(maxNumLetters - i,
                    Util.getConsonantsCount(hudLetters),
                    Util.getVowelsCount(hudLetters));
        }
        log(difficulty, player, level.getLevelStaticData().getId(), String.format("Beginning test: {hud: %s}", hudLetters));
        HintWord startingWord = LevelUtils.getInitialBoardWord(databaseGateway, currentXY, layoutModified);
        ArrayList<HintWord> playedWords = new ArrayList<HintWord>(){
            {add(startingWord);}
        };
        int[] targetXY = LevelUtils.getOptimalTargetXY(layoutModified, currentXY);
        SessionStatistics sessionStatistics = new SessionStatistics(player.ordinal());
        boolean forfeited = false;
        while(true){
            answer = getNextCPUWord(playedWords, layoutModified, targetXY, hudLetters, difficulty);
            if(answer == null){
                log(difficulty, player, level.getLevelStaticData().getId(),
                        String.format("Unable to complete level, FORFEIT (Points: %s, Words: %s)\n%s\n%s",
                        sessionStatistics.getTotalPoints()+"", sessionStatistics.getSubmittedWords().size()+"", LevelUtils.getLevelAsString(layoutModified).trim(), LOG_FAILED_LV));
                forfeited = true;
                break;
            }
            layoutModified = LevelUtils.applyHintWordToLevel(layoutModified, answer);
            for(HintLetter l : answer.getLetters()){
                l.setLockedOnBoard(true);
                l.setFocusTile(false);
            }
            playedWords.add(answer);
            sessionStatistics.addWord(answer.toString(), Util.getPointsForWord(answer.toString()));
            log(difficulty, player, level.getLevelStaticData().getId(), String.format("Playing '%s':\t[%d,%d] -> [%d,%d]. Points: %s.%s",
                    answer.toString(),
                    answer.getFirstLetter().getXY()[0], answer.getFirstLetter().getXY()[1],
                    answer.getLastLetter().getXY()[0], answer.getLastLetter().getXY()[1],
                    sessionStatistics.getTotalPoints()+"",
                    logGateInfo.isEmpty() ? "" : " Gate Points: [" + logGateInfo + "]"));
            if(LevelUtils.isWordOnEndzone(level.getLevelStaticData().getLayoutOriginal(), answer) != null){
                log(difficulty, player, level.getLevelStaticData().getId(), String.format("Level complete (Points: %s, Words: %s)\n%s\n%s",
                        sessionStatistics.getTotalPoints()+"", sessionStatistics.getSubmittedWords().size()+"", LevelUtils.getLevelAsString(layoutModified).trim(), LOG_TICKS));
                break;
            }
            handleUnderlyingSpecialTiles(level, layoutModified);
            handleSubmittedWordEvent(level, layoutModified, sessionStatistics);
            handleRogueTiles(layoutModified, playedWords);
            targetXY = LevelUtils.getOptimalTargetXY(layoutModified, answer.getLastLetter().getXY());
        }
        levelSummaries.add(new TestedLevelSummary(level.getLevelStaticData().getId(), player, difficulty, sessionStatistics, forfeited));
        return true;
    }

    private void handleUnderlyingSpecialTiles(Level level, ArrayList<String> layoutModified){
        int[] xyCoveredDetonatory = LevelUtils.isWordOnDetonator(level.getLevelStaticData().getLayoutOriginal(), answer);
        if(xyCoveredDetonatory != null){
            for(int i = 0; i < level.getLevelStaticData().getDetonatorLinks().size(); i++){
                DetonatorLink link = level.getLevelStaticData().getDetonatorLinks().get(i);
                if(Util.equal(xyCoveredDetonatory, link.getXyDetonator())){
                    //Change crates to maptiles
                    for(int[] xyCrate : link.getXyCrates()){
                        LevelUtils.updateLayout(level.getLevelStaticData().getLayoutOriginal(),
                                layoutModified, xyCrate, TileCodes.MapTile, false);
                    }
                }
            }
        }
    }

    private void handleSubmittedWordEvent(Level level, ArrayList<String> layoutModified, SessionStatistics sessionStatistics) {
        for(GateLockPoints g : level.getLevelStaticData().getPointsLocks()){
            if(sessionStatistics.getTotalPoints() >= g.getOriginalPoints()){
                if(LevelUtils.getCharacterAt(layoutModified, g.getXYGate()).equals(TileCodes.Gate.getCharacter())){
                    LevelUtils.updateLayout(level.getLevelStaticData().getLayoutOriginal(), layoutModified, g.getXYGate(),
                            TileCodes.MapTile, false);
                }
            }
        }
    }

    private void handleRogueTiles(ArrayList<String> layoutModified, ArrayList<HintWord> playedWords) {
        for(HintLetter l : answer.getLetters()){
            ArrayList<int[]> touchingRogueTiles = LevelUtils.getXYsOfTouchingRogueTilesRecursively(layoutModified, l.getXY());
            if(touchingRogueTiles != null && !touchingRogueTiles.isEmpty()){
                for(int[] xyRogueTile : touchingRogueTiles){
                    ArrayList<HintWord> words = LevelUtils.getWordsOfBoardLetter(databaseGateway, layoutModified, xyRogueTile, answer);
                    for(HintWord word : words){
                        if(!playedWords.contains(word)){
                            playedWords.add(word);
                        }
                    }
                }
            }
        }
    }

    private void log(AIDifficulty difficulty, PlayerType playerType, int levelId, String msg){
        System.out.println(String.format("[%s | %s | %s]: %s",
                difficulty.name(),
                playerType.name(),
                levelId+"",
                msg));
    }

    private void logLevelTitle(Level level, AIDifficulty difficulty, PlayerType player){
        String title = "Simulating level [" + level.getLevelStaticData().getId() + "] on " + difficulty.name() + " for " + player.name();
        System.out.println(LogUtils.getBlockWithText('*', title));
        System.out.println(LevelUtils.getLevelAsString(level.getLevelStaticData().getLayoutOriginal()).trim());
    }

    private void logTestPreview(int levelToTest, ArrayList<Level> levels, ArrayList<AIDifficulty> difficultiesToTest, ArrayList<PlayerType> playersToTest){
        System.out.println("Testing " + (levelToTest < 1 ? ("[" + levels.size() + "] levels") : ("level [" + levelToTest + "]") ));

        System.out.println("Testing on " + difficultiesToTest.size() + " difficulties:");
        for(AIDifficulty d : difficultiesToTest){
            System.out.println(" - " + d.name());
        }

        System.out.println("Testing for " + playersToTest.size() + " player types:");
        for(PlayerType p : playersToTest){
            System.out.println(" - " + p.name());
        }

        logBreak();
    }

    private void logTestResults() {
        StringBuilder sb = new StringBuilder();
        System.out.println(LogUtils.getBlockWithText('~', "End of test summary"));
        int prevLevel = 0;
        for(TestedLevelSummary l : levelSummaries){
            if (l.getId() != prevLevel){
                prevLevel = l.getId();
                sb.append("\n\nLevel " + String.format("%-2s", l.getId()) + " Difficulty Status    | Points | Turns | AvgPoints | AvgLength | All words played");
            }
            sb.append(String.format("\n(%s | %s) %s | %s | %s | %s | %s | %s",
                    l.getPlayerType().name(),
                    String.format("%-10s", l.getDifficulty().name()),
                    l.isForfeited() ? "FORFEITED" : "completed",
                    String.format("%-6s", l.getSessionStatistics().getTotalPoints()),
                    String.format("%-5s", l.getSessionStatistics().getSubmittedWords().size()),
                    String.format("%-9s", new DecimalFormat("0.0").format(l.getSessionStatistics().getAverageWordPoints())),
                    String.format("%-9s", new DecimalFormat("0.0").format(l.getSessionStatistics().getAverageWordLength())),
                    TextUtils.join(", ", l.getSessionStatistics().getSubmittedWords())));
        }
        System.out.println(sb.toString());
    }

    private void logBreak(){
        for(int i = 0; i < 3; i++) {
            System.out.println();
        }
    }

    //endregion

    @Override
    public boolean isTestAssertionEnabled() {
        return false;
    }

    @Override
    public boolean isPrintTestResultsEnabled() {
        return false;
    }

    @Override
    public boolean isLogCpuLoggerViaSystemOutPrintEnabled() {
        return LOG_CPU_LOGGER_LOGIC;
    }

    @Test
    @Ignore //Run this to find a good seed for nice hud letters
    public void testLettersacks() throws InterruptedException {
        LevelFactory lf = new LevelFactory(activity.getAssets(), Util.getApplicationManager(activity));
        levels = lf.getLevels(1, lf.getTotalLevelCount() - 2);
        for(long seed = 200000L; seed < 205000L; seed ++) {
            LetterSack letterSack = new LetterSack(levels.get(1).getLevelStaticData(), seed);
            String hudLetters = "";
            for (int i = 0; i < 10; i++) {
                hudLetters += letterSack.getLetterBalancingVowelsAndConsonants(10 - i,
                        Util.getConsonantsCount(hudLetters),
                        Util.getVowelsCount(hudLetters));
            }
            if(hudLetters.contains("r") && hudLetters.contains("e") && hudLetters.contains("d") &&
                    hudLetters.contains("o") && hudLetters.contains("s") ) {
                log(AIDifficulty.EASY, PlayerType.USER, 1, "seed " + seed + " = " + hudLetters);
            }
        }
    }

    @Test
    @Ignore //Run this to find the output from querying the dictionary db (Im using this for debugging)
    public void testRawSQLagainstDictionaryDb(){
        String sql = "SELECT 2, Word, DisplayWord, Frequency FROM Dictionary WHERE Word IN ('yearsp', 'yearsa', 'yearsr', 'yearss', 'yearsd', 'yearst', 'yearse', 'yearsi', 'yearso') \n" +
                "UNION\n" +
                "SELECT 4, Word, DisplayWord, Frequency FROM Dictionary WHERE Word IN ('p', 'a', 'r', 's', 'd', 't', 'e', 'i', 'o')";
        ArrayList<DictionaryEntry> output = databaseGateway.getDictionaryEntries(sql);
        System.out.println("--- Output from testRawSQLagainstDictionaryDb ---");
        System.out.println("Found " + output.size() + " words...");
        for(DictionaryEntry d : output){
            System.out.println(" - " + d.getWord());
        }
    }
}
