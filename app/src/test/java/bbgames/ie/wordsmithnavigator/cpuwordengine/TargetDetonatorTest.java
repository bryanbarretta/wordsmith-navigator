package bbgames.ie.wordsmithnavigator.cpuwordengine;

import org.junit.Test;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.Constants.TileCodes;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintLetter;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.cpuwordengine.base.BaseWETest;

public class TargetDetonatorTest extends BaseWETest {

    @Override
    public boolean isTestAssertionEnabled() {
        return false;
    }

    @Override
    public boolean isPrintTestResultsEnabled() {
        return false;
    }

    @Override
    public boolean isLogCpuLoggerViaSystemOutPrintEnabled() {
        return false;
    }

    @Test
    public void testLevelTrustIssuesPart1() throws InterruptedException {
        //Test CPU is more aggressive when aiming for a detonator
        testLevelTrustIssuesPart1(AIDifficulty.VERY_EASY, true, "shame", "er");
        testLevelTrustIssuesPart1(AIDifficulty.EASY,      true, "laugh", "spare");
        testLevelTrustIssuesPart1(AIDifficulty.NORMAL,    true, "shame", "er");
        testLevelTrustIssuesPart1(AIDifficulty.HARD,      true, "shame", "er");
        testLevelTrustIssuesPart1(AIDifficulty.VERY_HARD, true, "shame", "er");
        testLevelTrustIssuesPart1(AIDifficulty.IMPOSSIBLE,true, "shame", "er");
        //Test CPU is less aggressive when aiming for the endzone
        testLevelTrustIssuesPart1(AIDifficulty.VERY_EASY, false, "eye", "are");
        testLevelTrustIssuesPart1(AIDifficulty.EASY,      false, "laugh", "gas");
        testLevelTrustIssuesPart1(AIDifficulty.NORMAL,    false, "enough", "gas");
        testLevelTrustIssuesPart1(AIDifficulty.HARD,      false, "shame", "eleanor");
        testLevelTrustIssuesPart1(AIDifficulty.VERY_HARD, false, "shame", "er");
        testLevelTrustIssuesPart1(AIDifficulty.IMPOSSIBLE,false, "shame", "er");
    }

    @Test
    public void testLevelTrustIssuesPart2() throws InterruptedException {
        //testLevelTrustIssuesPart2(AIDifficulty.VERY_EASY, new String[]{"test"});
        //testLevelTrustIssuesPart2(AIDifficulty.EASY,      new String[]{"test"});
        testLevelTrustIssuesPart2(AIDifficulty.NORMAL,    new String[]{"test"});
        //testLevelTrustIssuesPart2(AIDifficulty.HARD,      new String[]{"test"});
        //testLevelTrustIssuesPart2(AIDifficulty.VERY_HARD, new String[]{"test"});
        //testLevelTrustIssuesPart2(AIDifficulty.IMPOSSIBLE,new String[]{"test"});
    }

    private void testLevelTrustIssuesPart1(AIDifficulty difficulty, boolean targetIsDetonator, String expectedAnswer1, String expectedAnswer2) throws InterruptedException {
        ArrayList<String> layout = new ArrayList<>();
        Character targetChar = targetIsDetonator ? TileCodes.Detonator.getCharacter() : TileCodes.EndZone.getCharacter();

        layout.add("patient-clueless");
        layout.add("0......-.......$");
        layout.add(".......-........");
        layout.add(".......-.......-");
        layout.add(".....++----...." + targetChar);
        layout.add(".....+^-1+-.....");
        layout.add(".....++-++-.....");
        layout.add(".......-........");
        layout.add(".......-........");
        layout.add(".......-........");

        String hudLetters = "eayhuglmno";
        final HintWord playedWord = new HintWord(new ArrayList<HintLetter>());
        playedWord.addLetter(new HintLetter(8, 0, 'c', true, false));
        playedWord.addLetter(new HintLetter(9, 0, 'l', true, false));
        playedWord.addLetter(new HintLetter(10, 0, 'u', true, false));
        playedWord.addLetter(new HintLetter(11, 0, 'e', true, false));
        playedWord.addLetter(new HintLetter(12, 0, 'l', true, false));
        playedWord.addLetter(new HintLetter(13, 0, 'e', true, false));
        playedWord.addLetter(new HintLetter(14, 0, 's', true, false));
        playedWord.addLetter(new HintLetter(15, 0, 's', true, false));
        final HintWord answer = getNextCPUWord(new ArrayList<HintWord>(){{add(playedWord);}},
                layout,
                LevelUtils.getFirstXYof(layout, targetChar),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer);
        if(isTestAssertionEnabled())
            assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswer1));

        //2nd Word

        ArrayList<HintWord> playedWords = new ArrayList<HintWord>(){{add(playedWord); add(answer);}};
        layout = LevelUtils.applyHintWordToLevel(layout, answer);
        hudLetters = "lfoneosarp";
        HintWord answer2 = getNextCPUWord(playedWords,
                layout,
                LevelUtils.getFirstXYof(layout, targetChar),
                hudLetters,
                difficulty);
        if(isPrintTestResultsEnabled())
            log(difficulty, answer2);
        if(isTestAssertionEnabled())
            assertTrue(answer2.getDictionaryEntry().getWord().equals(expectedAnswer2));
    }

    private void testLevelTrustIssuesPart2(AIDifficulty difficulty, String[] expectedAnswers) throws InterruptedException {

        HintWord answer = null;

        ArrayList<String> layout = new ArrayList<>();

        layout.add("patient-clueless");
        layout.add("0......-......h.");
        layout.add(".......-......a.");
        layout.add(".......-......m-");
        layout.add(".....++----...er");
        layout.add(".....+^-1.-.....");
        layout.add(".....++-..-.....");
        layout.add("1111111-........");
        layout.add("1111111-........");
        layout.add("1111111-........");

        String hudLetters = "eayhuglmno";

        ArrayList<HintWord> playedWords = new ArrayList(){{
            add(new HintWord(new ArrayList<HintLetter>(){{
                add(new HintLetter(8, 0, 'c', true, false));
                add(new HintLetter(9, 0, 'l', true, false));
                add(new HintLetter(10, 0, 'u', true, false));
                add(new HintLetter(11, 0, 'e', true, false));
                add(new HintLetter(12, 0, 'l', true, false));
                add(new HintLetter(13, 0, 'e', true, false));
                add(new HintLetter(14, 0, 's', true, false));
                add(new HintLetter(15, 0, 's', true, false));
            }}));
            add(new HintWord(new ArrayList<HintLetter>(){{
                add(new HintLetter(14, 0, 's', true, false));
                add(new HintLetter(14, 1, 'h', true, false));
                add(new HintLetter(14, 2, 'a', true, false));
                add(new HintLetter(14, 3, 'm', true, false));
                add(new HintLetter(14, 4, 'e', true, false));
            }}));
            add(new HintWord(new ArrayList<HintLetter>(){{
                add(new HintLetter(14, 4, 'e', true, false));
                add(new HintLetter(15, 4, 'r', true, false));
            }}));
        }};

        boolean hasReachedTarget = false;
        int iAnswer = 0;
        while (!hasReachedTarget) {
            if(answer != null){
                playedWords.add(answer);
                layout = LevelUtils.applyHintWordToLevel(layout, answer);
                hudLetters = "aeiongstrl";
            }
            answer = null;
            int[] targetXY = LevelUtils.getOptimalTargetXY(layout, playedWords.get(playedWords.size() - 1).getLetters()
                    .get(playedWords.get(playedWords.size() - 1).getLetters().size() - 1).getXY());
            answer = getNextCPUWord(playedWords, layout, targetXY, hudLetters, difficulty);
            if(answer == null){
                System.out.println("! Cannot think of a word => Forfeit !");
                break;
            }
            if (isPrintTestResultsEnabled()) {
                log(difficulty, answer);
                System.out.println(LevelUtils.getLevelAsString(layout) + "\n");
            }
            if (isTestAssertionEnabled()) {
                assertTrue(answer.getDictionaryEntry().getWord().equals(expectedAnswers[iAnswer]));
                iAnswer++;
            }
            break;
        }
        //System.out.println(LevelUtils.getLevelAsString(layout) + "\n");
    }

}
