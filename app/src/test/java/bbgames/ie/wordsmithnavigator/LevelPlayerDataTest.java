package bbgames.ie.wordsmithnavigator;

import android.test.InstrumentationTestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.HashMap;

import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.BridgeLockTurns;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.DetonatorLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.GateLockPoints;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.PowerUpLink;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.Rules.Star;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelPlayerData;
import bbgames.ie.wordsmithnavigator.GameObjects.Levels.v2.LevelStaticData;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;

/**
 * Created by Bryan on 23/08/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class LevelPlayerDataTest extends InstrumentationTestCase {

    private LevelStaticData levelStaticData;
    private LevelPlayerData levelPlayerData;
    private String jsonInitialPlayerData = "{\"plays\":0,\"complete\":false,\"unlocked\":true,\"star\":[\"{\\\"0\\\":false}\",\"{\\\"1\\\":false}\",\"{\\\"2\\\":false}\"],\"diamond\":[\"{\\\"0\\\":false}\",\"{\\\"1\\\":false}\"]}";
    private HashMap<Integer, Star> stars;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        stars = new HashMap<>();
        stars.put(0, new Star("<= 2 moves"));
        stars.put(1, new Star("<= 10 sec"));
        stars.put(2, new Star(">= 25 points"));
        levelStaticData = new LevelStaticData(new ArrayList<String>(),
                0,
                0,
                0,
                "",
                1,
                60,
                new ArrayList<DetonatorLink>(),
                stars,
                new ArrayList<GateLockPoints>(),
                new ArrayList<BridgeLockTurns>(),
                new ArrayList<PowerUpLink>(),
                2);
        levelPlayerData = new LevelPlayerData(levelStaticData);
    }

    @Test
    public void testSimpleIO(){
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertEquals(jsonInitialPlayerData, json);
        assertEquals(jsonInitialPlayerData, clone.toJSONObject().toString());
    }

    @Test
    public void testUnlock0Stars(){
        SessionStatistics sessionStatistics = new SessionStatistics(0);
        sessionStatistics.addWord("one", 1);
        sessionStatistics.addWord("two", 1);
        sessionStatistics.addWord("tri", 1);
        sessionStatistics.setTotalSeconds(100);
        levelPlayerData.updateStars(levelStaticData, sessionStatistics);
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertTrue(clone.getMapIdStarUnlocked().get(0) == false);
        assertTrue(clone.getMapIdStarUnlocked().get(1) == false);
        assertTrue(clone.getMapIdStarUnlocked().get(2) == false);
    }

    @Test
    public void testUnlock1Stars(){
        SessionStatistics sessionStatistics = new SessionStatistics(0);
        sessionStatistics.addWord("one", 1);
        sessionStatistics.addWord("two", 1);
        sessionStatistics.addWord("tri", 1);
        sessionStatistics.setTotalSeconds(5);
        levelPlayerData.updateStars(levelStaticData, sessionStatistics);
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertTrue(clone.getMapIdStarUnlocked().get(0) == false);
        assertTrue(clone.getMapIdStarUnlocked().get(1) == true);
        assertTrue(clone.getMapIdStarUnlocked().get(2) == false);
    }

    @Test
    public void testUnlock2Stars(){
        SessionStatistics sessionStatistics = new SessionStatistics(0);
        sessionStatistics.addWord("one", 1);
        sessionStatistics.setTotalSeconds(5);
        levelPlayerData.updateStars(levelStaticData, sessionStatistics);
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertTrue(clone.getMapIdStarUnlocked().get(0) == true);
        assertTrue(clone.getMapIdStarUnlocked().get(1) == true);
        assertTrue(clone.getMapIdStarUnlocked().get(2) == false);
    }

    @Test
    public void testUnlock3Stars(){
        SessionStatistics sessionStatistics = new SessionStatistics(0);
        sessionStatistics.addWord("one", 70);
        sessionStatistics.setTotalSeconds(5);
        levelPlayerData.updateStars(levelStaticData, sessionStatistics);
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertTrue(clone.getMapIdStarUnlocked().get(0) == true);
        assertTrue(clone.getMapIdStarUnlocked().get(1) == true);
        assertTrue(clone.getMapIdStarUnlocked().get(2) == true);
    }

    @Test
    public void testUnlockGem(){
        SessionStatistics sessionStatistics = new SessionStatistics(0);
        sessionStatistics.addDiamondId(0);
        levelPlayerData.updateDiamonds(sessionStatistics);
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertTrue(clone.getMapIdDiamondUnlocked().get(0) == true);
        assertTrue(clone.getMapIdDiamondUnlocked().get(1) == false);
    }

    @Test
    public void testPlays(){
        int mPlays = 23;
        levelPlayerData.setPlays(mPlays);
        String json = levelPlayerData.toJSONObject().toString();
        LevelPlayerData clone = new LevelPlayerData(json);
        assertTrue(clone.getPlays() == mPlays);
    }
}
