package bbgames.ie.wordsmithnavigator;

import junit.framework.Assert;

import org.junit.Test;

import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

public class SessionStatisticsTest {

    @Test
    public void testSessionLowScore(){
        SessionStatistics sessionStatistics = new SessionStatistics(1);

        sessionStatistics.addWord("test");
        sessionStatistics.addWord("set");
        sessionStatistics.addWord("get");
        sessionStatistics.setTotalSeconds(25);

        String score = sessionStatistics.getLevelScore(2);
        double s = Double.parseDouble(score);
        int sFinal = (int) Math.round(s);

        Assert.assertEquals("26.27", score);
        Assert.assertEquals(26, sFinal);
    }

    @Test
    public void testSessionLowHigh(){
        SessionStatistics sessionStatistics = new SessionStatistics(1);

        sessionStatistics.addWord("testing");
        sessionStatistics.addWord("master");
        sessionStatistics.addWord("feeling");
        sessionStatistics.addWord("burgers");
        sessionStatistics.addWord("shatter");
        sessionStatistics.addWord("cool");
        sessionStatistics.setTotalSeconds(73);

        String score = sessionStatistics.getLevelScore(119);
        double s = Double.parseDouble(score);
        int sFinal = (int) Math.round(s);

        Assert.assertEquals("129.21", score);
        Assert.assertEquals(129, sFinal);
    }

}
