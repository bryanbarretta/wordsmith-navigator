package bbgames.ie.wordsmithnavigator.mock;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.GameObjects.AI.CPULogicLogger.CpuLoggerInterface;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.PathFinderOption;

public class MockCpuLogger implements CpuLoggerInterface {

    private final boolean logViaSystemOutPrint;
    private final String mockCpuDelimiterLogTag;

    public MockCpuLogger(boolean logViaSystemOutPrint){
        this(">> ", logViaSystemOutPrint);
    }

    public MockCpuLogger(String mockCpuDelimiterLogTag, boolean logViaSystemOutPrint){
        this.logViaSystemOutPrint = logViaSystemOutPrint;
        this.mockCpuDelimiterLogTag = mockCpuDelimiterLogTag;
    }

    @Override
    public boolean isLoggingEnabled() {
        return logViaSystemOutPrint;
    }

    @Override
    public void logPlayedWords(ArrayList<HintWord> playedWords) {
        if(logViaSystemOutPrint) {
            System.out.println(mockCpuDelimiterLogTag + android.text.TextUtils.join(", ", playedWords));
        }
    }

    @Override
    public void logTargetXy(int[] targetXY) {
        if(logViaSystemOutPrint) {
            System.out.println(mockCpuDelimiterLogTag + "[" + (targetXY == null ? "?" : targetXY[0])
                                                      + ", " + (targetXY == null ? "?" : targetXY[1]) + "]");
        }
    }

    @Override
    public void logDepartureKeys(ArrayList<String> departureKeys) {
        if(logViaSystemOutPrint) {
            System.out.println(mockCpuDelimiterLogTag + android.text.TextUtils.join(", ", departureKeys));
        }
    }

    @Override
    public void logAStarData(ArrayList<PathFinderOption> pathFinderOptions) {
        if(logViaSystemOutPrint) {
            String data = "Found " + pathFinderOptions.size() + " Path Finder Options.\n";
            for(PathFinderOption p : pathFinderOptions){
                data += "\n" + pathFinderOptions.indexOf(p) + ".\n" + p.getLogDataAStar();
            }
            System.out.println(mockCpuDelimiterLogTag + data);
        }
    }

    @Override
    public void logAStarErrors(String errorLogs) {
        if(logViaSystemOutPrint) {
            System.out.println(mockCpuDelimiterLogTag + errorLogs);
        }
    }

    @Override
    public void logPathFinderOptionEvaluation(String log) {
        if(logViaSystemOutPrint) {
            System.out.println(mockCpuDelimiterLogTag + log);
        }
    }

    @Override
    public void logAnswer(HintWord answer) {
        if(logViaSystemOutPrint) {
            System.out.println(mockCpuDelimiterLogTag + answer.toString());
        }
    }

    @Override
    public void logLayout() {

    }

    @Override
    public void log(String log) {
        System.out.println(mockCpuDelimiterLogTag + log);
    }
}
