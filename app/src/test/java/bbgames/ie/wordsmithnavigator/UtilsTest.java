package bbgames.ie.wordsmithnavigator;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Util.LevelUtils;

public class UtilsTest {

    /**
     * ---------------------------
     * -                /\
     * -   BORDER       95
     * -                \/
     * -        ------------------
     * -        -       /\        |
     * -        -  MAP  95        |
     * -        -       \/        |
     * - <-95-> - <-95-> +        |  The center point pixel X and Y for the tile at [0, 0] is [190, 190]
     * -        -    [190, 190]   |
     * -        -                 |
     * -        -_________________|
     *
     */
    @Test
    public void testGetClosestTileCenterXY(){
        float[] xy1 = new float[]{ 192.123f, 95.413f };
        float[] xy2 = new float[]{ 286.044f, 284.75f };
        float[] xy3 = new float[]{ 2.123f,   185.413f };
        float[] expectedXy1 = new float[]{ 285f, 95f };
        float[] expectedXy2 = new float[]{ 285f, 285f };
        float[] expectedXy3 = new float[]{ 95f, 95f };
        float[] actualXy1 = LevelUtils.getClosestTileCenterXY(xy1[0], xy1[1]);
        float[] actualXy2 = LevelUtils.getClosestTileCenterXY(xy2[0], xy2[1]);
        float[] actualXy3 = LevelUtils.getClosestTileCenterXY(xy3[0], xy3[1]);
        Assert.assertEquals(expectedXy1[0], actualXy1[0]);
        Assert.assertEquals(expectedXy1[1], actualXy1[1]);
        Assert.assertEquals(expectedXy2[0], actualXy2[0]);
        Assert.assertEquals(expectedXy2[1], actualXy2[1]);
        Assert.assertEquals(expectedXy3[0], actualXy3[0]);
        Assert.assertEquals(expectedXy3[1], actualXy3[1]);
    }

}
