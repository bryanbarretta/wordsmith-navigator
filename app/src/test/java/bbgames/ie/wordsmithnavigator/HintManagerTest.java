package bbgames.ie.wordsmithnavigator;

import android.app.Activity;

import org.andengine.util.TextUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Constants.Direction;
import bbgames.ie.wordsmithnavigator.Constants.SupportedLanguages;
import bbgames.ie.wordsmithnavigator.Database.DatabaseGateway;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Objects.HintWord;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Word.HintManager;
import bbgames.ie.wordsmithnavigator.GameObjects.AI.Word.HintSQLHandler;
import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.Util;
import bbgames.ie.wordsmithnavigator.WordDefinition.DictionaryEntry;
import bbgames.ie.wordsmithnavigator.mock.MockCpuLogger;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Bryan on 26/09/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class HintManagerTest {

    private final int minFrequencyOfWord = 0;
    private boolean FLAG_WAIT = false;

    DatabaseGateway databaseGateway;

    @Before
    public void setUp() {
        if(databaseGateway != null){
            return;
        }
        System.out.println("Loading Dictionary!");
        Activity activity = Robolectric.buildActivity(Activity.class)
                .create()
                .resume()
                .get();
        databaseGateway = new DatabaseGateway(activity);
        ArrayList<DictionaryEntry> entries = new ArrayList<>();
        InputStream dict = activity.getResources().openRawResource(R.raw.dictionary_en_uk);
        InputStreamReader isr = new InputStreamReader(dict, Charset.forName("UTF-8")); //Null here? Edit Configurations...->MODULE_DIR
        BufferedReader br = new BufferedReader(isr);
        String line = "";
        try {
            double mostFreq = SupportedLanguages.ENGLISH_UK.getMostFrequentWordCount();
            double factor = Util.getFactor(mostFreq);
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(",");
                String displayWord = parts[0].toLowerCase();
                String baseWord = Util.getBaseWord(displayWord);
                double freq = parts.length > 1 ? Double.valueOf(parts[1]) : 0;
                entries.add(new DictionaryEntry(baseWord,
                        displayWord.equals(baseWord) ? null : displayWord,
                        freq <= 0 ? 0 : Math.round((freq / mostFreq) * factor) / factor));
            }
            br.close();
            dict.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        databaseGateway.addUpdateDictionaryEntries(entries);
    }

    //region simple

    @Test
    public void testSimpleForwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("......");
        layout.add(".0.r..");
        layout.add("...e..");
        layout.add("...s..");
        layout.add(".hate.");
        layout.add("......");
        simpleHintCountTest(layout, Direction.EAST, hudLetters, 47);
    }

    @Test
    public void testSimpleBackwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("......");
        layout.add(".r.0..");
        layout.add(".e....");
        layout.add(".s....");
        layout.add(".that.");
        layout.add("......");
        simpleHintCountTest(layout, Direction.WEST, hudLetters, 60);
    }

    @Test
    public void testSimpleForwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("....0.");
        layout.add("......");
        layout.add(".test.");
        layout.add("......");
        layout.add("......");
        layout.add("......");
        simpleHintCountTest(layout, Direction.SOUTH, hudLetters, 55);
    }

    @Test
    public void testSimpleBackwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("......");
        layout.add("......");
        layout.add(".test.");
        layout.add("......");
        layout.add("....0.");
        layout.add("......");
        simpleHintCountTest(layout, Direction.NORTH, hudLetters, 60);
    }

    //endregion

    //region wall

    @Test
    public void testWallForwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("............");
        layout.add("..--------..");
        layout.add("..-......-..");
        layout.add("..-.0.r..-..");
        layout.add("..-...e..-..");
        layout.add("..-...s..-..");
        layout.add("..-.hate.-..");
        layout.add("..-......-..");
        layout.add("..--------..");
        layout.add("............");
        simpleHintCountTest(layout, Direction.EAST, hudLetters, 47);
    }

    @Test
    public void testWallBackwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("............");
        layout.add("..--------..");
        layout.add("..-......-..");
        layout.add("..-.r.0..-..");
        layout.add("..-.e....-..");
        layout.add("..-.s....-..");
        layout.add("..-.that.-..");
        layout.add("..-......-..");
        layout.add("..--------..");
        layout.add("............");
        simpleHintCountTest(layout, Direction.WEST, hudLetters, 60);
    }

    @Test
    public void testWallForwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("............");
        layout.add("..--------..");
        layout.add("..-....0.-..");
        layout.add("..-......-..");
        layout.add("..-.test.-..");
        layout.add("..-......-..");
        layout.add("..-......-..");
        layout.add("..-......-..");
        layout.add("..--------..");
        layout.add("............");
        simpleHintCountTest(layout, Direction.SOUTH, hudLetters, 55);
    }

    @Test
    public void testWallBackwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("............");
        layout.add("..--------..");
        layout.add("..-......-..");
        layout.add("..-......-..");
        layout.add("..-.test.-..");
        layout.add("..-......-..");
        layout.add("..-....0.-..");
        layout.add("..-......-..");
        layout.add("..--------..");
        layout.add("............");
        simpleHintCountTest(layout, Direction.NORTH, hudLetters, 60);
    }

    //endregion

    //region adjacent locked letters

    @Test
    public void testAdjacentLockedLettersForwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("..r.t.");   //Must account for adjacent letters e.g. play 'role' so robe and ten are valid perpendicular words
        layout.add(".0....");
        layout.add("..b.n.");
        layout.add("..e...");
        layout.add("......");
        layout.add("......");
        simpleHintCountTest(layout, Direction.EAST, hudLetters, 101);
    }

    @Test
    public void testAdjacentLockedLettersBackwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add(".r.t..");   //Must account for adjacent letters e.g. play 'role' so robe and ten are valid perpendicular words
        layout.add("....0.");
        layout.add(".b.n..");
        layout.add(".e....");
        layout.add("......");
        layout.add("......");
        simpleHintCountTest(layout, Direction.WEST, hudLetters, 257);
    }

    @Test
    public void testAdjacentLockedLettersForwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add(".......");
        layout.add("....0..");
        layout.add("...r.be");
        layout.add(".......");
        layout.add("...t.n.");
        layout.add(".......");
        simpleHintCountTest(layout, Direction.SOUTH, hudLetters, 101);
    }

    @Test
    public void testAdjacentLockedLettersBackwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add(".......");
        layout.add("...r.be");
        layout.add(".......");
        layout.add("...t.n.");
        layout.add("....0..");
        layout.add(".......");
        simpleHintCountTest(layout, Direction.NORTH, hudLetters, 257);
    }

    //endregion

    //region Post Wall And Closest Touched Tile

    @Test
    public void testPostWallAndClosestTouchedTileForwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("---------");
        layout.add("-s0.....-");
        layout.add("-o......-");
        simpleHintCountTest(layout, Direction.EAST, hudLetters, 103);
    }

    @Test
    public void testPostWallAndClosestTouchedTileBackwardHorizontal(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("---------");
        layout.add("-.....0s-");
        layout.add("-......o-");
        simpleHintCountTest(layout, Direction.WEST, hudLetters, 172);
    }

    @Test
    public void testPostWallAndClosestTouchedTileForwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("---------");
        layout.add("-so------");
        layout.add("-0......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("---------");
        simpleHintCountTest(layout, Direction.SOUTH, hudLetters, 103);
    }

    @Test
    public void testPostWallAndClosestTouchedTileBackwardVertical(){
        String hudLetters = "foleomnrpt";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("---------");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-.......-");
        layout.add("-0......-");
        layout.add("-so------");
        layout.add("---------");
        simpleHintCountTest(layout, Direction.NORTH, hudLetters, 172);
    }

    //endregion

    //region Hints ending on a perpendicular word

    @Test
    public void testPerpendicularEndingForwardVertical(){
        String hudLetters = "atifyuilwo";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("---rogue---");
        layout.add("-..0......-");
        layout.add("-.........-");
        layout.add("-.........-");
        layout.add("-.........-");
        layout.add("-.........-");
        layout.add("-..TILES..-");
        layout.add("-----.-----");
        simpleHintCountTest(layout, Direction.SOUTH, hudLetters, 29);
    }

    @Test
    public void testPerpendicularEndingBackwardVertical(){
        String hudLetters = "atifyuilwo";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-----.-----");
        layout.add("-..TILES..-");
        layout.add("-.........-");
        layout.add("-.........-");
        layout.add("-.........-");
        layout.add("-.........-");
        layout.add("-..0......-");
        layout.add("---rogue---");
        simpleHintCountTest(layout, Direction.NORTH, hudLetters, 1);
    }

    @Test
    public void testPerpendicularEndingForwardHorizontal(){
        String hudLetters = "atifyuilwo";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-......-");
        layout.add("-......-");
        layout.add("-......-");
        layout.add("r0....T-");
        layout.add("o.....I-");
        layout.add("g.....L.");
        layout.add("u.....E-");
        layout.add("e.....S-");
        layout.add("-......-");
        layout.add("-......-");
        layout.add("-......-");
        simpleHintCountTest(layout, Direction.EAST, hudLetters, 29);
    }

    @Test
    public void testPerpendicularEndingBackwardHorizontal(){
        String hudLetters = "atifyuilwo";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("-......-");
        layout.add("-......-");
        layout.add("-......-");
        layout.add("-T....0r");
        layout.add("-I.....o");
        layout.add(".L.....g");
        layout.add("-E.....u");
        layout.add("-S.....e");
        layout.add("-......-");
        layout.add("-......-");
        layout.add("-......-");
        simpleHintCountTest(layout, Direction.WEST, hudLetters, 1);
    }

    @Test
    public void testReverseWordMultipleBoardwordsHorizontal(){
        String hudLetters = "ppelznfola";
        ArrayList<String> layout = new ArrayList<>();
        layout.add("----rogue----");
        layout.add("....e.i......");
        layout.add("....e.r0.....");
        layout.add("....d.l......");
        layout.add("......y......");
        simpleHintCountTest(layout, Direction.WEST, hudLetters, 7);
    }

    //endregion

    @Test
    public void testTwoLetterWordsWhereTheFocusTileIsTheLastTile() {
        ArrayList<String> layout = new ArrayList<String>(){
            {add("----Rogue----");}
            {add("....E.0......");}
            {add("....T........");}
            {add("....R........");}
            {add("....E........");}
            {add("....A....-+++");}
            {add("....TILES-111");}
            {add("....--!---111");}
        };
        int[] xyFocusTile = new int[]{6, 7};
        Direction direction = Direction.SOUTH;
        String hudLetters = "edeparosit";
        double minFreq = 0;
        ArrayList<HintWord> hints = new HintSQLHandler(layout, direction, xyFocusTile, hudLetters, databaseGateway, minFreq, new MockCpuLogger(true)).getHints();
        Assert.assertFalse(hints.isEmpty());
    }

    //----------------------------
    //-- PRIVATE HELPER METHODS --
    //----------------------------

    //region private helper methods

    /**
     * Carry out a simple hint finder test
     * @param layout the level layout. '0' will be used as the focus tile
     * @param direction the direction to find words in from '0'
     * @param hudLetters the letters available on the hud
     * @param expectedHintCount the expected number of hints to be found on this test
     */
    private void simpleHintCountTest(final ArrayList<String> layout,
                                     final Direction direction,
                                     final String hudLetters,
                                     final int expectedHintCount){

        HintManager hintManager = new HintManager(new MockCpuLogger(true)) {
            @Override
            public void onWordsFound(ArrayList<HintWord> hints) {
                FLAG_WAIT = false;
                logTestResults(hints, layout, direction, hudLetters, expectedHintCount);
                assertEquals(expectedHintCount, hints.size());
            }
        };

        FLAG_WAIT = true;

        hintManager.findWordsAsynchronously(layout,
                LevelUtils.getFirstXYof(layout, '0'),
                direction,
                databaseGateway,
                hudLetters,
                minFrequencyOfWord);

        while (FLAG_WAIT){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void logTestResults(ArrayList<HintWord> hints, ArrayList<String> layout, Direction direction, String hudLetters, int expectedHintCount) {
        System.out.println("-----------------------------------------------------------");
        for(String row : layout) {
            System.out.println(row);
        }
        System.out.println("Direction [" + direction.name().toLowerCase() + "] | HUD Letters [" + hudLetters + "]");
        System.out.println(" * Expected hints: " + expectedHintCount + " | Actual hints: " + hints.size());
        System.out.println(" > " + TextUtils.join(", ", hints.toString()));
    }

    //endregion
}
