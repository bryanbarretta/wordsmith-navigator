package bbgames.ie.wordsmithnavigator;


import java.util.Comparator;

import bbgames.ie.wordsmithnavigator.Constants.OptionEnums.AIDifficulty;
import bbgames.ie.wordsmithnavigator.GameObjects.Operations.SessionStatistics;
import bbgames.ie.wordsmithnavigator.GameObjects.Players.PlayerType;

public class TestedLevelSummary implements Comparator<TestedLevelSummary> {
    private int id;
    private PlayerType playerType;
    private AIDifficulty difficulty;
    private SessionStatistics sessionStatistics;
    private boolean forfeited;

    @Override
    public int compare(TestedLevelSummary s1, TestedLevelSummary s2) {
        if(s1.id != s2.id){
            return s1.id < s2.id ? 0 : 1;
        }
        if(s1.playerType != s2.playerType){
            return s1.playerType.ordinal() < s2.playerType.ordinal() ? 0 : 1;
        }
        if(s1.difficulty != s2.difficulty){
            return s1.difficulty.ordinal() < s2.difficulty.ordinal() ? 0 : 1;
        }
        throw new RuntimeException("Cannot compare identical TestedLevelSummary objects");
    }

    public TestedLevelSummary(int id, PlayerType playerType, AIDifficulty difficulty, SessionStatistics sessionStatistics, boolean forfeited) {
        this.id = id;
        this.playerType = playerType;
        this.difficulty = difficulty;
        this.sessionStatistics = sessionStatistics;
        this.forfeited = forfeited;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void setPlayerType(PlayerType playerType) {
        this.playerType = playerType;
    }

    public AIDifficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(AIDifficulty difficulty) {
        this.difficulty = difficulty;
    }

    public SessionStatistics getSessionStatistics() {
        return sessionStatistics;
    }

    public void setSessionStatistics(SessionStatistics sessionStatistics) {
        this.sessionStatistics = sessionStatistics;
    }

    public boolean isForfeited() {
        return forfeited;
    }

    public void setForfeited(boolean forfeited) {
        this.forfeited = forfeited;
    }
}
