package bbgames.ie.wordsmithnavigator;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

import bbgames.ie.wordsmithnavigator.Util.LevelUtils;
import bbgames.ie.wordsmithnavigator.Util.UserLevelUtils;

public class UserLevelUtilsTest {

    //0
    //101
    //203
    //306
    //410
    //515

    @Test
    public void testLevelUp1(){
        ArrayList<UserLevelUtils.LevelUpData> res = UserLevelUtils.getLevelUpDataForProgressBar(0, 33);
        System.out.println(">> " + new Exception().getStackTrace()[0].getMethodName());
        for(UserLevelUtils.LevelUpData lud : res){
            System.out.println(lud.toString());
        }
    }

    @Test
    public void testLevelUp2(){
        ArrayList<UserLevelUtils.LevelUpData> res = UserLevelUtils.getLevelUpDataForProgressBar(33, 68);
        System.out.println(">> " + new Exception().getStackTrace()[0].getMethodName());
        for(UserLevelUtils.LevelUpData lud : res){
            System.out.println(lud.toString());
        }
    }

    @Test
    public void testLevelUp3(){
        ArrayList<UserLevelUtils.LevelUpData> res = UserLevelUtils.getLevelUpDataForProgressBar(68, 105);
        System.out.println(">> " + new Exception().getStackTrace()[0].getMethodName());
        for(UserLevelUtils.LevelUpData lud : res){
            System.out.println(lud.toString());
        }
    }

    @Test
    public void testLevelUp4(){
        ArrayList<UserLevelUtils.LevelUpData> res = UserLevelUtils.getLevelUpDataForProgressBar(105, 309);
        System.out.println(">> " + new Exception().getStackTrace()[0].getMethodName());
        for(UserLevelUtils.LevelUpData lud : res){
            System.out.println(lud.toString());
        }
    }

}
