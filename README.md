 __        __            _               _ _   _       _   _             _             _             
 \ \      / /__  _ __ __| |___ _ __ ___ (_) |_| |__   | \ | | __ ___   _(_) __ _  __ _| |_ ___  _ __ 
  \ \ /\ / / _ \| '__/ _` / __| '_ ` _ \| | __| '_ \  |  \| |/ _` \ \ / / |/ _` |/ _` | __/ _ \| '__|
   \ V  V / (_) | | | (_| \__ \ | | | | | | |_| | | | | |\  | (_| |\ V /| | (_| | (_| | || (_) | |   
    \_/\_/ \___/|_|  \__,_|___/_| |_| |_|_|\__|_| |_| |_| \_|\__,_| \_/ |_|\__, |\__,_|\__\___/|_|   
                                                                           |___/                     
																		   
1. Software Requirements
2. Setting Up Project

----------------------------------------------
 1. Software Requirements
----------------------------------------------

 - Android Studio (3.0.1+)
 - Gradle (3+)
 - Java (1.8)
 
----------------------------------------------
 2. Setting Up Project
----------------------------------------------

Open Android Studio and import the project.

 - android.jks

The keystore needs to be added to your computer, and its location referenced in the gradle.properties file in the next step.
 
 - gradle.properties

For deploying the app to the play store you will need to do the following:
From the root directory of the project, open the 'app' folder. Create a file in here called 'gradle.properties' (This file can also be created via Android Studio).
In this file add the following lines:

	apkStoreFileUrl=path/to/android.jks
	apkStorePassword=*********************************
	apkStoreKeyAlias=*********************************
	apkStoreKeyPassword=******************************

Replace values with the relevant information for the apk keystore.

